---
title: "WHOAMI"
draft: false
---


## Hey everyone👋 ! I'm Maiky, and I'm a websec enthusiast 👨🏻‍💻

### CTF 🚩 player :

- [NETON](https://twitter.com/netonehc) 

and Computer Science student :) 🖥️.

![<- Not working](../Maiky.jpeg/)
