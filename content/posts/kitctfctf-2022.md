---
title: "KITCTFCTF 2022 - Static Application analysis with CodeQL"
date: 2022-12-20T16:40:29+01:00
draft: false
categories:
    - CTF
    - CodeQL
tags:
    - Ruby
    - CodeQL
---

# KITCTFCTF 2022

Name | Category |  CTF Time
------------|----------|---- 
**KITCTFCTF 2022**| **CodeQL** |  **https://ctftime.org/event/1810**

# [*] INDEX:

- [Grep it? CodeQL it! 1](#grep-it-codeql-it-1)
- [Grep it? CodeQL it! 2](#grep-it-codeql-it-2)
- [Grep it? CodeQL it! 4 - Revenge for 2: Hardcoding is boring](#grep-it-codeql-it-4---revenge-for-2-hardcoding-is-boring)
- [Grep it? CodeQL it! 3](#grep-it-codeql-it-3)
- [Grep it? CodeQL it! 5 - Revenge for 3: Hardcoding is boring](#grep-it-codeql-it-5---revenge-for-3-hardcoding-is-boring)

## Grep it? CodeQL it! 1

> Challenge description 

Oh no, you have to do an internal audit on hundreds of Ruby projects for system command executions. You decide to write a CodeQL to ease auditing.

Find ALL the system command executions marked with BAD and submit the query on the submission site to get the flag.

Note: The BAD comments will be stripped on the query verification page.

Note: The submission site expects the select clause to look like this: select [THE_COMMAND_EXECUTION_ELEMENT], "OPTIONAL_MESSAGE"

http://kitctf.me:23456/challenge_1.html

### Challenge Setup

In the first place we'll start choosing the database from the folder `challenge-one-database` :

![<- Not working](/static/HACKING/kitctfctf-1.png)

### Source analysis

In this first challenge we will have to find all those system command executions in the source code :

```ruby
require 'open3'

class MyApp < ActionController::Base
  index '/hello/:name' do
    rightin = params['name']
    failedni = "Justin"
    rightin = if ["admin", "root", "KITCTF"].include?(rightin)
      rightin # accept rightin as is
    else
      rightin = "Hacker"
    end

    return Open3.capture3("ls -la /tmp")[0] # BAD
  end

end
```

We can use `SystemCommandExecution` to get all those system command execution nodes with the following query :

```ql
import ruby
import codeql.ruby.Concepts

from SystemCommandExecution exec
select exec, "this is a command execution"
```

![<- Not working](/static/HACKING/kitctfctf-2.png)

This works, but if we look closely we're not matching those calls using `Open4.popen4` :`

```ruby
require 'open4'
class MyApp < ActionController::Base
  index '/hello/:name' do
    setin = params['name']
    index = "Gary"
    setin = if ["admin", "root", "KITCTF"].include?(setin)
      setin # accept setin as is
    else
      setin = "Hacker"
    end
    Open4.popen4("ls -la #{setin}") # BAD
    return "executed command #{setin}"
  end
end
```

So we have to extend `SystemCommandExecution` results, we can first look at how the `Open3` module is implemented and then do the same with `Open4` :

```ql
class Open3Call extends SystemCommandExecution::Range {
  MethodCall methodCall;

  Open3Call() {
    this.asExpr().getExpr() = methodCall and
    this =
      API::getTopLevelMember("Open3")
          .getAMethodCall(["popen3", "popen2", "popen2e", "capture3", "capture2", "capture2e"])
  }

  override DataFlow::Node getAnArgument() {
    result.asExpr().getExpr() = methodCall.getAnArgument()
  }

  override predicate isShellInterpreted(DataFlow::Node arg) {
    // These Open3 methods invoke a subshell if you provide a single string as argument
    methodCall.getNumberOfArguments() = 1 and arg.asExpr().getExpr() = methodCall.getAnArgument()
  }
}
```

This class extends `SystemCommandExecution::Range` and if we go to it's definition we can see that it's used to refine/modeling new OS Command API :

```ql
/** Provides a class for modeling new operating system command APIs. */
module SystemCommandExecution {
  /**
   * A data flow node that executes an operating system command, for instance by spawning a new
   * process.
   *
   * Extend this class to model new APIs. If you want to refine existing API models,
   * extend `SystemCommandExecution` instead.
   */
  abstract class Range extends DataFlow::Node {
    /** Gets an argument to this execution that specifies the command or an argument to it. */
    abstract DataFlow::Node getAnArgument();

    /** Holds if a shell interprets `arg`. */
    predicate isShellInterpreted(DataFlow::Node arg) { none() }
  }
}
```

### Query writing

First we have to initialize the `MethodCall` object and match all those `popen4` method calls:

```ql
MethodCall methodCall;
Open4Execution() {
    this.asExpr().getExpr() = methodCall and
    this = API::getTopLevelMember("Open4").getAMethodCall("popen4")
}
```

Once here we have to implement our own `getAnArgument()` abstract method which will be returning a `DataFlow::Node` object :

```ql
override DataFlow::Node getAnArgument() {
    result.asExpr().getExpr() = methodCall.getAnArgument()
}
```

Since our class is extending `SystemCommandExecution` results we just have to select all those SCE. Final query :

```ql
import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.ApiGraphs

private class Open4Execution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    Open4Execution() {
        this.asExpr().getExpr() = methodCall and
        this = API::getTopLevelMember("Open4").getAMethodCall("popen4")
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

from SystemCommandExecution exec
select exec, "this is a command execution"
```

![<- Not working](/static/HACKING/kitctfctf-3.png)

> [KCTF{c0u1d_h4v3_u53d_grep_f02_7h15}](http://kitctf.me:23456/challenge_1_view.html?id=10300749694800950721)

## Grep it? CodeQL it! 2

> Challenge description 

You are very happy that you've found all system command executions, but your developers are salty. They complain that you are finding system command executions that are constant! You won't get hacked (maybe?) if the system command execution doesn't include remote user input!

Find all the system command executions marked with BAD, but only those executions that execute remote user input. Submit the query on the submission site to get the flag.

Note: The BAD comments will be stripped on the query verification page.

Note: The submission site expects the select clause to look like this: select [THE_COMMAND_EXECUTION_ELEMENT], "OPTIONAL_MESSAGE"

http://kitctf.me:23456/challenge_2.html

### Challenge Setup

Choose the database from the folder `challenge-two-database`.

### Source analysis

In this second challenge have to match only those command executions that include user input :

```ruby
class MyApp < ActionController::Base
  index '/hello/:name' do
    instr = params['name']
    datani = "Deborah"

    return %x"ls -la #{instr}" # BAD
    
  end
end
```

So we have to avoid those constant commands :

```ruby
class MyApp < ActionController::Base
  index '/hello/:name' do
    f = params['name']
    no = "George"

    f = if f =~ /^[a-z]+$/i
      f # accept f as is
    else
      f = "Hacker"
    end
    
    return `ls -la /tmp` # 
  end

end
```

### Query writing

We can use `TaintTracking` to analyze the data flow where the valuees are not preserved. We can find a taint propation from a expression `Source` to its `Sink` in 0 or more local steps.

We can extend `TaintTracking::Configuration` defining our own `Sink/Source` configuration. Let's start for the easiest part, the `Source` will be instance of `RemoteFlowSource` which stands for remote user input data flow source.

```ql
class Source extends DataFlow::Node { abstract string getSourceType();}

class RemoteFlowSourceAsSource extends Source {
    RemoteFlowSourceAsSource() { this instanceof RemoteFlowSource }

    override string getSourceType() { result = "user-provided value" }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }
}
```

Once here we have to define our `Sink` which is the `SystemCommandExecution` object's argument :

```ql
override predicate isSink(DataFlow::Node sink) { 
    exists(SystemCommandExecution exec | exec.getAnArgument() = sink)
}
```

Now let's put everything together in our final query :

```ql
import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.frameworks.Railties
import codeql.ruby.DataFlow
import codeql.ruby.ApiGraphs
import codeql.ruby.TaintTracking
import codeql.ruby.dataflow.RemoteFlowSources

class Source extends DataFlow::Node { abstract string getSourceType();}

class Sink extends DataFlow::Node { }

class RemoteFlowSourceAsSource extends Source {
    RemoteFlowSourceAsSource() { this instanceof RemoteFlowSource }

    override string getSourceType() { result = "user-provided value" }
}

private class Open4Execution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    Open4Execution() {
        this.asExpr().getExpr() = methodCall and
        this = API::getTopLevelMember("Open4").getAMethodCall("popen4")
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }
    override predicate isSink(DataFlow::Node sink) { 
        exists(SystemCommandExecution exec | exec.getAnArgument() = sink)
    }
}

from CommandInjectionConfiguration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink)
select sink.getNode(), source, sink, "This depends on a $@", source.getNode(),
"user-provided value"
```

![<- Not working](/static/HACKING/kitctfctf-4.png)

> [KCTF{c4n_grep_also_d0_7h15__pr0b4b1y_n07}](http://kitctf.me:23456/challenge_2_view.html?id=17103226511973398518)

## Grep it? CodeQL it! 4 - Revenge for 2: Hardcoding is boring

> Challenge description

Do not hardcode. The database did not change, only the submission site You are very happy that you've found all system command executions, but your developers are salty. They complain that you are finding system command executions that are constant! You won't get hacked (maybe?) if the system command execution doesn't include remote user input!

Find all the system command executions marked with BAD, but only those executions that execute remote user input. Submit the query on the submission site to get the flag.

Note: The BAD comments will be stripped on the query verification page.

Note: The submission site expects the select clause to look like this: select [THE_COMMAND_EXECUTION_ELEMENT], "OPTIONAL_MESSAGE"

http://kitctf.me:23456/challenge_4.html

### Challenge Setup

Same database : `challenge-two-database`.

### Query writing

We use the same one as in Challenge 2 since we had not harcoded :

```ql
import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.frameworks.Railties
import codeql.ruby.DataFlow
import codeql.ruby.ApiGraphs
import codeql.ruby.TaintTracking
import codeql.ruby.dataflow.RemoteFlowSources

class Source extends DataFlow::Node { abstract string getSourceType();}

class Sink extends DataFlow::Node { }

class RemoteFlowSourceAsSource extends Source {
    RemoteFlowSourceAsSource() { this instanceof RemoteFlowSource }

    override string getSourceType() { result = "user-provided value" }
}

private class Open4Execution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    Open4Execution() {
        this.asExpr().getExpr() = methodCall and
        this = API::getTopLevelMember("Open4").getAMethodCall("popen4")
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }
    override predicate isSink(DataFlow::Node sink) { 
        exists(SystemCommandExecution exec | exec.getAnArgument() = sink)
    }
}

from CommandInjectionConfiguration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink)
select sink.getNode(), source, sink, "This depends on a $@", source.getNode(),
"user-provided value"
```

> [KCTF{u5e_th3_a5t_1uk3}](http://kitctf.me:23456/challenge_4_view.html?id=15317963799368442220)

## Grep it? CodeQL it! 3

> Challenge description


You have removed some alerts, but your developers are still not happy. You now only have executions that are remotely reachable, but some of these calls are still perfectly safe.

Find all the system command executions marked with BAD but only those executions that execute remote user input and where the remote input is not sanitized/restricted to a safe input. Submit the query on the submission site to get the flag.

Note: The BAD comments will be stripped on the query verification page.

Note: The submission site expects the select clause to look like this: select [THE_COMMAND_EXECUTION_ELEMENT], "OPTIONAL_MESSAGE"

http://kitctf.me:23456/challenge_3.html

### Challenge Setup

Choose the database from the folder `challenge-three-database`.

### Source analysis

Examples as the following with our previous query would be interpreted as a positive case when it is not actually exploitable :

```ruby
class MyApp < ActionController::Base
  index '/hello/:name' do
    valuesin = params['name']
    value = "Andrew"

    valuesin = if valuesin == "admin" || valuesin == "root" || valuesin == "KITCTF"
      valuesin # accept valuesin as is
    else
      valuesin = "Hacker"
    end
    IO.popen("ls -la #{valuesin}") # 
    return "executed command #{valuesin}"
    
  end
end
```

Since `valuesin` can only be 4 values ("admin", "root", "KITCTF" and "Hacker") it's not possible to inject code into `IO.popen()`. We have to discard all those cases in which the variable goes through a sanitizer. 

At this point we have to consider all those barriers that could sanitize the user input, in this case the comparison with constant strings, we can use the `StringConstCompareBarrier` class implemented in `BarrierGuards` to avoid all those cases :

```ql
/**
 * A validation of value by comparing with a constant string value, for example
 * in:
 *
 * ```rb
 * dir = params[:order]
 * dir = "DESC" unless dir == "ASC"
 * User.order("name #{dir}")
 * ```
 *
 * the equality operation guards against `dir` taking arbitrary values when used
 * in the `order` call.
 */
class StringConstCompareBarrier extends DataFlow::Node {
  StringConstCompareBarrier() {
    this = DataFlow::BarrierGuard<stringConstCompare/3>::getABarrierNode()
  }
}
```

This class is calling `stringConstCompare` predicate :

```ql
private predicate stringConstCompare(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
  exists(CfgNodes::ExprNodes::ComparisonOperationCfgNode c |
    c = g and
    exists(CfgNodes::ExprNodes::StringLiteralCfgNode strLitNode |
      c.getExpr() instanceof EqExpr and branch = true
      or
      c.getExpr() instanceof CaseEqExpr and branch = true
      or
      c.getExpr() instanceof NEExpr and branch = false
    |
      c.getLeftOperand() = strLitNode and c.getRightOperand() = e
      or
      c.getLeftOperand() = e and c.getRightOperand() = strLitNode
    )
  )
}
```

This predicate checks if the expressions is instance of `EqExpr` which is what we were looking for, if we try to implement a `isSanitizer` predicate in the configuration the query would look like :

```ql
/**
 * @kind path-problem
 */

import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.frameworks.Railties
import codeql.ruby.DataFlow
import codeql.ruby.ApiGraphs
import codeql.ruby.TaintTracking
import codeql.ruby.dataflow.RemoteFlowSources
import codeql.ruby.dataflow.BarrierGuards
import codeql.ruby.DataFlow::DataFlow
import PathGraph

private class GeneralExecution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    GeneralExecution() {
        exists(string member, string method |
            member = "Open4" and method = "popen4" 
        |
            this.asExpr().getExpr() = methodCall and
            this = API::getTopLevelMember(member).getAMethodCall(method)
        )
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }

    override predicate isSink(DataFlow::Node sink) { 
        exists(SystemCommandExecution exec | exec.getAnArgument() = sink) 
        // or sink = any(SystemCommandExecution s).getAnArgument()
    }
    
    override predicate isSanitizer(DataFlow::Node node) {
        node instanceof StringConstCompareBarrier or // No results here
        node instanceof StringConstArrayInclusionCallBarrier // This works fine
    }
}

from CommandInjectionConfiguration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink) 
select sink.getNode(), source, sink, "$@ depends on a $@", sink.getNode(), "This command", source.getNode(), "user-provided value"
```

Note : I also added `StringConstArrayInclusionCallBarrier` to check for those cases the if statement is using an array with a `.include?`

```rb
name = params[:user_name]
if %w(alice bob charlie).include? name
    User.find_by("username = #{name}")
end
```

But this is not working since `StringConstCompareBarrier` is not taking or (||) statements into account, so we have to extend the results, instead of that I'll be writing an independent barrier. First of all I coded `getOrs` predicate :

```ql
// Checks if a comparison is made with literal strings
predicate getOrs(Expr a, Expr b) {
    // To make this recursive
    exists(LogicalOrExpr loe | loe = a |
        getOrs(loe.getRightOperand(), b) or
        getOrs(loe.getLeftOperand(), b)
    ) 
    or
    // Check if it's comparing with a constant string
    // foo = "constant"
    exists(EqExpr ee | a = ee | 
        ee.getLeftOperand() = b  and ee.getRightOperand() instanceof Literal
    )
}
```

`LogicalOrExpr` matches all those || and in the case that first expression is a or expression it will call itself recursively with the Right/Left operand with the second expression. If the first expression is a equal then it will check if the right operand is a literal string returning a true.

![<- Not working](/static/HACKING/kitctfctf-5.png)

Now we have to define our second predicate with configuration nodes and branches, we'll use the `getOrs` we have previously defined :

```ql
predicate comparisonConstant(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists(CfgNodes::ExprNodes::ComparisonOperationCfgNode c, IfExpr ifexpr, VariableAccess va | va = e.getNode() |
        ifexpr.getCondition() = g.getExpr() and
        getOrs(g.getExpr(), va)
    ) and branch = true
}
```

This will work with if expressions and variable access, in the case that the condition of the if statement is the configuration node then `getOrs` will be called with the condition and the variable access.

Once here we'll create our predicate similar to `StringConstCompareBarrier` :

```ql
class MyStringConstCompareBarrier extends DataFlow::Node {
    MyStringConstCompareBarrier() {
      this = DataFlow::BarrierGuard<comparisonConstant/3>::getABarrierNode()
    }
}
```

This works, but we're still not taking a case into account... regular expressions! In the following case we can see how the user input is sanitized using regex :

```ruby
class MyApp < ActionController::Base
  index '/hello/:name' do
    current = params['name']
    outputni = "Timothy"

    current = if ["admin", "root", "KITCTF"].include?(current)
      current # accept current as is
    else
      current = "Hacker"
    end

    current = if current =~ /^[a-z]+$/i
      current # accept current as is
    else
      current = "Hacker"
    end
    Kernel.system("ls -la #{current}") # 
    return "executed command #{current}"
    
  end

end
```

With this regular expression `/^[a-z]+$/i` the user can only input letters then command injection would not be possible. In our case we'll not be evaluating if the regex is secure or not, we'll dictate that in case the variable goes through the sanitizer using regex, it is not vulnerable.

As well as `getOrs` we'll implement `getRegex` :

```ql
predicate getRegex(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists( RegExpMatchExpr matchExpr | matchExpr = g.getExpr() |
        e.getNode() = matchExpr.getLeftOperand()
    ) and branch = true
}
```

In my case I used `RegExpMatchExpr` which includes all those regex match expressions :

```ql
/**
 * A regexp match expression.
 * ```rb
 * input =~ /\d/
 * ```
 */
class RegExpMatchExpr extends BinaryOperation, TRegExpMatchExpr {
  final override string getAPrimaryQlClass() { result = "RegExpMatchExpr" }
}
```

Then we create the `MyRegexBarrier` class :

```ql
class MyRegexBarrier extends DataFlow::Node {
    MyRegexBarrier() {
      this = DataFlow::BarrierGuard<getRegex/3>::getABarrierNode()
    }
}
```

Once done all this, our final query would look like :

```ql
/**
 * @kind Command Injection
 */

import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.frameworks.Railties
import codeql.ruby.DataFlow
import codeql.ruby.ApiGraphs
import codeql.ruby.TaintTracking
import codeql.ruby.dataflow.RemoteFlowSources
import codeql.ruby.dataflow.BarrierGuards
import codeql.ruby.DataFlow::DataFlow
import codeql.ruby.Regexp as Foo
import PathGraph
private import codeql.ruby.CFG
private import codeql.ruby.controlflow.CfgNodes

private class GeneralExecution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    GeneralExecution() {
        exists(string member, string method |
            member = "Open4" and method = "popen4" 
        |
            this.asExpr().getExpr() = methodCall and
            this = API::getTopLevelMember(member).getAMethodCall(method)
        )
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

// Checks if a comparison is made with literal strings
predicate getOrs(Expr a, Expr b) {
    // To make this recursive
    exists(LogicalOrExpr loe | loe = a |
        getOrs(loe.getRightOperand(), b) or
        getOrs(loe.getLeftOperand(), b)
    ) 
    or
    // Check if it's comparing with a constant string
    // foo = "constant"
    exists(EqExpr ee | a = ee | 
        ee.getLeftOperand() = b  and ee.getRightOperand() instanceof Literal
    )
}

predicate comparisonConstant(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists(CfgNodes::ExprNodes::ComparisonOperationCfgNode c, IfExpr ifexpr, VariableAccess va | va = e.getNode() |
        ifexpr.getCondition() = g.getExpr() and
        getOrs(g.getExpr(), va)
    ) and branch = true
}

class MyStringConstCompareBarrier extends DataFlow::Node {
    MyStringConstCompareBarrier() {
      this = DataFlow::BarrierGuard<comparisonConstant/3>::getABarrierNode()
    }
}

predicate getRegex(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists( RegExpMatchExpr matchExpr | matchExpr = g.getExpr() |
        e.getNode() = matchExpr.getLeftOperand()
    ) and branch = true
}

class MyRegexBarrier extends DataFlow::Node {
    MyRegexBarrier() {
      this = DataFlow::BarrierGuard<getRegex/3>::getABarrierNode()
    }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }

    override predicate isSink(DataFlow::Node sink) { 
        exists(SystemCommandExecution exec | exec.getAnArgument() = sink) 
        // or sink = any(SystemCommandExecution s).getAnArgument()
    }
    
    override predicate isSanitizer(DataFlow::Node node) {
        node instanceof StringConstCompareBarrier or // No results here
        node instanceof StringConstArrayInclusionCallBarrier or// This works fine 
        node instanceof MyStringConstCompareBarrier or
        node instanceof MyRegexBarrier
        // and node.getLocation().getFile().getAbsolutePath().matches(["%intrigus/kitctf/kitctfctf/twozerotwotwo/codeql/3output_stage_THIRD/project002/foo_deriverd.rb%"])
    }
}

from CommandInjectionConfiguration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink) 
select sink.getNode(), source, sink, "$@ depends on a $@", sink.getNode(), "This command", source.getNode(), "user-provided value"
```

![<- Not working](/static/HACKING/kitctfctf-6.png)

> [KCTF{p20b13m_f1nd1n9_47_5c4l3_r0cks}](http://kitctf.me:23456/challenge_3_view.html?id=6379558027080100099)


## Grep it? CodeQL it! 5 - Revenge for 3: Hardcoding is boring

> Challenge description

Do not hardcode. The database did not change, only the submission site You have removed some alerts, but your developers are still not happy. You now only have executions that are remotely reachable, but some of these calls are still perfectly safe.

Find all the system command executions marked with BAD but only those executions that execute remote user input and where the remote input is not sanitized/restricted to a safe input. Submit the query on the submission site to get the flag.

Note: The BAD comments will be stripped on the query verification page.

Note: The submission site expects the select clause to look like this: select [THE_COMMAND_EXECUTION_ELEMENT], "OPTIONAL_MESSAGE"

http://kitctf.me:23456/challenge_5.html

### Challenge Setup

Same database : `challenge-three-database`.

### Query writing

We use the same one as in Challenge 3 since we had not harcoded :

```ql
/**
 * @kind Command Injection
 */

import ruby
import codeql.ruby.AST
import codeql.ruby.Concepts
import codeql.ruby.frameworks.Railties
import codeql.ruby.DataFlow
import codeql.ruby.ApiGraphs
import codeql.ruby.TaintTracking
import codeql.ruby.dataflow.RemoteFlowSources
import codeql.ruby.dataflow.BarrierGuards
import codeql.ruby.DataFlow::DataFlow
import codeql.ruby.Regexp as Foo
import PathGraph
private import codeql.ruby.CFG
private import codeql.ruby.controlflow.CfgNodes

private class GeneralExecution extends SystemCommandExecution::Range { // instanceof DataFlow::Node {
    MethodCall methodCall;
    GeneralExecution() {
        exists(string member, string method |
            member = "Open4" and method = "popen4" 
        |
            this.asExpr().getExpr() = methodCall and
            this = API::getTopLevelMember(member).getAMethodCall(method)
        )
    }
    
    override DataFlow::Node getAnArgument() {
        result.asExpr().getExpr() = methodCall.getAnArgument()
    }
}

// Checks if a comparison is made with literal strings
predicate getOrs(Expr a, Expr b) {
    // To make this recursive
    exists(LogicalOrExpr loe | loe = a |
        getOrs(loe.getRightOperand(), b) or
        getOrs(loe.getLeftOperand(), b)
    ) 
    or
    // Check if it's comparing with a constant string
    // foo = "constant"
    exists(EqExpr ee | a = ee | 
        ee.getLeftOperand() = b  and ee.getRightOperand() instanceof Literal
    )
}

predicate comparisonConstant(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists(CfgNodes::ExprNodes::ComparisonOperationCfgNode c, IfExpr ifexpr, VariableAccess va | va = e.getNode() |
        ifexpr.getCondition() = g.getExpr() and
        getOrs(g.getExpr(), va)
    ) and branch = true
}

class MyStringConstCompareBarrier extends DataFlow::Node {
    MyStringConstCompareBarrier() {
      this = DataFlow::BarrierGuard<comparisonConstant/3>::getABarrierNode()
    }
}

predicate getRegex(CfgNodes::ExprCfgNode g, CfgNode e, boolean branch) {
    exists( RegExpMatchExpr matchExpr | matchExpr = g.getExpr() |
        e.getNode() = matchExpr.getLeftOperand()
    ) and branch = true
}

class MyRegexBarrier extends DataFlow::Node {
    MyRegexBarrier() {
      this = DataFlow::BarrierGuard<getRegex/3>::getABarrierNode()
    }
}

class CommandInjectionConfiguration extends TaintTracking::Configuration {

    CommandInjectionConfiguration() { this = "CommandInjectionConfiguration" }
    
    override predicate isSource(DataFlow::Node source) { source instanceof RemoteFlowSource }

    override predicate isSink(DataFlow::Node sink) { 
        exists(SystemCommandExecution exec | exec.getAnArgument() = sink) 
        // or sink = any(SystemCommandExecution s).getAnArgument()
    }
    
    override predicate isSanitizer(DataFlow::Node node) {
        node instanceof StringConstCompareBarrier or // No results here
        node instanceof StringConstArrayInclusionCallBarrier or// This works fine 
        node instanceof MyStringConstCompareBarrier or
        node instanceof MyRegexBarrier
        // and node.getLocation().getFile().getAbsolutePath().matches(["%intrigus/kitctf/kitctfctf/twozerotwotwo/codeql/3output_stage_THIRD/project002/foo_deriverd.rb%"])
    }
}

from CommandInjectionConfiguration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink) 
select sink.getNode(), source, sink, "$@ depends on a $@", sink.getNode(), "This command", source.getNode(), "user-provided value"
```

> [KCTF{h4rdc0d3_15_b0r1n9}](http://kitctf.me:23456/challenge_5_view.html?id=14476536906176344975)

# THANKS FOR READING!!

Also many thanks to [@intrigus](https://twitter.com/intrigus_?s=21&t=ayms7G6CfJoMaNFpegxzqw) for bringing CodeQL challenges 😄, really enjoyed it! I hope that in the future this type of challenge will appear more in CTFs.