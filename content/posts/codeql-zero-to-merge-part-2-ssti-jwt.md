---
title: "CodeQL Zero to Merge: Part 2 - SSTI & JWT"
date: 2024-02-07T14:53:54+01:00
categories:
    - CodeQL
tags:
    - CodeQL
---

In this blog post, you'll find a step-by-step guide on how to write a CodeQL query covering [Ruby Server Side Template Injection](https://portswigger.net/web-security/server-side-template-injection). This post is the practical application of the theory explained in the [previous post](https://maikypedia.gitlab.io/posts/codeql-zero-to-merge-part-1-essentials/) (highly recommended to read before this one).


# [*] INDEX:

1. [Introduction](#1--introduction)
    - [Environment Configuration](#11--environment-configuration)
2. [Ruby Server Side Template Injection](#2--ruby-server-side-template-injection)
    - [Preparing the database](#21--preparing-the-database)
        - [ERB](#211--erb)
        - [Slim](#212--slim)
        - [Building the database](#213--building-the-database)
    - [Query Structure](#22--query-structure)
    - [Start Coding](#23--start-coding)
        - [Concepts](#231--concepts)
    - [ERB](#24--erb)
    - [Slim](#25--slim)
    - [Frameworks](#26--frameworks)
    - [Customizations](#27--customizations)
        - [Sanitizers](#271--sanitizers)
    - [Query](#28--query)
    - [TemplateInjection.ql](#29--templateinjectionql)
    - [QHelp](#210--qhelp)
        - [Overview](#2101--overview)
        - [Recommendation](#2102--recommendation)
        - [Examples](#2103--examples)
        - [Additional resources](#2104--additional-resources)
    - [Tests](#211--tests)
    - [Change note](#212--change-note)
3. [JWT Security Queries](#3--jwt-security-queries)
    - [Preparing the database](#31--preparing-the-database)
        - [Building the database](#311--building-the-database)
    - [Query Structure](#32--query-structure)
    - [Start Coding](#33--start-coding)
        - [Concepts](#331--concepts)
    - [JWT Decoding](#34--jwt-decoding)
    - [Frameworks](#35--frameworks)
    - [Customizations and Query](#36--customizations-and-query)
    - [MissingJWTVerification.ql](#37--missingjwtverificationql)
    - [QHelp](#38--qhelp)
        - [Overview](#381--overview)
        - [Recommendation](#382--recommendation)
        - [Examples](#383--examples)
        - [Additional resources](#384--additional-resources)
    - [Tests](#39--tests)
    - [Change note](#310--change-note)
    - [Preparing the database](#311--preparing-the-database)
        - [Building the database](#3111--building-the-database)
    - [Query Structure](#312--query-structure)
    - [Start Coding](#313--start-coding)
        - [Concepts](#3131--concepts)
    - [JWT Encoding](#314--jwt-encoding)
    - [Frameworks](#315--frameworks)
    - [Customizations and Query](#316--customizations-and-query)
    - [EmptyJwtSecret.ql](#317--emptyjwtsecretql)
    - [QHelp](#318--qhelp)
        - [Overview](#3181--overview)
        - [Examples](#3182--examples)
        - [Additional resources](#3183--additional-resources)
    - [Tests](#319--tests)
    - [Change note](#320--change-note)
4. [Bounty!](4--bounty)

# 1- Introduction

Most of the time we develop CodeQL we have to understand first how other queries are written. In this case, for Ruby SSTI I took as an example other languages like Python and JS. 

It's important to understand that writing a query that works is far from a good query. One of the strengths of CodeQL is the fact that it is modular, the possibility of reusing the code is very important. That's the reason why CodeQL is Declarative OOP.

# 1.1- Environment Configuration

Unlike other programming languages like Python or Ruby, we're incorporating our code into the CodeQL source code. Consequently, to make modifications or additions, we must clone the source from GitHub's CodeQL [repository](https://github.com/github/codeql). But as good practice I recommend forking the repository and then coning your own fork, this will allow us to better manage our branches to later make pull requests 😎.

```bash
git clone https://github.com/<YOUR_USERNAME>/codeql
cd codeql/
git checkout -b <YOUR_USERNAME>/ruby-ssti
```

We'll also need to install the CodeQL CLI binary. For macOS, you can use Homebrew by running brew install codeql. For other systems, refer to the guide in the [GitHub Documentation](https://docs.github.com/en/code-security/codeql-cli/getting-started-with-the-codeql-cli/setting-up-the-codeql-cli).

Finally we would have to install the VS Code extension (available in the Extensions Marketplace).

# 2- Ruby Server Side Template Injection

# 2.1- Preparing the database

CodeQL is a query language, just like SQL, where code is executed against a database, CodeQL also requires a database. But our database is actually CODE!

Initially we have to build the vulnerable code snippet we want to detect. In our case I have chosen Server Side Template Injection in Ruby, where the templating languages are ERB and Slim. 

Let's start understanding what SSTI is, this vulnerability occurs when user input is embedded in a template's code in an unsafe manner. By using native template syntax, an attacker could inject malicious code into the template, which is then executed in the server-side. Leading to remote code execution. So, let's start with ERB!

## 2.1.1- ERB

```rb
require 'erb'

name = "maiky"

text = "Hello %s" % name
template = ERB.new(text).result(binding)
puts template
# Output : Hello maiky
```

The above functionality is straightforward: a text takes input and ERB constructs a template using `.new()`. Subsequently, the template is rendered using `.result(binding)`. The vulnerability in this process can be demonstrated with the following payload:

```rb
require 'erb'

name = "<%= 7*7 %>"

text = "Hello %s" % name
template = ERB.new(text).result(binding)
puts template
# Output : Hello 49
```

As you can see, the above snippet makes ERB execute the operation. Now that we have identified the vulnerable code, it is crucial to understand how to address the issue. The key solution lies in modifying the code to ensure that parameters used in the queries are passed during template construction, rather than before.

```rb
require 'erb'

name = "<%= 7*7 %>"

text = "Hello <%= name %>"
template = ERB.new(text).result(binding)
puts template
# Output : Hello <%= 7*7 %>
```

## 2.1.2- Slim

```rb
require 'slim'

name = "Maiky"
text = "Hello %s" % name
template = Slim::Template.new { text }.render
puts template
# <Hello>Maiky</Hello>
```

The functionality is pretty similar to ERB, where a text takes an input, and Slim constructs the template using `.new{}`. Subsequently, it is rendered using `.render`. The corrective action for this scenario is also similar:

```rb
require 'slim'

name = "{ 7*7 }"
text = "<!DOCTYPE html>
      html
        body
          h2  == name;
    "
template = Slim::Template.new{ text }.render(Object.new, name: name)
puts template
# <!DOCTYPE html><html><body><h2>{ 7*7 }</h2></body></html>
```

Drawing parallels, this vulnerability has certain things in common with SQL Injection, which will serve as an example for the following reasons:

1- The template/statement is constructed before rendering/execution.

2- If the payload is constructed but NOT rendered/executed, it's not exploitable.

3- The correct way to use it is by using parameterized templates/queries.

## 2.1.3- Building the database

Let's start with a simple snippet as our database, we'll name it `code.rb`:

```rb
require 'erb'
require 'slim'

def some_request_handler
    name = "name"
    html_text = "
        <!DOCTYPE html><html><body>
        <h2>Hello %s </h2></body></html>
        " % name
    template = ERB.new(html_text).result(binding)
end


def some_request_handler
    name = "name"
    html_text = "
        <!DOCTYPE html><html><body>
        <h2>Hello %s </h2></body></html>
        " % name
    Slim::Template.new{ html_text }.render
end
```

Let's create the database by executing the following command:

```bash
codeql database create --language=ruby --source-root=Source RubySstiDB
```

In this process we're creating a Ruby database from the `Source/` folder, the database will be saved as `RubySstiDB`. The complete database building guide is available in the [GitHub Documentation](https://docs.github.com/en/code-security/codeql-cli/getting-started-with-the-codeql-cli/preparing-your-code-for-codeql-analysis). After successfully building the database, the next step is to add it into Visual Studio Code.

![image](https://gist.github.com/assets/76447395/d1674138-0dfc-4297-9a00-029b8cb06ffe)

# 2.2-  Query Structure

As said in the beginning of the post, understanding the structure of a query is crucial, since it has to be modular. While all the queries are written in CodeQL, it's important to note that the naming conventions for files and methods may differ across programming languages. 

- `Concepts.qll`: This file provides abstract classes representing generic concepts. One example can be the Command Execution class, intended to be extended by the models of diverse libraries responsible for executing code. It's worth noting that this file does not pertain directly to security-related aspects (command injection is not modeled but rather system command execution).

- `Frameworks.qll`: This file serves as a helper file responsible for importing all framework modeling components. For instance, if we are modeling ERB and Slim, we have to import them.

- `frameworks/`: This folder is designated for the organization of our library modeling components.

- `security/<Vuln>Customizations.qll`: Within this file, sources, sinks, sanitizers and additional taint steps are defined for the vulnerability.

- `security/<Vuln>Query.qll`: While this file might be classified as part of customizations in other languages, in the case of Ruby, it specifically manages the configuration for taint tracking.

- `change-notes/<date><Vuln>.md`: Tiny message to describe what we are implementing.

- `experimental/<Vuln>/<Vuln>.qhelp`: The file format may differ across languages; this documentation is dedicated to details about the vulnerability. It covers aspects such as a representation of the vulnerable code, recommended fixes, and additional resources for further understanding.

- `experimental/<Vuln>/<Vuln>.ql`: This represents the final query, also importing <Vuln>Query.qll. It serves as a sort of launcher for the overall vulnerability detection process.

# 2.3- Start Coding

## 2.3.1- Concepts

Before diving into the code, let's first think about what we want to do. I typically begin by modeling Concepts, (if what we need is already modeled, this part can be skipped).  Maintaining consistency with established implementations is essential to ensure clarity and coherence. As mentioned earlier, our objective closely resembles SQL Injection. Let's now examine the structure of SQL concepts:

```ql
/**
 * A data-flow node that constructs a SQL statement.
 *
 * Often, it is worthy of an alert if a SQL statement is constructed such that
 * executing it would be a security risk.
 *
 * If it is important that the SQL statement is executed, use `SqlExecution`.
 *
 * Extend this class to refine existing API models. If you want to model new APIs,
 * extend `SqlConstruction::Range` instead.
 */
class SqlConstruction extends DataFlow::Node instanceof SqlConstruction::Range {
  /** Gets the argument that specifies the SQL statements to be constructed. */
  DataFlow::Node getSql() { result = super.getSql() }
}
/** Provides a class for modeling new SQL execution APIs. */
module SqlConstruction {
  /**
   * A data-flow node that constructs a SQL statement.
   *
   * Often, it is worthy of an alert if a SQL statement is constructed such that
   * executing it would be a security risk.
   *
   * If it is important that the SQL statement is executed, use `SqlExecution`.
   *
   * Extend this class to model new APIs. If you want to refine existing API models,
   * extend `SqlConstruction` instead.
   */
  abstract class Range extends DataFlow::Node {
    /** Gets the argument that specifies the SQL statements to be constructed. */
    abstract DataFlow::Node getSql();
  }
}
/**
 * A data-flow node that executes SQL statements.
 *
 * If the context of interest is such that merely constructing a SQL statement
 * would be valuable to report, consider using `SqlConstruction`.
 *
 * Extend this class to refine existing API models. If you want to model new APIs,
 * extend `SqlExecution::Range` instead.
 */
class SqlExecution extends DataFlow::Node instanceof SqlExecution::Range {
  /** Gets the argument that specifies the SQL statements to be executed. */
  DataFlow::Node getSql() { result = super.getSql() }
}
/** Provides a class for modeling new SQL execution APIs. */
module SqlExecution {
  /**
   * A data-flow node that executes SQL statements.
   *
   * If the context of interest is such that merely constructing a SQL
   * statement would be valuable to report, consider using `SqlConstruction`.
   *
   * Extend this class to model new APIs. If you want to refine existing API models,
   * extend `SqlExecution` instead.
   */
  abstract class Range extends DataFlow::Node {
    /** Gets the argument that specifies the SQL statements to be executed. */
    abstract DataFlow::Node getSql();
  }
}
```

SQL consists of two modules: one for SQL Construction and another for SQL Execution. This comes in handy because our goal involves Template Construction and Template Rendering. You might wonder why Template Construction is necessary, especially when our payload is not executed at that point. QLdoc explains this in SqlConstruction:

```bash
 * Often, it is worthy of an alert if a SQL statement is constructed such that
 * executing it would be a security risk.
```

Let's start with our first module `TemplateConstruction`. To model new APIs we have to create an abstract class within the module called `Range`, which will extends `DataFlow::Node`:

```ql
module TemplateConstruction {
  abstract class Range extends DataFlow::Node {
  }
}
```

Additionally, it's necessary to define the abstract function getTemplate():

```ql
module TemplateConstruction {
  abstract class Range extends DataFlow::Node {
    abstract DataFlow::Node getTemplate();
  }
}
```

Once our module is ready let's write our `TemplateConstruction` class. This class inherits from `DataFlow::Node` and belongs to the type `TemplateConstruction::Range`. Furthermore, we need to define the `getTemplate()` function. This function overrides the `getTemplate()` method of the parent class, and returns the result of the parent class's function.

```ql
class TemplateConstruction extends DataFlow::Node instanceof TemplateConstruction::Range {
  DataFlow::Node getTemplate() { result = super.getTemplate() }
}
```

Now, let's proceed with `TemplateRendering`, the structure of which is similar to `TemplateConstruction`:

```ql
class TemplateRendering extends DataFlow::Node instanceof TemplateRendering::Range {
  DataFlow::Node getTemplate() { result = super.getTemplate() }
}

module TemplateRendering {
  abstract class Range extends DataFlow::Node {
    abstract DataFlow::Node getTemplate();
  }
}
```

## 2.4- ERB
Firstly, let's define the `ERB` module with a `SummarizedCallable` (a callable with a flow summary), which is an alternative to the following:

```ql
propagatesFlow(
   SummaryComponentStack input, SummaryComponentStack output, boolean preservesValue
)
```

About how to write our `TemplateSummary` we can refer to the example of `Arel` (Sql Injection):


```ql
  /**
   * Flow summary for `Arel.sql`. This method wraps a SQL string, marking it as
   * safe.
   */
  private class SqlSummary extends SummarizedCallable {
    SqlSummary() { this = "Arel.sql" }

    override MethodCall getACall() {
      result = API::getTopLevelMember("Arel").getAMethodCall("sql").asExpr().getExpr()
    }

    override predicate propagatesFlowExt(string input, string output, boolean preservesValue) {
      input = "Argument[0]" and output = "ReturnValue" and preservesValue = false
    }
  }
```

This `SqlSummary` includes a `getACall()` method that returns an `Arel.sql()` expression. In our case, the structure would be quite similar, but instead of `Arel.sql()`, we'd be using with `ERB.new`.

Let's start modeling `ErbTemplateNewCall`. This class extends both `TemplateConstruction::Range` (previously defined in `Concepts`) and `DataFlow::CallNode`. As it extends `DataFlow::CallNode`, it should return a call. Similarly, as it extends `TemplateConstruction::Range`, the `getTemplate()` method needs to be overridden.

To obtain `ERB.new` we have to first get `ERB`, we can do this using `API::getTopLevelMember()` which gets an access to the top-level constant. Subsequently, we retrieve the instantiation call, as it creates an object (otherwise it would be `getAMethodCall("methodName")`). Regarding `getTemplate()`, we use the override keyword and specify the desired return type (`DataFlow::Node` in this case). Since the template we aim to return is the template string, it corresponds to the argument at index 0.

```ql
private class ErbTemplateNewCall extends TemplateConstruction::Range, DataFlow::CallNode {
    ErbTemplateNewCall() { this = API::getTopLevelMember("ERB").getAnInstantiation() }

    override DataFlow::Node getTemplate() { result = this.getArgument(0) }
}
```

Once having this we can finish our `TemplateSummary`:

```ql
private class TemplateSummary extends SummarizedCallable {
    TemplateSummary() { this = "ERB.new" }

    override MethodCall getACall() { result = any(ErbTemplateNewCall c).asExpr().getExpr() }

    override predicate propagatesFlowExt(string input, string output, boolean preservesValue) {
        input = "Argument[0]" and output = "ReturnValue" and preservesValue = false
    }
}
```

The result in our `getACall()` will be the expression of any ERB new call: `any(ErbTemplateNewCall c).asExpr().getExpr()`. What's missing now is Template Rendering, correct? Here, we desire a node that gets rendered, but not just any node — specifically, we want a `TemplateConstruction` node.

```ql
  private class ErbTemplateRendering extends TemplateRendering::Range, DataFlow::CallNode {
    private DataFlow::Node template;

    ErbTemplateRendering() {
      exists(ErbTemplateNewCall templateConstruction |
        this = templateConstruction.getAMethodCall("result") and
        template = templateConstruction.getTemplate()
      )
    }

    override DataFlow::Node getTemplate() { result = template }
  }
```

We can declare a field within the class, in this case `template`, this field will store the value returned from `TemplateConstruction.getTemplate()`. This class node will be `ERB.new().result` and the `getTemplate()` method will return the template node.

Now we have `ERB` modeled! This is final `Erb.qll`:

```ql
/**
 * Provides templating for embedding Ruby code into text files, allowing dynamic content generation in web applications.
 */

private import codeql.ruby.ApiGraphs
private import codeql.ruby.dataflow.FlowSummary
private import codeql.ruby.Concepts

/**
 * Provides templating for embedding Ruby code into text files, allowing dynamic content generation in web applications.
 */
module Erb {
  /**
   * Flow summary for `ERB.new`. This method wraps a template string, compiling it.
   */
  private class TemplateSummary extends SummarizedCallable {
    TemplateSummary() { this = "ERB.new" }

    override MethodCall getACall() { result = any(ErbTemplateNewCall c).asExpr().getExpr() }

    override predicate propagatesFlowExt(string input, string output, boolean preservesValue) {
      input = "Argument[0]" and output = "ReturnValue" and preservesValue = false
    }
  }

  /** A call to `ERB.new`, considered as a template construction. */
  private class ErbTemplateNewCall extends TemplateConstruction::Range, DataFlow::CallNode {
    ErbTemplateNewCall() { this = API::getTopLevelMember("ERB").getAnInstantiation() }

    override DataFlow::Node getTemplate() { result = this.getArgument(0) }
  }

  /** A call to `ERB.new(foo).result(binding)`, considered as a template rendering. */
  private class ErbTemplateRendering extends TemplateRendering::Range, DataFlow::CallNode {
    private DataFlow::Node template;

    ErbTemplateRendering() {
      exists(ErbTemplateNewCall templateConstruction |
        this = templateConstruction.getAMethodCall("result") and
        template = templateConstruction.getTemplate()
      )
    }

    override DataFlow::Node getTemplate() { result = template }
  }
}
```

## 2.5- Slim

This module will be pretty similar to ERB but with some little differences, let's start defining `SlimTemplateNewCall` this as `ERB` we're extending `TemplateConstruction::Range` and `DataFlow::CallNode`. It's worth noting that, in contrast to `ERB`, `Slim` has two members (Slim::Template) rather than one. In this case, `Slim` serves as the top-level member of Template. To retrieve the instantiation, we can use:

```ql
SlimTemplateNewCall() {
      this = API::getTopLevelMember("Slim").getMember("Template").getAnInstantiation()
}
```

I've been stuck many days at this point, I didn't know how to get a call using brackets instead of parenthesis like `Slim::Template.new{text}`. At this point I asked for help in the [GitHub Security Lab Slack forum](https://gh.io/securitylabslack) (where I highly recommend to read and ask). To obtain a call using brackets instead of parentheses in expressions like `Slim::Template.new{text}`, you can leverage `this.getBlock()` to locate the block. However, since it returns a `Node`, you need to cast it to a `BlockNode` and then call `asCallableAstNode()`, a method specific to `BlockNode`. This sequence helps to access a statement within the block. Importantly, remember that this returns an Expr, and you can handle this by employing the following code: 

```ql
result.asExpr().getExpr() = this.getBlock().(DataFlow::BlockNode).asCallableAstNode().getAStmt()
```

In this scenario, by asserting that the expression of our node is equivalent to the `BlockNode` statement, the result is designated as a `DataFlow::Node`. 

Regarding `SlimTemplateRendering`, its structure closely parallels that of `ErbTemplateRendering`.

`SlimTemplateRendering` is essentially a `SlimTemplateNewCall` that invokes `renders` method. Additionally we have to define the private field `template` which is the result of `SlimTemplateNewCall.getTemplate()`:

```ql
private class SlimTemplateRendering extends TemplateRendering::Range, DataFlow::CallNode {
    private DataFlow::Node template;

    SlimTemplateRendering() {
      exists(SlimTemplateNewCall templateConstruction |
        this = templateConstruction.getAMethodCall("render") and
        template = templateConstruction.getTemplate()
      )
    }

    override DataFlow::Node getTemplate() { result = template }
}
```

`Slim.qll` will look like:

```ql
/**
 * Provides templating for embedding Ruby code into text files, allowing dynamic content generation in web applications.
 */

private import codeql.ruby.ApiGraphs
private import codeql.ruby.dataflow.FlowSummary
private import codeql.ruby.Concepts

/**
 * Provides templating for embedding Ruby code into text files, allowing dynamic content generation in web applications.
 */
module Slim {
  /** A call to `Slim::Template.new`, considered as a template construction. */
  private class SlimTemplateNewCall extends TemplateConstruction::Range, DataFlow::CallNode {
    SlimTemplateNewCall() {
      this = API::getTopLevelMember("Slim").getMember("Template").getAnInstantiation()
    }

    override DataFlow::Node getTemplate() {
      result.asExpr().getExpr() =
        this.getBlock().(DataFlow::BlockNode).asCallableAstNode().getAStmt()
    }
  }

  /** A call to `Slim::Template.new{ foo }.render`, considered as a template rendering */
  private class SlimTemplateRendering extends TemplateRendering::Range, DataFlow::CallNode {
    private DataFlow::Node template;

    SlimTemplateRendering() {
      exists(SlimTemplateNewCall templateConstruction |
        this = templateConstruction.getAMethodCall("render") and
        template = templateConstruction.getTemplate()
      )
    }

    override DataFlow::Node getTemplate() { result = template }
  }
}
```

## 2.6- Frameworks

Once we have modeled our libraries, it's important to include them in the `Frameworks.qll` file. This step is crucial, otherwise those libraries won't be considered:

```ql
private import codeql.ruby.frameworks.Erb
private import codeql.ruby.frameworks.Slim
```

## 2.7- Customizations

Customizations is the file where we're gonna define what will be our sources, sinks, sanitizers, additional taint steps... Let's start from the source, our source will be a `DataFlow::Node` as well as the sink and the sanitizer:

```ql
module TemplateInjection {
    abstract class Source extends DataFlow::Node { }

    abstract class Sink extends DataFlow::Node { }

    abstract class Sanitizer extends DataFlow::Node { }
}
```

Let's consider the specific nodes that we want to designate as sources. In this context we're interested in those cases that user input is embedded. Conveniently, CodeQL already provides a class named `RemoteFlowSource`, which aligns with our requirements. Therefore, we can use instances of this class as our sources:

```ql
private class RemoteFlowSourceAsSource extends Source, RemoteFlowSource { }
```

So what about the sink? As previosuly mentioned this query is pretty similar to SQL Injection, SQL Injection sink is:

```ql
/**
* An SQL statement of a SQL execution, considered as a flow sink.
*/
private class SqlExecutionAsSink extends Sink {
    SqlExecutionAsSink() { this = any(SqlExecution e).getSql() }
}
```

Since we were extending `TemplateRendering` we just have to make some tiny modifications:

```ql
private class TemplateRenderingAsSink extends Sink {
    TemplateRenderingAsSink() { this = any(TemplateRendering e).getTemplate() }
}
```

### 2.7.1- Sanitizers

What is a sanitizer? A sanitizer serves as a node that, in the event that the flow passes through it, we lose interest because it functions as a barrier. To illustrate, here's a typical representation of a sanitizer:

```rb
name = "<%= 7*7 %>"
name = if ["admin", "guest"].include? name
      name
    else 
      name = "none"
    end
    text = "
<!DOCTYPE html><html><body>
<h2>Hello %s </h2></body></html>
" % name
template_bar = ERB.new(text).result(binding)
```

In this case, the expected result won't be `Hello 49` because the variable name undergoes a sanitation process. The sanitation involves checking whether the value is present in a whitelist, and if not, the variable name is set to "none". Another approach, instead of using `include?`, could be utilizing `==` as follows:

```rb
name = "<%= 7*7 %>"
name = if name == "admin"
      name
    else 
      name = "none"
    end
    text = "
<!DOCTYPE html><html><body>
<h2>Hello %s </h2></body></html>
" % name
template_bar = ERB.new(text).result(binding)
```

We also should consider the use of Regular Expressions as sanitizers:

```rb
name = "<%= 7*7 %>"
name = if name =~ /^[a-z]+$/i
      name
    else 
      name = "none"
    end
    text = "
<!DOCTYPE html><html><body>
<h2>Hello %s </h2></body></html>
" % name
template_bar = ERB.new(text).result(binding)
```

In this case the value of name only can be alphanumeric this would have been a false positive, we should also recognize this as sanitizer. 

CodeQL already offer dedicated a class for such sanitizers: `StringConstCompareBarrier` and `StringConstArrayInclusionCallBarrier`:

```ql
  private class StringConstCompareAsSanitizerGuard extends Sanitizer, StringConstCompareBarrier { }

  private class StringConstArrayInclusionCallAsSanitizer extends Sanitizer,
    StringConstArrayInclusionCallBarrier
  { }
```

This is the final version of our `TemplateInjectionCustomizations.qll` file:

```ql
/**
 * Provides default sources, sinks and sanitizers for detecting
 * ERB Server Side Template Injections, as well as extension points for adding your own
 */

private import codeql.ruby.Concepts
private import codeql.ruby.DataFlow
private import codeql.ruby.dataflow.BarrierGuards
private import codeql.ruby.dataflow.RemoteFlowSources

/**
 * Provides default sources, sinks and sanitizers for detecting
 * Server Side Template Injections, as well as extension points for adding your own
 */
module TemplateInjection {
  /** A data flow source for SSTI vulnerabilities */
  abstract class Source extends DataFlow::Node { }

  /** A data flow sink for SSTI vulnerabilities */
  abstract class Sink extends DataFlow::Node { }

  /** A sanitizer for SSTI vulnerabilities. */
  abstract class Sanitizer extends DataFlow::Node { }

  /**
   * A source of remote user input, considered as a flow source.
   */
  private class RemoteFlowSourceAsSource extends Source, RemoteFlowSource { }

  /**
   * An Server Side Template Injection rendering, considered as a flow sink.
   */
  private class TemplateRenderingAsSink extends Sink {
    TemplateRenderingAsSink() { this = any(TemplateRendering e).getTemplate() }
  }

  /**
   * A comparison with a constant string, considered as a sanitizer-guard.
   */
  private class StringConstCompareAsSanitizerGuard extends Sanitizer, StringConstCompareBarrier { }

  /**
   * An inclusion check against an array of constant strings, considered as a
   * sanitizer-guard.
   */
  private class StringConstArrayInclusionCallAsSanitizer extends Sanitizer,
    StringConstArrayInclusionCallBarrier
  { }
}
```

## 2.8- Query

This file serves as a wrapper for the Customizations file. Here, we define our TaintTracking configuration, noting that this is now deprecated, and ConfigSig should be used instead. Within the TaintTracking configuration, we need to override four predicates (or three in our case, as we haven't defined any additional taint steps).

First we have to import the `TemplateInjection` module from our `TemplateInjectionCustomizations` file, as well as `DataFlow` and `TaintTracking`:

```ql
private import codeql.ruby.DataFlow
private import codeql.ruby.TaintTracking
import TemplateInjectionCustomizations::TemplateInjection
```

Let's define our `Configuration`, where a `DataFlow::Node` will be a `source` if this type is `Source`(from Customizations extending `RemoteFlowSource`). And we're doing the same with `Sink` and `Sanitizer`:

```ql
/**
 * Provides default sources, sinks and sanitizers for detecting
 * Server Side Template Injections, as well as extension points for adding your own
 */

private import codeql.ruby.DataFlow
private import codeql.ruby.TaintTracking
import TemplateInjectionCustomizations::TemplateInjection

/**
 * A taint-tracking configuration for detecting Server Side Template Injections vulnerabilities.
 */
class Configuration extends TaintTracking::Configuration {
  Configuration() { this = "TemplateInjection" }

  override predicate isSource(DataFlow::Node source) { source instanceof Source }

  override predicate isSink(DataFlow::Node sink) { sink instanceof Sink }

  override predicate isSanitizer(DataFlow::Node node) { node instanceof Sanitizer }
}
```

## 2.9- TemplateInjection.ql

This file is kind of the summary of all we've done, we have to describe the vulnerability, the severity, id, precission...

We have to import `DataFlow`, the Query file we have just written and `DataFlow::PathGraph` (this will help up to see all the steps that our source has taken to the sink).

CodeQL queries syntax is similar to SQL:

```sql
from Type t where predicate(t) select t
```

The keywords are `from`, `where` and `select`. Using `from` we can define the variable that we are going to use, with `where` we can define the some predicate containing the varibale and using `select` we define the results.

In this scenario, we utilize the `TaintTracking Configuration`, along with `DataFlow::PathNode source` and `DataFlow::PathNode sink`. Our objective is to identify cases in the `Configuration` where there is a flow from the `source` to the `sink`. Subsequently, we select both the sink and the source.

```ql
/**
 * @name Server-side template injection
 * @description Building a server-side template from user-controlled sources is vulnerable to
 *              insertion of malicious code by the user.
 * @kind path-problem
 * @problem.severity error
 * @security-severity 8.8
 * @precision high
 * @id rb/server-side-template-injection
 * @tags security
 *       external/cwe/cwe-94
 */

import codeql.ruby.DataFlow
import codeql.ruby.security.TemplateInjectionQuery
import DataFlow::PathGraph

from Configuration config, DataFlow::PathNode source, DataFlow::PathNode sink
where config.hasFlowPath(source, sink)
select sink.getNode(), source, sink, "This template depends on a $@.", source.getNode(),
  "user-provided value"
```

## 2.10- QHelp

QHelp is an XML file with information regarding the vulnerability, guidance on resolution, examples of both vulnerable and secure code snippets, and provides additional resources.

QHelp are usually quite similar within the same language, you can always base yourself on another to write yours. 

### 2.10.1- Overview

Firstly we have to include an overview of the vulnerability:

```xml
<overview>
<p>
Template Injection occurs when user input is embedded in a template's code in an unsafe manner.
An attacker can use native template syntax to inject a malicious payload into a template, which is then executed server-side.
This permits the attacker to run arbitrary code in the server's context.
</p>
</overview>
```

### 2.10.2- Recommendation

Additionally we have to add a recomendation field to explain how to properly fix the vulnerabilty:

```xml
<recommendation>
<p>
To fix this, ensure that untrusted input is not used as part of a template's code. If the application requirements do not allow this,
use a sandboxed environment where access to unsafe attributes and methods is prohibited.
</p>
</recommendation>
```

### 2.10.3- Examples

Within the same folder we have to create the `examples` folder and add there both safe and unsafe code snippets. Then we can refer to the code using `<sample>`. Unlike test files, the example file has to be simple and easy to understand.

- `SSTIBad.rb`

```rb
require 'erb'
require 'slim'

class BadERBController < ActionController::Base
  def some_request_handler
    name = params["name"]
    html_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name
    template = ERB.new(html_text).result(binding) 
  end
end

class BadSlimController < ActionController::Base
  def some_request_handler
    name = params["name"]
    html_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name
    Slim::Template.new{ html_text }.render 
  end
end
```

- `SSTIGood.rb`

```rb
require 'erb'
require 'slim'

class GoodController < ActionController::Base
  def some_request_handler
    name = params["name"]
    html_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello <%= name %> </h2></body></html>
      "
    template = ERB.new(html_text).result(binding) 
  end
end

class GoodController < ActionController::Base
  def some_request_handler
    name = params["name"]
    html_text = "
    <!DOCTYPE html>
      html
        body
          h2  == name;
    "
    Slim::Template.new{ html_text }.render(Object.new, name: name)
  end
end
```

Then we have to explain briefly the reason of why the snippet is safe or not.

```xml
<example>
<p>
Consider the example given below, an untrusted HTTP parameter <code>name</code> is used to generate a template string. This can lead to remote code execution.
</p>
<sample src="examples/SSTIBad.rb" />

<p>
Here we have fixed the problem by including ERB/Slim syntax in the string, then the user input will be rendered but not evaluated.
</p>
<sample src="examples/SSTIGood.rb" />
</example>
```

### 2.10.4- Additional resources

Additional resources are always welcome, such as portswigger blogs, wikipedia or blogs of interest.

```xml
<references>
<li>
Wikipedia: <a href="https://en.wikipedia.org/wiki/Code_injection#Server_Side_Template_Injection">Server Side Template Injection</a>.
</li>
<li>
Portswigger : <a href="https://portswigger.net/web-security/server-side-template-injection">Server Side Template Injection</a>.
</li>
</references>
```

Now let's put together everything we've written:

```xml
<!DOCTYPE qhelp PUBLIC "-//Semmle//qhelp//EN" "qhelp.dtd">
<qhelp>
<overview>
<p>
Template Injection occurs when user input is embedded in a template's code in an unsafe manner.
An attacker can use native template syntax to inject a malicious payload into a template, which is then executed server-side.
This permits the attacker to run arbitrary code in the server's context.
</p>
</overview>

<recommendation>
<p>
To fix this, ensure that untrusted input is not used as part of a template's code. If the application requirements do not allow this,
use a sandboxed environment where access to unsafe attributes and methods is prohibited.
</p>
</recommendation>

<example>
<p>
Consider the example given below, an untrusted HTTP parameter <code>name</code> is used to generate a template string. This can lead to remote code execution.
</p>
<sample src="examples/SSTIBad.rb" />

<p>
Here we have fixed the problem by including ERB/Slim syntax in the string, then the user input will be rendered but not evaluated.
</p>
<sample src="examples/SSTIGood.rb" />
</example>

<references>
<li>
Wikipedia: <a href="https://en.wikipedia.org/wiki/Code_injection#Server_Side_Template_Injection">Server Side Template Injection</a>.
</li>
<li>
Portswigger : <a href="https://portswigger.net/web-security/server-side-template-injection">Server Side Template Injection</a>.
</li>
</references>
</qhelp>
```

## 2.11- Tests

Writing comprehensive test files is crucial for ensuring the reliability of the query and detecting any changes in behavior resulting from updates. These test files are located in `ruby/ql/test/query-tests/experimental/TemplateInjection/`.

A robust test file should be thorough, covering various cases including positive, negative, and scenarios involving sanitizers. It's essential to include comments indicating GOOD and BAD cases, along with explanations for each case. This ensures clarity and aids in understanding the expected behavior of the query.

- Bad case:

```rb
# A string tainted by user input is inserted into a template 
# (i.e a remote flow source)
name = params[:name]

# Template with the source
bad_text = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name

# BAD: user input is evaluated
# where name is unsanitized
template = ERB.new(bad_text).result(binding) 
```

- Good case:

```rb
# A string tainted by user input is inserted into a template 
# (i.e a remote flow source)
name = params[:name]

# Template with the source 
good_text = "
    <!DOCTYPE html><html><body>
    <h2>Hello <%= name %> </h2></body></html>
    "

# GOOD: user input is not evaluated
template2 = ERB.new(good_text).result(binding) 
```

- Sanitized case:

```rb
name1 = params["name1"]
# GOOD: barrier guard prevents taint flow
if name == "admin"
    text_bar1 = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name
else
    text = "
    <!DOCTYPE html><html><body>
    <h2>Hello else </h2></body></html>
    " 
end
template_bar1 = ERB.new(text_bar1).result(binding)
```

- Another sanitized case:

```rb
name2 = params["name2"]
# GOOD: barrier guard prevents taint flow
name2 = if ["admin", "guest"].include? name2
    name2
else 
    name2 = "none"
end
text_bar2 = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name2
template_bar2 = ERB.new(text_bar2).result(binding)
```

- This is how our final `ErbInjection` code would look like:

```rb
class FooController < ActionController::Base
  def some_request_handler
    # A string tainted by user input is inserted into a template 
    # (i.e a remote flow source)
    name = params[:name]

    # Template with the source
    bad_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name

    # BAD: user input is evaluated
    # where name is unsanitized
    template = ERB.new(bad_text).result(binding) 

    # Template with the source 
    good_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello <%= name %> </h2></body></html>
      "

    # GOOD: user input is not evaluated
    template2 = ERB.new(good_text).result(binding) 
  end
end

class BarController < ApplicationController
  def safe_paths
    name1 = params["name1"]
    # GOOD: barrier guard prevents taint flow
    if name == "admin"
      text_bar1 = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name
    else
      text = "
      <!DOCTYPE html><html><body>
      <h2>Hello else </h2></body></html>
      " 
    end
    template_bar1 = ERB.new(text_bar1).result(binding)


    name2 = params["name2"]
    # GOOD: barrier guard prevents taint flow
    name2 = if ["admin", "guest"].include? name2
      name2
    else 
      name2 = "none"
    end
    text_bar2 = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name2
    template_bar2 = ERB.new(text_bar2).result(binding)
  end
end
```

Now let's proceed with creating `SlimInjection.rb` as well as `Erb` we have to include positives, negatives and some barriers.

- Bad case:

```rb
# A string tainted by user input is inserted into a template 
# (i.e a remote flow source)
name = params[:name]

# Template with the source (no sanitizer)
bad_text = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name
# BAD: renders user input
# where text is unsanitized
Slim::Template.new{ bad_text }.render 
```

- Another bad case:

```rb
# A string tainted by user input is inserted into a template 
# (i.e a remote flow source)
name = params[:name]

# Template with the source (no sanitizer)
bad2_text = "
    <!DOCTYPE html><html><body>
    <h2>Hello #{name} </h2></body></html>
    " 
# BAD: renders user input
# where text is unsanitized
Slim::Template.new{ bad2_text }.render 
```

- Good case using parametized template:

```rb
# A string tainted by user input is inserted into a template 
# (i.e a remote flow source)
name = params[:name]

# Template with the source (no render)
good_text = "
<!DOCTYPE html>
    html
    body
        h2  == name;
"
# GOOD: user input is not evaluated
Slim::Template.new{ good_text }.render(Object.new, name: name)
```

- Good case using barriers:

```rb
name1 = params["name1"]
# GOOD: barrier guard prevents taint flow
if name == "admin"
    text_bar1 = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name
else
    text_bar1 = "
    <!DOCTYPE html><html><body>
    <h2>Hello else </h2></body></html>
    " 
end
template_bar1 = Slim::Template.new{ text_bar1 }.render 
```

- Another good case using barriers:

```rb
name2 = params["name2"]
# GOOD: barrier guard prevents taint flow
name2 = if ["admin", "guest"].include? name2
    name2
else 
    name2 = "none"
end
text_bar2 = "
    <!DOCTYPE html><html><body>
    <h2>Hello %s </h2></body></html>
    " % name2
template_bar1 = Slim::Template.new{ text_bar2 }.render 
end
```

- And this is the final test file:

```rb
class FooController < ActionController::Base  
  def some_request_handler
    # A string tainted by user input is inserted into a template 
    # (i.e a remote flow source)
    name = params[:name]

    # Template with the source (no sanitizer)
    bad_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name
    # BAD: renders user input
    # where text is unsanitized
    Slim::Template.new{ bad_text }.render 

    # Template with the source (no sanitizer)
    bad2_text = "
      <!DOCTYPE html><html><body>
      <h2>Hello #{name} </h2></body></html>
      " 
    # BAD: renders user input
    # where text is unsanitized
    Slim::Template.new{ bad2_text }.render 

    # Template with the source (no render)
    good_text = "
    <!DOCTYPE html>
      html
        body
          h2  == name;
    "
    # GOOD: user input is not evaluated
    Slim::Template.new{ good_text }.render(Object.new, name: name)
  end
end

class BarController < ApplicationController
  def safe_paths
    name1 = params["name1"]
    # GOOD: barrier guard prevents taint flow
    if name == "admin"
      text_bar1 = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name
    else
      text_bar1 = "
      <!DOCTYPE html><html><body>
      <h2>Hello else </h2></body></html>
      " 
    end
    template_bar1 = Slim::Template.new{ text_bar1 }.render 

    name2 = params["name2"]
    # GOOD: barrier guard prevents taint flow
    name2 = if ["admin", "guest"].include? name2
      name2
    else 
      name2 = "none"
    end
    text_bar2 = "
      <!DOCTYPE html><html><body>
      <h2>Hello %s </h2></body></html>
      " % name2
    template_bar1 = Slim::Template.new{ text_bar2 }.render 
  end
end
```

CodeQL verifies the proper functioning of the query by executing the query located in `ruby/ql/test/query-tests/experimental/TemplateInjection/TemplateInjection.qlref`:

```ql
experimental/template-injection/TemplateInjection.ql
```

After running the query against the test cases, CodeQL generates a `.expected` file. This file contains the expected results for each test case. Subsequent runs of the test suite will compare the actual results with the expected results stored in these `.expected` files, ensuring consistency and allowing for automated validation of the query's correctness over time. This mechanism is crucial for maintaining the reliability of the query across different versions and updates.


## 2.12- Change note

We also need a change note to document the new query, this can be added by creating a new file `ruby/ql/src/change-notes/2023-03-15-ssti-query.md`:

```md
---
category: newQuery
---
* Added a new experimental query, `rb/server-side-template-injection`, to detect cases where user input may be embedded into a template's code in an unsafe manner.
```
  
# 3- JWT Security Queries
  
## 3.1- Preparing the database

Initially we have to build the vulnearble code snippet we want to detect. In this case we'll write a query to detect Missing JWT (JSON Web Token) Verification in Ruby.

This vulnerability occurs when server doesn't verify the key of the JWT token. If a JWT token is utilized and the server omits key verification, it poses a risk of allowing an unauthorized user to take control of any account.

This is what the vulnerable code looks like:

```rb
require 'jwt'

payload = { foo: 'bar' }

# Unsecure token
token_without_signature = JWT.encode(payload, nil, 'none')

# BAD: it does not verify
decoded_token1 = JWT.decode(token_without_signature, nil, false, algorithm: 'HS256')

# BAD: it's using none
decoded_token3 = JWT.decode(token_without_signature, secret, true, algorithm: 'none')

# BAD: it's using none
decoded_token4 = JWT.decode(token_without_signature, secret, true, { algorithm: 'none' })
```

To properly verify it has to use a secret instead of `nil`, set the third argumento to `true` otherwise it won't verify the key and use a valid algorithm other than `none`.

### 3.1.1- Building the database

We can use as code the one we just used in the previous section, we can name it `code.rb`:

```rb
require 'jwt'

payload = { foo: 'bar' }

# Unsecure token
token_without_signature = JWT.encode(payload, nil, 'none')

# BAD: it does not verify
decoded_token1 = JWT.decode(token_without_signature, nil, false, algorithm: 'HS256')

# BAD: it's using none
decoded_token3 = JWT.decode(token_without_signature, secret, true, algorithm: 'none')

# BAD: it's using none
decoded_token4 = JWT.decode(token_without_signature, secret, true, { algorithm: 'none' })
```

Let's create a database:

```bash
codeql database create --language=ruby --source-root=Source RubyJwtMissingVerification
```

In this process we're creating a Ruby database from the `Source/` folder, the database will be saved as `RubyJwtMissingVerification`, the complete database building guide is available in the [GitHub Documentation](https://docs.github.com/en/code-security/codeql-cli/getting-started-with-the-codeql-cli/preparing-your-code-for-codeql-analysis) After successfully building the database, the next step is to add it into Visual Studio.

## 3.2- Query Structure 
already explained

## 3.3- Start Coding

### 3.3.1- Concepts

To achieve what we want we have to define a `JwtDecoding` module which has to have the following methods and predicates:

- `getPayload()`: Gets the argument containing the decoding payload (the token).

- `getAlgorithm()`: Gets the argument containing the decoding algorithm.

- `getKey()`: Gets the argument containing the decoding key.

- `getOptions()`: Gets the argument containing the decoding options.

- `verifiesSignature()`: Checks if the signature gets verified while decoding.

Once this is known, we begin to model the `JwtDecoding` module with an abstract class `Range`, which we will extend to model new APIs:

```ql
module JwtDecoding {
  abstract class Range extends DataFlow::Node {
  }
}
```

Following that we also have to include our abstract methods and predicates we have previously mentioned:

```ql
module JwtDecoding {
  abstract class Range extends DataFlow::Node {

    abstract DataFlow::Node getPayload();

    abstract DataFlow::Node getAlgorithm();

    abstract DataFlow::Node getKey();

    abstract DataFlow::Node getOptions();

    abstract predicate verifiesSignature();
  }
}
```

All methods will return a `DataFlow::Node` and we have also declared the abstract predicate `verifiesSignature()`. 

Subsequently we have to use this `Range` in our `JwtDecoding` class:

```ql
class JwtDecoding extends DataFlow::Node instanceof JwtDecoding::Range {
  DataFlow::Node getPayload() { result = super.getPayload() }

  DataFlow::Node getAlgorithm() { result = super.getAlgorithm() }

  DataFlow::Node getOptions() { result = super.getOptions() }

  predicate verifiesSignature() { super.verifiesSignature() }
}
```

This class inherits from `DataFlow::Node` and belongs to the type `JwtDecoding::Range`. Furthermore, we need to define the `getPayload()`, `getAlgorithm()` and `getOptions()` functions. These functions overrides the respective methods of the parent class, and returns the result of the parent class's function.

## 3.4- JWT Decoding

Firstly, let's define the `Jwt` module:

```ql
private import ruby
private import codeql.ruby.Concepts

module Jwt {
}
```

Just like `TemplateConstruction` we have to define a `JwtDecode` class. This class extends both `JwtDecoding::Range` (previously defined in `Concepts`) and `DataFlow::CallNode`. As it extends `DataFlow::CallNode`, it should return a call. Similarly, as it extends `JwtDecoding::Range`; `getPayload()`, `getAlgorithm()`, `getKey()`, `getOptions()` methods and `verifiesSignature()` predicate needs to be overridden.

```ql
private class JwtDecode extends JwtDecoding::Range, DataFlow::CallNode {
    JwtDecode() { }

    override DataFlow::Node getPayload() {  }

    override DataFlow::Node getAlgorithm() { }

    override DataFlow::Node getKey() {  }

    override DataFlow::Node getOptions() {  }

    override predicate verifiesSignature() { }
}
```

`JwtDecode` has to return a decode `CallNode`, in this case we're looking for `JWT.decode()`. We can use `API::getTopLevelMember()` which gets an access to the top-level constant (`JWT`). Subsequently, we retrieve the `decode()` call using `getAMethodCall("decode")` :

```ql
JwtDecode() { this = API::getTopLevelMember("JWT").getAMethodCall("decode") }
```

About how to define `getPayload()` is pretty simple, `JwtDecode` is a `CallNode` we just have to get the argument 0 (`JWT.decode( payload, nil, false, algorithm: 'HS256')`):

```ql
override DataFlow::Node getPayload() { result = this.getArgument(0) }
```

Now its `getAlgorithm()` turn, this is not as intuitive as what we have been doing since it can be representated in code in different ways. 

The first possibility is that the algorithm is specified in argument 3, indicated by the value of the key `algorithm`.

```rb
token = JWT.decode(token, secret, true, algorithm: 'none')
```

This is considered a `DataFlow::PairNode`, we can check the QLdoc of `PairNode`:

```ql
/**
 * A representation of a pair such as `K => V` or `K: V`.
 *
 * Unlike most expressions, pairs do not evaluate to actual objects at runtime and their nodes
 * cannot generally be expected to have meaningful data flow edges.
 * This node simply provides convenient access to the key and value as data flow nodes.
 */
class PairNode extends ExprNode {...}
```

This fits perfectly with what we are looking for, we can simply cast our node to `DataFlow::PairNode` and then invoke `getValue()` (if we don't do this last step we will be defining `algorithm: 'none'` instead):

```ql
override DataFlow::Node getAlgorithm() {
    result = this.getArgument(3).(DataFlow::PairNode).getValue()
}
```

The second possibility is the same as the first one but in the middle of brackets. It is very important to cover all cases since small differences like these would ahve caused our query not to detect the case. And although it may seem small, this difference can give us a lot of headaches.

```rb
token = JWT.decode(token, secret, true, { algorithm: 'none' })
```

This type of structure `{ algorithm: 'none' }` corresponds to `DataFlow::HashLiteralNode`:

```ql
/**
 * A data-flow node that corresponds to a hash literal. Hash literals are desugared
 * into calls to `Hash.[]`, so this includes both desugared calls as well as
 * explicit calls.
 */
class HashLiteralNode extends LocalSourceNode, ExprNode {...}
```

This class has a method that comes in handy to us: `getElementFromKey(key)` which receives a `ConstantValue` as argument, we can define a `Ast::ConstantValue` which string value is `algorithm`. To achieve this we can use `isStringlikeValue("string")` predicate:

```ql
result =
        this.getArgument(3)
            .(DataFlow::HashLiteralNode)
            .getElementFromKey(any(Ast::ConstantValue cv | cv.isStringlikeValue("algorithm")))
```

Using `any` here would be mostly the same as using `exists`, but using `any` in this case is less confusing to read. 

This is the last and simplest way to specify an algorithm: 

```rb
token = JWT.decode(token, secret, 'none')
```

This is very simple, we just have to select the argument 2:

```ql
result = this.getArgument(2)
```

Now we just need to join the 3 possibilities by chaining them with an `or`:

```ql
override DataFlow::Node getAlgorithm() {
    result = this.getArgument(3).(DataFlow::PairNode).getValue() or
    result =
    this.getArgument(3)
        .(DataFlow::HashLiteralNode)
        .getElementFromKey(any(Ast::ConstantValue cv | cv.isStringlikeValue("algorithm"))) or
    result = this.getArgument(2)
}
```

We continue with `getKey()`, as we can see in the following code:

```rb
token = JWT.decode(token, secret, 'none')
```

The key is located in the argument 1. We just have to specify it using `getArgument(1)`:

```ql
override DataFlow::Node getKey() { result = this.getArgument(1) }
```

As the last method we have `getOptions()`, which is located in the argument 3:

```rb
token = JWT.decode(token, secret, true, { algorithm: 'none' })
```

We just have to specify it using `getArgument(3)`:

```ql
override DataFlow::Node getOptions() { result = this.getArgument(3) }
```

Once we have finished all the methods we move on to the `verifiesSignature()` predicate. 

This case just like `getAlgorithm()` there can be several ways:

The first possibility is when the verify option is disabled:

```rb
token = JWT.decode(token, secret, false, { algorithm: 'none' })
```

This is specified in the argument 2 and the value can't be `false`. We can use `getConstantValue()` and then check if it's `false` using `isBoolean(false)`:

```ql
not this.getArgument(2).getConstantValue().isBoolean(false)
```

The second possibility is the case that the algorithm is `none`:

```rb
token = JWT.decode(token, secret, true, { algorithm: 'none' })
```

This is specified in the argument 2 and the value can't be `none`. We're using `getConstantValue()` again and then check if it's value is `none`:

```ql
not this.getAlgorithm().getConstantValue().isStringlikeValue("none")
```

And the last and simplest, when no algorithm is explicitly specified, the default behavior is to verify the token:

```rb
token = JWT.decode(token, secret)
```

In this case we just have to check that its number of arguments is less than 3:

```
this.getNumberOfArguments() < 3
```

And with this we would have already covered all the cases, this is our final predicate:

```ql
override predicate verifiesSignature() {
    not this.getArgument(2).getConstantValue().isBoolean(false) and
    not this.getAlgorithm().getConstantValue().isStringlikeValue("none")
    or
    this.getNumberOfArguments() < 3
}
```

We have finished our class and module! This is the final result:

```ql

private import ruby
private import codeql.ruby.ApiGraphs
private import codeql.ruby.dataflow.FlowSummary
private import codeql.ruby.Concepts

module Jwt {
  private class JwtDecode extends JwtDecoding::Range, DataFlow::CallNode {
    JwtDecode() { this = API::getTopLevelMember("JWT").getAMethodCall("decode") }

    override DataFlow::Node getPayload() { result = this.getArgument(0) }

    override DataFlow::Node getAlgorithm() {
      result = this.getArgument(3).(DataFlow::PairNode).getValue() or
      result =
        this.getArgument(3)
            .(DataFlow::HashLiteralNode)
            .getElementFromKey(any(Ast::ConstantValue cv | cv.isStringlikeValue("algorithm"))) or
      result = this.getArgument(2)
    }

    override DataFlow::Node getKey() { result = this.getArgument(1) }

    override DataFlow::Node getOptions() { result = this.getArgument(3) }

    override predicate verifiesSignature() {
      not this.getArgument(2).getConstantValue().isBoolean(false) and
      not this.getAlgorithm().getConstantValue().isStringlikeValue("none")
      or
      this.getNumberOfArguments() < 3
    }
  }
}
```

## 3.5-  Frameworks

Once we have modeled our library, it's important to include it in the `Frameworks.qll` file. This step is crucial, otherwise it won't be considered:

```ql
private import codeql.ruby.frameworks.Jwt
```

## 3.6- Customizations and Query

Since this query doesn't contain sources, sinks or any additional taint step these files are not needed.

## 3.7- MissingJWTVerification.ql

This file is kind of the summary of all we've done, we have to describe the vulnerability, the severity, id, precission... We also have to provide the CWE id, in this case 347.

We're interested in cases that `JwtDecoding` is performed and does not `verifiesSignature()`:

```ql
/**
 * @name JWT missing secret or public key verification
 * @description The application does not verify the JWT payload with a cryptographic secret or public key.
 * @kind problem
 * @problem.severity warning
 * @precision high
 * @id rb/jwt-missing-verification
 * @tags security
 *       external/cwe/cwe-347
 */

private import codeql.ruby.Concepts

from JwtDecoding jwtDecoding
where not jwtDecoding.verifiesSignature()
select jwtDecoding.getPayload(), "is not verified with a cryptographic secret or public key."
```

## 3.8- QHelp

QHelp is an XML file with information regarding the vulnerability, guidance on resolution, examples of both vulnerable and secure code snippets, and provides additional resources.

QHelp are usually quite similar within the same language, you can always base yourself on another to write yours. 

### 3.8.1- Overview

Firstly we have to include an overview of the vulnerability:

```xml
<overview>
<p>
Applications decoding a JSON Web Token (JWT) may be vulnerable when the key isn't verified.
</p>
</overview>
```

### 3.8.2- Recommendation

Additionally we have to add a recomendation field to explain how to properly fix the vulnerabilty:

```xml
<recommendation>
<p>
Calls to <code>verify()</code> functions should use a cryptographic secret or key to decode JWT payloads.</p>
</recommendation>
```

### 3.8.3- Examples

Within the same folder we have to create the `examples` folder and add there both safe and unsafe code snippets. Then we can refer to the code using `<sample>`. Unlike test files, the example file has to be simple and easy to understand.

- `MissingJWTVerificationBad.rb`

```rb
require 'jwt'

token = JWT.encode({ foo: 'bar' }, nil, 'none')

decoded1 = JWT.decode(token, nil, false, algorithm: 'HS256')

decoded2 = JWT.decode(token, "secret", true, algorithm: 'none')
```

- `MissingJWTVerificationGoods.rb`

```rb
require 'jwt'

token = JWT.encode({ foo: 'bar' }, nil, 'HS256')

decoded = JWT.decode(token, "secret", true, algorithm: 'HS256')
```

Additionally we have to explain briefly the reason of why the snippet is safe or not:

```xml
<example>
<p>
In the example below, false is used to disable the integrity enforcement of a JWT payload and none algorithm is used. This may allow a malicious actor to make changes to a JWT payload.
</p>

<sample src="examples/MissingJWTVerificationBad.rb" />

<p>
The following code fixes the problem by using a cryptographic secret or key to decode JWT payloads.
</p>

<sample src="examples/MissingJWTVerificationGood.rb" />
</example>
```

### 3.8.4- Additional resources

Additional resources are always welcome, such as portswigger blogs, wikipedia or blogs of interest.

```xml
<references>
<li>Auth0 Blog: <a href="https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/#Meet-the--None--Algorithm">Meet the "None" Algorithm</a>.</li>
</references>
```

Now let's put together everything we've written:

```xml
<!DOCTYPE qhelp PUBLIC
  "-//Semmle//qhelp//EN"
  "qhelp.dtd">
<qhelp>

<overview>
<p>
Applications decoding a JSON Web Token (JWT) may be vulnerable when the key isn't verified.
</p>
</overview>

<recommendation>
<p>
Calls to <code>verify()</code> functions should use a cryptographic secret or key to decode JWT payloads.</p>
</recommendation>

<example>
<p>
In the example below, false is used to disable the integrity enforcement of a JWT payload and none algorithm is used. This may allow a malicious actor to make changes to a JWT payload.
</p>

<sample src="examples/MissingJWTVerificationBad.rb" />

<p>
The following code fixes the problem by using a cryptographic secret or key to decode JWT payloads.
</p>

<sample src="examples/MissingJWTVerificationGood.rb" />
</example>

<references>
<li>Auth0 Blog: <a href="https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/#Meet-the--None--Algorithm">Meet the "None" Algorithm</a>.</li>
</references>
</qhelp>
```

## 3.9- Tests

Writing comprehensive test files is crucial for ensuring the reliability of the query and detecting any changes in behavior resulting from updates. These test files are located in `ruby/ql/test/query-tests/experimental/MissingJWTVerification/`.

It's essential to include comments indicating GOOD and BAD cases, along with explanations for each case. This ensures clarity and aids in understanding the expected behavior of the query.

We proceed to create two cookies, one signed using a key and another one without signing:

```rb
require 'jwt'

payload = { foo: 'bar' }

# Unsecure token
token_without_signature = JWT.encode(payload, nil, 'none')

# Secure token
token = JWT.encode(payload, "secret", 'HS256')
```

Subsequently we introduce all those cases that `JWT` doesn't verify the key:

```rb
# BAD: it does not verify
decoded_token1 = JWT.decode(token_without_signature, nil, false, algorithm: 'HS256')

# BAD: it's using none
decoded_token3 = JWT.decode(token_without_signature, secret, true, algorithm: 'none')

# BAD: it's using none
decoded_token4 = JWT.decode(token_without_signature, secret, true, { algorithm: 'none' })
``` 

And finally some scenarios where `JWT` verifies the key during decoding:

```rb
# GOOD: it does verify
decoded_token5 = JWT.decode(token, secret, 'HS256')

# GOOD: it does verify
decoded_token2 = JWT.decode(token,secret)
```

CodeQL verifies the proper functioning of the query by executing the query located in `ruby/ql/test/query-tests/experimental/cwe-347/MissingJWTVerification.qlref`:

```ql
experimental/cwe-347/MissingJWTVerification.ql
```

After running the query against the test cases, CodeQL generates a `.expected` file. This file contains the expected results for each test case. Subsequent runs of the test suite will compare the actual results with the expected results stored in these `.expected` files, ensuring consistency and allowing for automated validation of the query's correctness over time. This mechanism is crucial for maintaining the reliability of the query across different versions and updates.


## 3.10- Change note

We also need a change note to document the new query, this can be added by creating a new file `ruby/ql/src/change-notes/2023-09-16-jwt-security-query.md`:

```md
---
category: newQuery
---
* Added a new experimental query, `rb/jwt-missing-verification`, to detect when the application does not verify a JWT payload.
```

# 3.11- Preparing the database 

Once written `MissingJWTVerification` we can start our second query, `EmptyJWTSecret`.

The vulnerability occurs when server doesn't sign the JWT token. If a JWT token is utilized and the server doesn't sign the key also means that they're also not verifying that the token has not been manipulated, this could lead an attacker to get account takover.

This is what the vulnerable code looks like:

```rb
require 'jwt'

payload = { foo: 'bar' }

# BAD: the token is not signed
token1 = JWT.encode({ foo: 'bar' }, "secret", 'none')

# BAD: the secret used is empty
token2 = JWT.encode({ foo: 'bar' }, nil, 'HS256')

# BAD: the secret used is empty
token3 = JWT.encode({ foo: 'bar' }, "", 'HS256')
```

To properly encode it has to use a secret instead of `nil` or empty string and use a valid algorithm other than `none`.

## 3.11.1- Building the database

We can use as code the one we just used in the previous section, we can name it `code.rb`:

```rb
require 'jwt'

payload = { foo: 'bar' }

# BAD: the token is not signed
token1 = JWT.encode({ foo: 'bar' }, "secret", 'none')

# BAD: the secret used is empty
token2 = JWT.encode({ foo: 'bar' }, nil, 'HS256')

# BAD: the secret used is empty
token3 = JWT.encode({ foo: 'bar' }, "", 'HS256')
```

Let's create a database:

```bash
codeql database create --language=ruby --source-root=Source RubyEmptyJWTSecret
```

In this process we're creating a Ruby database from the `Source/` folder, the database will be saved as `RubyEmptyJWTSecret`, the complete database building guide is available in the [GitHub Documentation](https://docs.github.com/en/code-security/codeql-cli/getting-started-with-the-codeql-cli/preparing-your-code-for-codeql-analysis) After successfully building the database, the next step is to add it into Visual Studio.

# 3.12- Query Structure 
already explained

# 3.13- Start Coding

## 3.13.1- Concepts

Just like `JwtDecoding` now we have to define `JwtEncoding` module, which has to have the following methods and predicates:

- `getPayload`: Gets the argument containing the encoding payload.

- `getAlgorithm()`: Gets the argument containing the encoding algorithm.

- `getKey()`: Gets the argument containing the encoding key.

- `signsPayload()`: Checks if the payloads gets signed while encoding.

Once this is known, we begin to model the `JwtEncoding` module with an abstract class `Range`, which we will extend to model new APIs:

```ql
module JwtEncoding {
  abstract class Range extends DataFlow::Node {
  }
}
```

Following that we also have to include our abstract methods and predicates we have previously mentioned:

```ql
module JwtEncoding {
  abstract class Range extends DataFlow::Node {

    abstract DataFlow::Node getPayload();

    abstract DataFlow::Node getAlgorithm();

    abstract DataFlow::Node getKey();

    abstract predicate signsPayload();
  }
}
```

All methods will return a `DataFlow::Node` and we have also declared the abstract predicate `signsPayload()`. 

Subsequently we have to use this `Range` in our `JWtEncoding` class:

```ql
class JwtEncoding extends DataFlow::Node instanceof JwtEncoding::Range {
  DataFlow::Node getPayload() { result = super.getPayload() }

  DataFlow::Node getAlgorithm() { result = super.getAlgorithm() }

  DataFlow::Node getKey() { result = super.getKey() }

  predicate signsPayload() { super.signsPayload() }
}
```

This class inherits from `DataFlow::Node` and belongs to the type `JwtEncoding::Range`. Furthermore, we need to define the `getPayload()`, `getAlgorithm()` and `getKey()` functions. These functions overrides the respective methods of the parent class, and returns the result of the parent class's function.

## 3.14- JWT Encoding

We have already started modeling `Jwt` library but `JwtEncode` is missing which is pretty similar to `JwtDecode`:

```
private class JwtEncode extends JwtEncoding::Range, DataFlow::CallNode {
    JwtEncode() { }

    override DataFlow::Node getPayload() {  }

    override DataFlow::Node getAlgorithm() {  }

    override DataFlow::Node getKey() {  }

    override predicate signsPayload() {  }
}
```

`JwtEncode` has to return a decode `CallNode`, in this case we're looking for `JWT.encode()`. We can use `API::getTopLevelMember()` which gets an access to the top-level constant (`JWT`). Subsequently, we retrieve the `decode()` call using `getAMethodCall("encode")` :

```ql
JwtDecode() { this = API::getTopLevelMember("JWT").getAMethodCall("encode") }
```

About how to define `getPayload()` is pretty simple, `JwtEncode` is a `CallNode` we just have to get the argument 0:

```rb
token = JWT.encode({ foo: 'bar' }, "secret", 'HS256')
```

```ql
override DataFlow::Node getPayload() { result = this.getArgument(0) }
```

About `getAlgorithm()` it's always the argument 2:

```rb
token = JWT.encode({ foo: 'bar' }, "secret", 'algorithm')
```

```rb
override DataFlow::Node getAlgorithm() { result = this.getArgument(2) }
```

`getKey()` is in the argument 1:

```rb
token = JWT.encode({ foo: 'bar' }, "secret", 'algorithm')
```

```rb
override DataFlow::Node getKey() { result = this.getArgument(1) }
```

Finally we have to code `signsPayload()` predicate:

```ql
override predicate signsPayload() {
      not (
        this.getKey().getConstantValue().isStringlikeValue("") or
        this.getKey().(DataFlow::ExprNode).getConstantValue().isNil()
      )
    }
```

This ensures that the key is not empty or null. We have finished our class and module! This is the final result:

```ql
  /** A call to `JWT.encode`, considered as a JWT encoding. */
  private class JwtEncode extends JwtEncoding::Range, DataFlow::CallNode {
    JwtEncode() { this = API::getTopLevelMember("JWT").getAMethodCall("encode") }

    override DataFlow::Node getPayload() { result = this.getArgument(0) }

    override DataFlow::Node getAlgorithm() { result = this.getArgument(2) }

    override DataFlow::Node getKey() { result = this.getArgument(1) }

    override predicate signsPayload() {
      not (
        this.getKey().getConstantValue().isStringlikeValue("") or
        this.getKey().(DataFlow::ExprNode).getConstantValue().isNil()
      )
    }
  }
```

## 3.15- Frameworks
We already imported `Jwt`.

## 3.16- Customizations and Query
Since this query doesn't contain sources, sinks or any additional taint step these files are not needed.

## 3.17- EmptyJwtSecret.ql

We're interested in cases that `JwtEncoding` is performed and does not `signsPayload()`:


```ql
/**
 * @name JWT encoding using empty key or algorithm
 * @description The application uses an empty secret or algorithm while encoding a JWT Token.
 * @kind problem
 * @problem.severity warning
 * @precision high
 * @id rb/jwt-empty-secret-or-algorithm
 * @tags security
 */

private import codeql.ruby.Concepts

from JwtEncoding jwtEncoding
where not jwtEncoding.signsPayload()
select jwtEncoding.getPayload(), "This JWT encoding uses an empty key or none algorithm."
```

## 3.18- QHelp

QHelp is an XML file with information regarding the vulnerability, guidance on resolution, examples of both vulnerable and secure code snippets, and provides additional resources.

QHelp are usually quite similar within the same language, you can always base yourself on another to write yours.

### 3.18.1- Overview
Firstly we have to include an overview of the vulnerability:

```xml
<overview>
<p>
Applications encoding a JSON Web Token (JWT) may be vulnerable when it's not verified or algorithm is <code>none</code>.
</p>
</overview>
```

### Recommendation
Additionally we have to add a recomendation field to explain how to properly fix the vulnerabilty:

```xml
<recommendation>
<p>
Calls to <code>verify()</code> functions should use a cryptographic secret or key to decode JWT payloads.</p>
</recommendation>
```

### 3.18.2- Examples

- `EmptyJWTSecretBad.rb`

```rb
require 'jwt'

token1 = JWT.encode({ foo: 'bar' }, "secret", 'none')

token2 = JWT.encode({ foo: 'bar' }, nil, 'HS256')
```

- `EmptyJWTSecretGood.rb`

```rb
require 'jwt'

token = JWT.encode({ foo: 'bar' }, "secret", 'HS256')
```

Additionally we have to explain briefly the reason of why the snippet is safe or not:

```xml
<example>
<p>
In the example below, the secret used is an empty string and none algorithm is used. This may allow a malicious actor to make changes to a JWT payload.
</p>

<sample src="examples/EmptyJWTSecretBad.rb" />

<p>
The following code fixes the problem by using a non-empty cryptographic secret or key to encode JWT payloads.
</p>

<sample src="examples/EmptyJWTSecretGood.rb" />
</example>
```

### 3.18.3- Additional resources
Additional resources are always welcome, such as portswigger blogs, wikipedia or blogs of interest.

```xml
<references>
<li>Auth0 Blog: <a href="https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/#Meet-the--None--Algorithm">Meet the "None" Algorithm</a>.</li>
</references>
```

## 3.19- Tests

Writing comprehensive test files is crucial for ensuring the reliability of the query and detecting any changes in behavior resulting from updates. These test files are located in `ruby/ql/test/query-tests/experimental/EmptyJWTSecret/`.

It's essential to include comments indicating GOOD and BAD cases, along with explanations for each case. This ensures clarity and aids in understanding the expected behavior of the query.

We proceed to create four tokens, three improperly signed:

```rb
require 'jwt'

payload = { foo: 'bar' }

# BAD: the token is not signed
token1 = JWT.encode({ foo: 'bar' }, "secret", 'none')

# BAD: the secret used is empty
token2 = JWT.encode({ foo: 'bar' }, nil, 'HS256')

# BAD: the secret used is empty
token3 = JWT.encode({ foo: 'bar' }, "", 'HS256')

# GOOD: the token is signed
token4 = JWT.encode({ foo: 'bar' }, "secret", 'HS256')
```

CodeQL verifies the proper functioning of the query by executing the query located in `ruby/ql/test/query-tests/experimental/cwe-347/EmptyJWTSecret.qlref`:

```ql
experimental/cwe-347/MissingJWTVerification.ql
```

After running the query against the test cases, CodeQL generates a .expected file. This file contains the expected results for each test case. Subsequent runs of the test suite will compare the actual results with the expected results stored in these .expected files, ensuring consistency and allowing for automated validation of the query's correctness over time. This mechanism is crucial for maintaining the reliability of the query across different versions and updates.

## 3.20- Change note

We also need a change note to document the new query, this can be added by creating a new file `ruby/ql/src/change-notes/2023-09-16-jwt-security-query.md`:

```md
---
category: newQuery
---
* Added a new experimental query, `rb/jwt-empty-secret-or-algorithm`, to detect when application uses an empty secret or weak algorithm.
```

## 4- Bounty!

Writing queries not only allows us to discover interesting bugs but also enables us to earn bounties for query submissions. The queries discussed in this post were classified as medium difficulty and received a reward of $1800 each. Additionally, the SSTI query received a $500 bonus. Thanks [GitHub Security Lab](https://securitylab.github.com/) and all the PR reviewers for this! 

GitHub Security Lab's [Hackerone](https://hackerone.com/github-security-lab/).

# And that's the guide to my queries! If you have any questions or suggestions, do not hesitate to reach out to me on Twitter!

# THANKS FOR READING 😊 !!