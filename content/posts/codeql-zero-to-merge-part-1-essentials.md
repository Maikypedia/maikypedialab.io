---
title: "CodeQL Zero to Merge: Part 1 - CodeQL Essentials"
date: 2023-12-23T14:37:37+01:00
categories:
    - CodeQL
tags:
    - CodeQL
---

# [*] INDEX:

1. [Abstract Syntax Tree (AST)](#1--abstract-syntax-tree-ast)
    - [Lexer](#11--lexer)
    - [Parser](#12--parser) 
2. [Control Flow Graph (CFG)](#2--control-flow-graph-cfg)
3. [Call Graph](#3--call-graph)
4. [Static Single-Assignment (SSA)](#4--static-single-assignment)
5. [Data Tracing](#5--data-tracing)
    - [DataFlow Analysis](#51--dataflow-analysis)
    - [Taint Tracking](#52--taint-tracking) 
6. [CodeQL Basics](#6--codeql-basics)
    - [Formula Syntax](#61--formula-syntax) 
        - [Query Format](#query-format) 
        - [Any / Exists](#any--exists) 
        - [Abstract Classes](#abstract-classes)
    - [Common Classes](#62--common-classes) 
        - [Node](#node)
        - [Expression](#expression)
        - [API](#api)
    - [Query Parts](#63--query-parts) 
        - [Sources and Sinks](#sources-and-sinks)
        - [Sanitizers](#sanitizers)
        - [Additional Taint Steps](#additional-taint-steps)
        - [Flow Labels](#flow-labels)
    - [Extra Features](#64--extra-features) 
        - [View AST](#view-ast)
        - [Suites](#suites)

# CodeQL Zero to Merge: Part 1 - CodeQL Essentials

# Introduction

CodeQL is the code analysis engine developed by Github to automate security code analysis, displaying the results as code scanning alerts.

In this post we will be covering some introductory concepts to understand CodeQL queries. By the end of this reading, you should have sufficient knowledge to start understanding and writing queries. Later, I’ll publish more detailed blogs on writing specific queries, but understanding the concepts in this blog is crucial before diving into those. So this will be like your first query kit.

![<- Not working](/static/CodeQL/first-query-kit.jpg)

# 1- Abstract Syntax Tree (AST) 

The AST is a data structure used to represent the syntactic structure of source code in a programming language. As the name suggests, it is a tree representation where each node in the tree represents a syntactic construct in the source code. ASTs are commonly used in compilers, interpreters, and tools that analyze code.

## 1.1- Lexer

The lexer is a preliminary step in the generation of the AST process, occurring before parsing. It is responsible for tokenizing the input code by breaking down the source code into a series of tokens. Tokens represent small units of meaning in programming languages, they might be keywords, identifiers, operators... It performs lexical analysis by recognizing and categorizing these tokens based on the language's grammar rules. 

Lexer's role is to tokenize the source code and provide a collection of tokens to the parser. Each token carries information about its type and sometimes its associated value.

In Ruby, the mostly used parser is `Ripper`, let's take a look at how `Ripper` tokenizes the code. Firstly we have to import `ripper` and lex the code using `Ripper.lex`:

```rb
require 'ripper'

# Ruby source code to be parsed
ruby_code = <<~RUBY
  def sayHello(name)
    puts "Hello, \#{name}!"
  end

  sayHello("World")
RUBY

res = Ripper.lex(ruby_code)

puts res.inspect
```

```md
[[[1, 0], :on_kw, "def", #<Ripper::Lexer::State: EXPR_FNAME>], 
[[1, 3], :on_sp, " ", #<Ripper::Lexer::State: EXPR_FNAME>], 
[[1, 4], :on_ident, "sayHello", #<Ripper::Lexer::State: EXPR_ENDFN>], 
[[1, 12], :on_lparen, "(", #<Ripper::Lexer::State: EXPR_BEG|EXPR_LABEL>], 
[[1, 13], :on_ident, "name", #<Ripper::Lexer::State: EXPR_ARG>], 
[[1, 17], :on_rparen, ")", #<Ripper::Lexer::State: EXPR_ENDFN>], 
[[1, 18], :on_ignored_nl, "\n", #<Ripper::Lexer::State: EXPR_BEG>], 
[[2, 0], :on_sp, "  ", #<Ripper::Lexer::State: EXPR_BEG>], 
[[2, 2], :on_ident, "puts", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 6], :on_sp, " ", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 7], :on_tstring_beg, "\"", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 8], :on_tstring_content, "Hello, ", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 15], :on_embexpr_beg, "\#{", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 17], :on_ident, "name", #<Ripper::Lexer::State: EXPR_END|EXPR_LABEL>], 
[[2, 21], :on_embexpr_end, "}", #<Ripper::Lexer::State: EXPR_END|EXPR_LABEL>], 
[[2, 22], :on_tstring_content, "!", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[2, 23], :on_tstring_end, "\"", #<Ripper::Lexer::State: EXPR_END>], 
[[2, 24], :on_nl, "\n", #<Ripper::Lexer::State: EXPR_BEG>], 
[[3, 0], :on_kw, "end", #<Ripper::Lexer::State: EXPR_END>], 
[[3, 3], :on_nl, "\n", #<Ripper::Lexer::State: EXPR_BEG>], 
[[4, 0], :on_ignored_nl, "\n", #<Ripper::Lexer::State: EXPR_BEG>], 
[[5, 0], :on_ident, "sayHello", #<Ripper::Lexer::State: EXPR_CMDARG>], 
[[5, 8], :on_lparen, "(", #<Ripper::Lexer::State: EXPR_BEG|EXPR_LABEL>], 
[[5, 9], :on_tstring_beg, "\"", #<Ripper::Lexer::State: EXPR_BEG|EXPR_LABEL>], 
[[5, 10], :on_tstring_content, "World", #<Ripper::Lexer::State: EXPR_BEG|EXPR_LABEL>], 
[[5, 15], :on_tstring_end, "\"", #<Ripper::Lexer::State: EXPR_END>], 
[[5, 16], :on_rparen, ")", #<Ripper::Lexer::State: EXPR_ENDFN>], 
[[5, 17], :on_nl, "\n", #<Ripper::Lexer::State: EXPR_BEG>]
```

As we can see in the output, the tokens contain information about the names of the functions, the line and position. For example `EXPR_BEG` represents the beginning of an expression, `EXPR_FNAME` is a token type that represents a function name... Once done the tokenizing let's jump to the parsing.


## 1.2- Parser

`Ripper` also provides a parser that parses all the tokens, in this case `Ripper` uses S-expressions instead of AST (but both are pretty similar).

```rb
require 'ripper'

# Ruby source code to be parsed
ruby_code = <<~RUBY
  def sayHello(name)
    puts "Hello, \#{name}!"
  end

  sayHello("World")
RUBY

parsed_result = Ripper.sexp(ruby_code)

parsed_result.each do |token|
  puts token.inspect
end
```

```md
:program
[[:def, [:@ident, "sayHello", [1, 4]], [:paren, [:params, [[:@ident, "name", [1, 13]]], nil, nil, nil, nil, nil, nil]], [:bodystmt, [[:command, [:@ident, "puts", [2, 2]], [:args_add_block, [[:string_literal, [:string_content, [:@tstring_content, "Hello, ", [2, 8]], [:string_embexpr, [[:var_ref, [:@ident, "name", [2, 17]]]]], [:@tstring_content, "!", [2, 22]]]]], false]]], nil, nil, nil]], [:method_add_arg, [:fcall, [:@ident, "sayHello", [5, 0]]], [:arg_paren, [:args_add_block, [[:string_literal, [:string_content, [:@tstring_content, "World", [5, 10]]]]], false]]]]
```

Differences between S-expression and AST: 
S-expression (symbolic expression) is a notation used in programming languages to represent a tree data structure by utilizing nested lists, where each sublist is a subtree. It was used for the first time in the programming language Lisp and has since been used by other languages and data formats.

They are used not only to represent Lisp code but also in data serialization like JSON, XML… Its syntax simplicity and regularity make it easy to parse.

AST represents the structure of code in compilers while interpreters and S-expressions represent data structures in Lisp-like languages. Although both ASTs and S-expressions are concise, S-expressions are more human-readable. S-expressions are designed with human understanding in mind, while ASTs cater to computer comprehension.

We can generate an AST in Ruby using `parser/current`:


```rb
require 'parser/current'

file_path = 'code.rb'
ruby_code = File.read(file_path)

ast = Parser::CurrentRuby.parse(ruby_code)

puts ast.inspect
```

The output of the script is:

```rb
s(:begin,
  s(:def, :sayHello,
    s(:args,
      s(:arg, :name)),
    s(:send, nil, :puts,
      s(:dstr,
        s(:str, "Hello, "),
        s(:begin,
          s(:lvar, :name)),
        s(:str, "!")))),
  s(:send, nil, :sayHello,
    s(:str, "World")))
```

The given AST describes a Ruby program featuring a method named `sayHello`. This method accepts a parameter named `name` and utilizes the `puts` method to output a string constructed with string interpolation. The program concludes by invoking the `sayHello` method with the argument "World." The AST serves as a structured representation of the code, capturing both its hierarchical structure and functional relationships.

# 2- Control Flow Graph (CFG)

Control Flow Graph is a static graphical representation of the flow of control within a program. It shows all possible paths that the execution of the program can take by representing the relationships between different parts of the code. The flow refers to the progression of a program during its execution, it captures the evolution of the program as it navigates through different sections of code during runtime. It is worth pointing out that CFG may represent executions that can't happen.

A CFG is composed of several key components that together provide a graphical representation of the flow control within a program. The CFG include:

- `Nodes`: A basic block of code. A basic block is one instruction although there may be more than one, with a single entry point and a single exit point. 

- `Basic block`:Sequences of code without any branches, except possibly at the beginning and end. Each node in a CFG typically corresponds to a basic block.

- `Edges`: Edges between nodes represent the flow of control between basic blocks. An edge from one node to another indicates that the execution can transfer from the source basic block to the destination basic block. Edges capture the sequential order of execution. 

- `Entry and Exit nodes`: Usually the CFG has a single entry node, representing the starting point of the program. Similarly, there might be one or more exit nodes representing possible termination points. The entry node is where the program begins, and the exit nodes are where it ends or branches to another part of the code. 

- `Branches`: Programs are rarely linear; they often involve conditionals and loops which result vary from one execution to another, leading to the emergence of branches at these specific points. Conditional branches in the code, such as if statements and loops, lead to multiple outgoing edges from a node. Each outgoing edge represents a different path that the program can take based on the conditions.

- `Loop Structures`: In CFG, loops are cycles. Since CFG is a static graph, it may represent impossible execution cases (e.g. `if (1 == 0) {}`), and that's the reason why all the loops has an entry edge from an external node to the loop conditional node and an cycle exit edge (from the loop conditional to an external node).


![<- Not working](/static/CodeQL/basics-8.png)

Let's create a CFG from the following ruby code:

```rb
x = 5
y = 10
if x > y
    x -= 1
    y += 1
else
    x += 1
    y -= 1
end
puts "Result : " + x
```

`Node1`: Represents the entry node 

`Node2`: Represents the assignment of `x = 5` and `y = 10`. This node has a single outgoing edge to `Node2`.

`Node3`: Represents the beginning of the `if` statement `(if x > y)`. It has two outgoing edges: 
  - Edge to Node4, which represents the "then" branch when the condition is true. 
  - Edge to Node5, which represents the "else" branch when the condition is false.

`Node4`: Represents the block executed when `x > y` is true. It contains the operations `x -= 1` and `y += 1`. This node has a single outgoing edge to `Node6`. 

`Node5`: Represents the block executed when `x > y` is false. It contains the operations `x += 1` and `y -= 1`. This node has a single outgoing edge to `Node6`. 

`Node6`: Represents the continuation after the `if` statement. It has a single outgoing edge to the End node.

![<- Not working](/static/CodeQL/basics-1.png)

The next section may not be essential for the remainder of the blog post, but I found it to be incredibly insightful in understanding the working of decompilers. I came across a valuable [blog by Nicolo]((https://nicolo.dev/en/blog/role-control-flow-graph-static-analysis/)) that shed light on the topic. Now, let's attempt to manually create a Control Flow Graph (CFG) from an assembly code snippet and represent it graphically:


```assembly
fun1:
    mov     eax, 1           
    mov     ebx, 2          
    cmp     eax, 0           
    jle     .L1
    add     eax, 1           
    add     ebx, 1          
    jmp     .L2
.L1:
    sub     eax, 1          
    mov     ecx, eax        
.L2:
    mov     eax, 0          
    ret
```

The steps described by Nicolo is first finding the leading instruction in the code which can be categorized in 3 groups:

- 1. First Instruction
- 2. Instruction pointed from a jump instruction
- 3. Instruction after a non-jump

We can deconstruct the given code into elementary blocks, each block contains one flow-changing instruction.

Elementary Block 1| 
-------|
```s
mov eax, 1
mov ebx, 2
cmp eax, 0
jle .L1
```

Elementary Block 2| 
-------|
```s
add     eax, 1           
add     ebx, 1          
jmp     .L2
```

Elementary Block 3| 
-------|
```s
sub     eax, 1          
mov     ecx, eax     
```

Elementary Block 4| 
-------|
```s
mov     eax, 0          
ret
```

As we can see Elementary Block 1 may jump to Block 3 or continue the execution to Block 2. Block 2 always jumps to the Block 4 and, after finishing the last instruction, Block 3 continues to the Block 4 too.

![<- Not working](/static/CodeQL/basics-2.png)

The Elementary Block 1 is a conditional one since it uses `jle` (jump if less or equal to). We can simplify this code by turning `add eax, 1` to `eax += 1` and `mov eax, 1` to `eax = 1`. We can also simplify the conditional.

Elementary Block 1| 
-------|
```s
eax = 1
ebx = 2
if eax <= 0
    jmp Block 3
end
```

Elementary Block 2| 
-------|
```s
eax += 1
ebx += 1
jmp .L2
```

Elementary Block 3| 
-------|
```s
eax -= 1
ecx = eax
```

Elementary Block 4| 
-------|
```s
eax = 0
ret
```

Once having this is pretty simple to just rebuild the code:

```rb
eax = 1
ebx = 2

if eax > 0
    eax += 1
    ebx += 1
else
    eax -= 1
    ecx = eax
end

eax = 0
```

![<- Not working](/static/CodeQL/basics-3.png)

# 3- Call Graph

A Call Graph represents the relationships between procedures or functions. In this representation, every node symbolizes a procedure, and each directed edge (f, g) signifies that procedure f invokes procedure g. Consequently, the presence of a cycle within the graph indicates recursive calls between procedures. 

Some differences between a Call Graph and Control Flow Graph:


Feature | Control Flow Graph (CFG) | Call Graph
--------|--------------------------|------------
Purpose	|Flow of control within a single procedure or function|	Calling relationships between different procedures or functions
Nodes	|Statements in the procedure or function|	Procedures or functions in the program
Edges	|Flow of control between statements|	Calling relationships between procedures or functions
Labels	|Type of statement (assignment, if, loop, etc.)|	Name of procedure or function
Applications|	Analyzing the control flow of a program	|Identifying potential problems, understanding data flow

Given the following code:

```rb
def main
  puts "Main function"
  func_a
  func_b
end

def func_a
  puts "Function A"
  func_b
end

def func_b
  puts "Function B"
  func_c
end

def func_c
  puts "Function C"
end

main
```

As we can see in the code `func_a` is executed once called by `main`. `func_b` is called twice, by `main` and `func_a`. Then `func_c` is called once by `func_b`.

![<- Not working](/static/CodeQL/basics-4.png)

# 4- Static Single-Assignment

`SSA` (Static single-assignment) is a form of intermediate representation used in compilers. In SSA, each variable in the program is assigned only once, and every variable is defined before it is used. 

Even though at a high level it seems that the variable is being overwritten, a new variable is actually being created:

```rb
x = 1
x +=1
```

This code in `SSA` format would be represented as:

```rb
x1 = 1
x2 = x1 + 1
```

The idea is to maintain a one-to-one correspondence between variable definitions and their uses allowing simplification and optimization.

# 5- Data Tracing

## 5.1- DataFlow Analysis

DataFlow Analysis leverages all the techniques we have previously introduced. The primary goal of data flow analysis is to gather information about the possible values that variables can take at different points in the program and how these values propagate through the program. 

Data flow analysis is a valuable technique for assessing whether a source variable reaches a destination without significant modification. But this has a notable drawback, since the data has to flow without being modified it may overlook a lot of positive results.

![<- Not working](/static/CodeQL/basics-5.png)

As we can see in the image, `a` is used to calculate at `B4` the value of `u` and later the value `a` value is modified using `u`, so we can say that `a` is relevant to `B5`.

## 5.2- Taint Tracking

Taint Tracking has the same purpose as DataFlow Analysis, but as the name suggests, it doesn't look for variables that flow without modification but rather looks for tainted variables. Tainted variables are variables that have been marked as unreliable (e.g. if a variable is marked as tainted, any other variables derived from or influenced by the tainted variable may also be considered tainted). If a variable `A` is marked as tainted, and you use `A` to construct another variable `B`, then `B` would also be considered tainted.

Tainted variables may be a user-controlled variable inserted into a template, and so the template would be tainted. Taint tracking is specifically designed to follow the flow of sensitive data (taint) through a program. It offers more precision in identifying and tracing the propagation of data that is considered insecure or tainted. 

This is great for security analysis as it helps spot issues such as data leaks and injection attacks, which are potential security threats. On the other hand, if you're looking for a more general understanding of a program or want to optimize it, traditional data flow analysis might be a better fit. 

# 6- CodeQL Basics

## 6.1- Formula Syntax

CodeQL is a Declarative Object Oriented Programming language. This means that it has classes and methods like Java, but the approach is more similar to SQL.

### Query Format

Query syntax is similar to SQL:

```sql
from Type t where predicate(t) select t
```

Keywords are `from`, `where` and `select`. Using `from` we can define the variable that we are going to use, with `where` we can define some predicate containing the variable and using `select` we define the results.

### Any / Exists

Both `any` and `exists`  are formulas that can be used for the same purposes but depending on the context it is more appropriate to use one or the other. 

`exists` quantifier has the following syntax:

```ql
exists(<variable declarations> | <formula>)
```

Those variables will only be accessible within the `exists`, consider following code snippet:

```rb
name = params["name"]
html_text = "
  <!DOCTYPE html><html><body>
  <h2>Hello %s </h2></body></html>
  " % name
template = ERB.new(html_text) # Template construction

rendered = template.result(binding) # Template rendering
```

We have `TemplateConstruction` and we want to model `TemplateRendering`. As we can see `ERB.new` is a `TemplateConstruction` call and `.result` is a call from a `TemplateConstruction` node. To sum up, `TemplateRendering` is a `TemplateConstruction` invoking `.result` method:

```ql
exists(TemplateConstruction templateConstruction |
        this = templateConstruction.getAMethodCall("result")
      )
```

As I said before, this can be also be done using `any`:

```ql
any(TemplateNewCall templateConstruction).getAMethodCall("result")
```

### Abstract Classes

Since modularity is important for CodeQL, we have to learn how to write and use abstract classes. This is used in CodeQL files like `Concepts.qll`, where classes like `SqlConstruction` or `SqlExecution` are located. We can define a class `TemplateConstruction` which is a `DataFlow::Node`:

```ql
abstract class TemplateConstruction extends DataFlow::Node {
}
```

`TemplateConstruction` can actually include various cases (libraries like `ERB` or `Slim`).

```ql
class ErbTemplateConstruction extends TemplateConstruction {
  this = // code
}

class SlimTemplateConstruction extends TemplateConstruction {
  this = // code
}
```

We can reference both calling `TemplateConstruction`:

```ql
from TemplateConstruction t
select t
```

## 6.2- Common Classes

### Node

A node or `DataFlow::Node` as the name suggests is an element of the dataflow graph. 

We can simply select a node with the following query:

```ql
from DataFlow::Node n select n
```

![<- Not working](/static/CodeQL/basics-6.png)

### Expression

An `Expr` is compatible with `DataFlow::Node` since this has `asExpr` and `getExpr` methods. An expression is a combination of constants, variables, operators, and function calls that can be evaluated to produce a value. Examples of expressions include `2 + 3`, `x * 5`, or `sqrt(y)`. 

Some of the most commonly used Expressions are `Literals`, `Any` or `Variable References`. We can select an expression with the following query:

```ql
from Expr e select e
```

![<- Not working](/static/CodeQL/basics-7.png)

### API

The `API` graph is used for tracking the values of specific types, considering inheritance. This will be very useful since it allows us to select members and methods.

```rb
# API::getTopLevelMember("Foo").getMethod("bar").getArgument(0).asSink()
Foo.bar(x)
   *
# API::getTopLevelMember("Foo").getMethod("bar").getKeywordArgument("foo").asSink()
Foo.bar(foo: x)
   *
# API::getTopLevelMember("Foo").getInstance().getMethod("bar").getArgument(0).asSink()
Foo.new.bar(x)
   *
Foo.bar do |x| # API::getTopLevelMember("Foo").getMethod("bar").getBlock().getParameter(0).asSource()
end
```

## 6.3- Query Parts

### Sources and Sinks

Both source and sink are concepts commonly used in the context of security and data flow analysis. `Source` is the node where data is generated or enters the system. This could be user input or data received from external websites. `Sink` is the final sensitive node, this is where data is potentially sensitive and improper handling of data at these points can lead to security vulnerabilities.

![<- Not working](/static/CodeQL/basics-11.png)

An example of source could be a GET parameter:

```rb
name = params["name"]
```

But not always a source has to be a user-controlled value, SAST tools like CodeQL also check for misconfigurations where the improper values are not user controlled. For example:

```js
const origin = true;
const server_1 = new ApolloServer({
       cors: { origin: origin }
   });
```

In this example the source is `true` and it's not user controlled. 

The sink is often associated with operations that involve permissions, database queries, file write or code execution. When we talk about a vulnerable sink we don't mean a method which is vulnerable, we mean a method that in case of incorrect use is vulnerable. Sink examples:


```rb
# py Unsafe Deserialization
pickle.loads(source) # sink: first argument


# rb X-Path Injection
REXML::XPath.first(doc, "//#{source}") # sink: second argument


# rb SQL Injection
qry1 = "SELECT * FROM users WHERE username = '#{source}';"
conn.exec(qry1) # sink: first argument
```

We use this since using taint tracking we can see if the user's input reaches said sensitive methods.

### Sanitizers

A `Sanitizer` is a node where we filter out the data containing possible malicious input, so in the case our `source` flows to a `sanitizer` before reaching `sink`, it wouldn’t be a positive case.

![<- Not working](/static/CodeQL/basics-12.png)

A case of `sanitizer` could be  `Regexp.quote` or `Regexp.escape` to sanitize possible regular expression injection this sanitizer is already modeled in CodeQL:

```ql
  /**
   * A call to `Regexp.escape` (or its alias, `Regexp.quote`), considered as a
   * sanitizer.
   */
  class RegexpEscapeSanitization extends Sanitizer {
    RegexpEscapeSanitization() {
      this = API::getTopLevelMember("Regexp").getAMethodCall(["escape", "quote"])
    }
  }
```

Additionally, casting to an integer is also a commonly used sanitization technique.

### Additional Taint Steps

Sometimes we will have to write an `AdditionalTaintStep` since CodeQL assumes that the flow may not continue, but in cases like the following it does:

![<- Not working](/static/CodeQL/basics-13.png)

```rb
ldap.search(base: "", filter: "", attributes: [name])
```

Here, `name` is actually flowing to `ldap.search` but since there is a call to brackets `[]`, this case is not detected. The additional step we want to include here is that `[name]` is the same as `name`:

```ql
private predicate attributeArrayTaintStep(DataFlow::Node n1, DataFlow::Node n2) {
  exists(DataFlow::CallNode filterCall |
    filterCall.getMethodName() = "[]" and
    n1 = filterCall.getArgument(_) and
    n2 = filterCall
  )
}
```

This predicate `attributeArrayTaintStep` is to express that in the case that the method invoked is `[]`, the flow of the elements inside it will continue as if the element is the array.

### Flow Labels

Sometimes we need more information about the flow, if the taint is complete (if it’s not it means that we cannot control the entire sink), or what type of source/sink we're interested in. Let's introduce an example, check the following code snippet about overly permissive CORS:

```js
import { ApolloServer } from 'apollo-server';

// BAD: CORS too permissive
const server_1 = new ApolloServer({
    cors: { origin: true }
});


const cors = require('cors');
var express = require('express');

// BAD: CORS too permissive 
var app3 = express();
var corsOption3 = {
    origin: '*'
};
app3.use(cors(corsOption3));
```

We have two examples of this misconfiguration that may lead to CSRF, but take a look of the differences of both packages, `Apollo` takes `true` as value for origin and `cors` takes `*` as value for origin. We cannot just declare consider `true` and `*` as source for both, since `true` is only valid for `Apollo` and `*` for `cors`. 

Within our customizations file we must create two abstract classes extending `DataFlow::FlowLabel`, one for each value:


```ql
/** A flow label representing `true` and `null` values. */
abstract class TrueAndNull extends DataFlow::FlowLabel {
  TrueAndNull() { this = "TrueAndNull" }
}

TrueAndNull truenullLabel() { any() }

/** A flow label representing `*` value. */
abstract class Wildcard extends DataFlow::FlowLabel {
  Wildcard() { this = "Wildcard" }
}

Wildcard wildcardLabel() { any() }
```

The function of `flowlabel` is to have some extra information. We have to include both labels to our query file:

```ql
override predicate isSource(DataFlow::Node source, DataFlow::FlowLabel label) {
  source instanceof TrueNullValue and label = truenullLabel()
  or
  source instanceof WildcardValue and label = wildcardLabel()
}
```

If the source type is `TrueNullValue` the label will be `truenullLabel()` and if the source is a wildcard the label will be `wildcardLabel()`. Then we just have to define in our `isSink` predicate that if the sink is `Apollo` the label must be `truenullLabel()` and `wildcardLabel()` for `Express`:

```ql
override predicate isSink(DataFlow::Node sink, DataFlow::FlowLabel label) {
  sink instanceof CorsApolloServer and label = truenullLabel()
  or
  sink instanceof ExpressCors and label = wildcardLabel()
}
```

## 6.4- Extra Features

### View AST

Many times during our query development we will need to find certain structures that will not be intuitive and the possibility that CodeQL provides us an interesting feature called `View AST` breaking it down and suggesting what methods we can use for each block/expression.

![<- Not working](/static/CodeQL/basics-9.png)

![<- Not working](/static/CodeQL/basics-10.png)

We can use AST to know what method we should use to detect a specific structure, `getComponent()`, `getArgument()`, `getLeftOperand()`...

### Suites

A CodeQL suite is a collection of CodeQL queries organized in a single configuration `.qls` yaml file. This configuration file provides instructions on how to select and execute multiple CodeQL queries. 

The suite allows users to group queries based on filename, location, or metadata properties, simplifying the process of running multiple queries at once. 

This is an example of suite `go-security-and-quality.qls`:

```ql
- description: Security-and-quality queries for Go
- queries: .
- apply: security-and-quality-selectors.yml
  from: codeql/suite-helpers
```

Within `security-and-quality-selectors.yml` we can specify the kind of queries we want to run (only high precision, path-problem related):

```yml
- description: Selectors for selecting the security-and-quality queries for a language
- include:
    kind:
    - problem
    - path-problem
    precision:
    - high
    - very-high
- include:
    kind:
    - problem
    - path-problem
    precision: medium
    problem.severity:ex
      - error
      - warning
- include:
    kind:
    - diagnostic
- include:
    kind:
    - metric
    tags contain:
    - summary
```

We can also exclude some queries we're not interested in (e.g. experimental queries):

```yml
- exclude:
    deprecated: //
- exclude:
    query path: /^experimental\/.*/
```

# THANKS FOR READING 😊 !!