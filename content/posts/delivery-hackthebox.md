---
title: "Delivery Hackthebox"
date: 2021-04-07T01:46:24+02:00
draft: false
image: "/static/HACKING/delivery-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/delivery-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : ippsec
IP : 10.10.10.222
```

# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.222](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.222)

```
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
8065/tcp open  unknown
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p22,80,8065 10.10.10.222](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C53%2C80+10.10.10.222)

```
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 9c:40:fa:85:9b:01:ac:ac:0e:bc:0c:19:51:8a:ee:27 (RSA)
|   256 5a:0c:c0:3b:9b:76:55:2e:6e:c4:f4:b9:5d:76:17:09 (ECDSA)
|_  256 b7:9d:f7:48:9d:a2:f2:76:30:fd:42:d3:35:3a:80:8c (ED25519)
80/tcp   open  http    nginx 1.14.2
|_http-server-header: nginx/1.14.2
|_http-title: Welcome
8065/tcp open  unknown
| fingerprint-strings: 
|   GenericLines, Help, RTSPRequest, SSLSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Accept-Ranges: bytes
|     Cache-Control: no-cache, max-age=31556926, public
|     Content-Length: 3108
|     Content-Security-Policy: frame-ancestors 'self'; script-src 'self' cdn.rudderlabs.com
|     Content-Type: text/html; charset=utf-8
|     Last-Modified: Tue, 06 Apr 2021 23:50:10 GMT
|     X-Frame-Options: SAMEORIGIN
|     X-Request-Id: yq43t4rzuigkxftg5akd9wns5o
|     X-Version-Id: 5.30.0.5.30.1.57fb31b889bf81d99d8af8176d4bbaaa.false
|     Date: Wed, 07 Apr 2021 00:05:08 GMT
|     <!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"><meta name="robots" content="noindex, nofollow"><meta name="referrer" content="no-referrer"><title>Mattermost</title><meta name="mobile-web-app-capable" content="yes"><meta name="application-name" content="Mattermost"><meta name="format-detection" content="telephone=no"><link re
|   HTTPOptions: 
|     HTTP/1.0 405 Method Not Allowed
|     Date: Wed, 07 Apr 2021 00:05:09 GMT
|_    Content-Length: 0
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port8065-TCP:V=7.91%I=7%D=4/6%Time=606CF72C%P=x86_64-pc-linux-gnu%r(Gen
SF:ericLines,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20te
SF:xt/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x2
SF:0Request")%r(GetRequest,DF3,"HTTP/1\.0\x20200\x20OK\r\nAccept-Ranges:\x
SF:20bytes\r\nCache-Control:\x20no-cache,\x20max-age=31556926,\x20public\r
SF:\nContent-Length:\x203108\r\nContent-Security-Policy:\x20frame-ancestor
SF:s\x20'self';\x20script-src\x20'self'\x20cdn\.rudderlabs\.com\r\nContent
SF:-Type:\x20text/html;\x20charset=utf-8\r\nLast-Modified:\x20Tue,\x2006\x
SF:20Apr\x202021\x2023:50:10\x20GMT\r\nX-Frame-Options:\x20SAMEORIGIN\r\nX
SF:-Request-Id:\x20yq43t4rzuigkxftg5akd9wns5o\r\nX-Version-Id:\x205\.30\.0
SF:\.5\.30\.1\.57fb31b889bf81d99d8af8176d4bbaaa\.false\r\nDate:\x20Wed,\x2
SF:007\x20Apr\x202021\x2000:05:08\x20GMT\r\n\r\n<!doctype\x20html><html\x2
SF:0lang=\"en\"><head><meta\x20charset=\"utf-8\"><meta\x20name=\"viewport\
SF:"\x20content=\"width=device-width,initial-scale=1,maximum-scale=1,user-
SF:scalable=0\"><meta\x20name=\"robots\"\x20content=\"noindex,\x20nofollow
SF:\"><meta\x20name=\"referrer\"\x20content=\"no-referrer\"><title>Matterm
SF:ost</title><meta\x20name=\"mobile-web-app-capable\"\x20content=\"yes\">
SF:<meta\x20name=\"application-name\"\x20content=\"Mattermost\"><meta\x20n
SF:ame=\"format-detection\"\x20content=\"telephone=no\"><link\x20re")%r(HT
SF:TPOptions,5B,"HTTP/1\.0\x20405\x20Method\x20Not\x20Allowed\r\nDate:\x20
SF:Wed,\x2007\x20Apr\x202021\x2000:05:09\x20GMT\r\nContent-Length:\x200\r\
SF:n\r\n")%r(RTSPRequest,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent
SF:-Type:\x20text/plain;\x20charset=utf-8\r\nConnection:\x20close\r\n\r\n4
SF:00\x20Bad\x20Request")%r(Help,67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\
SF:nContent-Type:\x20text/plain;\x20charset=utf-8\r\nConnection:\x20close\
SF:r\n\r\n400\x20Bad\x20Request")%r(SSLSessionReq,67,"HTTP/1\.1\x20400\x20
SF:Bad\x20Request\r\nContent-Type:\x20text/plain;\x20charset=utf-8\r\nConn
SF:ection:\x20close\r\n\r\n400\x20Bad\x20Request")%r(TerminalServerCookie,
SF:67,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nContent-Type:\x20text/plain;\
SF:x20charset=utf-8\r\nConnection:\x20close\r\n\r\n400\x20Bad\x20Request");
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we can point out the website and I don't know what's the port 8065, let's check the website first :

![<- Not working](/static/HACKING/delivery-1.png)

First of all we should fuzz the website, in my case I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.222/FUZZ -L --hc=404

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.222/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================                                                                                                                                                          
000002530:   200        53 L     116 W      1398 Ch     "error"                                                                                                                                                                   
000041849:   200        311 L    760 W      10850 Ch    "http://10.10.10.222/"   
```

Sadly nothing interesting (I've already checked those directories and nothing...) Let's take a look into `Helpdesk`, but first of all we have to add `helpdesk.delivery.htb` to our `/etc/hosts` file :

![<- Not working](/static/HACKING/delivery-2.png)

![<- Not working](/static/HACKING/delivery-3.png)

But we can't do nothing useful here right now, let's keep enumerating, the port 8065 for example :

![<- Not working](/static/HACKING/delivery-4.png)

We can see the `Mattermost` service, after trying if it was vulnerable to SQL injection I decided to create a user :

![<- Not working](/static/HACKING/delivery-6.png)

![<- Not working](/static/HACKING/delivery-5.png)

Oh so bad, is necessary a email address, HackTheBox doesn't send real mails, so maybe we can get a mail from `HelpDesk`? Let's go back to `http://helpdesk.delivery.htb/` and open a new ticket.

![<- Not working](/static/HACKING/delivery-7.png)

![<- Not working](/static/HACKING/delivery-8.png)

![<- Not working](/static/HACKING/delivery-9.png)

Nice, we got a mail : `4883810@delivery.htb` and a ticket id : `4883810`. Let's create a user in Mattermost.

![<- Not working](/static/HACKING/delivery-10.png)

![<- Not working](/static/HACKING/delivery-11.png)

Let's check our ticket in HelpDesk :

![<- Not working](/static/HACKING/delivery-12.png)

![<- Not working](/static/HACKING/delivery-13.png)

As the mail says, let's go to `http://delivery.htb:8065/do_verify_email?token=n86sdauigk3y4qpx38c5644whj7jqohbtiyab9mbymq4e7iqjxs1576dcpsfzqtj&email=4883810%40delivery.htb` through the browser, and then login into Mattermost :

![<- Not working](/static/HACKING/delivery-14.png)

![<- Not working](/static/HACKING/delivery-16.png)

We can see some creds there -> `maildeliverer:Youve_G0t_Mail!` and we see that there is a little hint about another password, root says that it's a variant of  "PleaseSubscribe!"... we'll take this into account. So let's login as `maildeliverer` through `ssh` :

![<- Not working](/static/HACKING/delivery-15.png)

And we’re in 😎 !

# [+] PART 2 - PRIVESC

After enumerating the system, we can see inside `mattermost`'s config, a file called config.json : 

![<- Not working](/static/HACKING/delivery-17.png)

Let's read it:

![<- Not working](/static/HACKING/delivery-18.png)

We can see some stored data about the database, and we can see a user and his password in plain text -> `mmuser:Crack_The_MM_Admin_PW` and the database name is `mattermost` and the port is the `3306`, so let's dump the info:

> [mysql -u mmuser -D mattermost -P 3306 -p](https://explainshell.com/explain?cmd=mysql+-u+mmuser+-D+mattermost+-P+3306+-p)

![<- Not working](/static/HACKING/delivery-19.png)

Now we can list all the databases :

> show DATABASES;

![<- Not working](/static/HACKING/delivery-20.png)

Actually mattermost is the only interesting database, if we want to use `information_schema` we could run : 

> use information_schema

But this is not our case, let's list `mattermost`'s tables :

> show tables;
 
![<- Not working](/static/HACKING/delivery-21.png)

We can see a table called "Users", let's extract of the info from there :

> select * FROM Users;

![<- Not working](/static/HACKING/delivery-22.png)

This is not actually easy to read, so let's filter just for "username" and "password" :

> select username,password FROM Users;

![<- Not working](/static/HACKING/delivery-23.png)

There we can see root's password hash : `$2a$10$VM6EeymRxJ29r8Wjkr8Dtev0O.1STWb4.4ScG.anuu7v0EFJwgjjO`. Let's try to crack it but first of all, we can see that it's `bcrypt $2*$`, if we don't know this we can always check the hash [here](https://hashcat.net/wiki/doku.php?id=example_hashes)

![<- Not working](/static/HACKING/delivery-24.png)

Let's try to crack it :

> hashcat -a 0 -m 3200 hash /usr/share/wordlists/rockyou.txt

But this didn't work :astonished:, if we remember during the intrusion we read this :

![<- Not working](/static/HACKING/delivery-25.png)

So we have to create our own dictionary around `PleaseSubscribe!`, so let's create it with `hashcat` and best64.rule to generate variants of it:

> [echo 'PleaseSubscribe!' > hint](https://explainshell.com/explain?cmd=echo+%27PleaseSubscribe%21%27+%3E+hint)

> hashcat -r /usr/share/hashcat/rules/best64.rule --stdout hint  > password.txt

![<- Not working](/static/HACKING/delivery-26.png)

And now we just have to crack it :

> hashcat -a 0 -m 3200 hash password.txt

Once cracked :

> hashcat -a 0 -m 3200 hash password.txt --show

![<- Not working](/static/HACKING/delivery-27.png)

So the password is `PleaseSubscribe!21` :smile: let's login into root :

![<- Not working](/static/HACKING/delivery-27.png)

# THANKS FOR READING 😊 !!















