---
title: "My OSCP Journey"
date: 2021-10-23T03:27:26+01:00
draft: false
categories:
    - Certifications
tags:
    - OSCP
---
![<- Not working](/static/HACKING/offsec-oscp.png)

# - INDEX 
1. [INTRODUCTION 📚](#1-introduction-)
    - [TL;DR](#tldr)
    - [What is OSCP?](#what-is-oscp)
2. [MY BACKGROUND](#2-my-background)
3. [PWK](#3-pwk)
    - [Course Videos & PDF](#pwk-course-videos--pdf)
    - [Labs](#pwk-labs)
4. [RECOMMENDATIONS](#4-recommendations)
5. [THE EXAM](#5-the-exam)
6. [CONCLUSION](#6-conclusion)
7. [WHAT'S NEXT?](#7-whats-next)


# 1. INTRODUCTION 📚

## TL;DR

In this post I'll be talking abount my experience with OSCP. This post DOES NOT pretend to be a guide for the certification, I will just be giving my standpoint. 

Note : Keep in mind that the contents of the certification changed (although the new exam structure won't be implemented till January 2021). A novelty of the new exam is that it will include Active Directory. For more information about the new exam read [this post from Offensive Security](https://www.offensive-security.com/offsec/oscp-exam-structure/?utm_content=189109585&utm_medium=social&utm_source=twitter&hss_channel=tw-134994790)

## WHAT IS OSCP? 

OSCP (Offensive Security Certified Professional), is an ethical hacking certification offered by Offensive Security that teaches penetration testing methodologies and the use of Kali Linux's tools. This certification comes with the PWK course (Penetration Testing With Kali Linux), this course includes videos as well as a PDF of 851 pages.

The exam lasts 24 hours, but you have 24 hours extra to make a professional report. It's proctored, this means that you'll be recorded, and someone will monitor what you're doing as well as you have to share screen. You can't leave the room without asking for permission.

# 2. MY BACKGROUND 

I "discovered" Cybersecurity at the end of 2020 (I have to empashize out that I have always been a hater of IT, my "dream" in that moment was actually being a pharmacist). 

I started with [TryHackme](https://tryhackme.com/) and I highly recommend the platform, it's pretty beginner-friendly (as well as [OverTheWire](https://overthewire.org/wargames/) and I'm sure that if you're starting you won't regret about it . One day I decided to tried luck with [HackTheBox](https://www.hackthebox.com/) (another online cybersecurity platform), since that day I spent there most of my free time. To be honest, I can't be objective with HTB, I'm a fanboy but I gotta say that the VIP subscription is a MUST for the OSCP preparison (this subscription allows you to access all the retired machines, around 150)

At the same time I was preparing for the entrance exams to the university, so I didn't have much time for Cybersec. It wasn't until July-2021 that I resumed my "hacktivity" and in August I enrolled to the PWK Labs for a 60 day period.

# 3. PWK

## PWK COURSE (VIDEOS + PDF)

I'll be straight, I haven't spent more than 10 min reading the PDF and watching the videos, I felt that I was wasting my time so directly skipped to the labs. 

Without having read the PDF and without having watched the videos I can't really tell if they are useful. But if you ask me, I would definitely pass.

## PWK LABS

When I did my labs, there was 75 boxes available. I didn't have a nice experience with them, some of the boxes had dependencies on others (credentials for example, and that was such a waste of time), others were too outdated, others were too slow; and if I'm not wrong we can only revert 12 times a day, and that for a enviroment shared with other users is not enough. I finally did 45 boxes, and although I don't recommend them, there are 5 retired exam-box that you must do.

Have I learned with the labs? Definitely, but I have to recognise that I could have learned much more investing my time on HackTheBox or ProvingGrounds (I'll talk about this one later).

# 4. RECOMMENDATIONS

> [PROVING GROUNDS](https://www.offensive-security.com/labs/)

More than the PWK Labs I highly recommend Proving Grounds, this is also from Offensive Security, but from my standpoint those are much more OSCP-like. I really enjoyed those labs following this [PG OSCP-like list](https://defaultcredentials.com/ctf/proving-grounds/oscp-like-boxes-on-proving-grounds/).

**Price : 16€/month**

> [HackTheBox](https://www.hackthebox.com/)

As I said, HackTheBox is an amazing platform and the VIP subscription is absolutely worth, even though I haven't followed any OSCP list I realized that I did almost every box of this [OSCP-like list from TJnull](https://pbs.twimg.com/media/ECG-gPnW4AMs32A.jpg:large). I did 60-70 easy-medium and some hard boxes.

**Price : 12€/month**

> [Privilege Scalation Courses]()

I'm not a big fan of video courses, but I recommend the content of these two amazing teachers :

*[Tib3rius](https://twitter.com/0xTib3rius)* :
1. [Windows Privilege Escalation for OSCP & Beyond!](https://www.udemy.com/course/windows-privilege-escalation/)
2. [Linux Privilege Escalation for OSCP & Beyond!](https://www.udemy.com/course/linux-privilege-escalation/)

*[Heath Adams](https://twitter.com/thecybermentor)* :
1. [Windows Privilege Escalation for Beginners](https://academy.tcm-sec.com/p/windows-privilege-escalation-for-beginners)
2. [Linux Privilege Escalation for Beginners](https://academy.tcm-sec.com/p/linux-privilege-escalation)

# 5. THE EXAM

The exam lasts 24 hours, in those 24 hours you have to attack 5 systems with the following point distributions :

- 2x 25pts
- 2x 20pts
- 1x 10pts

To pass the exam you have to get 70 points.

My exam started at 11:00, after 1 hour I didn't understand why I couldn't exploit my BufferOverflow machine so I skipped to the 20 points machines. I rooted both 20 points machines in 3 hours and took a break. About 18:30 I was back to work and tried with 10 points machine but I was stuck at the foothold. After some "think-out-of-the-box" I got root on the 10 points machine at 23:50 so I decided to take a long break and get a nap. I woke up at 5:30 and after a talk with the technician because my BOF machine was kinda buggy I rooted the machine at 8:30 and in that moment I got my 75 points to pass, I did't even tried with the last 25 points because I was too tired.

At the next day I submited my OSCP Report (I used [this Template](https://github.com/noraj/OSCP-Exam-Report-Template-Markdown)) and after 24 hours I received the mail :

![<- Not working](/static/HACKING/oscp.jpeg)

# 6. CONCLUSION

I strongly recommend this certification, although is not a cheap certification (+1000$ is a huge ammount), the real investment is the time and the effort. I really learned a lot and the exam was an outstanding experience, OSCP taught me to try again and again, harder and harder, and above all, smarter. 

If I had to qualify in terms of difficulty this certification I would say that it's a 6 out of 10, this certification is oriented to people who want to become a penetration tester, so this is considered an entry certification. This doesn't mean that it's easy, because it's definitely not.


# 7. WHAT'S NEXT?

Cybersecurity is a field which is constantly evolving, and you will never learn everything, and that's what makes it beautiful. Learning everyday and going beyond oneself is one of the most fulfilling feeling out there. So I'll do my best and enjoy the ride.

# THANKS FOR READING 😊 !!
